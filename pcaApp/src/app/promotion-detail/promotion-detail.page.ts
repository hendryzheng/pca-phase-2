import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { MenuService } from 'src/services/menu.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-promotion-detail',
  templateUrl: './promotion-detail.page.html',
  styleUrls: ['./promotion-detail.page.scss'],
})
export class PromotionDetailPage implements OnInit {

  form = {
    name: '',
    email: '',
    phone: '',
    agent_name: '',
    agent_email: '',
    agent_phone: ''
  }

  constructor(private _utilitiesService: UtilitiesService,
              private location: Location,
              private _menuService: MenuService) {
  }
  
  ngOnInit(){

  }

  goBack(){
    this.location.back();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromotionDetailPage');
    
  }

  getSelectedPromotion() {
    return this._menuService.selectedPromotion;
  }

  submit() {
    if (this.validateForm()) {
      let formSubmission = {
        'Form[name]': this.form.name,
        'Form[email]': this.form.email,
        'Form[phone]': this.form.phone,
        'Form[agent_name]': this.form.agent_name,
        'Form[agent_email]': this.form.agent_email,
        'Form[agent_phone]': this.form.agent_phone,
        'Form[promotion_id]': this.getSelectedPromotion().id
      };
      this._menuService.submitPromotionContactForm(formSubmission, () => {
        this._utilitiesService.alertMessageCallback('Success', 'Form submitted successfully', () => {
          this.form = {
            name: '',
            email: '',
            phone: '',
            agent_name: '',
            agent_email: '',
            agent_phone: ''
          }
        });
      });
    }
  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.name)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.phone)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.agent_name)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.agent_email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.agent_phone)) {
      validate = false;
    }
    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
    }
    console.log(validate);
    return validate;
  }

}
