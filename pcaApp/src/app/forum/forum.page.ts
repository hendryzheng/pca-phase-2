import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AccountService } from 'src/services/account.service';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { CONFIGURATION } from 'src/services/config.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.page.html',
  styleUrls: ['./forum.page.scss'],
})
export class ForumPage implements OnInit {

  constructor(
    private _accountService: AccountService,
    private router: Router,
    private _menuService: MenuService,
    private alertCtrl: AlertController,
    private location: Location,
    private _utilitiesService: UtilitiesService) {
  }

  search_input: any = '';

  loaded: boolean = false;
  img_url: any = 'assets/img/person-placeholder.png';

  ngOnInit(){

  }

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    if(this._accountService.isUserSessionAlive()){
      if (this._accountService.userData.profile_picture !== null) {
        this.img_url = CONFIGURATION.base_url + this._accountService.userData.profile_picture;
      }
    }
    this._menuService.getPost(() => {
      this.loaded = true;
    });
  }

  getUserData(){
    return this._accountService.userData;
  }

  getPost() {
    return this._menuService.post;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForumPage');
  }

  goBack(){
    this.location.back();
  }

  eventHandler(event) {
    console.log(event, event.keyCode, event.keyIdentifier);
    let params = {
      keyword: event.target.value
    };
    if (event.target.value === '') {
      this.loaded = false;
      this._menuService.getPost(() => {
        this.loaded = true;
      });
    } else {
      this.loaded = false;
      this._menuService.searchForum(params, () => {
        this.loaded = true;
      });
    }

  }

  async createPost() {
    if (this._accountService.isUserSessionAlive()) {
      this.router.navigateByUrl('/forum-create-post');
    } else {

      const forgot = await this.alertCtrl.create({
        subHeader: 'Oops !',
        message: 'Only member is allowed to submit a post. You ',
        buttons: [
          {
            text: 'Register Now',
            handler: () => {
              // this.navCtrl.pop();
              // this.navCtrl.push(RegisterPage);
              this.router.navigateByUrl('/sign-up');
            }
          },
          {
            text: 'Sign In As Member',
            handler: () => {
              // this.navCtrl.pop();
              this.router.navigateByUrl('/login');

            }
          }
        ]
      });
      await forgot.present();
    }
  }

  createPostHandler(){
    this.createPost();
  }

  viewComments(item) {
    this._menuService.selectedPost = item;
    this.router.navigateByUrl('/forum-discussion');
    // this.navCtrl.push(CommentPage);
  }

  getImagePath(item) {
    return CONFIGURATION.base_url + item.image;
  }

}
