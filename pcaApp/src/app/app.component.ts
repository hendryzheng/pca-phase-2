import { Component, AfterViewInit, OnInit, NgZone } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import * as $ from "jquery";
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';

declare var window: any;
declare var Keyboard: any;
declare var cordova: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements AfterViewInit, OnInit {
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public _accountService: AccountService,
    private _utilitiesService: UtilitiesService,
    private zone: NgZone,
    private router: Router
  ) {
    this.initializeApp();
  }

  ngAfterViewInit() {
    // This element never changes.
    this._accountService.checkUserSession();
    // this.platform.ready().then(() => {
      document.addEventListener('deviceready', () => {
        
        
      });
      
    // });
  }

  ngOnInit() {
    this._accountService.checkUserSession();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      if (typeof window.FirebasePlugin !== 'undefined') {
        document.addEventListener('deviceready', () => {
          
          if (this.platform.is('ios')) {
            window.FirebasePlugin.grantPermission();
            window.FirebasePlugin.getToken(token => {
              // save this server-side and use it to push notifications to this device
              console.log(token);
              if (token !== null) {
                this._accountService.token = token;
                this._accountService.registerToken();
              }
            }, error => {
              console.error(error);
            });
            window.FirebasePlugin.onTokenRefresh(token => {
              // save this server-side and use it to push notifications to this device
              console.log(token);
              if (token !== null) {
                this._accountService.token = token;
                this._accountService.registerToken();
              }

            }, error => {
              console.error(error);
            });

          } else {
            window.FirebasePlugin.getToken(token => {
              // save this server-side and use it to push notifications to this device
              console.log(token);
              if (token !== null) {
                this._accountService.token = token;
                this._accountService.registerToken();
              }
            }, error => {
              console.error(error);
            });
            window.FirebasePlugin.onTokenRefresh(token => {
              // save this server-side and use it to push notifications to this device
              console.log(token);
              if (token !== null) {
                this._accountService.token = token;
                this._accountService.registerToken();
              }

            }, error => {
              console.error(error);
            });
          }
          // window.FirebasePlugin.onNotificationOpen(payload => {
          //   console.log(payload);
          //   console.log(payload.aps);
          //   console.log(payload.aps.alert);
          //   this._utilitiesService.alertMessageCallback('Notification received', payload.aps.alert.title, () => {
          //     // this.nav.setRoot(TabsPage);
          //   })
          // }, (error) => {
          //   console.error(error);
          // });



        }, false);

      }


      // if (this.platform.is('ios')) {
      // let
      //   appEl = <HTMLElement>(document.getElementsByTagName('ION-APP')[0]),
      //   appElHeight = appEl.clientHeight;

      // // this.keyboard.disableScroll(true);
      // cordova.plugins.Keyboard.shrinkView(true);
      // console.log(cordova.plugins.Keyboard);
      // Keyboard.shrinkView(true);
      // console.log(Keyboard);
      // document.addEventListener('deviceready', ()=> {
      //   cordova.plugins.Keyboard.shrinkView(true)
      //   window.addEventListener('keyboardDidShow', ()=> {
      //     console.log('aaaa');
      //     document.activeElement.scrollIntoView()
      //   })
      // })
      // window.addEventListener('keyboardDidShow', (e) => {
      //   appEl.style.height = (appElHeight - (<any>e).keyboardHeight) + 'px';

      //   const offset = $(document.activeElement).offset().top;
      //   let height = (offset - e.keyboardHeight)*-1;
      //   height = height > 0 ? 0 : height;      
      //   $('body').animate({ 'marginTop': height + 'px' }, 100);
      //   console.log('keyboard show');
      // });

      // window.addEventListener('keyboardDidHide', () => {
      //   appEl.style.height = '100%';
      //   cordova.plugins.Keyboard.hide();
      //   $('body').animate({ 'marginTop': 0 + 'px' }, 100);
      //   console.log('keyboard hide');
      // });
      // }

    });
  }
}
