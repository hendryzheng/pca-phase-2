import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ENamecardViewPage } from './e-namecard-view.page';

describe('ENamecardViewPage', () => {
  let component: ENamecardViewPage;
  let fixture: ComponentFixture<ENamecardViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ENamecardViewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ENamecardViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
