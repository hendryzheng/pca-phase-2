import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ENamecardViewPage } from './e-namecard-view.page';

const routes: Routes = [
  {
    path: '',
    component: ENamecardViewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ENamecardViewPage]
})
export class ENamecardViewPageModule {}
