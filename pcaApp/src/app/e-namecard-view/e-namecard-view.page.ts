import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-e-namecard-view',
  templateUrl: './e-namecard-view.page.html',
  styleUrls: ['./e-namecard-view.page.scss'],
})
export class ENamecardViewPage implements OnInit {

  isNotif: boolean = true;
  notifState: any = 'my-listing';
  loaded: boolean= false;
  img_url: any = 'assets/img/person-placeholder.png';


  constructor(private _accountService: AccountService,
              private location: Location) { }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    let param = {
      user_id: this._accountService.viewUserData.id,
      user_type: this._accountService.viewUserData.member_type
    }
    this._accountService.getUserDetailParam(param, ()=>{
      if (this._accountService.viewUserData.profile_picture !== null) {
        this.img_url = CONFIGURATION.base_url + this._accountService.viewUserData.profile_picture;
      }
    });
  }

  goBack(){
    this.location.back();
  }


  getUserData() {
    return this._accountService.viewUserData;
  }

}
