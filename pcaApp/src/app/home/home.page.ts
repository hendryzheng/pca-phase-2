import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { PopoverController, IonSlides } from '@ionic/angular';
import { UtilitiesService } from 'src/services/utilities.service';
import { MenuService } from 'src/services/menu.service';
import { AccountService } from 'src/services/account.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Router } from '@angular/router';
import { CONFIGURATION } from 'src/services/config.service';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

declare var window: any;


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {


  @ViewChild('mySlides') Slides: IonSlides;

  menuList: any = [];
  img_url: any = 'assets/img/person-placeholder.png';
  showSearch: boolean = false;
  listing_result: any = [];
  myInput: any = '';
  loaded: boolean = false;
  sliderOpts = {
    autoplay: true,
    pager: true,
    zoom: {
      maxRatio: 5
    }
  };

  constructor(
    public popoverCtrl: PopoverController,
    private geolocation: Geolocation,
    private _utilitiesService: UtilitiesService,
    private _propertyListingService: PropertyListingService,
    private _menuService: MenuService,
    private router: Router,
    private zone: NgZone,
    private _accountService: AccountService,
    private iab: InAppBrowser) {
  }

  ngOnInit() {
    
    this._menuService.getMenu(() => {
      this.menuList = [];
      for (var i = 0; i < this._menuService.menu_list.length; i++) {
        if (i < 12) {
          this.menuList.push(this._menuService.menu_list[i]);
        }
      }
      if(this.menuList.length > 0){
        this.menuList = this._utilitiesService.chunk(this.menuList, 4);
      }
      

      // this.menuList = this._utilitiesService.chunk(this._menuService.menu_list, 5);
      console.log(this.menuList);
    });
    this._menuService.getBanner(() => {
      // this.Slides.startAutoplay();

    });
    
  }

  ionViewWillEnter() {
    console.log(this._propertyListingService.pcaPropertyListing);
    this._accountService.checkUserSession();
    let cache = localStorage.getItem('menu_list');
    if (cache) {
        this.menuList = [];
        var menuListParse = JSON.parse(localStorage.getItem('menu_list'));
        for (var i = 0; i < menuListParse.length; i++) {
          if (i < 12) {
            this.menuList.push(menuListParse[i]);
          }
        }
        if(this.menuList.length > 0){
          this.menuList = this._utilitiesService.chunk(this.menuList, 4);
        }
    }
    let cacheMatchingConnectPca = localStorage.getItem('matchingConnectPca');
    let cacheMatchingConnect99co = localStorage.getItem('matchingConnect99co');
    if(cacheMatchingConnectPca){
      this._propertyListingService.pcaPropertyListing = JSON.parse(cacheMatchingConnectPca);
    }
    if(cacheMatchingConnect99co){
      this._propertyListingService.propertyListingResult = JSON.parse(cacheMatchingConnect99co);
    }
    if (this._accountService.isUserSessionAlive()) {
      this.loaded = false;
      this._accountService.getUserDetail(() => {
        this.zone.run(()=>{
          if (this._accountService.userData.profile_picture !== null) {
            this.img_url = CONFIGURATION.base_url + this._accountService.userData.profile_picture;
          }
          this._propertyListingService.getPublicPropertyListing({}, ()=>{
            this._menuService.listing_connect = this._propertyListingService.publicPropertyListing;
          });
        });
        
        this.loaded = true;
      });
    }
    this.geolocation.getCurrentPosition().then((resp) => {
      // if(this._propertyListingService.searchListingParams.query_coords == ''){
      //   // this._propertyListingService.searchListingParams.query_coords = resp.coords.latitude+','+resp.coords.longitude;
      // }
      // this._propertyListingService.searchListings(() => {
      //   this._zone.run(() => {
      //     this.listings = this._propertyListingService.propertyListingResult;
      //   })
      // })
    });
    
  }

  onInput(ev) {
    console.log(ev);
    if (ev.type == 'input') {
      let params = {
        keyword: ev.target.value
      };
      if (ev.target.value == '') {
        this.showSearch = false;
      } else {
        this._menuService.searchListing(params, () => {
          this.showSearch = true;
          this.listing_result = this._menuService.listing_result;
        });
      }
    }
  }

  checkFocus() {

  }

  viewListingPca(item){
    this._propertyListingService.selectedPropertyListingDetail = item;
    this._propertyListingService.selectedPropertyListingDetail.project_name = item.property_name;
    this._propertyListingService.selectedPropertyListingDetail.address_line_1 = item.address;
    this._propertyListingService.selectedPropertyListingDetail.address_line_2 = '';
    this._propertyListingService.selectedPropertyListingDetail.area_size_formatted = item.area_size;
    this._propertyListingService.selectedPropertyListingDetail.location = {
      lat: item.lat,
      lng: item.lng
    }
    this._propertyListingService.selectedPropertyListingDetail.attributes = {
      price_formatted: item.price,
      bedrooms: item.bedrooms,
      bathrooms: item.number_of_bathroom,
      area_ppsf_formatted: item.floor_size,
      area_size_formatted: item.area_size
    };

    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/property-listing');
  }

  updateProfile() {
    // this.nav.push(ProfilePage);
    this.router.navigateByUrl('/profile');
  }

  checkBlur() {
    if (this.myInput == '') {
      this.showSearch = false;
      this.listing_result = [];
    }
  }

  onCancel(ev) {
    console.log(ev);
    this.showSearch = false;
    this.listing_result = [];
  }

  getIconPath(item) {
    if (item.icon_path !== null) {
      return item.icon_path;
    } else {
      return this._menuService.selectedMenu.icon_path;
    }
  }

  getUserType() {
    if (this._accountService.isUserSessionAlive()) {
      return this._accountService.userData.member_type;
    } else {
      return '';
    }
  }

  viewAllMatchingConnect() {
    // for (let key in this._propertyListingService.searchListingParams) {
    //   for (let k in this._menuService.matching_connect_obj) {
    //     if (key == k) {
    //       this._propertyListingService.searchListingParams[key] = this._menuService.matching_connect_obj[k];
    //       break;
    //     }
    //   }
    //   // Use `key` and `value`
    // }
    // this._propertyListingService.searchListingParams.search_id = this._menuService.matching_connect_obj.id;
    // this._propertyListingService.selectedKeywordSearch = this._menuService.matching_connect_obj.location_name;
    // console.log(this._propertyListingService.searchListingParams);
    this.router.navigateByUrl('/request-information-list');
  }

  viewListingConnect(){
    this.router.navigateByUrl('/view-listing-connect');
  }

  getMatchingConnect() {
    return this._menuService.matching_connect;
  }

  getListingConnect(){
    return this._menuService.listing_connect;
  }

  getMatchingConnnectObj() {
    return this._menuService.matching_connect_obj;
  }

  createSearch() {
    this._propertyListingService.searchListingParams = {
      search_id: '',
      location_name: '',
      location_subtitle: '',
      search_name: '',
      query_coords: '',
      listing_type: 'sale',
      main_category: 'hdb',
      keywords: '',
      sub_categories: '',
      query_type: '',
      query_ids: '',
      radius_max: 1000,
      price_min: 100000,
      price_max: 20000000,
      floorarea_min: 500,
      floorarea_max: 10000,
      rooms: 'any',
      is_new_launch: 'any',
      tenure: 'any',
      sort_field: '',
      sort_order: ''
    }
    this.router.navigateByUrl('/request-information');
  }

  createListing(){
    
  }

  viewListing(item) {
    this._propertyListingService.selectedPropertyListingDetail = item;
    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/property-listing');
  }

  openAttachment(item) {
    var base_url = CONFIGURATION.apiEndpoint;
    if (item.listing_type == '2') {
      console.log(item.listing_attachment);
      this._utilitiesService.openBrowser(item.listing_name, item.listing_attachment);
      // const browser = this.iab.create(item.listing_attachment, '_blank', 'location=no');
    } else if (item.listing_type == '3') {
      console.log(item);
      window.InAppYouTube.openVideo(item.youtube_id, {
        fullscreen: true
      }, (result) => {
        console.log(JSON.stringify(result));
      }, (reason) => {
        console.log(reason);
      });
      // this.youtube.openVideo(item.youtube_id);

    } else {
      this._menuService.selected_listing = item;
      this._menuService.selected_listing.image_path = base_url + item.listing_attachment;
      this.router.navigateByUrl('/preview-image');
      // this._utilitiesService.openBrowser(item.listing_name, base_url + item.listing_attachment);
      // this.iab.create(base_url+item.listing_attachment, '_blank', 'location=no');
    }
  }

  openSearch() {
    this.router.navigateByUrl('/search');
  }

  getUserData() {
    return this._accountService.userData;
  }

  isUserLoggedIn() {
    return this._accountService.isUserSessionAlive();
  }

  alertPermission() {
    this._utilitiesService.alertMessage('Permission Denied', 'Your account is not authorized to access this menu');
  }

  view(item) {
    this._menuService.selectedMenu = item;

    var allowed = false;
    if (item.access_status !== '1') {
      if (item.access_status === '2') {
        if (this.getUserType() === 'agent') {
          allowed = true;
        } else {
          allowed = false;
          // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
        }
      } else if (item.access_status === '3') {
        if (this.getUserType() === 'account') {
          allowed = true;
          // this.router.navigateByUrl('/categories')
        } else {
          allowed = false;
          // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
        }
      } else {
        if (this.getUserType() === 'pca_member') {
          allowed = true;
          // this.router.navigateByUrl('/categories')
        } else {
          allowed = false;
          // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
        }
      }
    } else {
      allowed = true;
    }
    if (item.is_pca_affiliaties === '1') {

      if (allowed) {

        this.router.navigateByUrl('/pca-categories');

      } else {
        this.alertPermission();
      }
    } else if (item.is_connect_to_project === '1') {
      if (allowed) {
        var base_url = CONFIGURATION.base_url;
        // this.iab.create(base_url + '/connect_to_project','_blank');
        // this._utilitiesService.openBrowser('Connect to Portals', base_url + '/connect_to_project');
        this.router.navigateByUrl('/connect-to-project');
      } else {
        this.alertPermission();
      }
    } else if (item.is_pca_agent_tools === '1') {
      
      if (allowed) {
        // this.nav.push(PcaAgentToolsPage);
        this.router.navigateByUrl('/pca-agent-tools');
      } else {
        this.alertPermission();
      }
      // this._utilitiesService.openBrowser('Connect to Portals', CONFIGURATION.apiEndpoint + 'connect_to_project');
    } else {
      if (allowed) {
        if (item.menu_name.indexOf('Senior Leaders') !== -1) {
          this._menuService.listing = item.listing;
          this.router.navigateByUrl('/pca-senior-leaders');
        } else {
          this.router.navigateByUrl('/categories')
        }
      } else {
        this.alertPermission();
      }
    }
  }

  openBanner(item) {
    

    if (item.listing_id !== null) {
      if (item.listing_type === '2') {
        // const browser = this.iab.create(item.listing_attachment, '_blank', 'location=no');
        this._utilitiesService.openBrowser(item.listing_name, item.listing_attachment);
      } else {
        var base_url = CONFIGURATION.base_url;
        // const browser = this.iab.create(base_url + item.listing_attachment, '_blank', 'location=no');
        this._utilitiesService.openBrowser(item.listing_name, base_url + item.listing_attachment);
      }
    } else if (item.url_link !== null && item.url_link !== '') {
      this._utilitiesService.openBrowser(item.url_link, item.url_link);
    } else if (item.pca_affiliates_listing_id !== null) {
      let temp = {
        id: item.pca_id,
        banner_image: item.pca_banner_image,
        title: item.pca_banner_title,
        description: item.pca_description
      }
      this._menuService.selected_pca_listing = temp;
      // this.nav.push(PcaContactFormPage);
    }

  }

  presentNotifications(myEvent) {
    console.log(myEvent);
    // let popover = this.popoverCtrl.create(NotificationsPage);
    // popover.present({
    //   ev: myEvent
    // });
  }

  getMenu() {
    return this._menuService.menu_list;
  }

  getBanner() {
    return this._menuService.banner;
  }

}
