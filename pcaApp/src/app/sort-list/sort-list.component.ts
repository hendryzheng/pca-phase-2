import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PropertyListingService } from 'src/services/property-listing.service';

@Component({
  selector: 'app-sort-list',
  templateUrl: './sort-list.component.html',
  styleUrls: ['./sort-list.component.scss'],
})
export class SortListComponent implements OnInit {

  constructor(private modalController: ModalController,
              private _propertyListingService: PropertyListingService) { }

  ngOnInit() {}

  select(sort_field,sort_order){
    this._propertyListingService.searchListingParams.sort_field = sort_field;
    this._propertyListingService.searchListingParams.sort_order = sort_order;
    this.modalController.dismiss();
  }

}
