import { Component, OnInit, NgZone } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MenuService } from 'src/services/menu.service';
import { PropertyListingService } from 'src/services/property-listing.service';

@Component({
  selector: 'app-customer-data',
  templateUrl: './customer-data.page.html',
  styleUrls: ['./customer-data.page.scss'],
})
export class CustomerDataPage implements OnInit {

  isNotif: boolean = true;
  notifState: any = 'properties';
  customerState: any = 'PDPA';
  loaded: boolean= false;
  img_url: any = 'assets/img/person-placeholder.png';
  customerList: any = [];
  pdpaList: any = [];
  marketingConsentList: any = [];
  myListing: any = [];
  showContact: boolean = false;
  contact: any = [];
  param_id = '';
  userData: any = [];
  hideCustomer: boolean = false;
  publicPropertyLoaded: boolean = false;


  constructor(private _accountService: AccountService,
              private location: Location,
              private router: Router,
              private _utilitiesService: UtilitiesService,
              private _DomSanitizationService: DomSanitizer,
              private _zone: NgZone,
              private _menuService: MenuService,
              private _propertyListingService: PropertyListingService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.fetchCustomerData();
  }

  ionViewWillEnter(){
    this.fetchCustomerData();
  }

  addCustomer(){
    this._menuService.customer_type = this.customerState;
    this.router.navigateByUrl('/add-customer');
  }

  goBack(){
    this.location.back();
  }

  toggleCustomer(string){
    this.customerState = string;
    
  }


  async deleteCustomerConfirmation(item) {
    let alert = await this._utilitiesService.getAlertCtrl().create({
      header: 'Are you sure you want to delete this customer?',
      // message: message,
      buttons: [
        {
          text: 'Yes',
          handler: data => {
            this.deleteCustomer(item);
            console.log('OK clicked');
          }
        },
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  editCustomer(item){
    this._menuService.customer_type = item.type;
    this._menuService.customer = item;
    this.router.navigateByUrl('/edit-customer');
  }

  deleteCustomer(item){
    
    let params = {
      id: item.id
    }
    this._menuService.deleteCustomer(params, ()=>{
      this.fetchCustomerData();
    })
  }

  viewCustomer(item){
    
  }

  getCustomerData(){
    return this.customerList;
  }

  fetchCustomerData(){
    this.customerList = [];
    this._menuService.getCustomerData(()=>{
      this._zone.run(()=>{
        this.customerList = this._menuService.customerList;
        this.pdpaList = [];
        this.marketingConsentList = [];
        for(var i=0;i<this.customerList.length;i++){
          if(this.customerList[i].type == 'PDPA'){
            this.pdpaList.push(this.customerList[i]);
          }else{
            this.marketingConsentList.push(this.customerList[i]);
          }
        }
        console.log(this.customerList);
      });
      
    });
  }

}
