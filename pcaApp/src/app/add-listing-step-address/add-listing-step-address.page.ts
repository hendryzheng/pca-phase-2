import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-listing-step-address',
  templateUrl: './add-listing-step-address.page.html',
  styleUrls: ['./add-listing-step-address.page.scss'],
})
export class AddListingStepAddressPage implements OnInit {

  search: any = '';
  

  
  disabled: boolean = true;
  locationList: any = [];

  constructor(private _accountService: AccountService,
              private router: Router,
              private location: Location,
              private _propertyListingService: PropertyListingService) { }

  ngOnInit() {

  }

  getForm(){
    return this._propertyListingService.form;
  }

  goBack(){
    this.location.back();
  }

  change(ev){
    console.log(ev.target.value);
    if(ev.target.value == 'industrial' || ev.target.value == 'commercial' || ev.target.value == 'land'){
      this.disabled = true;
    }else{
      this.disabled = false;
    }
    this.getForm().main_category = '';
    this.getForm().sub_category = '';
    this.getForm().land_use = '';
    this.locationList = [];
  }

  getShortFormMap(){
    return this._propertyListingService.shortFormMap;
  }

  changeMainCategory(ev){
    if(ev.target.value == 'industrial'){
      this.disabled = true;
    }else{
      this.disabled = false;
    }
  }

  changeLandUse(ev){
    this.disabled = false;
  }
  

  searchLocation($ev){
    let query = this.search;
    if(query == ''){
      this.locationList = [];
    }else{
      if(this.getForm().main_category !== ''){
        query += '&main_category='+this.getForm().main_category;
      }
      if(this.getForm().land_use !== ''){
        query += '&land_use='+this.getForm().land_use;
      }
      this._propertyListingService.getLocationStep1(query, this.getForm().property_segment, ()=>{
        this.locationList = this._propertyListingService.step1Location;
      });
    }
    
  }

  selectLocation(item){
    this._propertyListingService.step1SelectedLocation = item;
    this._propertyListingService.form.address = item.address;
    this._propertyListingService.form.id = item.id;
    this._propertyListingService.form.lat = item.coordinates.lat;
    this._propertyListingService.form.lng = item.coordinates.lng;
    this._propertyListingService.form.district = item.district;
    this._propertyListingService.form.photo_url = item.photo_url;
    this._propertyListingService.form.property_name = item.name;
    this._propertyListingService.getLocationStepDetail(item.id,this.getForm().property_segment, item.type, ()=>{
      this.router.navigateByUrl('/add-listing-step-basic-info');
    });
  }

}
