import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { AccountService } from 'src/services/account.service';

@Component({
  selector: 'app-account-property-type',
  templateUrl: './account-property-type.page.html',
  styleUrls: ['./account-property-type.page.scss'],
})
export class AccountPropertyTypePage implements OnInit {

  categories: any = [];
  subcategories: any = [];
  property_segments: any = '';
  main_category: any = '';


  constructor(private router: Router,
              private _accountService: AccountService,
              private _propertyListingService: PropertyListingService,
              private location: Location) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    if(typeof this._accountService.userData.property_segments !== 'undefined'){
      this.property_segments = this._accountService.userData.property_segments;
      for(var i=0;i<this._propertyListingService.listing_categories.length;i++){
        if(this.property_segments == this._propertyListingService.listing_categories[i].key){
          this.categories = this._propertyListingService.listing_categories[i].main_categories;
          if(typeof this._accountService.userData.main_category !== 'undefined'){
            this.main_category = this._accountService.userData.main_category;
          }
        }
      }
      
      
    }
    
  }

  goBack(){
    this.location.back();
  }

  segmentChosen(item){
    this.categories = [];
    this.subcategories = [];
    this.categories = item.main_categories;
  }

  mainCatChosen(item){
    this.subcategories = [];
    this.subcategories = item.sub_categories;
    console.log(item);
  }

  update(){
    let param = {
      user_type: this._accountService.userData.member_type,
      user_id: this._accountService.userData.id,
      property_segments:this.property_segments,
      main_category: this.main_category
    }
    this._accountService.updatePropertyType(param, ()=>{
      this.router.navigateByUrl('/public-properties');
    });
  }

}
