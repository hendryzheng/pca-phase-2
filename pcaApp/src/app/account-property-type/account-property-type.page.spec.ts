import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountPropertyTypePage } from './account-property-type.page';

describe('AccountPropertyTypePage', () => {
  let component: AccountPropertyTypePage;
  let fixture: ComponentFixture<AccountPropertyTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountPropertyTypePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPropertyTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
