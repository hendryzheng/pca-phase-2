import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { Platform } from '@ionic/angular';
import { MenuService } from 'src/services/menu.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { UtilitiesService } from 'src/services/utilities.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

declare var navigator: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  img_url: any = 'assets/img/person-placeholder.png';
  loaded: boolean = false;

  constructor(
    public platform: Platform,
    private _menuService: MenuService,
    private iab: InAppBrowser,
    private _utilitiesService: UtilitiesService,
    private _accountService: AccountService,
    private router: Router,
    private location: Location,
    private barcodeScanner: BarcodeScanner) {
  }


  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    if (this._accountService.userData.profile_picture !== null) {
      this.img_url = CONFIGURATION.base_url + this._accountService.userData.profile_picture;
    }
    setTimeout(()=>{
      this.loaded = true;
    },1000);
  }

  getUserData() {
    return this._accountService.userData;
  }

  openQR() {
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      let text = barcodeData.text;
      console.log('Scanned something', text);
      this._utilitiesService.openBrowser('QR Result', text);
    }).catch(err => {
      console.log('Error', err);
    });
  }

  openMaps() {
    // this.navCtrl.push(MapsCategoriesPage);
    this.router.navigateByUrl('/maps');
  }

  viewCompass() {
    this._utilitiesService.openBrowser('Compass', CONFIGURATION.apiEndpoint + 'compass');
  }

  logout() {
    localStorage.clear();
    this._accountService.userData = null;
    this.router.navigate(['/splash-login'], {replaceUrl: true})
  }

  isUserSessionAlive() {
    return this._accountService.isUserSessionAlive();
  }

  share() {
    var url = 'PLAYSTORE/APPSTORE URL < URL HERE >';
    if(this.platform.is('ios')){
      url = 'https://itunes.apple.com/us/app/property-connect-alliance/id1400685329?ls=1&mt=8';
    }else{
      url ='https://play.google.com/store/apps/details?id=com.propertyconnectalliance.sg';
    }

    navigator.share({
      'title': 'Download and Install PropertyConnectAlliance App now !',
      'text': 'Download and Install PropertyConnectAlliance App now !',
      'url': url
    }).then(() => {
      console.log('Successful share');
    }).catch(error => {
      console.log('Error sharing:', error)
    });
  }

  isLoggedIn(){
    return this._accountService.isUserSessionAlive();
  }

}
