import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pca-events',
  templateUrl: './pca-events.page.html',
  styleUrls: ['./pca-events.page.scss'],
})
export class PcaEventsPage implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
    
  }

  goBack(){
    this.location.back();
  }

}
