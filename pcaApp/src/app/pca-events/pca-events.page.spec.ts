import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcaEventsPage } from './pca-events.page';

describe('PcaEventsPage', () => {
  let component: PcaEventsPage;
  let fixture: ComponentFixture<PcaEventsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcaEventsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcaEventsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
