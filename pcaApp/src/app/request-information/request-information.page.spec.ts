import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestInformationPage } from './request-information.page';

describe('RequestInformationPage', () => {
  let component: RequestInformationPage;
  let fixture: ComponentFixture<RequestInformationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestInformationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestInformationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
