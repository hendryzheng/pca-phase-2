import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UtilitiesService } from 'src/services/utilities.service';
import * as moment from 'moment';

@Component({
  selector: 'app-request-information',
  templateUrl: './request-information.page.html',
  styleUrls: ['./request-information.page.scss'],
})
export class RequestInformationPage implements OnInit {

  constructor(private location: Location,
              private router: Router,
              private alertCtrl: AlertController,
              private _utilitiesService: UtilitiesService,
              private _propertyListingService: PropertyListingService) { }

  from: any = 100000;
  to: any = 20000000;

  fromFloor: any = 500;
  toFloor: any = 10000;
  subcategories: any = [];
  hideSearch: boolean = true;
  searchKeywordListingResult: any = [];
  searchKeywordModel: any = '';
  locationList: any = [];
  categories: any = '';
  height: any = '500';

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.locationList = [];
    this.searchKeywordModel = '';
      this._propertyListingService.selectedKeywordSearch = '';
      this._propertyListingService.searchListingParams.query_type = '';
      this._propertyListingService.searchListingParams.query_ids = '';
      this._propertyListingService.searchListingParams.query_coords = '';
    var physicalScreenHeight = window.screen.height;
    this.height = physicalScreenHeight;
    let obj: any = this._propertyListingService.listing_categories[0].main_categories[0];
    this.subcategories = obj.sub_categories;
    if(this._propertyListingService.searchListingParams.sub_categories !== ''){
      let subcat = this._propertyListingService.searchListingParams.sub_categories.split(',');
      for(var i=0;i<this.subcategories.length;i++){
        for (var j=0;j<subcat.length;j++){
          if(this.subcategories[i].key == subcat[j]){
            this.subcategories[i].checked = true;
          }
        }
      }
    }
    this.segmentChosen(this._propertyListingService.listing_categories[0]);
    this._propertyListingService.searchListingParams.property_segments = 'residential';

  }

  goBack(){
    this.location.back();
  }

  change(ev){
    console.log(ev);
    this.from = ev.detail.value.lower;
    this._propertyListingService.searchListingParams.price_min = this.from;
    this.to = ev.detail.value.upper;
    this._propertyListingService.searchListingParams.price_max = this.to;
  }

  changeFloorArea(ev){
    console.log(ev);
    this.fromFloor = ev.detail.value.lower;
    this._propertyListingService.searchListingParams.floorarea_min = this.fromFloor;
    this.toFloor = ev.detail.value.upper;
    this._propertyListingService.searchListingParams.floorarea_max = this.toFloor;
  }

  getSearchListingParam(){
    return this._propertyListingService.searchListingParams;
  }

  segmentChosen(item){
    this.categories = [];
    this.subcategories = [];
    this.categories = item.main_categories;
  }

  mainCatChosen(item){
    this.subcategories = [];
    this.subcategories = item.sub_categories;
    console.log(item);
  }

  async saveSearch(params) {
    const forgot = await this.alertCtrl.create({
      subHeader: 'Keep your search?',
      message: "You may keep your search and name it as you wish. We will keep you notified if there is new listing for your search.",
      inputs: [
        {
          name: 'search_name',
          placeholder: 'Name your search',
          value: this._propertyListingService.searchListingParams.search_name,
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel and Continue',
          handler: data => {
            console.log('Cancel clicked');
            this.router.navigateByUrl('/matching-request');
          }
        },
        {
          text: 'Save and Search',
          handler: data => {
            console.log('Send clicked');
            if(this._propertyListingService.selectedKeywordSearch !== '' && this._propertyListingService.selectedKeywordSearchSubtitle !== ''){
              params['search_name'] = data.search_name;
              params['location_name'] = this._propertyListingService.selectedKeywordSearch;
              params['location_subtitle'] = this._propertyListingService.selectedKeywordSearchSubtitle;
            }
            
            this._propertyListingService.addRequestInformation(params, ()=>{
              this.router.navigateByUrl('/matching-request');
            });
          }
        }
      ]
    });
    await forgot.present();
  }

  search(){
    console.log(this.getSearchListingParam());
    if(this.validateSearchParam()){
      let params = JSON.parse(JSON.stringify(this._propertyListingService.searchListingParams));
      this.saveSearch(params);
    }
  }
  
  searchLocation(ev){
    if(ev.target.value !== ''){
      this.hideSearch = false;
    }
    var query = ev.target.value;
    if(this.getSearchListingParam().main_category !== ''){
      query += '&main_category='+this.getSearchListingParam().main_category;
    }
    this._propertyListingService.getLocationStep1(query, this.getSearchListingParam().property_segments, ()=>{
      this.locationList = this._propertyListingService.step1Location;
    });
    
  }

  getShortFormMap(){
    return this._propertyListingService.shortFormMap;
  }

  searchKeyword(ev){
    if(ev.target.value == ''){
      this.hideSearch = true;
      this.searchKeywordListingResult = [];
      this._propertyListingService.selectedKeywordSearch = '';
      this._propertyListingService.searchListingParams.query_type = '';
      this._propertyListingService.searchListingParams.query_ids = '';
      this._propertyListingService.searchListingParams.query_coords = '';
    }else{
      this._propertyListingService.getInputRequestInfo(ev.target.value,()=>{
        this.searchKeywordListingResult = this._propertyListingService.searchInputRequestResult;
        this.hideSearch = false;
      })
    }
  }

  cancel(ev){
    this.hideSearch = true;
    this._propertyListingService.selectedKeywordSearch = '';
  }

  selectKeywordResult(item){
    console.log(item);
    this._propertyListingService.searchListingParams.keywords = this._propertyListingService.selectedKeywordSearch;
    this._propertyListingService.selectedKeywordSearch = item.name;
    this._propertyListingService.searchListingParams.location_name = item.name;
    this._propertyListingService.searchListingParams.location_subtitle = item.address;
    this._propertyListingService.selectedKeywordSearchSubtitle = item.address;
    this._propertyListingService.searchListingParams.query_type = item.type;
    this._propertyListingService.searchListingParams.query_ids = item.id;
    if(typeof item.coordinates !== 'undefined' && typeof item.coordinates !== 'undefined'){
      this._propertyListingService.searchListingParams.query_coords = item.coordinates.lat+','+item.coordinates.lng;
    }
    this.hideSearch = true;

  }

  validateSearchParam(){
    var subcategories = '';
    for(var i=0;i<this.subcategories.length;i++){
      if(this.subcategories[i].checked){
        subcategories += this.subcategories[i].key+',';
      }
    }
    if(subcategories !== ''){
      subcategories = subcategories.substring(0, subcategories.length - 1);
    }
    // this._propertyListingService.searchListingParams.sub_categories = subcategories;
    if(this._propertyListingService.selectedKeywordSearch == ''){
      this._utilitiesService.alertMessage('Location is compulsory','Please select location to narrow down your search');
      return false;
    }
    // if(this._propertyListingService.searchListingParams.sub_categories == ''){
    //   this._utilitiesService.alertMessage('Subcategories is compulsory','Please select subcategories to narrow down your search');
    //   return false;
    // }
    return true;
  }

}
