import { Component, OnInit, NgZone } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Base64 } from '@ionic-native/base64/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { CONFIGURATION } from 'src/services/config.service';
import { Location } from '@angular/common';
import { Platform } from '@ionic/angular';


declare var navigator: any;
declare var Camera: any;
declare var plugins: any;

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  form = {
    firstname: '',
    title: '',
    qualification: '',
    lastname: '',
    gender: '',
    marketing_video: '',
    facebook: '',
    instagram: '',
    wechat: '',
    user_type: '',
    user_id: '',
    email: '',
    mobile: ''
  }


  showImage: boolean = false;
  imageUrl: any = 'assets/img/placeholder.png';
  cameraUpdate: boolean = false;
  loaded: boolean = false;

  constructor(
    public _utilitiesService: UtilitiesService,
    public _zone: NgZone,
    private base64: Base64,
    private location: Location,
    private platform: Platform,
    private _DomSanitizationService: DomSanitizer,
    public _accountService: AccountService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  ngOnInit

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    this.form.user_id = this._accountService.userData.id;
    this.form.user_type = this._accountService.userData.member_type;
    this._accountService.getUserDetailProfile(() => {
      this.form.email = this._accountService.userData.email;
      this.form.title = this._accountService.userData.title;
      this.form.qualification = this._accountService.userData.qualification;
      this.form.firstname = this._accountService.userData.firstname;
      this.form.lastname = this._accountService.userData.lastname;
      this.form.gender = this._accountService.userData.gender;
      this.form.facebook = this._accountService.userData.facebook;
      this.form.marketing_video = this._accountService.userData.marketing_video;
      this.form.instagram = this._accountService.userData.instagram;
      this.form.wechat = this._accountService.userData.wechat;
      this.form.mobile = this._accountService.userData.mobile
      if (this._accountService.userData.profile_picture !== null) {
        this.imageUrl = CONFIGURATION.base_url + this._accountService.userData.profile_picture;
      }
      this.loaded = true;
    });
  }

  goBack(){
    this.location.back();
  }

  updateProfile() {
    this._accountService.updateProfile(this.form, () => {
      if (this.cameraUpdate) {
        this._accountService.uploadImage(this.imageUrl, () => {
          this.imageUrl = this._accountService.userData.profile_picture;
          this._utilitiesService.alertMessageCallback('Successful', 'Profile updated successfully', () => {
            // this.navCtrl.setRoot(HomePage);
          });
        });
      } else {
        this._utilitiesService.alertMessageCallback('Successful', 'Profile updated successfully', () => {
          // this.navCtrl.setRoot(HomePage);
        });
      }

    });
  }

  async takePhotoOptionPopup(){
    const alertController =this._utilitiesService.getAlertCtrl();
        const alert = await alertController.create({
                header: 'Choose option',
                message: '',
                buttons: [
                    {
                        text: 'Take Photo',
                        handler: data => {
                            console.log('OK clicked');
                            this.takePhotoCamera();
                        }
                    },
                    {
                      text: 'Choose from Gallery',
                      handler: data => {
                          console.log('OK clicked');
                          this.takePhotoGallery();
                      }
                  },
                    
                ]
            });
        return alert.present();
  }


  takePhotoCamera() {
    navigator.camera.getPicture(imageData => {
      this._zone.run(() => {
        plugins.crop.promise(imageData, {
          quality: 75
        }).then(newPath => {

          this.base64.encodeFile(newPath).then((base64File: string) => {
            console.log(base64File);
            this._zone.run(() => {

              this.showImage = true;
              if (this.platform.is('ios')) {
                this.imageUrl = base64File.replace(/^file:\/\//, '');
              }else{
                this.imageUrl = base64File;
              }
              // console.log(this.imageUrl);
              this.cameraUpdate = true;
            });
          }, (err) => {
            console.log(err);
          });
        },
          error => {
            console.log('CROP ERROR -> ' + JSON.stringify(error));
            // alert('CROP ERROR: ' + JSON.stringify(error));
          }
        );

      });

    }, error => {
      console.log(error);
    },
      {
        destinationType: Camera.DestinationType.FILE_URI,
        quality: 75,
        correctOrientation: true,
        sourceType: Camera.PictureSourceType.CAMERA
      });
  }
  takePhotoGallery() {
    navigator.camera.getPicture(imageData => {
      this._zone.run(() => {
        plugins.crop.promise(imageData, {
          quality: 75
        }).then(newPath => {
          this.base64.encodeFile(newPath).then((base64File: string) => {
            console.log(base64File);
            this._zone.run(() => {

              this.showImage = true;
              if (this.platform.is('ios')) {
                this.imageUrl = base64File.replace(/^file:\/\//, '');
              }else{
                this.imageUrl = base64File;
              }
              // console.log(this.imageUrl);
              this.cameraUpdate = true;
            });
          }, (err) => {
            console.log(err);
          });
        },
          error => {
            console.log('CROP ERROR -> ' + JSON.stringify(error));
            // alert('CROP ERROR: ' + JSON.stringify(error));
          }
        );

      });

    }, error => {
      console.log(error);
    },
      {
        quality: 75,
        correctOrientation: true,
        encodingType: Camera.EncodingType.JPEG,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM
      });

  }

  toBase64(url: string) {
    return new Promise<string>(resolve => {
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = () => {
        var reader: any = new FileReader();
        reader.onloadend = () => {
          resolve(reader.result);
        }
        reader.readAsDataURL(xhr.response);
      };
      xhr.open('GET', url);
      xhr.send();
    });
  }

  resize(base64Img, width, height) {
    var img = new Image();
    img.src = base64Img;
    var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
    canvas.width = width;
    canvas.height = height;
    ctx.drawImage(img, 0, 0, width, height);
    return canvas.toDataURL('image/jpeg');
  }

}
