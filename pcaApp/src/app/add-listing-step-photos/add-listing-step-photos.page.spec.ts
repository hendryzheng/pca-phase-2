import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddListingStepPhotosPage } from './add-listing-step-photos.page';

describe('AddListingStepPhotosPage', () => {
  let component: AddListingStepPhotosPage;
  let fixture: ComponentFixture<AddListingStepPhotosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddListingStepPhotosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddListingStepPhotosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
