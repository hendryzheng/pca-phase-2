import { Component, OnInit, NgZone } from '@angular/core';
import { PropertyListingService } from 'src/services/property-listing.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { Crop } from '@ionic-native/crop/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { MenuService } from 'src/services/menu.service';
import { Platform } from '@ionic/angular';

declare var plugins: any;
declare var Camera: any;
declare var navigator: any;
declare var window: any;

@Component({
  selector: 'app-add-listing-step-photos',
  templateUrl: './add-listing-step-photos.page.html',
  styleUrls: ['./add-listing-step-photos.page.scss'],
})
export class AddListingStepPhotosPage implements OnInit {

  constructor(private _propertyListingService: PropertyListingService,
    private _utilitiesService: UtilitiesService,
    private _menuService: MenuService,
    private router: Router,
    private imagePicker: ImagePicker,
    private platform: Platform,
    private crop: Crop,
    private file: File,
    private _zone: NgZone,
    private photoViewer: PhotoViewer,
    private base64: Base64,
    private _DomSanitizer: DomSanitizer,
    private location: Location) { }


    fileUrl: any = null;
    respData: any;
    imageArr: any = [];
    imageArrDisplay: any = [];
    imageDelete: any = [];
    imageUploading: boolean = false;
    interval: any = null;


  ngOnInit() {
  }

  ionViewWillEnter(){
    this._menuService.checkUserSession();
    this._propertyListingService.form.pca_member_id = this._menuService.getUserData().id;
  }

  getStep1PropertyDetail(){
    return this._propertyListingService.step1PropertyDetail;
  }

  getStep1SelectedLocation(){
    return this._propertyListingService.step1SelectedLocation;
  }

  goBack(){
    this.location.back();
  }

  getShortFormMap(){
    return this._propertyListingService.shortFormMap;
  }

  getForm(){
    return this._propertyListingService.form;
  }

  view(item){
    this.photoViewer.show(item.path, '');
  }

  async takePhotoOption() {
    let alert = await this._utilitiesService.getAlertCtrl().create({
      header: 'Choose your option',
      // message: message,
      buttons: [
        {
          text: 'Take Photo',
          handler: data => {
            this.selectImageCamera();
            console.log('OK clicked');
          }
        },
        {
          text: 'Choose From Gallery',
          handler: data => {
            this.selectImageGallery();
            console.log('OK clicked');
          }
        }
      ]
    });
    alert.present();
  }

  selectImageCamera() {
    this.actionSelectPicture(Camera.PictureSourceType.CAMERA);
  }

  getTrustImg(imageSrc){
    let path = window.Ionic.WebView.convertFileSrc(imageSrc);
    console.log(path);
    return path;
 }

  continue(){
    console.log(this.getForm());
    this._propertyListingService.createNewListing(this.getForm(), ()=>{
      if(this.imageArr.length > 0){
        this._utilitiesService.showLoadingSyncImages();
        this._propertyListingService.uploadSucceed = 0;
        for(var i=0;i < this.imageArr.length;i++){
          if(this.imageArr[i].uploaded == false){
            let params = {
              'id' : this._propertyListingService.propertyListingDetail.id
            };
            this._propertyListingService.uploadImage(params, this.imageArr[i].path, this.imageArr[i].fileName, res => {
              console.log(this.imageArr);
              console.log(res);
              for(var j=0;j < this.imageArr.length; j++){
                if(this.imageArr[j].fileName == res.fileName){
                  this.imageArr[j].uploaded = true;
                  this.imageArr[j].path = res.imageUrl;
                }
              }
              
            }, ()=>{
              this._utilitiesService.hideLoading().then(resp => {
                this._utilitiesService.alertMessage('Error','There is a problem with image uploading at the moment. Please try again later');
              });
            });
          }
        }
        this.interval = setInterval(()=>{
          var count = 0;
          console.log(this.imageArr);
          console.log(this._propertyListingService.uploadSucceed);
          for(var j=0;j < this.imageArr.length; j++){
            if(this.imageArr[j].uploaded == true){
              count++;
            }
          }
          if(count == this.imageArr.length){
            clearInterval(this.interval);
            this._utilitiesService.hideLoading().then(resp => {
              setTimeout(()=>{
                this._utilitiesService.alertMessageCallback('Upload success','Listing images uploaded successfully', ()=>{
                  this.router.navigateByUrl('/view-listing-connect');

                });
              },1500)
              
            });
            
          }
        },1000);
      }else{
        this._utilitiesService.alertMessageCallback('Upload success','Property listing created successfully', ()=>{
          this._menuService.eNamecardState = 'my-listing';
          this.router.navigateByUrl('/view-listing-connect');
        });
      }
    });
    
  }


  actionSelectPicture(source) {
    navigator.camera.getPicture(imageData => {
      this._zone.run(() => {
        var imgData = imageData;
        if (this.platform.is('ios')) {
          imgData = imageData.replace(/^file:\/\//, '');
        }
        plugins.crop.promise(imageData, {
          quality: 10
        }).then(newPath => {
          console.log(newPath);
          let convrted = this.getTrustImg(newPath);
          console.log(convrted);
          this.base64.encodeFile(convrted).then((base64File: string) => {
            console.log(base64File);
            this._zone.run(() => {
              let temp = {
                filePath: '-',
                uploaded: false,
                fileName: Math.random().toString(36).substring(10),
                path: base64File
              };
              console.log(temp);
              this.imageArr.push(temp);
              // var date = new Date();
              // var timestamp = date.getTime();
              // let filename = timestamp + '.jpeg';
              // this.writeFile(base64File, filename);
            });
          }, (err) => {
            console.log(err);
          });
        },
          error => {
            console.log('CROP ERROR -> ' + JSON.stringify(error));
            // alert('CROP ERROR: ' + JSON.stringify(error));
          }
        );

      });

    }, error => {
      console.log(error);
    },
      {
        destinationType: Camera.DestinationType.FILE_URI,
        quality: 10,
        correctOrientation: true,
        sourceType: source
      });
  }

  selectImageGallery() {
    this.actionSelectPicture(Camera.PictureSourceType.SAVEDPHOTOALBUM);
  }

  async promptDelete(callback: () => void) {
    const alertController = this._utilitiesService.alertCtrl;
    const alert = await alertController.create({
      header: 'Confirmation',
      message: 'Are you sure to delete this item?',
      buttons: [
        {
          text: 'Yes',
          handler: data => {
            console.log('OK clicked');
            callback();
          }
        },
        {
          text: 'No',
          handler: data => {
            console.log('No clicked');
          }
        }
      ]
    });
    return alert.present();
  }

  delete(item) {
    console.log(item);
    this.promptDelete(() => {
      for (var i = 0; i < this.imageArr.length; i++) {
        if (this.imageArr[i].fileName == item.fileName) {
          this._zone.run(() => {
            if(this.imageArr[i].uploaded == true){
              this.imageDelete.push(this.imageArr[i].fileName);
            }
            this.imageArr.splice(i, 1);
            this.imageArrDisplay.splice(i,1);
            // this.saveCurrentRecord();
          });
        }
      }
    });

  }

  readImgFile(filePath, fileName){
    this.file.readAsDataURL(filePath, fileName).then(result => {
      return result;
    });
  }


  //here is the method is used to write a file in storage  
  public writeFile(base64Data: any, fileName: any) {
    let contentType = this.getContentType(base64Data);
    let DataBlob = this.base64toBlob(base64Data, contentType);
    // here iam mentioned this line this.file.dataDirectory is a native pre-defined file path storage. You can change a file path whatever pre-defined method.  
    let filePath = this.file.dataDirectory;
    this.file.writeFile(filePath, fileName, DataBlob, contentType).then((success) => {
      this.file.readAsDataURL(filePath, fileName).then(result => {
        // console.log(result);
        this._zone.run(() => {
          let temp = {
            filePath: filePath,
            fileName: fileName,
            path: filePath+fileName
          };
          this.imageArr.push(temp);
          let temp_2 = {
            filePath: filePath,
            fileName: fileName,
            path: result
          };
          this.imageArrDisplay.push(temp_2);
          // this.saveCurrentRecord();
        });
      });

      console.log("File Writed Successfully", success);
    }).catch((err) => {
      console.log("Error Occured While Writing File", err);
    })
  }
  //here is the method is used to get content type of an bas64 data  
  public getContentType(base64Data: any) {
    let block = base64Data.split(";");
    let contentType = block[0].split(":")[1];
    return contentType;
  }
  //here is the method is used to convert base64 data to blob data  
  public base64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    let sliceSize = 512;
    let byteCharacters = atob(b64Data.replace(/^data:image\/\*;charset=utf-8;base64,/, ''));
    var byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);
    var blob = new Blob([byteArray], {
      type: undefined
    });
    return blob;
  }

  selectImageCrop() {
    this.imagePicker.getPictures({ maximumImagesCount: 1, outputType: 0 }).then((results) => {
      for (let i = 0; i < results.length; i++) {
        // console.log('Image URI: ' + results[i]);
        this.crop.crop(results[i], { quality: 80 })
          .then(
            newImage => {
              // console.log('new image path is: ' + newImage);

            },
            error => console.error('Error cropping image', error)
          );
      }
    }, (err) => { console.log(err); });
  }

}
