import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddListingStepPhotosPage } from './add-listing-step-photos.page';

const routes: Routes = [
  {
    path: '',
    component: AddListingStepPhotosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddListingStepPhotosPage]
})
export class AddListingStepPhotosPageModule {}
