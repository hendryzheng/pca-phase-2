import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PcaCategoriesPage } from './pca-categories.page';

const routes: Routes = [
  {
    path: '',
    component: PcaCategoriesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PcaCategoriesPage]
})
export class PcaCategoriesPageModule {}
