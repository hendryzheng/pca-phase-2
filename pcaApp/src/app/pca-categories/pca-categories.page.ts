import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pca-categories',
  templateUrl: './pca-categories.page.html',
  styleUrls: ['./pca-categories.page.scss'],
})
export class PcaCategoriesPage implements OnInit {

  pcaCategories: any = [];

  constructor(private _menuService: MenuService,
              private router: Router,
              private location: Location) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this._menuService.getPcaCategories(() => {
      this.pcaCategories = this._menuService.pca_categories;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PcaCategoriesPage');
  }

  getPcaCategories(){
    return this._menuService.pca_categories;
  }

  getMenu() {
    return this._menuService.selectedMenu.menu_name;
  }

  goBack(){
    this.location.back();
  }

  viewItem(item){
    this._menuService.selected_pca_category = item;
    this.router.navigateByUrl('/pca-listing');
  }

}
