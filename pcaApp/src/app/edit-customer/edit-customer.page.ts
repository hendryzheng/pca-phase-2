import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { MenuService } from 'src/services/menu.service';
import { AccountService } from 'src/services/account.service';
import { Location } from '@angular/common';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { CONFIGURATION } from 'src/services/config.service';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.page.html',
  styleUrls: ['./edit-customer.page.scss'],
})
export class EditCustomerPage implements OnInit {

  @ViewChild(SignaturePad) public signaturePad : SignaturePad;

  public signaturePadOptions : Object = {
    'minWidth': 2,
    'canvasWidth': 340,
    'canvasHeight': 200
  };
  public signatureImage : string = null;
  signed: boolean = false;
  email: any = '';
  agent_email: any = '';
  toggleSignature: boolean = false;

  constructor(private _utilitiesService: UtilitiesService,
    private router: Router,
    private location: Location,
    private _menuService: MenuService,
    private _accountService: AccountService) { }

    form = {
      name: '',
      phone: '',
      email: '',
      gender:'',
      address: '',
      purpose_of_contact: '',
      listing_type: '',
      property_type: '',
      property_address: '',
      asking_price: '',
      built_in_area: '',
      type: '',
      pdpa_signed: false
    }

  ngOnInit() {
  }

  toggle(){
    this.toggleSignature = !this.toggleSignature;
  }

  goBack(){
    this.location.back();
  }

  getCustomerType(){
    return this._menuService.customer_type;
  }


  ionViewWillEnter() {
    this._accountService.checkUserSession();
    this.form.name = this._menuService.customer.fullname;
    this.form.email = this._menuService.customer.email;
    this.form.gender = this._menuService.customer.gender;
    this.form.phone = this._menuService.customer.mobile;
    this.form.address = this._menuService.customer.address;
    this.form.purpose_of_contact = this._menuService.customer.purpose_of_contact;
    this.form.listing_type = this._menuService.customer.listing_type;
    this.form.property_type = this._menuService.customer.property_type;
    this.form.property_address = this._menuService.customer.property_address;
    this.form.asking_price = this._menuService.customer.asking_price;
    this.form.built_in_area = this._menuService.customer.built_in_area;
    this.form.type = this._menuService.customer.type;

    if(this._menuService.customer.status == '1'){
      this.form.pdpa_signed = true;
    }else{
      this.toggleSignature = false;
    }
  }

  submit() {

  }

  getCustomer(){
    return this._menuService.customer;
  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.name)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.phone)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.gender)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.address)) {
      validate = false;
    }
    if (this.form.pdpa_signed == false) {
      validate = false;
    }

    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details and ensure your customer has agreed to the PDPA consent');
    }
    console.log(validate);
    return validate;
  }

  drawCancel() {
    // this.navCtrl.push(HomePage);
  }

   drawComplete() {
    this.signatureImage = this.signaturePad.toDataURL();
    this.signed = true;
    console.log(this.signatureImage);
    // this.navCtrl.push(HomePage, {signatureImage: this.signatureImage});
  }

  drawClear() {
    this.signed = false;
    this.signatureImage = null;
    this.signaturePad.clear();
    console.log(this.signaturePad.toDataURL());
    console.log(this.signaturePad.toDataURL().length);
  }

  getBaseUrl(){
    return CONFIGURATION.base_url;
  }

  update() {
    if (this.validateForm()) {
      let formSubmission = {
        'id' : this._menuService.customer.id,
        'Customer[fullname]': this.form.name,
        'Customer[email]': this.form.email,
        'Customer[mobile]': this.form.phone,
        'Customer[gender]': this.form.gender,
        'Customer[address]': this.form.address
      };
      
      this._menuService.updateCustomer(formSubmission, () => {
        this.signatureImage = this.signaturePad.toDataURL();
        console.log(this.signatureImage);
        if(this.signatureImage !== null && this.signatureImage.length !== 2266){
          let customer_id = this._menuService.customer.id;
          this._menuService.uploadImageSignature(customer_id, this.signatureImage, res=>{
            this._utilitiesService.alertMessageCallback('Customer added successfully','You have successfully added this customer',()=>{
              this._menuService.eNamecardState = 'customer';
              this.router.navigateByUrl('/customer-data',{replaceUrl: true});
            });
          });
        }else{
          this._menuService.eNamecardState = 'customer';
          this.router.navigateByUrl('/customer-data',{replaceUrl: true});
        }

      });
    }
  }



}
