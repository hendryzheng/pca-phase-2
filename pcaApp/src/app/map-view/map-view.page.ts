import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import leaflet from 'leaflet';
import { UtilitiesService } from 'src/services/utilities.service';
import { CONFIGURATION } from 'src/services/config.service';
import { PropertyListingService } from 'src/services/property-listing.service';

declare var plugin: any;
declare var google: any;
declare var window: any;
declare var L: any;
@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.page.html',
  styleUrls: ['./map-view.page.scss'],
})
export class MapViewPage implements OnInit {
  constructor(
    private router: Router,
    private _utilitiesService: UtilitiesService,
    private _propertyListingService: PropertyListingService,
    private location: Location,
    private geolocation: Geolocation) {
  }

  height: any = 500;
  width: any = 500;
  basemap: any = null;

  isLocationTurnedOn: boolean = false;

  @ViewChild('map') mapContainer: ElementRef;
  map: any;

  ngOnInit() {

  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad MapsPage');
    var physicalScreenHeight = window.screen.height;
    this.height = physicalScreenHeight;
    let body: any = document.getElementsByTagName('body');
    var physicalScreenWidth = body[0].offsetWidth;
    this.width = physicalScreenWidth;
    this.loadmap(this._propertyListingService.selectedPropertyListingDetail.location.lat, this._propertyListingService.selectedPropertyListingDetail.location.lng);

  }

  getSelectedListing(){
    return this._propertyListingService.selectedPropertyListingDetail;
  }

  ionViewDidEnter() {
    this.geolocation.getCurrentPosition().then((resp) => {

    });

  }

  goBack() {
    this.location.back();
  }

  loadmap(lat, lng) {
    L.Icon.Default.imagePath = "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/images"; // missing setup

    var center = L.bounds([1.56073, 104.11475], [1.16, 103.502]).getCenter();
    var map = L.map('mapdiv').setView([center.x, center.y], 16);

    var basemap = L.tileLayer('https://maps-{s}.onemap.sg/v3/Original/{z}/{x}/{y}.png', {
      detectRetina: true,
      maxZoom: 18,
      minZoom: 12
    });

    map.setMaxBounds([[1.56073, 104.1147], [1.16, 103.502]]);

    basemap.addTo(map);
    var content = document.getElementById('map-pin').innerHTML;
    var marker = new L.Marker([lat, lng], { bounceOnAdd: false }).addTo(map);
    var popup = L.popup()
      .setLatLng([lat, lng])
      .setContent(content)
      .openOn(map);

  }


  getWidth() {
    return this.width;
  }

  getHeight() {
    return this.height;
  }
}
