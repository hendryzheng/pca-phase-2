import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  form = {
    firstname: '',
    lastname: '',
    gender: '',
    isAgent: false,
    email: '',
    password: '',
    confirm_password: '',
    mobile: '',
    agree: false
  }

  constructor(
    public location: Location,
    public _accountService: AccountService,
    public _utilitiesService: UtilitiesService
  ) { }

  ngOnInit() {
  }

  private callbackSuccess() {
    this._utilitiesService.hideLoading();
    this.location.back();
  }

  goBack(){
    this.location.back();
  }

  toggleAgent(event) {
    console.log(event);
  }

  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }

  ionViewWillLeave() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });

    }
  }


  submitRegister() {
    if (this.validateForm()) {
      let formSubmission = {
        'Account[firstname]': this.form.firstname,
        'Account[lastname]': this.form.lastname,
        'Account[password]': this.form.password,
        'Account[email]': this.form.email,
        'Account[mobile]': this.form.mobile,
        'Account[gender]': this.form.gender,
        'isAgent': this.form.isAgent
      };
      this._accountService.register(formSubmission, () => {
        this.callbackSuccess();
      });
    }

  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.firstname)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.lastname)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.mobile)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.password)) {
      validate = false;
    }
    if (this.form.password !== this.form.confirm_password) {
      this._utilitiesService.alertMessage('Validation Error', 'Password not equal with Confirm Password');
      return false;
    }
    if (this.form.isAgent) {
      if (this.form.email.indexOf('@') >= 0) {
        this._utilitiesService.alertMessage('Validation Error', 'Please only include your email username without @');
        return false;
      }
    }
    if(this.form.agree === false){
      this._utilitiesService.alertMessage('Validation Error', 'Please agree to the terms to proceed.');
      return false;
    }

    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
      return false;
    }
    console.log(validate);
    return validate;
  }

  // go to login page
  login() {
    this.location.back();
  }


}
