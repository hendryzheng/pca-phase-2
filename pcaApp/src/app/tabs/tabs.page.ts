import { Component, OnInit, AfterViewChecked, AfterViewInit } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit, AfterViewInit {

  constructor(private _accountService: AccountService, private _menuService: MenuService, private router: Router) { }
  unread: any = 0;
  ionViewWillEnter(){
    this._accountService.checkUserSession();
    var uread = localStorage.getItem('unread');
    if(uread){
      this.unread = uread;
    }
    if(this._menuService.eNamecardState !== ''){
      this.router.navigateByUrl('/e-namecard');
    }
  }

  ngAfterViewInit(){
    // this._accountService.checkUserSession();
    // var uread = localStorage.getItem('unread');
    // if(uread){
    //   this.unread = uread;
    // }
    // if(this._menuService.eNamecardState !== ''){
    //   this.router.navigateByUrl('/e-namecard');
    // }
  }


  ngOnInit() {
  }

  getUserData(){
    return this._accountService.userData;
  }

  isLoggedIn(){
    return this._accountService.isUserSessionAlive();
  }

}
