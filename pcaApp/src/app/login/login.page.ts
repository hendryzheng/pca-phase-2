import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { HttpService } from 'src/services/http.service';
import { AlertController } from '@ionic/angular';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private router: Router,
    private location: Location,
    public forgotCtrl: AlertController,
    private _utilitiesService: UtilitiesService,
    private _httpService: HttpService,
    private _accountService: AccountService,
  ) { }

  ngOnInit() {
  }

  form = {
    email: '',
    password: ''
  };

  token: any = '';

  // go to register page
  register() {
    this.router.navigateByUrl('/sign-up');
  }

  ngAfterViewInit() {
    // let tabs = document.querySelectorAll('.show-tabbar');
    // if (tabs !== null) {
    //   Object.keys(tabs).map((key) => {
    //     tabs[key].style.display = 'none';
    //   });
    // }
  }

  ionViewWillLeave() {
    // let tabs = document.querySelectorAll('.show-tabbar');
    // if (tabs !== null) {
    //   Object.keys(tabs).map((key) => {
    //     tabs[key].style.display = 'flex';
    //   });

    // }
  }

  goBack(){
    this.location.back();
  }

  private callbackSuccess(response: any) {
    localStorage.setItem('userData', JSON.stringify(response));
    this._accountService.userData = response;
    this._accountService.registerToken();
    this.router.navigate(['/tabs'], {replaceUrl: true})
  }

  loginSubmit() {
    if (this.validateForm()) {
      this._utilitiesService.showLoading();
      this._httpService.postRequest(CONFIGURATION.URL.login, this.form).subscribe(response => {
        console.log(response);
        var responseHttp = response;
        this._utilitiesService.hideLoading().then(response => {
          if (responseHttp.status === 'OK') {
            this.callbackSuccess(responseHttp.data);
          } else {
            this._utilitiesService.alertMessage('Oops! Something happened', responseHttp.message);
          }
        });

      },error => {
        this._utilitiesService.hideLoading();
        this._utilitiesService.alertMessage('Oops! Something happened', '');
        console.log(error);
      });
    }

  }

  public focusInput (event): void {

    let total = 0;
    let container = null;

    const _rec = (obj) => {

        total += obj.offsetTop;
        const par = obj.offsetParent;
        if (par && par.localName !== 'ion-content') {
            _rec(par);
        } else {
            container = par;
        }
    }
    _rec(event.target);
    container.scrollToPoint(0, total - 50, 400);
}

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.password)) {
      validate = false;
    }
    if (!validate) {
      this._utilitiesService.alertMessage('Oops! Something happened', CONFIGURATION.MESSAGE.REGISTER_MANDATORY);
    }
    console.log(validate);
    return validate;
  }

  forgotPwd(){
    alert('a');
    this.forgotPass();
  }

  async forgotPass() {
    const forgot = await this.forgotCtrl.create({
      subHeader: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let param = {
              email: data.email
            }
            this._accountService.forgotPassword(param, () => {

            });
            // let toast = this.toastCtrl.create({
            //   message: 'Email was sended successfully',
            //   duration: 3000,
            //   position: 'top',
            //   cssClass: 'dark-trans',
            //   closeButtonText: 'OK',
            //   showCloseButton: true
            // });
            // toast.present();
          }
        }
      ]
    });
    await forgot.present();
  }

}
