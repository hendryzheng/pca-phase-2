import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewLaunchedPage } from './new-launched.page';

const routes: Routes = [
  {
    path: '',
    component: NewLaunchedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NewLaunchedPage]
})
export class NewLaunchedPageModule {}
