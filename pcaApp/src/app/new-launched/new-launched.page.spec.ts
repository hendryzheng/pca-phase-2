import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewLaunchedPage } from './new-launched.page';

describe('NewLaunchedPage', () => {
  let component: NewLaunchedPage;
  let fixture: ComponentFixture<NewLaunchedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewLaunchedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewLaunchedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
