import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-new-launched',
  templateUrl: './new-launched.page.html',
  styleUrls: ['./new-launched.page.scss'],
})
export class NewLaunchedPage implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

}
