import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectToProjectPage } from './connect-to-project.page';

describe('ConnectToProjectPage', () => {
  let component: ConnectToProjectPage;
  let fixture: ComponentFixture<ConnectToProjectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectToProjectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectToProjectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
