import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { CONFIGURATION } from 'src/services/config.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-connect-to-project',
  templateUrl: './connect-to-project.page.html',
  styleUrls: ['./connect-to-project.page.scss'],
})
export class ConnectToProjectPage implements OnInit {

  form = {
    search: ''
  };

  showResult: boolean = false;
  loadingResult: boolean = false;
  selected_option: any = '';
  selected_item: any = [];

  constructor(
    private _menuService: MenuService,
    private location: Location,
    private _utilitiesService: UtilitiesService) {
  }

  ngOnInit(){

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConnectToProjectPage');
  }

  goBack(){
    this.location.back();
  }

  openLink(type) {
    var search = this.form.search;
    // var property_guru_url = 'https://www.propertyguru.com.sg/singapore-property-listing/property-for-sale?freetext=' + search+'&unselected=PROPERTY%7C141';
    let keyword = search.replace(/ /g,"-");
    var property_guru_url = 'http://m.stproperty.sg/property-for-sale/search/' + keyword;
    console.log(property_guru_url);
    var co = 'https://www.99.co/singapore/sale?listing_type=sale&name='+this.selected_option+'&query_coords=1.314705209196403%2C103.84169169890549&query_type=cluster&query_ids='+this.selected_item.id+'&zoom=15&radius_max=1000&query_limit=radius&property_segments=residential';
    var srx_url = 'https://www.srx.com.sg/search/sale/residential/' + search;
    if (type === '1') {
      this._utilitiesService.openBrowser('99.co', co);
    } else if (type === '2') {
      this._utilitiesService.openBrowser('STProperty', property_guru_url);
    } else if (type === '3') {
      this._utilitiesService.openBrowser('SRX', srx_url);
    } else {
      var base_url = CONFIGURATION.base_url;
      // this._utilitiesService.openBrowser('Connect to Portals', base_url + '/connect_to_project');
    }
  }

  onKey(ev){
    let params = {
      name: this.form.search
    };
    if(this.form.search !== ''){
      this.loadingResult = true;
      this._menuService.getAutoComplete(params, ()=>{
        this.showResult = true;
        this.loadingResult = false;
      });
    }else{
      this.showResult = false;
      this.selected_item = [];
      this.selected_option = '';
    }
    
  }

  select(item){
    this.selected_option = item.title;
    this.selected_item = item;
    this.form.search = this.selected_option;
    this._menuService.autocomplete = [];
  }

  getAutoCompleteList(){
    return this._menuService.autocomplete;
  }

}
