import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Location } from '@angular/common';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-preview-image',
  templateUrl: './preview-image.page.html',
  styleUrls: ['./preview-image.page.scss'],
})
export class PreviewImagePage implements OnInit {

  constructor(  
    public _menuService: MenuService,
    private photoViewer: PhotoViewer,
    private location: Location) {
  }

  ngOnInit() {
  }

  detail: any = [];

  viewImage(url){
    this.photoViewer.show(url);

  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreviewImagePage');
  }

  getDetail(){
    return this._menuService.selected_listing;
  }

  goBack(){
    this.location.back();
  }

}
