import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewImagePage } from './preview-image.page';

describe('PreviewImagePage', () => {
  let component: PreviewImagePage;
  let fixture: ComponentFixture<PreviewImagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewImagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewImagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
