import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.page.html',
  styleUrls: ['./change-email.page.scss'],
})
export class ChangeEmailPage implements OnInit {


  form = {
    email: '',
    newEmail: '',
    user_type: '',
    user_id: '',
  }
  showImage: boolean = false;
  imageUrl: any = 'assets/img/placeholder.png';
  cameraUpdate: boolean = false;
  loaded: boolean = false;


  constructor(private _accountService: AccountService,
    private location: Location,
    private _utilitiesService: UtilitiesService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    this.form.user_id = this._accountService.userData.id;
    this.form.user_type = this._accountService.userData.member_type;
    this._accountService.getUserDetailProfile(() => {
      this.form.email = this._accountService.userData.email;

      if (this._accountService.userData.profile_picture !== null) {
        this.imageUrl = CONFIGURATION.base_url + this._accountService.userData.profile_picture;
      }
      this.loaded = true;
    });
  }

  goBack(){
    this.location.back();
  }

  saveEmail() {
    if(this.form.newEmail !== ''){
      let param = {
        user_type: this.form.user_type,
        user_id: this.form.user_id,
        email: this.form.newEmail
      }
      this._accountService.updateEmail(param, () => {
        this._utilitiesService.alertMessageCallback('Successful', 'Email updated successfully', () => {
          // this.navCtrl.setRoot(HomePage);
        });
      });
    }else{
      this._utilitiesService.alertMessage('Validation Error','New email cannot be empty');
    }
    
  }

}
