import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.page.html',
  styleUrls: ['./promotions.page.scss'],
})
export class PromotionsPage implements OnInit {

  constructor(
    private _menuService: MenuService,
    private router: Router,
    private location: Location,
    private _utilitiesService: UtilitiesService) {
  }

  ngOnInit(){

  }

  goBack(){
    this.location.back();
  }

  ionViewWillEnter() {
    this._menuService.getPromotion(() => {

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromotionPage');
  }

  viewDetail(item) {
    this._menuService.selectedPromotion = item;
    this.router.navigateByUrl('/promotion-detail');
  }

  getPromotion() {
    return this._menuService.promotion;
  }
}
