import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PcaSeniorLeadersPage } from './pca-senior-leaders.page';

const routes: Routes = [
  {
    path: '',
    component: PcaSeniorLeadersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PcaSeniorLeadersPage]
})
export class PcaSeniorLeadersPageModule {}
