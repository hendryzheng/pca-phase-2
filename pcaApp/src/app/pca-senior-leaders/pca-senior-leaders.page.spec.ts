import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcaSeniorLeadersPage } from './pca-senior-leaders.page';

describe('PcaSeniorLeadersPage', () => {
  let component: PcaSeniorLeadersPage;
  let fixture: ComponentFixture<PcaSeniorLeadersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcaSeniorLeadersPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcaSeniorLeadersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
