import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
import { CONFIGURATION } from 'src/services/config.service';

@Component({
  selector: 'app-pca-senior-leaders',
  templateUrl: './pca-senior-leaders.page.html',
  styleUrls: ['./pca-senior-leaders.page.scss'],
})
export class PcaSeniorLeadersPage implements OnInit {

  constructor(private _menuService: MenuService,
    private router: Router,
    private _utilitiesService: UtilitiesService,
    private location: Location) { }

  ngOnInit() {
  }

  ionViewWillEnter() {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PcaCategoriesPage');
  }

  getListing(){
    return this._menuService.listing;
  }

  getMenu() {
    return this._menuService.selectedMenu.menu_name;
  }

  getMenuObj() {
    return this._menuService.selectedMenu;
  }

  goBack() {
    this.location.back();
  }

  viewItem(item) {

  }
  getBase() {
    return CONFIGURATION.base_url;
  }



}
