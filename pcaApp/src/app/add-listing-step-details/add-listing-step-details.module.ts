import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddListingStepDetailsPage } from './add-listing-step-details.page';

const routes: Routes = [
  {
    path: '',
    component: AddListingStepDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddListingStepDetailsPage]
})
export class AddListingStepDetailsPageModule {}
