import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';
import { PropertyListingService } from 'src/services/property-listing.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import * as moment from 'moment';

@Component({
  selector: 'app-add-listing-step-details',
  templateUrl: './add-listing-step-details.page.html',
  styleUrls: ['./add-listing-step-details.page.scss'],
})
export class AddListingStepDetailsPage implements OnInit {

  constructor(private _propertyListingService: PropertyListingService,
    private _utilitiesService: UtilitiesService,
    private router: Router,
    private datePicker: DatePicker,
    private location: Location) { }


  ngOnInit() {
  }

  getStep1PropertyDetail(){
    return this._propertyListingService.step1PropertyDetail;
  }

  getStep1SelectedLocation(){
    return this._propertyListingService.step1SelectedLocation;
  }

  goBack(){
    this.location.back();
  }

  getShortFormMap(){
    return this._propertyListingService.shortFormMap;
  }

  getForm(){
    return this._propertyListingService.form;
  }

  openDatePickerAvailability(){ 
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date =>{
        console.log('Got date: ', date);
        let dt = moment(date).format('YYYY-MM-DD');
        this._propertyListingService.form.date_of_availbility = dt;
        console.log(dt);
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  openDatePickerTenancyLease(){ 
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date =>{
        console.log('Got date: ', date);
        let dt = moment(date).format('YYYY-MM-DD');
        this._propertyListingService.form.tenancy_lease_expiration = dt;
        console.log(dt);
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  continue(){
    var selected = '';
    for(var i=0;i<this._propertyListingService.featuresOptions.length;i++){
      if(this._propertyListingService.featuresOptions[i].checked){
        selected +=  this._propertyListingService.featuresOptions[i]['value']+',';
      }
    }
    selected = selected.substring(0, selected.length - 1);
    this._propertyListingService.form.features = selected;
    this.router.navigateByUrl('/add-listing-step-photos');
  }

  getFeaturesOptions(){
    return this._propertyListingService.featuresOptions;
  }


}
