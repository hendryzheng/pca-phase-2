import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentToolsConnectPage } from './agent-tools-connect.page';

describe('AgentToolsConnectPage', () => {
  let component: AgentToolsConnectPage;
  let fixture: ComponentFixture<AgentToolsConnectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentToolsConnectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentToolsConnectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
