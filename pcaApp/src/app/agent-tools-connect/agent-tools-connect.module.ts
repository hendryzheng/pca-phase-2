import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AgentToolsConnectPage } from './agent-tools-connect.page';

const routes: Routes = [
  {
    path: '',
    component: AgentToolsConnectPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AgentToolsConnectPage]
})
export class AgentToolsConnectPageModule {}
