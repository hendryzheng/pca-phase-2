import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.page.html',
  styleUrls: ['./calculator.page.scss'],
})
export class CalculatorPage implements OnInit {

  loaded: boolean = false;
  calculator: any = [];

  constructor(
    private iab: InAppBrowser,
    private location: Location,
    private _menuService: MenuService,
    private _utilitiesService: UtilitiesService) {
  }

  ngOnInit(){

  }

  goBack(){
    this.location.back();
  }

  ionViewWillEnter() {
    this._menuService.getCalculators(() => {
      this.calculator = this._menuService.calculators;
      this.loaded = true;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalculatorPage');
  }

  open(item) {
    
    this._utilitiesService.openBrowser(item.ame, item.link);
  }

}
