import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyFilterPage } from './property-filter.page';

describe('PropertyFilterPage', () => {
  let component: PropertyFilterPage;
  let fixture: ComponentFixture<PropertyFilterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyFilterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyFilterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
