import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-property-filter',
  templateUrl: './property-filter.page.html',
  styleUrls: ['./property-filter.page.scss'],
})
export class PropertyFilterPage implements OnInit {

  isNotif: boolean = true;
  notifState: any = 'private';

  constructor(private location: Location) { }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

  toggleNotif(string){
    this.notifState = string;
  }

}
