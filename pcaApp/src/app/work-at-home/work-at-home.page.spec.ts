import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkAtHomePage } from './work-at-home.page';

describe('WorkAtHomePage', () => {
  let component: WorkAtHomePage;
  let fixture: ComponentFixture<WorkAtHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkAtHomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkAtHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
