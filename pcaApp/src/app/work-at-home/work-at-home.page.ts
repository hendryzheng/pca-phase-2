import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-work-at-home',
  templateUrl: './work-at-home.page.html',
  styleUrls: ['./work-at-home.page.scss'],
})
export class WorkAtHomePage implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

}
