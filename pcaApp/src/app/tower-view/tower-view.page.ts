import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-tower-view',
  templateUrl: './tower-view.page.html',
  styleUrls: ['./tower-view.page.scss'],
})
export class TowerViewPage implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

}
