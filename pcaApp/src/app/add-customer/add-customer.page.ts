import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { AccountService } from 'src/services/account.service';
import { Router } from '@angular/router';
import { MenuService } from 'src/services/menu.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.page.html',
  styleUrls: ['./add-customer.page.scss'],
})
export class AddCustomerPage implements OnInit {

  constructor(private _utilitiesService: UtilitiesService,
              private router: Router,
              private location: Location,
              private _menuService: MenuService,
              private _accountService: AccountService) { }

  form = {
    name: '',
    phone: '',
    email: '',
    gender:'',
    address: '',
    purpose_of_contact: '',
    listing_type: '',
    property_type: '',
    property_address: '',
    asking_price: '',
    built_in_area: '',
    type: '',
    pdpa_signed: false
  }
  type: any = '';

  purpose_of_contact = [
    {
      val: 'Follow up on your property availability',
      isChecked: false
    },
    {
      val: 'Potential Buyer / Tenant Enquiries',
      isChecked: false
    },
    {
      val: 'Property Market Update / Report',
      isChecked: false
    },
    {
      val: 'New Launches in Singapore',
      isChecked: false
    },
    {
      val: 'New Launches in Overseas',
      isChecked: false
    }
  ]
  ngOnInit() {
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    this.form.type = this._menuService.customer_type;
    if(this.form.type == 'pdpa'){
      this.type = 'PDPA';
    }else{
      this.type = 'Marketing Consent';
    }
  }

  submit(){

  }


  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.name)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.phone)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.gender)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.address)) {
      validate = false;
    }
    if (this.form.pdpa_signed == false) {
      validate = false;
    }

    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details and ensure your customer has agreed to the PDPA consent');
    }
    console.log(validate);
    return validate;
  }

  getCustomerType(){
    return this._menuService.customer_type;
  }

  goBack(){
    this.location.back();
  }

  submitWithPdpa(){
    if (this.validateForm()) {
      var purpose_of_contact = '';
      for(var i=0;i<this.purpose_of_contact.length;i++){
        if(this.purpose_of_contact[i].isChecked == true){
          purpose_of_contact += this.purpose_of_contact[i].val+',';
        }
      }
      let formSubmission = {
        'Customer[fullname]': this.form.name,
        'Customer[email]': this.form.email,
        'Customer[type]': this.type,
        'Customer[mobile]': this.form.phone,
        'Customer[property_type]': this.form.property_type,
        'Customer[property_address]': this.form.property_address,
        'Customer[listing_type]': this.form.listing_type,
        'Customer[asking_price]': this.form.asking_price,
        'Customer[built_in_area]': this.form.built_in_area,
        'Customer[gender]': this.form.gender,
        'Customer[address]': this.form.address,
        'Customer[purpose_of_contact]': purpose_of_contact,
        'Customer[pca_member_id]': this._accountService.userData.id,
        'user_type' : this._accountService.userData.member_type,
        'user_id': this._accountService.userData.id
      };
      if(this._accountService.viewUserDataState == true){
        formSubmission['Customer[pca_member_id]'] = this._accountService.viewUserData.id;
      }
      this._menuService.submitAddNewCustomer(formSubmission, () => {
        this.router.navigateByUrl('/edit-customer');
      });
    }
  }

  submitWithoutPdpa(){
    if (this.validateForm()) {
      var purpose_of_contact = '';
      for(var i=0;i<this.purpose_of_contact.length;i++){
        if(this.purpose_of_contact[i].isChecked == true){
          purpose_of_contact += this.purpose_of_contact[i].val+',';
        }
      }
      let formSubmission = {
        'Customer[fullname]': this.form.name,
        'Customer[status]' : 1,
        'Customer[type]': this.type,
        'Customer[email]': this.form.email,
        'Customer[property_type]': this.form.property_type,
        'Customer[property_address]': this.form.property_address,
        'Customer[listing_type]': this.form.listing_type,
        'Customer[asking_price]': this.form.asking_price,
        'Customer[built_in_area]': this.form.built_in_area,
        'Customer[gender]': this.form.gender,
        'Customer[address]': this.form.address,
        'Customer[purpose_of_contact]': purpose_of_contact,
        'Customer[pca_member_id]': this._accountService.userData.id,
        'user_type' : this._accountService.userData.member_type,
        'user_id': this._accountService.userData.id
      };
      if(this._accountService.viewUserDataState == true){
        formSubmission['Customer[pca_member_id]'] = this._accountService.viewUserData.id;
      }
      this._menuService.submitAddNewCustomer(formSubmission, () => {
        this.router.navigateByUrl('/customer-data');
      });
    }
  }


}
