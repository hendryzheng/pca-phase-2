import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartResultPage } from './chart-result.page';

describe('ChartResultPage', () => {
  let component: ChartResultPage;
  let fixture: ComponentFixture<ChartResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartResultPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
