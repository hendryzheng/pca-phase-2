import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart-result',
  templateUrl: './chart-result.page.html',
  styleUrls: ['./chart-result.page.scss'],
})
export class ChartResultPage implements OnInit {

  isNotif: boolean = true;
  notifState: any = 'avg-psf';

  constructor() { }

  ngOnInit() {
  }

  toggleNotif(string){
    this.notifState = string;
  }

}
