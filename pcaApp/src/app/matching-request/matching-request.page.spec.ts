import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchingRequestPage } from './matching-request.page';

describe('MatchingRequestPage', () => {
  let component: MatchingRequestPage;
  let fixture: ComponentFixture<MatchingRequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchingRequestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchingRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
