import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';

@Component({
  selector: 'app-matching-request',
  templateUrl: './matching-request.page.html',
  styleUrls: ['./matching-request.page.scss'],
})
export class MatchingRequestPage implements OnInit {

  isNotif: boolean = true;
  notifState: any = 'listing-grid';
  searchKeyword: any = '';
  listings: any = [];
  listingCount: any = 0;
  loaded: boolean = false;

  constructor(private location: Location,
    private geolocation: Geolocation,
    private _utilitiesService: UtilitiesService,
    private router: Router,
    private _zone: NgZone,
    private _propertyListingService: PropertyListingService) { }

  ngOnInit() {
    this._propertyListingService.searchListingParams['page_num'] = 1;
    this._propertyListingService.searchListingParams['page_size'] = 20;
    this.searchKeyword = this._propertyListingService.selectedKeywordSearch;
    this.geolocation.getCurrentPosition().then((resp) => {
      // if(this._propertyListingService.searchListingParams.query_coords == ''){
      //   // this._propertyListingService.searchListingParams.query_coords = resp.coords.latitude+','+resp.coords.longitude;
      // }
      
    });
    this._propertyListingService.getPCAListingMatch(()=>{
      this._propertyListingService.searchListings(() => {
        this._zone.run(() => {
          this.listingCount = this._propertyListingService.pcaPropertyListing.length+this._propertyListingService.propertyListingResult.length;
          this.listings = this._propertyListingService.propertyListingResult;
          this.loaded = true;
        })
      })
    })
  }

  toggleNotif(string){
    this._zone.run(()=>{
      this.notifState = string;
    })
  }

  map(){
    this._propertyListingService.propertyListingResult = this.listings;
    this.router.navigateByUrl('/map-listing');
  }

  whatsapp(phone){
    return 'https://api.whatsapp.com/send?phone='+this._utilitiesService.formatWhatsappNo(phone);
  }


  viewListingPca(item){
    this._propertyListingService.selectedPropertyListingDetail = item;
    this._propertyListingService.selectedPropertyListingDetail.project_name = item.property_name;
    this._propertyListingService.selectedPropertyListingDetail.address_line_1 = item.address;
    this._propertyListingService.selectedPropertyListingDetail.address_line_2 = '';
    this._propertyListingService.selectedPropertyListingDetail.area_size_formatted = item.area_size;
    this._propertyListingService.selectedPropertyListingDetail.location = {
      lat: item.lat,
      lng: item.lng
    }
    this._propertyListingService.selectedPropertyListingDetail.attributes = {
      price_formatted: item.price,
      bedrooms: item.bedrooms,
      bathrooms: item.number_of_bathroom,
      area_ppsf_formatted: item.floor_size,
      area_size_formatted: item.area_size
    };

    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/property-listing');
  }

  viewListing(item){
    this._propertyListingService.selectedPropertyListingDetail = item;
    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/property-listing');
  }

  viewMap(item) {
    this._propertyListingService.selectedPropertyListingDetail = item;
    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/map-view');
  }

  viewMapPCA(item) {
    this._propertyListingService.selectedPropertyListingDetail = item;
    this._propertyListingService.selectedPropertyListingDetail.project_name = item.property_name;
    this._propertyListingService.selectedPropertyListingDetail.address_line_1 = item.address;
    this._propertyListingService.selectedPropertyListingDetail.address_line_2 = '';
    this._propertyListingService.selectedPropertyListingDetail.area_size_formatted = item.area_size;
    this._propertyListingService.selectedPropertyListingDetail.location = {
      lat: item.lat,
      lng: item.lng
    }
    this._propertyListingService.selectedPropertyListingDetail.attributes = {
      price_formatted: item.price,
      bedrooms: item.bedrooms,
      bathrooms: item.number_of_bathroom,
      area_ppsf_formatted: item.floor_size,
      area_size_formatted: item.area_size
    };

    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/map-view');
  }

  ionViewWillEnter() {

    


  }

  goBack() {
    this.location.back();
  }

  search(ev) {
    console.log(ev);
    // if(this.searchKeyword !== ''){
    //   this._propertyListingService.searchListingParams.keyword = this.searchKeyword;
    //   this._propertyListingService.searchListings( ()=>{
    //     this.listings = this._propertyListingService.propertyListingResult;
    //   });
    // }
  }

  getSearchListingResult() {
    this._propertyListingService.propertyListingResult;
  }

}
