import { Component, OnInit, NgZone } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
import { CONFIGURATION } from 'src/services/config.service';


declare var navigator: any;
declare var Camera: any;
declare var plugins: any;

@Component({
  selector: 'app-forum-create-post',
  templateUrl: './forum-create-post.page.html',
  styleUrls: ['./forum-create-post.page.scss'],
})
export class ForumCreatePostPage implements OnInit {

  
  constructor(
    private _zone: NgZone,
    private _accountService: AccountService,
    private _menuService: MenuService,
    private _location: Location,
    private _utilitiesService: UtilitiesService) {
  }

  showImage: boolean = false;
  imageUrl: any = 'assets/img/placeholder.png';
  img_url: any = 'assets/img/person-placeholder.png';


  form = {
    title: '',
    content: '',
    user_type: this._accountService.userData.member_type,
    user_id: this._accountService.userData.id
  }

  ngOnInit(){

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatePostPage');
  }

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    if (this._accountService.userData.profile_picture !== null) {
      this.img_url = CONFIGURATION.base_url + this._accountService.userData.profile_picture;
    }
  }

  getUserData(){
    return this._accountService.userData;
  }

  getAccount() {
    return this._accountService.userData;
  }
  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.title)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.content)) {
      validate = false;
    }
    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
    }
    console.log(validate);
    return validate;
  }

  createPost() {
    if (this.validateForm()) {
      let formSubmission = {
        'Forumpost[post_title]': this.form.title,
        'Forumpost[post_description]': this.form.content
      };
      if (this._accountService.userData.member_type === 'agent') {
        formSubmission['Forumpost[agent_id]'] = this._accountService.userData.id;
      } else {
        formSubmission['Forumpost[account_id]'] = this._accountService.userData.id;
      }
      this._menuService.submitPost(formSubmission, id => {
        console.log(id);
        if (this.imageUrl !== 'assets/img/placeholder.png') {
          this._menuService.uploadPostImage(id, this.imageUrl, () => {
            this._utilitiesService.alertMessageCallback('Success', 'Post submitted successfully', () => {
              this._location.back();
            });
          });
        }else{
          this._utilitiesService.alertMessageCallback('Success', 'Post submitted successfully', () => {
            this._location.back();
          });
        }

      });
    }
  }

  goBack(){
    this._location.back();
  }


  toBase64(url: string) {
    return new Promise<string>(function (resolve) {
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = function () {
        var reader:any = new FileReader();
        reader.onloadend = function () {
          resolve(reader.result);
        }
        reader.readAsDataURL(xhr.response);
      };
      xhr.open('GET', url);
      xhr.send();
    });
  }

  takePhotoCamera() {
    navigator.camera.getPicture(imageData => {
      this._zone.run(() => {
        plugins.crop.promise(imageData, {
          quality: 75
        }).then(newPath => {
          return this.toBase64(newPath).then((base64Img) => {
            this._zone.run(() => {
              console.log(base64Img);
              this.showImage = true;
              this.imageUrl = base64Img;
              // this._studentService.uploadStudentImage(this.imageUrl, () => { });
            });

          });
        },
          error => {
            console.log('CROP ERROR -> ' + JSON.stringify(error));
            // alert('CROP ERROR: ' + JSON.stringify(error));
          }
        );

      });

    }, error => {
      console.log(error);
    },
      {
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA
      });
  }

  takePhotoGallery() {
    navigator.camera.getPicture(imageData => {
      this._zone.run(() => {
        plugins.crop.promise(imageData, {
          quality: 75
        }).then(newPath => {
          return this.toBase64(newPath).then((base64Img) => {
            this._zone.run(() => {
              console.log(base64Img);
              this.showImage = true;
              this.imageUrl = base64Img;
              // this._studentService.uploadStudentImage(this.imageUrl, () => { });
            });

          });
        },
          error => {
            console.log('CROP ERROR -> ' + JSON.stringify(error));
            // alert('CROP ERROR: ' + JSON.stringify(error));
          }
        );

      });

    }, error => {
      console.log(error);
    },
      {
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
      });

  }

}
