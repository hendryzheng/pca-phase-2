import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ForumCreatePostPage } from './forum-create-post.page';

const routes: Routes = [
  {
    path: '',
    component: ForumCreatePostPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ForumCreatePostPage]
})
export class ForumCreatePostPageModule {}
