import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { CONFIGURATION } from 'src/services/config.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Location } from '@angular/common';

@Component({
  selector: 'app-home-menu',
  templateUrl: './home-menu.page.html',
  styleUrls: ['./home-menu.page.scss'],
})
export class HomeMenuPage implements OnInit {

  menuList: any = [];
  constructor(private _menuService: MenuService,
              private _accountService: AccountService,
              private router: Router,
              private location: Location,
              private iab: InAppBrowser,
              private _utilitiesService: UtilitiesService) { }


  ionViewWillEnter(){
    this.menuList = this._menuService.menu_list;
  }

  ngOnInit() {
  }

  getUserType() {
    if (this._accountService.isUserSessionAlive()) {
      return this._accountService.userData.member_type;
    } else {
      return '';
    }
  }

  goBack(){
    this.location.back();
  }

  alertPermission() {
    this._utilitiesService.alertMessage('Permission Denied', 'Your account is not authorized to access this menu');
  }

  view(item) {
    this._menuService.selectedMenu = item;

    var allowed = false;
    if (item.access_status !== '1') {
      if (item.access_status === '2') {
        if (this.getUserType() === 'agent') {
          allowed = true;
        } else {
          allowed = false;
          // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
        }
      } else if (item.access_status === '3') {
        if (this.getUserType() === 'account') {
          allowed = true;
          // this.router.navigateByUrl('/categories')
        } else {
          allowed = false;
          // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
        }
      } else {
        if (this.getUserType() === 'pca_member') {
          allowed = true;
          // this.router.navigateByUrl('/categories')
        } else {
          allowed = false;
          // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
        }
      }
    } else {
      allowed = true;
    }
    if (item.is_pca_affiliaties === '1') {

      
      if (allowed) {
        
        this.router.navigateByUrl('/pca-categories');

      } else {
        this.alertPermission();
      }
    } else if (item.is_connect_to_project === '1') {
      
      if (allowed) {
        var base_url = CONFIGURATION.base_url;
        // this.iab.create(base_url + '/connect_to_project','_blank');
        // this._utilitiesService.openBrowser('Connect to Portals', base_url + '/connect_to_project');
        this.router.navigateByUrl('/connect-to-project');
      } else {
        this.alertPermission();
      }
    } else if (item.is_pca_agent_tools === '1') {
      
      if (allowed) {
        // this.nav.push(PcaAgentToolsPage);
        this.router.navigateByUrl('/pca-agent-tools');
      } else {
        this.alertPermission();
      }
      // this._utilitiesService.openBrowser('Connect to Portals', CONFIGURATION.apiEndpoint + 'connect_to_project');
    } else {
      if (allowed) {
        if(item.menu_name.indexOf('Senior Leaders') !== -1){
          this._menuService.listing = item.listing;
          this.router.navigateByUrl('/pca-senior-leaders');
        }else{
          this.router.navigateByUrl('/categories');
        }
      } else {
        this.alertPermission();
      }
    }
  }

  

}
