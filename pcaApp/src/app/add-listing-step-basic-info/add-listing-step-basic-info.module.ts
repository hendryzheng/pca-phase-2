import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddListingStepBasicInfoPage } from './add-listing-step-basic-info.page';

const routes: Routes = [
  {
    path: '',
    component: AddListingStepBasicInfoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddListingStepBasicInfoPage]
})
export class AddListingStepBasicInfoPageModule {}
