import { Component, OnInit } from '@angular/core';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Location } from '@angular/common';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-listing-step-basic-info',
  templateUrl: './add-listing-step-basic-info.page.html',
  styleUrls: ['./add-listing-step-basic-info.page.scss'],
})
export class AddListingStepBasicInfoPage implements OnInit {

  constructor(private _propertyListingService: PropertyListingService,
              private _utilitiesService: UtilitiesService,
              private router: Router,
              private location: Location) { }

  ngOnInit() {

  }

  getStep1PropertyDetail(){
    return this._propertyListingService.step1PropertyDetail;
  }

  getStep1SelectedLocation(){
    return this._propertyListingService.step1SelectedLocation;
  }

  goBack(){
    this.location.back();
  }

  getShortFormMap(){
    return this._propertyListingService.shortFormMap;
  }

  getForm(){
    return this._propertyListingService.form;
  }

  continue(){
    if(this.validateForm()){
      this.router.navigateByUrl('/add-listing-step-details');
    }else{
      this._utilitiesService.alertMessage('Incomplete information','Please fill up all information to proceed');
    }
  }

  validateForm(){
    var validate = true;
    if(this.getForm().listing_type == ''){
      validate = false;
    }
    if(this.getForm().floor_size == ''){
      validate = false;
    }
    if(this.getForm().floor_size_type == ''){
      validate = false;
    }
    if(this.getForm().price == ''){
      validate = false;
    }
    if(this.getForm().agents_to_market == ''){
      validate = false;
    }
    return validate;
  }

}
