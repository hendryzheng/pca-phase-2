import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PropertyListingService } from 'src/services/property-listing.service';
import { AccountService } from 'src/services/account.service';
import { Location } from '@angular/common';
import { UtilitiesService } from 'src/services/utilities.service';

@Component({
  selector: 'app-matching-customer',
  templateUrl: './matching-customer.page.html',
  styleUrls: ['./matching-customer.page.scss'],
})
export class MatchingCustomerPage implements OnInit {

  myListing: any = [];
  publicPropertyLoaded: boolean = false;
  search: any = '';

  constructor(private location: Location,
              private router: Router,
              private _utilitiesService: UtilitiesService,
              private _accountService: AccountService,
              private _propertyListingService: PropertyListingService) { }


  ngOnInit() {

  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    let params = {
      pca_member_id: this._accountService.userData.id
    }
    this._propertyListingService.getMatchingConnectCustomer(params, ()=>{
      this.publicPropertyLoaded = true;
      if(this._propertyListingService.matchingConnectCustomer.length > 0){
        for(var i=0;i<this._propertyListingService.matchingConnectCustomer.length;i++){
          let tempListing = JSON.parse(this._propertyListingService.matchingConnectCustomer[i].matching_connect_details);
          tempListing.customer_name = this._propertyListingService.matchingConnectCustomer[i].customer_name;
          tempListing.customer_mobile = this._propertyListingService.matchingConnectCustomer[i].customer_mobile;
          tempListing.created_date = this._propertyListingService.matchingConnectCustomer[i].created_date;
          this.myListing.push(tempListing);
        }
        console.log(this.myListing);
      }
    });
  }

  goBack(){
    this.location.back();
  }

  whatsapp(phone){
    return 'https://api.whatsapp.com/send?phone='+this._utilitiesService.formatWhatsappNo(phone);
  }



}
