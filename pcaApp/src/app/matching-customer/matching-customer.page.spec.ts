import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchingCustomerPage } from './matching-customer.page';

describe('MatchingCustomerPage', () => {
  let component: MatchingCustomerPage;
  let fixture: ComponentFixture<MatchingCustomerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchingCustomerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchingCustomerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
