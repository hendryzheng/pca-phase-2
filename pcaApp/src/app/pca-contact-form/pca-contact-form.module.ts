import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PcaContactFormPage } from './pca-contact-form.page';

const routes: Routes = [
  {
    path: '',
    component: PcaContactFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PcaContactFormPage]
})
export class PcaContactFormPageModule {}
