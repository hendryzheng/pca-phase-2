import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pca-contact-form',
  templateUrl: './pca-contact-form.page.html',
  styleUrls: ['./pca-contact-form.page.scss'],
})
export class PcaContactFormPage implements OnInit {

  form = {
    name: '',
    email: '',
    phone: '',
    agent_name: '',
    agent_email: '',
    agent_phone: ''
  }

  constructor(
    private _menuService: MenuService,
    private location: Location,
    private _utilitiesService: UtilitiesService) {
  }
  ngOnInit() {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PcaContactFormPage');
  }

  getMenu() {
    return this._menuService.selectedMenu.menu_name;
  }

  getSelectedPcaCategory() {
    return this._menuService.selected_pca_category;
  }

  getSelectedPcaListing() {
    return this._menuService.selected_pca_listing;
  }

  submit() {
    if (this.validateForm()) {
      let formSubmission = {
        'Form[name]': this.form.name,
        'Form[email]': this.form.email,
        'Form[phone]': this.form.phone,
        'Form[agent_name]': this.form.agent_name,
        'Form[agent_email]': this.form.agent_email,
        'Form[agent_phone]': this.form.agent_phone,
        'Form[pca_affiliates_listing_id]': this.getSelectedPcaListing().id
      };
      this._menuService.submitContactForm(formSubmission, () => {
        this._utilitiesService.alertMessageCallback('Success', 'Form submitted successfully', () => {
          this.form = {
            name: '',
            email: '',
            phone: '',
            agent_name: '',
            agent_email: '',
            agent_phone: ''
          }
        });
      });
    }
  }

  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.name)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.phone)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.agent_name)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.agent_email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.agent_phone)) {
      validate = false;
    }
    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
    }
    console.log(validate);
    return validate;
  }

  openWebsite() {
    this._utilitiesService.openBrowser(this.getSelectedPcaListing().title, this.getSelectedPcaListing().website);
  }

  goBack(){
    this.location.back();
  }

}
