import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcaContactFormPage } from './pca-contact-form.page';

describe('PcaContactFormPage', () => {
  let component: PcaContactFormPage;
  let fixture: ComponentFixture<PcaContactFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcaContactFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcaContactFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
