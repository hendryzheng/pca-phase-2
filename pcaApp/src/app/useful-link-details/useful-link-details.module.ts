import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UsefulLinkDetailsPage } from './useful-link-details.page';

const routes: Routes = [
  {
    path: '',
    component: UsefulLinkDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UsefulLinkDetailsPage]
})
export class UsefulLinkDetailsPageModule {}
