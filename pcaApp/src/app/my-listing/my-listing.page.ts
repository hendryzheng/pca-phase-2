import { Component, OnInit } from '@angular/core';
import { PropertyListingService } from 'src/services/property-listing.service';
import { AccountService } from 'src/services/account.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';

@Component({
  selector: 'app-my-listing',
  templateUrl: './my-listing.page.html',
  styleUrls: ['./my-listing.page.scss'],
})
export class MyListingPage implements OnInit {

  myListing: any = [];

  constructor(private _propertyListingService: PropertyListingService,
              private location: Location,
              private router: Router,
              private _utilitiesService: UtilitiesService,
              private _accountService: AccountService) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    this.fetchMyPropertyListing();
  }

  getUserData() {
    return this._accountService.userData;
  }

  fetchMyPropertyListing(){
    this.myListing = [];
    this._propertyListingService.getMyPropertyListing(()=>{
      this.myListing = this._propertyListingService.myPropertyListing;
    });
  }

  goBack(){
    this.location.back();
  }

  editListing(item) {
    this._propertyListingService.propertyDetailPhotos = item.listing_images;
    this._propertyListingService.form = {
      update: true,
      pca_member_id: this._accountService.userData.id,
      property_segment: item.property_segment,
      main_category: item.main_category,
      sub_category: item.sub_category,
      land_use: item.land_use,
      property_name: item.property_name,
      district: item.district,
      lat: item.lat,
      lng: item.lng,
      address: item.address,
      type: item.type,
      id: item.id,
      photo_url: item.photo_url,
      listing_type: item.listing_type,
      hdb_type: item.hdb_type,
      bedrooms: item.bedrooms,
      floor_size: item.floor_size,
      floor_size_type: item.floor_size_type,
      area_size: item.area_size,
      price: item.price,
      agents_to_market: item.agents_to_market,
      highlights: item.highlights,
      description: item.description,
      floor: item.floor,
      age_of_property: item.age_of_property,
      last_renovation_done: item.last_renovation_done,
      ceiling_height: item.
      _height,
      unit_no: item.unit_no,
      number_of_bathroom: item.number_of_bathroom,
      date_of_availbility: item.date_of_availbility,
      facing: item.facing,
      tenancy_lease_expiration: item.tenancy_lease_expiration,
      tenancy_current_rent: item.tenancy_current_rent,
      tenure: item.tenure,
      features: item.features,
      other_restrictions: item.other_restrictions,
      youtube_video: item.youtube_video,
      iframe_v360: item.iframe_v360
    }
    this.router.navigateByUrl('/add-listing-step-basic-info');
  }

  async deleteProperty(item) {
    let alert = await this._utilitiesService.getAlertCtrl().create({
      header: 'Are you sure you want to delete this property?',
      // message: message,
      buttons: [
        {
          text: 'Yes',
          handler: data => {
            this.deleteListing(item);
            console.log('OK clicked');
          }
        },
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  deleteListing(item) {
    let params = {
      listing_id: item.id
    }
    this._propertyListingService.deleteListing(params, () => {
      this.fetchMyPropertyListing();
    });
  }

}
