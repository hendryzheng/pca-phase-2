import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profile-listing',
  templateUrl: './profile-listing.page.html',
  styleUrls: ['./profile-listing.page.scss'],
})
export class ProfileListingPage implements OnInit {

  isNotif: boolean = true;
  notifState: any = 'for-sale';

  constructor(private location: Location) { }

  ngOnInit() {
  }

  toggleNotif(string){
    this.notifState = string;
  }

  goBack(){
    this.location.back();
  }

}
