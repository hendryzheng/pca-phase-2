import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyMarketingConsentPage } from './property-marketing-consent.page';

describe('PropertyMarketingConsentPage', () => {
  let component: PropertyMarketingConsentPage;
  let fixture: ComponentFixture<PropertyMarketingConsentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyMarketingConsentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyMarketingConsentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
