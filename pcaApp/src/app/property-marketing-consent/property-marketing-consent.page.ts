import { Component, OnInit, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { MenuService } from 'src/services/menu.service';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-property-marketing-consent',
  templateUrl: './property-marketing-consent.page.html',
  styleUrls: ['./property-marketing-consent.page.scss'],
})
export class PropertyMarketingConsentPage implements OnInit {

  @ViewChild(SignaturePad) public signaturePad : SignaturePad;

  public signaturePadOptions : Object = {
    'minWidth': 2,
    'canvasWidth': 340,
    'canvasHeight': 200
  };
  public signatureImage : string = null;
  signed: boolean = false;
  email: any = '';
  agent_email: any = '';

  constructor(private _menuService: MenuService,
              private _utilitiesService: UtilitiesService,
              private location: Location,
              private router: Router,
              private _accountService: AccountService) { }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    this.agent_email = this._accountService.userData.email;
    this.email = this._menuService.customer.email;
  }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

  drawCancel() {
    // this.navCtrl.push(HomePage);
  }

   drawComplete() {
    this.signatureImage = this.signaturePad.toDataURL();
    this.signed = true;
    // this.navCtrl.push(HomePage, {signatureImage: this.signatureImage});
  }

  drawClear() {
    this.signed = false;
    this.signatureImage = null;
    this.signaturePad.clear();
  }

  async cancel(){
    const alertController =this._utilitiesService.getAlertCtrl();
        const alert = await alertController.create({
                header: 'Cancellation',
                message: 'Are you sure you want to cancel? By proceeding to cancel, your customer has not agreed to the PDPA Consent and Property Marketing Consent. You shall be redirected to home dashbaord page.',
                buttons: [
                    {
                        text: 'OK',
                        handler: data => {
                          this.router.navigate(['/tabs'], {replaceUrl: true})
                        }
                    },
                    {
                      text: 'Cancel',
                      handler: data => {
                      }
                  }
                    
                ]
            });
        return alert.present();
  }

  submit(){
    this.signatureImage = this.signaturePad.toDataURL();
    console.log(this.signatureImage);
    if(this.signatureImage !== null && this.signatureImage.length !== 2266){
      let customer_id = this._menuService.customer.id;
      this._menuService.uploadImageSignature(customer_id, this.signatureImage, res=>{
        this._utilitiesService.alertMessageCallback('Customer added successfully','You have successfully added this customer',()=>{
          this._menuService.eNamecardState = 'customer';
          this.router.navigate(['/tabs'], {replaceUrl: true});
        });
      });
    }else{
      this._utilitiesService.alertMessage('Signature Empty','Please let customer sign on the signature pad section');
    }
    
  }


}
