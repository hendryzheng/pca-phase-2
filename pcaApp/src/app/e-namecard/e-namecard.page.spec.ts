import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ENamecardPage } from './e-namecard.page';

describe('ENamecardPage', () => {
  let component: ENamecardPage;
  let fixture: ComponentFixture<ENamecardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ENamecardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ENamecardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
