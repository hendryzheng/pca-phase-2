import { Component, OnInit, NgZone } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { MenuService } from 'src/services/menu.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { PropertyListingService } from 'src/services/property-listing.service';
import { UtilitiesService } from 'src/services/utilities.service';

declare var cordova: any;
declare var navigator: any;

@Component({
  selector: 'app-e-namecard',
  templateUrl: './e-namecard.page.html',
  styleUrls: ['./e-namecard.page.scss'],
})
export class ENamecardPage implements OnInit {

  isNotif: boolean = true;
  notifState: any = 'properties';
  customerState: any = 'PDPA';
  loaded: boolean= false;
  img_url: any = 'assets/img/person-placeholder.png';
  customerList: any = [];
  myListing: any = [];
  showContact: boolean = false;
  contact: any = [];
  param_id = '';
  userData: any = [];
  hideCustomer: boolean = false;
  publicPropertyLoaded: boolean = false;


  constructor(private _accountService: AccountService,
              private location: Location,
              private router: Router,
              private _utilitiesService: UtilitiesService,
              private _DomSanitizationService: DomSanitizer,
              private _zone: NgZone,
              private _menuService: MenuService,
              private _propertyListingService: PropertyListingService,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this._accountService.checkUserSession();
    this.param_id = this.route.snapshot.paramMap.get('id');
    console.log(this.param_id);
    if(this.param_id !== '' && typeof this.param_id !== 'undefined' && this.param_id !== null){
      let params = {
        user_id: this.param_id,
        user_type: 'pca_member'
      }
      this._accountService.getUserDetailParam(params, ()=>{
        this.loaded = true;
        this.userData = this._accountService.viewUserData;
        this._accountService.viewUserDataState = true;
          if (this.userData.profile_picture !== null) {
            this.img_url = CONFIGURATION.base_url + this.userData.profile_picture;
          }
        this.hideCustomer = true;
        this.fetchMyPropertyListing();
      });
    }else{
      if (this._accountService.isUserSessionAlive()) {
        this.loaded = false;
        this._accountService.viewUserDataState = false;
        this._accountService.getUserDetail(() => {
          
          this.userData = this._accountService.userData;
          if (this.userData.profile_picture !== null) {
            this.img_url = CONFIGURATION.base_url + this.userData.profile_picture;
          }
          this.loaded = true;
        });
      }
      if(this._menuService.eNamecardState !== ''){
        this.notifState = this._menuService.eNamecardState;
        this.toggleNotif(this.notifState);
        this._menuService.eNamecardState = '';
        this.fetchMyPropertyListing();
  
      }else{
        this.fetchMyPropertyListing();
      }
    }
    
    
  }

  addCustomer(type){
    this._menuService.customer_type = type;
    this.router.navigateByUrl('/add-customer');
  }

  toggleNotif(string){
    this.notifState = string;
    if(this.notifState == 'video-marketing'){
      this.fetchVideoMarketing();
    }else if(this.notifState == 'customer'){
      this.fetchCustomerData();
    }else if(this.notifState == 'my-listing'){
      this.fetchMyPropertyListing();
    }
  }

  toggleCustomer(string){
    this.customerState = string;
    
  }

  getUserData() {
    return this.userData;
  }

  whatsapp(phone){
    return 'https://api.whatsapp.com/send?phone='+this._utilitiesService.formatWhatsappNo(phone);
  }


  openMarketingVideo(){
      var url = this.userData.marketing_video;
      this._utilitiesService.openBrowser('Marketing Video',url);
      // cordova.InAppBrowser.open(url, '_blank', 'location=yes');
  }

  share() {
    var url = 'https://pca.app.link/M70ilIrxU0?e-namecard='+this.userData.id;
    
    navigator.share({
      'title': 'Hello, kindly visit my Property Connect Alliance E-Namecard at the following link.If you have the PCA app open, please close it, then click the following link to view my E-Namecard.',
      'text': 'Hello, kindly visit my Property Connect Alliance E-Namecard at the following link.If you have the PCA app open, please close it, then click the following link to view my E-Namecard.',
      'url': url
    }).then(() => {
      console.log('Successful share');
    }).catch(error => {
      console.log('Error sharing:', error)
    });
  }

  openInstagram(){
    if(this.userData.instagram !== ''){
      var url = 'https://instagram.com/'+this.userData.instagram;
      cordova.InAppBrowser.open(url, '_system', 'location=yes');
    }else{
      this._utilitiesService.alertMessage('Oops','You have not added instagram yet');
    }
  }

  openFacebook(){
    if(this.userData.facebook !== ''){
      var url = 'https://facebook.com/'+this.userData.facebook;
      cordova.InAppBrowser.open(url, '_system', 'location=yes');
    }else{
      this._utilitiesService.alertMessage('Oops','You have not added facebook yet');
    }
  }

  openMail(){
    var url =  'mailto:'+this.userData.email;
    cordova.InAppBrowser.open(url, '_system', 'location=yes');
  }

  openCall(){
    var url =  'tel:'+this._utilitiesService.formatWhatsappNo(this.userData.mobile);
    cordova.InAppBrowser.open(url, '_system', 'location=yes');

  }

  openSms(){
    var url =  'sms:'+this._utilitiesService.formatWhatsappNo(this.userData.mobile);
    cordova.InAppBrowser.open(url, '_system', 'location=yes');

  }
  openWhatsapp(){
    var url =  'https://api.whatsapp.com/send?phone='+this._utilitiesService.formatWhatsappNo(this.userData.mobile);
    cordova.InAppBrowser.open(url, '_system', 'location=yes');
  }

  fetchCustomerData(){
    this.customerList = [];
    // this._menuService.getCustomerData(()=>{
    //   this._zone.run(()=>{
    //     this.customerList = this._menuService.customerList;
    //     console.log(this.customerList);
    //   });
      
    // });
  }

  fetchMyPropertyListing(){
    this.myListing = [];
    // this._propertyListingService.getPublicPropertyListing(()=>{
    //   this.myListing = this._propertyListingService.publicPropertyListing;
    //   this.publicPropertyLoaded = true;
    // });
  }

  contactAgent(item){
    this.contact = item;
    this.showContact = true;
  }

  viewListing(item) {
    this._propertyListingService.selectedPropertyListingDetail = item;
    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/property-listing/'+this.userData.id);
  }

  openPropertyPage(){
    this._propertyListingService.selectedViewUser = this.userData;
    this.router.navigateByUrl('/account-property-type');
  }

  async deleteCustomerConfirmation(item) {
    let alert = await this._utilitiesService.getAlertCtrl().create({
      header: 'Are you sure you want to delete this customer?',
      // message: message,
      buttons: [
        {
          text: 'Yes',
          handler: data => {
            this.deleteCustomer(item);
            console.log('OK clicked');
          }
        },
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  editCustomer(item){
    this._menuService.customer_type = item.type;
    this._menuService.customer = item;
    this.router.navigateByUrl('/edit-customer');
  }

  deleteCustomer(item){
    
    let params = {
      id: item.id
    }
    this._menuService.deleteCustomer(params, ()=>{
      this.fetchCustomerData();
    })
  }

  viewCustomer(item){
    
  }

  getCustomerData(){
    return this.customerList;
  }

  fetchVideoMarketing(){
    this._menuService.getVideoMarketing(()=>{

    });
  }

  goBack(){
    this.router.navigateByUrl('/tabs',{replaceUrl: true});
  }

  getVideoMarketing(){
    return this._menuService.videoMarketing;
  }

  getContact(){
    return this.contact;
  }

  form = {
    name: '',
    phone: '',
    email: '',
    gender:'',
    address: '',
    purpose_of_contact: '',
    listing_type: '',
    property_type: '',
    property_address: '',
    asking_price: '',
    built_in_area: '',
    type: '',
    pdpa_signed: false
  }

  purpose_of_contact = [
    {
      val: 'Follow up on your property availability',
      isChecked: false
    },
    {
      val: 'Potential Buyer / Tenant Enquiries',
      isChecked: false
    },
    {
      val: 'Property Market Update / Report',
      isChecked: false
    },
    {
      val: 'New Launches in Singapore',
      isChecked: false
    },
    {
      val: 'New Launches in Overseas',
      isChecked: false
    }
  ]
  

  submit(){

  }


  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.name)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.phone)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.email)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.gender)) {
      validate = false;
    }
    if (this._utilitiesService.isEmpty(this.form.address)) {
      validate = false;
    }
    if (this.form.pdpa_signed == false) {
      validate = false;
    }

    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details and ensure your customer has agreed to the PDPA consent');
    }
    console.log(validate);
    return validate;
  }

  getCustomerType(){
    return this._menuService.customer_type;
  }

  submitWithPdpa(){
    if (this.validateForm()) {
      let formSubmission = {
        'Customer[fullname]': this.form.name,
        'Customer[email]': this.form.email,
        'Customer[mobile]': this.form.phone,
        'Customer[gender]': this.form.gender,
        'Customer[address]': this.form.address,
        'Customer[pca_member_id]': this._accountService.userData.id,
        'user_type' : this.userData.member_type,
        'user_id': this.userData.id
      };
      this._menuService.submitAddNewCustomer(formSubmission, () => {
        this.router.navigateByUrl('/property-marketing-consent');
      });
    }
  }

  submitWithoutPdpa(){
    if (this.validateForm()) {
      let formSubmission = {
        'Customer[fullname]': this.form.name,
        'Customer[status]' : 1,
        'Customer[email]': this.form.email,
        'Customer[mobile]': this.form.phone,
        'Customer[gender]': this.form.gender,
        'Customer[address]': this.form.address,
        'Customer[pca_member_id]': this._accountService.userData.id,
        'user_type' : this.userData.member_type,
        'user_id': this.userData.id
      };
      this._menuService.submitAddNewCustomer(formSubmission, () => {
        this.location.back();
      });
    }
  }
}
