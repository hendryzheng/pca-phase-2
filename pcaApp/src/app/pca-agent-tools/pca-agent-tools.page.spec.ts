import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcaAgentToolsPage } from './pca-agent-tools.page';

describe('PcaAgentToolsPage', () => {
  let component: PcaAgentToolsPage;
  let fixture: ComponentFixture<PcaAgentToolsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcaAgentToolsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcaAgentToolsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
