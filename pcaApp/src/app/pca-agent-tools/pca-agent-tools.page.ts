import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pca-agent-tools',
  templateUrl: './pca-agent-tools.page.html',
  styleUrls: ['./pca-agent-tools.page.scss'],
})
export class PcaAgentToolsPage implements OnInit {

  constructor(private _utilitiesService: UtilitiesService,
    private router: Router,
    private location: Location) {
  }

  ngOnInit() {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PcaAgentToolsPage');
  }

  calculators() {
    this.router.navigateByUrl('/calculator');
  }

  // research() {
  //   this.navCtrl.push(ResearchPage);
  // }

  goBack() {
    this.location.back();
  }

  valuation() {
    this._utilitiesService.openBrowser('Valuation', 'https://forms.uob.com.sg/property/agent/valuation/');
  }

}
