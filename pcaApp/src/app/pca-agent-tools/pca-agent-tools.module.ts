import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PcaAgentToolsPage } from './pca-agent-tools.page';

const routes: Routes = [
  {
    path: '',
    component: PcaAgentToolsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PcaAgentToolsPage]
})
export class PcaAgentToolsPageModule {}
