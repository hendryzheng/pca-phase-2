import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Location } from '@angular/common';
import { CONFIGURATION } from 'src/services/config.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { IonSearchbar } from '@ionic/angular';

declare var window: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {


  showSearch: boolean = false;
  listing_result: any = [];
  myInput: any = '';
  @ViewChild('search') searchbar: IonSearchbar;



  constructor(private _menuService: MenuService,
              private router: Router,
              private _utilitiesService: UtilitiesService,
              private location: Location) { }

  ngOnInit() {
  }
  
  ngAfterViewInit() {
    // setTimeout(() => this.inputEl.nativeElement.focus(),1000);
    this.searchbar.setFocus();
  }


  onInput(ev) {
    console.log(ev);
    if (ev.type == 'ionInput') {
      let params = {
        keyword: ev.target.value
      };
      if (ev.target.value == '') {
        this.showSearch = false;
      } else {
        this._menuService.searchListing(params, () => {
          this.showSearch = true;
          this.listing_result = this._menuService.listing_result;
        });
      }
    }
  }

  getIconPath(item) {
    if (item.icon_path !== null) {
      return item.icon_path;
    } else {
      return this._menuService.selectedMenu.icon_path;
    }
  }

  openAttachment(item) {
    var base_url = CONFIGURATION.apiEndpoint;
    if (item.listing_type == '2') {
      console.log(item.listing_attachment);
      this._utilitiesService.openBrowser(item.listing_name, item.listing_attachment);
      // const browser = this.iab.create(item.listing_attachment, '_blank', 'location=no');
    } else if (item.listing_type == '3') {
      console.log(item);
      window.InAppYouTube.openVideo(item.youtube_id, {
        fullscreen: true
      }, (result) => {
        console.log(JSON.stringify(result));
      }, (reason) => {
        console.log(reason);
      });
      // this.youtube.openVideo(item.youtube_id);

    } else {
      this._menuService.selected_listing = item;
      this._menuService.selected_listing.image_path = base_url + item.listing_attachment;
      this.router.navigateByUrl('/preview-image');
      // this._utilitiesService.openBrowser(item.listing_name, base_url + item.listing_attachment);
      // this.iab.create(base_url+item.listing_attachment, '_blank', 'location=no');
    }
  }

  goBack(){
    this.location.back();
  }

  checkBlur() {
    if (this.myInput == '') {
      this.showSearch = false;
      this.listing_result = [];
    }
  }

  onCancel(ev) {
    console.log(ev);
    this.showSearch = false;
    this.listing_result = [];
  }

}
