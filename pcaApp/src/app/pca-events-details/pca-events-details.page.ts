import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pca-events-details',
  templateUrl: './pca-events-details.page.html',
  styleUrls: ['./pca-events-details.page.scss'],
})
export class PcaEventsDetailsPage implements OnInit {

  constructor(private location : Location) { }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

}
