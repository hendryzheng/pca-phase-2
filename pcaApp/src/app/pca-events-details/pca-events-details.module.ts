import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PcaEventsDetailsPage } from './pca-events-details.page';

const routes: Routes = [
  {
    path: '',
    component: PcaEventsDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PcaEventsDetailsPage]
})
export class PcaEventsDetailsPageModule {}
