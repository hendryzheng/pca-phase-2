import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeMoreMenuPage } from './home-more-menu.page';

const routes: Routes = [
  {
    path: '',
    component: HomeMoreMenuPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeMoreMenuPage]
})
export class HomeMoreMenuPageModule {}
