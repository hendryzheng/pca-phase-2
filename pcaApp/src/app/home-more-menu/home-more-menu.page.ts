import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-home-more-menu',
  templateUrl: './home-more-menu.page.html',
  styleUrls: ['./home-more-menu.page.scss'],
})
export class HomeMoreMenuPage implements OnInit {

  constructor(private _menuService: MenuService,
              private location :Location,
              private router: Router) { }

  ngOnInit() {
  }

  customerData(){
    this.router.navigateByUrl('/customer-data');

  }

  map(){
    this.router.navigateByUrl('/maps');
  }

  marketing(){
    this._menuService.eNamecardState = 'video-marketing'
    this.router.navigateByUrl('/e-namecard');

  }

  goBack(){
    this.location.back();
  }

}
