import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMoreMenuPage } from './home-more-menu.page';

describe('HomeMoreMenuPage', () => {
  let component: HomeMoreMenuPage;
  let fixture: ComponentFixture<HomeMoreMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMoreMenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMoreMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
