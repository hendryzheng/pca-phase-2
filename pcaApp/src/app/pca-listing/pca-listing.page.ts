import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pca-listing',
  templateUrl: './pca-listing.page.html',
  styleUrls: ['./pca-listing.page.scss'],
})
export class PcaListingPage implements OnInit {

  constructor(
    public _menuService: MenuService,
    private router: Router,
    private location: Location) {
  }

  pcaListing: any = [];

  ngOnInit(){

  }

  ionViewWillEnter() {
    this._menuService.getPcaListing(() => {
      this.pcaListing = this._menuService.pca_listing;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PcaCategoriesPage');
  }

  getPcaListing() {
    return this.pcaListing
  }

  getMenu() {
    return this._menuService.selected_pca_category.categories_name;
  }

  goBack(){
    this.location.back();
  }

  viewItem(item) {
    this._menuService.selected_pca_listing = item;
    this.router.navigateByUrl('/pca-contact-form');
    // this.navCtrl.push(PcaContactFormPage);
  }

}
