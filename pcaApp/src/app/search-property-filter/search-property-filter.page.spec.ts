import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPropertyFilterPage } from './search-property-filter.page';

describe('SearchPropertyFilterPage', () => {
  let component: SearchPropertyFilterPage;
  let fixture: ComponentFixture<SearchPropertyFilterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPropertyFilterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPropertyFilterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
