import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SearchPropertyFilterPage } from './search-property-filter.page';

const routes: Routes = [
  {
    path: '',
    component: SearchPropertyFilterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  entryComponents: [
       
    SearchPropertyFilterPage
    ],
  declarations: [SearchPropertyFilterPage]
})
export class SearchPropertyFilterModule {}
