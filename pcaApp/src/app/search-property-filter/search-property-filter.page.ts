import { Component } from '@angular/core';
import { PropertyListingService } from 'src/services/property-listing.service';
import { AlertController, ModalController } from '@ionic/angular';
import { UtilitiesService } from 'src/services/utilities.service';

@Component({
  selector: 'app-search-property-filter',
  templateUrl: './search-property-filter.page.html',
  styleUrls: ['./search-property-filter.page.scss'],
})
export class SearchPropertyFilterPage{

  constructor(private alertCtrl: AlertController,
    private _utilitiesService: UtilitiesService,
    private modalController: ModalController,
    private _propertyListingService: PropertyListingService) { }

  from: any = 100000;
  to: any = 20000000;

  fromFloor: any = 500;
  toFloor: any = 10000;
  categories: any = [];
  subcategories: any = [];
  hideSearch: boolean = true;
  searchKeywordListingResult: any = [];
  searchKeywordModel: any = '';
  height: any = '500';
  locationList: any = [];


  ionViewWillEnter() {
    var physicalScreenHeight = window.screen.height;
    this.height = physicalScreenHeight;
    let obj: any = this._propertyListingService.listing_categories[0].main_categories[0];
    this.subcategories = obj.sub_categories;
    this.searchKeywordListingResult = [];
      this._propertyListingService.selectedKeywordSearch = '';
      this._propertyListingService.searchListingParams.query_type = '';
      this._propertyListingService.searchListingParams.query_ids = '';
      this._propertyListingService.searchListingParams.query_coords = '';
      this.segmentChosen(this._propertyListingService.listing_categories[0]);
    this._propertyListingService.searchListingParams.property_segments = 'residential';
  }
  change(ev) {
    console.log(ev);
    this.from = ev.detail.value.lower;
    this._propertyListingService.searchListingParams.price_min = this.from;
    this.to = ev.detail.value.upper;
    this._propertyListingService.searchListingParams.price_max = this.to;
  }

  changeFloorArea(ev) {
    console.log(ev);
    this.fromFloor = ev.detail.value.lower;
    this._propertyListingService.searchListingParams.floorarea_min = this.fromFloor;
    this.toFloor = ev.detail.value.upper;
    this._propertyListingService.searchListingParams.floorarea_max = this.toFloor;
  }

  close(){
    // if (this.validateSearchParam()) {
      this.modalController.dismiss();
    // }
  }

  getSearchListingParam() {
    return this._propertyListingService.searchListingParams;
  }

  mainCatChosen(item) {
    this.subcategories = [];
    this.subcategories = item.sub_categories;
    console.log(item);
  }

  segmentChosen(item){
    this.categories = [];
    this.subcategories = [];
    this.categories = item.main_categories;
  }


  search() {
    console.log(this.getSearchListingParam());
    let params = JSON.parse(JSON.stringify(this._propertyListingService.searchListingParams));
    if (this.validateSearchParam()) {
      this.modalController.dismiss();
    }
  }

  searchKeyword(ev) {
    if(ev.target.value !== ''){
      this.hideSearch = false;
    }
    var query = ev.target.value;
    if(this.getSearchListingParam().main_category !== ''){
      query += '&main_category='+this.getSearchListingParam().main_category;
    }
    this._propertyListingService.getLocationStep1(query, this.getSearchListingParam().property_segments, ()=>{
      this.locationList = this._propertyListingService.step1Location;
    });
  }

  selectKeywordResult(item) {
    console.log(item);
    this._propertyListingService.searchListingParams.keywords = this._propertyListingService.selectedKeywordSearch;
    this._propertyListingService.selectedKeywordSearch = item.name;
    this._propertyListingService.selectedKeywordSearchSubtitle = item.address;
    this._propertyListingService.searchListingParams.main_category = item.type;
    this._propertyListingService.searchListingParams.query_ids = item.id;
    this._propertyListingService.searchListingParamLat = item.coordinates.lat;
    this._propertyListingService.searchListingParamLng = item.coordinates.lng;
    this.hideSearch = true;

  }

  getShortFormMap(){
    return this._propertyListingService.shortFormMap;
  }

  validateSearchParam() {
    var subcategories = '';
    for (var i = 0; i < this.subcategories.length; i++) {
      if (this.subcategories[i].checked) {
        subcategories += this.subcategories[i].key + ',';
      }
    }
    if (subcategories !== '') {
      subcategories = subcategories.substring(0, subcategories.length - 1);
    }
    this._propertyListingService.searchListingParams.sub_categories = subcategories;
    if (this._propertyListingService.selectedKeywordSearch == '') {
      this._utilitiesService.alertMessage('Location is compulsory', 'Please select location to narrow down your search');
      return false;
    }
    // if (this._propertyListingService.searchListingParams.sub_categories == '') {
    //   this._utilitiesService.alertMessage('Subcategories is compulsory', 'Please select subcategories to narrow down your search');
    //   return false;
    // }
    return true;
  }

}
