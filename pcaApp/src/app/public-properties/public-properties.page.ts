import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Router } from '@angular/router';
import { AccountService } from 'src/services/account.service';

@Component({
  selector: 'app-public-properties',
  templateUrl: './public-properties.page.html',
  styleUrls: ['./public-properties.page.scss'],
})
export class PublicPropertiesPage implements OnInit {

  myListing: any = [];
  publicPropertyLoaded: boolean = false;
  search: any = '';
  height: any = 500;
  hideSearch: boolean = true;
  searchKeywordListingResult: any = [];
  locationList: any = [];
  

  constructor(private location: Location,
              private router: Router,
              private zone: NgZone,
              private _accountService: AccountService,
              private _propertyListingService: PropertyListingService) { }

  ngOnInit() {
    
  }

  


  searchKeyword(ev){
    if(ev.target.value == ''){
      this.hideSearch = true;
      this.searchKeywordListingResult = [];
      this._propertyListingService.selectedKeywordSearch = '';
      this._propertyListingService.searchListingParams.query_type = '';
      this._propertyListingService.searchListingParams.query_ids = '';
      this._propertyListingService.searchListingParams.query_coords = '';
    }else{
      this._propertyListingService.getInputRequestInfo(ev.target.value,()=>{
        this.searchKeywordListingResult = this._propertyListingService.searchInputRequestResult;
        this.hideSearch = false;
      })
    }
  }

  getShortFormMap(){
    return this._propertyListingService.shortFormMap;
  }


  selectKeywordResult(item){
    console.log(item);
    this.search = item.title;
    this._propertyListingService.selectedKeywordSearch = item.title;
    this._propertyListingService.selectedKeywordSearchSubtitle = item.subtitle;
    this._propertyListingService.searchListingParams.query_type = 'google';
    // this._propertyListingService.searchListingParams.query_ids = item.id;
    this._propertyListingService.searchListingParams.radius_max = 1000;
    // this._propertyListingService.searchListingParams.location_query = this.search;

    if(typeof item.main_categories !== 'undefined'){
      this._propertyListingService.searchListingParams.main_category = item.main_categories[0];

    }
    this._propertyListingService.searchListingParams.query_coords = item.coordinates.lat+','+item.coordinates.lng;
    this.hideSearch = true;
    this._propertyListingService.searchListingParams['page_num'] = 1;
    this._propertyListingService.searchListingParams['page_size'] = 20;
    this._propertyListingService.searchListings(() => {
      this.myListing = [];
      this.zone.run(() => {
        this.myListing = this._propertyListingService.propertyListingResult;
        this.publicPropertyLoaded = true;
      })
    })

    // console.log(item);
    // this._propertyListingService.selectedKeywordSearch = item.name;
    // this._propertyListingService.searchListingParams.location_name = item.name;
    // this._propertyListingService.searchListingParams.location_subtitle = item.address;
    // this._propertyListingService.selectedKeywordSearchSubtitle = item.address;
    // this._propertyListingService.searchListingParams.keywords = this.search;
    // // this._propertyListingService.searchListingParams.query_type = item.type;
    // // this._propertyListingService.searchListingParams.query_ids = item.id;
    // this._propertyListingService.searchListingParams.main_category = item.type;
    // if(typeof item.coordinates !== 'undefined' && typeof item.coordinates !== 'undefined'){
    //   this._propertyListingService.searchListingParams.query_coords = item.coordinates.lat+','+item.coordinates.lng;
    // }
    // this.hideSearch = true;
    // this._propertyListingService.searchListings(() => {
    //   this.myListing = [];
    //   this.zone.run(() => {
    //     this.myListing = this._propertyListingService.propertyListingResult;
    //     this.publicPropertyLoaded = true;
    //   })
    // })

  }


  ionViewWillEnter(){
    this._accountService.checkUserSession();
    this.publicPropertyLoaded = false;
    var physicalScreenHeight = window.screen.height;
    this.height = physicalScreenHeight;
    this._accountService.checkUserSession();
    if(typeof this._accountService.userData.main_category !== 'undefined'){
      this._propertyListingService.searchListingParams.main_category = this._accountService.userData.main_category;
      this._propertyListingService.searchListingParams.property_segments = this._accountService.userData.property_segments;
      this._propertyListingService.searchListingParams['page_num'] = 1;
      this._propertyListingService.searchListingParams['page_size'] = 20;
      this._propertyListingService.searchListings(() => {
        this.myListing = [];
        this.zone.run(() => {
          this.myListing = this._propertyListingService.propertyListingResult;
          this.publicPropertyLoaded = true;
        })
      })
    }else{
      this.fetchMyPropertyListing();
    }
  }

  goBack(){
    this.router.navigateByUrl('/e-namecard');
  }

  searchProperty(ev){
    console.log(ev);
    this.fetchMyPropertyListing();
  }

  viewListing(item) {
    this._propertyListingService.selectedPropertyListingDetail = item;
    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/property-listing/'+this._propertyListingService.selectedViewUser.id);
  }

  fetchMyPropertyListing(){
    let params = {
      keyword: this.search
    };
    this.publicPropertyLoaded = false;
    this.myListing = [];
    this._propertyListingService.getPublicPropertyListing(params, ()=>{
      this.zone.run(()=>{
        this.myListing = this._propertyListingService.publicPropertyListing;
        this.publicPropertyLoaded = true;
      });
      
    });
  }

}
