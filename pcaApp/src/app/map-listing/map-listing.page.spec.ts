import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapListingPage } from './map-listing.page';

describe('MapListingPage', () => {
  let component: MapListingPage;
  let fixture: ComponentFixture<MapListingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapListingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapListingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
