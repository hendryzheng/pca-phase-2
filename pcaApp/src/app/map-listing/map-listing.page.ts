import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

import leaflet from 'leaflet';
import { UtilitiesService } from 'src/services/utilities.service';
import { CONFIGURATION } from 'src/services/config.service';
import { PropertyListingService } from 'src/services/property-listing.service';
import { ModalController } from '@ionic/angular';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

declare var plugin: any;
declare var google: any;
declare var window: any;
declare var L: any;
declare var onemap: any;

@Component({
  selector: 'app-map-listing',
  templateUrl: './map-listing.page.html',
  styleUrls: ['./map-listing.page.scss'],
})
export class MapListingPage implements OnInit, AfterViewInit {

  height: any = 500;
  width: any = 500;
  basemap: any = null;

  isLocationTurnedOn: boolean = false;

  @ViewChild('map') mapContainer: ElementRef;
  map: any;


  constructor(private elementRef : ElementRef, private _propertyListingService: PropertyListingService, private location: Location, private modalController: ModalController,  private router: Router) { }

  ngOnInit() {
  }

  ngAfterViewInit(){
  
  }

  ionViewWillEnter() {
    var physicalScreenHeight = window.screen.height;
    this.height = physicalScreenHeight+150;
    let body: any = document.getElementsByTagName('body');
    var physicalScreenWidth = body[0].offsetWidth;
    this.width = physicalScreenWidth;
    this.loadmap();
  }

  ionViewDidLeave(){
    this._propertyListingService.searchListingParamLat = '';
    this._propertyListingService.searchListingParamLng = '';
  }

  goBack(){
    this.location.back();
  }

  close() {
    this.modalController.dismiss();
  }

  loadmap() {
    if(this._propertyListingService.searchListingParamLat == '' || this._propertyListingService.searchListingParamLng == ''){
      this._propertyListingService.searchListingParamLat = '1.3';
      this._propertyListingService.searchListingParamLng = '103.8';
    }
    var map = onemap.initializeMap('mapdiv2', "default", 14, this._propertyListingService.searchListingParamLat, this._propertyListingService.searchListingParamLng, 0.8);

    // //Add Layer at the back
    // var backLayer = onemap.addBackLayer(map, L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
    //   detectRetina: true,
    //   attribution: '© OpenStreetMap contributors',
    //   maxZoom: 18,
    //   minZoom: 0,
    //   opacity: 1
    // }));

    // //Add Layer at the front
    // var frontLayer = onemap.addFrontLayer(map, L.tileLayer("https://maps-{s}.onemap.sg/v3/PACDC/{z}/{x}/{y}.png", {
    //   detectRetina: true,
    //   attribution: '© OneMap',
    //   maxZoom: 18,
    //   minZoom: 0,
    //   opacity: 0.5
    // }));

    //Layer to be removed later
    var removeLayer = onemap.addFrontLayer(map, L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      detectRetina: true,
      attribution: '© OpenStreetMap contributors',
      maxZoom: 20,
      minZoom: 0,
      opacity: 1
    }));
    //Removed Layer from Map
    onemap.removeLayer(map, removeLayer);

    //Setup configuration for REST API Services (Your Access Token)
    //Our Documentation @  https://docs.onemap.sg
    onemap.config("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMzNywidXNlcl9pZCI6MzM3LCJlbWFpbCI6ImthaWthaWNvbmdAZ21haWwuY29tIiwiZm9yZXZlciI6ZmFsc2UsImlzcyI6Imh0dHA6XC9cL29tMi5kZmUub25lbWFwLnNnXC9hcGlcL3YyXC91c2VyXC9zZXNzaW9uIiwiaWF0IjoxNTExMjQ2NzQzLCJleHAiOjE1MTE2Nzg3NDMsIm5iZiI6MTUxMTI0Njc0MywianRpIjoiZDg2OWFmZTYzNDRiM2JhYmY2MWEyN2VhMWQwMjg3YjgifQ.LcHgH06vJS46wsRuGVdSldC47y05bECuto3VI1kgGQo");


    // var content = document.getElementById('map-pin').innerHTML;
    for (var i = 0; i < this._propertyListingService.propertyListingResult.length; i++) {
      var content_ = this.getContentHtml(this._propertyListingService.propertyListingResult[i]);
      var hidden = L.DomUtil.create()
      var content = L.DomUtil.create('div', 'content');
      content.innerHTML = content_;
      content.setAtt
      L.DomEvent.addListener(content, 'click', event=>{
          // do stuff
          console.log(event);
          var path = event.path || (event.composedPath && event.composedPath());

          console.log(path[9].attributes[0].nodeValue);
          var id = path[9].attributes[0].nodeValue;
          if(id !== ''){
            for(var i=0;i<this._propertyListingService.propertyListingResult.length;i++){
              if(this._propertyListingService.propertyListingResult[i].id == id){
                this._propertyListingService.selectedPropertyListingDetail = this._propertyListingService.propertyListingResult[i];
              }
            }
          }     
          this.router.navigateByUrl('/property-listing');
          // alert('a');
      });
      var m = new L.Marker([this._propertyListingService.propertyListingResult[i].location.lat, this._propertyListingService.propertyListingResult[i].location.lng], { bounceOnAdd: false }).addTo(map).on('click', e => {
        console.log(e.latlng);
        alert('marker click');
    }).on('popupopen', el=> {
        console.log(el.popup);
      }),
        p = new L.Popup()
          .setContent(content)
          .setLatLng([this._propertyListingService.propertyListingResult[i].location.lat, this._propertyListingService.propertyListingResult[i].location.lng]).addTo(map);
      m.bindPopup(p);
    };
    setInterval( () => {
      map.invalidateSize();
    }, 500);

  }

  openProperties(){
    alert('id');
  }

  getContentHtml(item) {
    var html = '<div data-id="'+item.id+'" class="map-pin-property"><div class="details"><ion-item><ion-thumbnail slot="start">';
    html += '<img src="' + item.photo_url + '">';
    html += '</ion-thumbnail><ion-label class="ion-text-wrap"><h3>' + item.project_name + '</h3>';
    // html += '<div class="place"><ion-icon name="md-pin"></ion-icon>';
    // html += '<span>' + item.address_line_1 + ' ' + item.address_line_2 + '</span></div>';
    html += '<div class="price"><ion-button color="primary" size="small">' + item.attributes.price_formatted + '</ion-button>';
    html += '</div></ion-label></ion-item></div></div>';
    return html;


  }


}
