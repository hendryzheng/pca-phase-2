import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import leaflet from 'leaflet';
import { UtilitiesService } from 'src/services/utilities.service';
import { CONFIGURATION } from 'src/services/config.service';

declare var plugin: any;
declare var google: any;
declare var window: any;
declare var L: any;
declare var onemap: any;

@Component({
  selector: 'app-maps',
  templateUrl: './maps.page.html',
  styleUrls: ['./maps.page.scss'],
})
export class MapsPage implements OnInit {

  constructor(
    private router: Router,
    private _utilitiesService: UtilitiesService,
    private location: Location,
    private geolocation: Geolocation) {
  }

  height: any = 500;
  width: any = 500;
  basemap: any = null;

  isLocationTurnedOn: boolean = false;

  @ViewChild('map') mapContainer: ElementRef;
  map: any;

  ngOnInit(){

  }

  ionViewWillEnter(){
    console.log('ionViewDidLoad MapsPage');
    var physicalScreenHeight = window.screen.height;
    this.height = physicalScreenHeight;
    let body: any = document.getElementsByTagName('body');
    var physicalScreenWidth = body[0].offsetWidth;
    this.width = physicalScreenWidth;
    this.geolocation.getCurrentPosition().then((resp) => {
      this.isLocationTurnedOn = true;
      console.log(resp);
      this.isLocationTurnedOn = true;
      console.log(resp);
      this.loadmap(resp.coords.latitude,resp.coords.longitude);
      // this._utilitiesService.openBrowser('Map',CONFIGURATION.base_url+'/onemap.php?lat='+resp.coords.latitude+'&lng='+resp.coords.longitude+'&width='+this.width+'&height='+this.height);
    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

 
  ionViewDidEnter() {
    this.geolocation.getCurrentPosition().then((resp) => {
      
    });
    
  }


  goBack(){
    this.location.back();
  }
 
  loadmap(lat, lng) {
    var map = onemap.initializeMap('mapdiv',"default",11,1.3,103.8,0.8);

					//Add Layer at the back
					var backLayer = onemap.addBackLayer(map,L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
						            			detectRetina: true,
												attribution: '© OpenStreetMap contributors',
												maxZoom: 18,
						            			minZoom: 0,
						            			opacity:1
											}));

					//Add Layer at the front
					var frontLayer = onemap.addFrontLayer(map,L.tileLayer("https://maps-{s}.onemap.sg/v3/PACDC/{z}/{x}/{y}.png", {
						            			detectRetina: true,
												attribution: '© OneMap',
												maxZoom: 18,
						            			minZoom: 0,
						            			opacity:0.5
											}));

					//Layer to be removed later
					var removeLayer = onemap.addFrontLayer(map,L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
						            			detectRetina: true,
												attribution: '© OpenStreetMap contributors',
												maxZoom: 18,
						            			minZoom: 0,
						            			opacity:1
											}));
					//Removed Layer from Map
					onemap.removeLayer(map,removeLayer);

					//Setup configuration for REST API Services (Your Access Token)
					//Our Documentation @  https://docs.onemap.sg
					onemap.config("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMzNywidXNlcl9pZCI6MzM3LCJlbWFpbCI6ImthaWthaWNvbmdAZ21haWwuY29tIiwiZm9yZXZlciI6ZmFsc2UsImlzcyI6Imh0dHA6XC9cL29tMi5kZmUub25lbWFwLnNnXC9hcGlcL3YyXC91c2VyXC9zZXNzaW9uIiwiaWF0IjoxNTExMjQ2NzQzLCJleHAiOjE1MTE2Nzg3NDMsIm5iZiI6MTUxMTI0Njc0MywianRpIjoiZDg2OWFmZTYzNDRiM2JhYmY2MWEyN2VhMWQwMjg3YjgifQ.LcHgH06vJS46wsRuGVdSldC47y05bECuto3VI1kgGQo");


    			
          var marker = new L.Marker([lat, lng], {bounceOnAdd: false});
          marker.addTo(map);             
          var popup = L.popup()
          .setLatLng([lat, lng]) 
          .setContent('You are here!');
          
          popup.openOn(map);    
  }
 

  getWidth() {
    return this.width;
  }

  getHeight() {
    return this.height;
  }

}
