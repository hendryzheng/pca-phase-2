import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { SearchPropertyFilterPage } from '../search-property-filter/search-property-filter.page';
import { ModalController } from '@ionic/angular';
import { SortListComponent } from '../sort-list/sort-list.component';

import leaflet from 'leaflet';
import { UtilitiesService } from 'src/services/utilities.service';
import { CONFIGURATION } from 'src/services/config.service';

declare var plugin: any;
declare var google: any;
declare var window: any;
declare var L: any;
@Component({
  selector: 'app-search-show-listings',
  templateUrl: './search-show-listings.page.html',
  styleUrls: ['./search-show-listings.page.scss'],
})
export class SearchShowListingsPage implements OnInit {

  searchKeyword: any = '';
  listings: any = [];

  height: any = 500;
  width: any = 500;
  basemap: any = null;

  isLocationTurnedOn: boolean = false;
  showContact: boolean = false;
  contact: any = [];

  @ViewChild('map') mapContainer: ElementRef;
  map: any;


  constructor(private location: Location,
    private geolocation: Geolocation,
    private _zone: NgZone,
    private _utilitiesService: UtilitiesService,
    private router: Router,
    private modalController: ModalController,
    private _propertyListingService: PropertyListingService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this._propertyListingService.searchListingParams['page_num'] = 1;
    this._propertyListingService.searchListingParams['page_size'] = 20;
    this.searchKeyword = this._propertyListingService.selectedKeywordSearch;
    this.geolocation.getCurrentPosition().then((resp) => {
      // if(this._propertyListingService.searchListingParams.query_coords == ''){
      //   // this._propertyListingService.searchListingParams.query_coords = resp.coords.latitude+','+resp.coords.longitude;
      // }
      // this._propertyListingService.searchListings(() => {
      //   this._zone.run(() => {
      //     this.listings = this._propertyListingService.propertyListingResult;
      //   })
      // })
    });
    this.listings = this._propertyListingService.propertyListingResult;
    if(this.searchKeyword == ''){
      this.presentModal();
    }


  }

  getContact(){
    return this.contact;
  }

  contactAgent(item){
    this.contact = item;
    this.showContact = true;
  }

  showMap(){
    this.presentModalMap();
  }


  goBack() {
    this.location.back();
  }

  viewListing(item) {
    this._propertyListingService.selectedPropertyListingDetail = item;
    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/property-listing');
  }

  whatsapp(phone){
    return 'https://api.whatsapp.com/send?phone='+this._utilitiesService.formatWhatsappNo(phone);
  }

  getSearchListingParam(){
    return this._propertyListingService.searchListingParams;
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SearchPropertyFilterPage
    });
    modal.onDidDismiss()
      .then((data) => {
        this.searchKeyword = this._propertyListingService.selectedKeywordSearch;
        if(this.searchKeyword !== ''){
          this._propertyListingService.searchListings(() => {
            this._zone.run(() => {
              this.listings = this._propertyListingService.propertyListingResult;
            })
          })
        }
        
    });
    return await modal.present();
  }

  close(){
    this.showContact = false;
  }

  async presentModalMap() {
    // const modal = await this.modalController.create({
    //   component: MapListingPage
    // });
    // modal.onDidDismiss()
    //   .then((data) => {
    // });
    // return await modal.present();
  }

  async presentModalSort(){
    const modal = await this.modalController.create({
      component: SortListComponent
    });
    modal.onDidDismiss()
      .then((data) => {
        this._propertyListingService.searchListings(() => {
            this._zone.run(() => {
              this.listings = this._propertyListingService.propertyListingResult;
            })
          })
    });
    return await modal.present();
  }

  viewMap(item) {
    this._propertyListingService.selectedPropertyListingDetail = item;
    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/map-view');
  }

  search(ev) {
    console.log(ev);
    // if(this.searchKeyword !== ''){
    //   this._propertyListingService.searchListingParams.keyword = this.searchKeyword;
    //   this._propertyListingService.searchListings( ()=>{
    //     this.listings = this._propertyListingService.propertyListingResult;
    //   });
    // }
  }

  getSearchListingResult() {
    this._propertyListingService.propertyListingResult;
  }
  

}
