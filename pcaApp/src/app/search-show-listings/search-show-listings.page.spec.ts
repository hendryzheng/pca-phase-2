import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchShowListingsPage } from './search-show-listings.page';

describe('SearchShowListingsPage', () => {
  let component: SearchShowListingsPage;
  let fixture: ComponentFixture<SearchShowListingsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchShowListingsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchShowListingsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
