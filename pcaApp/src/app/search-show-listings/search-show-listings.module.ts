import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SearchShowListingsPage } from './search-show-listings.page';
import { SortListComponent } from '../sort-list/sort-list.component';
import { SearchPropertyFilterModule } from '../search-property-filter/search-property-filter.module';

const routes: Routes = [
  {
    path: '',
    component: SearchShowListingsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchPropertyFilterModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [SortListComponent],
  declarations: [SearchShowListingsPage, SortListComponent]
})
export class SearchShowListingsPageModule {}
