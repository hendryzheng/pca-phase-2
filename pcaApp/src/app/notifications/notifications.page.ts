import { Component, OnInit, NgZone } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { Badge } from '@ionic-native/badge/ngx';
import { AccountService } from 'src/services/account.service';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  isNotif: boolean = true;
  notifState: any = 'all';
  loaded = false;
  notifications: any = [];
  constructor(
    public _utilitiesService: UtilitiesService,
    private badge: Badge,
    private _zone: NgZone,
    private _propertyListingService: PropertyListingService,
    private location: Location,
    private router: Router,
    public _accountService: AccountService) { }

  close() {
    // this.viewCtrl.dismiss();
  }

  ngOnInit(){

  }

  goBack(){
    this.location.back();
  }

  ionViewWillEnter() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });

    }

      this._accountService.getNotification(() => {
        this._zone.run(()=>{
        this.notifications = this._accountService.notification;
        this.loaded = true;
      });

      });
  }

  async clearBadges(){
    try {
      let badge = await this.badge.clear();
      console.log(badge);
    }
    catch(e){
      console.error(e);
    }
  }

  ngAfterViewInit(){
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });

    }
  }

  open(item) {
    if(item.title == 'Matching connect !'){
        var item_n = JSON.parse(item.payload);
        console.log(item_n);
        this._propertyListingService.selectedPropertyListingDetail = item_n;
        this._propertyListingService.selectedPropertyListingDetail.project_name = item_n.property_name;
        this._propertyListingService.selectedPropertyListingDetail.attributes = {
          price_formatted: item_n.price,
          bedrooms: item_n.bedrooms,
          bathrooms: item_n.number_of_bathroom,
          area_ppsf_formatted: item_n.floor_size,
          area_size_formatted: item_n.area_size
        };
        this._propertyListingService.selectedPropertyListingDetail.address_line_1 = item_n.address;
        this._propertyListingService.selectedPropertyListingDetail.address_line_2 = '';
        this._propertyListingService.selectedPropertyListingDetail.area_size_formatted = item.area_size;;
      
        console.log(this._propertyListingService.selectedPropertyListingDetail);
        this.router.navigateByUrl('/property-listing');
        return;
    }
    if (item.url !== null && typeof item.url !== 'undefined' && item.url !== '') {
      if(item.url.indexOf("http") != -1){
        let url = 'http://'+item.url;
        this._utilitiesService.openBrowser(item.title, url);
      }else{
        this._utilitiesService.openBrowser(item.title, item.url);
      }
    }
  }

  getNotification() {
    return this.notifications;
  }

  
  toggleNotif(string){
    this._zone.run(()=>{
      this.notifState = string;
    });
  }

}
