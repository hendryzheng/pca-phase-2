import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddListingSearchLocationPage } from './add-listing-search-location.page';

const routes: Routes = [
  {
    path: '',
    component: AddListingSearchLocationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddListingSearchLocationPage]
})
export class AddListingSearchLocationPageModule {}
