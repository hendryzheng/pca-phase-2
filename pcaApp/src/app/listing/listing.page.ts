import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';
import { CONFIGURATION } from 'src/services/config.service';
import { Location } from '@angular/common';

declare var window: any;

@Component({
  selector: 'app-listing',
  templateUrl: './listing.page.html',
  styleUrls: ['./listing.page.scss'],
})
export class ListingPage implements OnInit {

  pcaCategories: any = [];

  constructor(private _menuService: MenuService,
              private router: Router,
              private _utilitiesService: UtilitiesService,
              private location: Location) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PcaCategoriesPage');
  }

  getPcaCategories(){
    return this._menuService.pca_categories;
  }

  getMenu() {
    return this._menuService.selected_pca_category.category_name;
  }

  getMenuObj(){
    return this._menuService.selectedMenu;
  }

  getListing(){
    return this._menuService.listing;
  }

  openListing(item){
    this._menuService.selected_pca_category = item;
    this.router.navigateByUrl('/pca-listing');
  }

  goBack(){
    this.location.back();
  }

  getBase(){
    return CONFIGURATION.base_url;
  }

  viewItem(item){
    
  }

  openAttachment(item) {
    var base_url = CONFIGURATION.apiEndpoint;
    if (item.listing_type == '2') {
      console.log(item.listing_attachment);
      this._utilitiesService.openBrowser(item.listing_name, item.listing_attachment);
      // const browser = this.iab.create(item.listing_attachment, '_blank', 'location=no');
    } else if (item.listing_type == '3') {
      console.log(item);
      window.InAppYouTube.openVideo(item.youtube_id, {
        fullscreen: true
      }, (result)=> {
        console.log(JSON.stringify(result));
      }, (reason)=> {
        console.log(reason);
      });
      // this.youtube.openVideo(item.youtube_id);

    } else {
      this._menuService.selected_listing = item;
      this._menuService.selected_listing.image_path = base_url + item.listing_attachment;
      // this.navCtrl.push(PreviewImagePage);
      this.router.navigateByUrl('/preview-image');
      // this._utilitiesService.openBrowser(item.listing_name, base_url + item.listing_attachment);
      // this.iab.create(base_url+item.listing_attachment, '_blank', 'location=no');
    }
  }

}
