import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-new-launched-details',
  templateUrl: './new-launched-details.page.html',
  styleUrls: ['./new-launched-details.page.scss'],
})
export class NewLaunchedDetailsPage implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

}
