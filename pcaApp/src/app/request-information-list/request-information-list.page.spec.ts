import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestInformationListPage } from './request-information-list.page';

describe('RequestInformationListPage', () => {
  let component: RequestInformationListPage;
  let fixture: ComponentFixture<RequestInformationListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestInformationListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestInformationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
