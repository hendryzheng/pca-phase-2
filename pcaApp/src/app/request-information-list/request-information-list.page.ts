import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';

import * as moment from 'moment';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-request-information-list',
  templateUrl: './request-information-list.page.html',
  styleUrls: ['./request-information-list.page.scss'],
})
export class RequestInformationListPage implements OnInit {

  informationList: any = [];

  constructor(private location: Location, 
              private alertCtrl: AlertController,
              private zone: NgZone,
              private _propertyListingService: PropertyListingService,
              private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this._propertyListingService.getRequestInformationList(()=>{
      this.informationList = this._propertyListingService.requestInformationList;
    });
  }

  goBack(){
    this.location.back();
  }

  formatDate(dt){
    return moment(dt,'YYYY-MM-DD hh:mm:ss').format('MMM DD hh:mm:ss');
  }

  search(item){
    console.log(item);
    for (let key in this._propertyListingService.searchListingParams) {
        for (let k in item){
          if(key == k){
            this._propertyListingService.searchListingParams[key] = item[k];
            break;
          }
        }
        // Use `key` and `value`
    }
    this.saveSearch(item);
  }

  createSearch(){
    this._propertyListingService.searchListingParams = {
      search_id: '',
      location_name: '',
      location_subtitle: '',
      search_name: '',
      query_coords: '',
      listing_type: 'sale',
      main_category: 'hdb',
      keywords: '',
      sub_categories: '',
      query_type: '',
      query_ids: '',
      radius_max: 1000,
      price_min: 100000,
      price_max: 20000000,
      floorarea_min: 500,
      floorarea_max: 10000,
      rooms: 'any',
      is_new_launch: 'any',
      tenure: 'any',
      sort_field: '',
      sort_order: ''
    }
    this.router.navigateByUrl('/request-information');
  }

  async delete(item){
    const forgot = await this.alertCtrl.create({
      subHeader: 'Delete this record?',
      message: "Are you sure to delete this record?",
      buttons: [
        {
          text: 'Yes',
          handler: data => {
            console.log('Cancel clicked');
            let params = {
              id: item.id
            };
            let session = localStorage.getItem('matchingconnect_name');
            if(item.location_name == session){
              localStorage.removeItem('matchingconnect_name');
              localStorage.removeItem('matchingConnect99co'); 
              localStorage.removeItem('matchingConnectPca');
            }
            this._propertyListingService.deleteRequestInformation(params, ()=>{
              this.zone.run(()=>{
                this._propertyListingService.getRequestInformationList(()=>{
                  this.informationList = this._propertyListingService.requestInformationList;
                });
              });
              
            })
          }
        },
        {
          text: 'Cancel',
          handler: data => {
            console.log('Send clicked');
            
          }
        }
      ]
    });
    await forgot.present();
  }

  async saveSearch(item) {
    const forgot = await this.alertCtrl.create({
      subHeader: 'Choose your option?',
      message: "Do you want to update your search?",
      buttons: [
        {
          text: 'Yes I want to Edit',
          handler: data => {
            console.log('Cancel clicked');
            this._propertyListingService.searchListingParams.search_id = item.id;
            this._propertyListingService.selectedKeywordSearch = item.location_name;
            this._propertyListingService.searchListingParams.search_name = item.search_name;
            this._propertyListingService.editSearchListingParams = true;
            console.log(this._propertyListingService.searchListingParams);
            this.router.navigateByUrl('/request-information');
            localStorage.setItem('matchingconnect_name',item.location_name);
          }
        },
        {
          text: 'No, Show me Search Results',
          handler: data => {
            console.log('Send clicked');
            this._propertyListingService.searchListingParams.search_id = item.id;
            this._propertyListingService.selectedKeywordSearch = item.location_name;
            console.log(this._propertyListingService.searchListingParams);
            this.zone.run(()=>{this.router.navigateByUrl('/matching-request');});
          }
        }
      ]
    });
    await forgot.present();
  }

}
