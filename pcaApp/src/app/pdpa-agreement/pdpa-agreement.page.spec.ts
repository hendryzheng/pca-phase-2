import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdpaAgreementPage } from './pdpa-agreement.page';

describe('PdpaAgreementPage', () => {
  let component: PdpaAgreementPage;
  let fixture: ComponentFixture<PdpaAgreementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdpaAgreementPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdpaAgreementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
