import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pdpa-agreement',
  templateUrl: './pdpa-agreement.page.html',
  styleUrls: ['./pdpa-agreement.page.scss'],
})
export class PdpaAgreementPage implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  goBack(){
    this.location.back();
  }

}
