import { Component, OnInit, NgZone } from '@angular/core';
import { SignUpPage } from '../sign-up/sign-up.page';
import { LoginPage } from '../login/login.page';
import { HomePage } from '../home/home.page';
import { TabsPage } from '../tabs/tabs.page';
import { NavParams, Platform } from '@ionic/angular';
import { AccountService } from 'src/services/account.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';

declare var cordova: any;
declare var window: any;
declare var Branch: any;


@Component({
  selector: 'app-splash-login',
  templateUrl: './splash-login.page.html',
  styleUrls: ['./splash-login.page.scss'],
})
export class SplashLoginPage implements OnInit {

  constructor(public router: Router,
    private _accountService: AccountService,
    private appVersion: AppVersion,
    private _utilitiesService: UtilitiesService,
    private _zone: NgZone,
    private platform: Platform) {
  }

  token: any = '';

  ngOnInit(){
    
    
  }

  ngAfterViewInit() {
    // let tabs = document.querySelectorAll('.show-tabbar');
    // if (tabs !== null) {
    //   Object.keys(tabs).map((key) => {
    //     tabs[key].style.display = 'none';
    //   });
    // }

  }

  ionViewWillLeave() {
    // let tabs = document.querySelectorAll('.show-tabbar');
    // if (tabs !== null) {
    //   Object.keys(tabs).map((key) => {
    //     tabs[key].style.display = 'flex';
    //   });

    // }
  }

  private callbackSessionLogin() {
    if (this._accountService.isUserSessionAlive()) {
      this.initBranchIO();
      this.router.navigate(['/tabs'], {replaceUrl: true})
    }else{
      this.initBranchIO();
    }
  }

  initBranchIO(){
    document.addEventListener('deviceready', () => {
      this.initBranchEnameCard();
    });
    document.addEventListener("resume", ()=> {
      this.initBranchEnameCard();
    });
  }
  initBranchEnameCard(){
    const Branch = window['Branch'];
      console.log(Branch);
      // alert(Branch);
      Branch.initSession().then(data => {
        // alert(data);
        if (data['+clicked_branch_link']) {
          // read deep link data on click
          // alert("Deep Link Data: " + JSON.stringify(data));
          // console.log(data);
          if(typeof data['e-namecard'] !== 'undefined' && data['e-namecard'] !== null){
            this.router.navigate(['/e-namecard/'+data['e-namecard']])
          }
          return;
        }
      });
  }

  ionViewWillEnter() {
    // let tabs = document.querySelectorAll('.show-tabbar');
    // if (tabs !== null) {
    //   Object.keys(tabs).map((key) => {
    //     tabs[key].style.display = 'none';
    //   });
    // }
    this._accountService.checkUserSession();
    if(typeof cordova == 'undefined'){
      this.callbackSessionLogin();
    }
    
    document.addEventListener('deviceready', () => {
      // only on devices
      this.appVersion.getVersionNumber().then(response => {
        let num = response.toString().replace(/\./g, '');
        console.log(num);
        this._accountService.getAppVersion(() => {
          if (parseInt(num) > 0 && this._accountService.appVersion !== '') {
            if (this.platform.is('ios')) {
              let version = this._accountService.appVersion.ios_version;
              let ios_url = this._accountService.appVersion.ios_url;
              if (version > num) {
                this._utilitiesService.alertMessageCallback('Oops !', 'Your app version is outdated. Please update your app', () => {
                  localStorage.clear();
                  this._accountService.userData = [];
                  var ref = cordova.InAppBrowser.open(ios_url, '_system', 'location=yes');
                  ref.addEventListener('loadstop', event => {
                    ref.show();
                  });
                });
              } else {
                this.callbackSessionLogin();
              }
            } else {
              let version = this._accountService.appVersion.android_version;
              let android_url = this._accountService.appVersion.android_url;
              if (version > num) {
                this._utilitiesService.alertMessageCallback('Oops !', 'Your app version is outdated. Please update your app', () => {
                  localStorage.clear();
                  this._accountService.userData = [];
                  var ref = cordova.InAppBrowser.open(android_url, '_system', 'location=yes');
                  ref.addEventListener('loadstop', event => {
                    ref.show();
                  });
                });
              } else {
                this.callbackSessionLogin();
              }
            }
          } else {
            alert('error');
          }
        });
      });
    });




  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
  }

  register() {
    this.router.navigateByUrl('/sign-up');
  }

  login() {
    this.router.navigateByUrl('/login');
  }

  home() {

  }

}
