import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewListingConnectPage } from './view-listing-connect.page';

describe('ViewListingConnectPage', () => {
  let component: ViewListingConnectPage;
  let fixture: ComponentFixture<ViewListingConnectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewListingConnectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewListingConnectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
