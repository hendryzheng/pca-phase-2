import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';

import { SearchPropertyFilterPage } from '../search-property-filter/search-property-filter.page';
import { SortListComponent } from '../sort-list/sort-list.component';
import { ModalController } from '@ionic/angular';
import { AccountService } from 'src/services/account.service';

@Component({
  selector: 'app-view-listing-connect',
  templateUrl: './view-listing-connect.page.html',
  styleUrls: ['./view-listing-connect.page.scss'],
})
export class ViewListingConnectPage implements OnInit {

  isNotif: boolean = true;
  notifState: any = 'listing-grid';
  notifStateTab: any = 'listing-connect';
  searchKeyword: any = '';
  listings: any = [];
  listingCount: any = 0;
  myListing: any = [];
  loaded: boolean = false;

  constructor(private location: Location,
    private geolocation: Geolocation,
    private _utilitiesService: UtilitiesService,
    private _accountService: AccountService,
    private modalController: ModalController,
    private router: Router,
    private _zone: NgZone,
    private _propertyListingService: PropertyListingService) { }

  ngOnInit() {
    if(this._propertyListingService.publicPropertyListing.length == 0){
      this._propertyListingService.getPublicPropertyListing({}, () => {
        this.loaded = true;
        this.listings = this._propertyListingService.publicPropertyListing;
        this._propertyListingService.propertyListingResult = this.listings;
      });
    }else{
      this.loaded = true;
      this.listings = this._propertyListingService.publicPropertyListing;
    }
    
  }

  toggleNotif(string) {
    this.notifState = string;
  }

  toggleNotifTab(string) {
    this.notifStateTab = string;
    if (this.notifStateTab == 'my-listing') {
      this.fetchMyPropertyListing();
    }
  }

  fetchMyPropertyListing() {
    this._zone.run(() => {
      this.myListing = [];
      this._propertyListingService.getMyPropertyListing(() => {
        this.myListing = this._propertyListingService.myPropertyListing;
      });
    });

  }


  viewMap(item) {
    this._propertyListingService.selectedPropertyListingDetail = item;
    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/map-view');
  }

  ionViewWillEnter() {

  }

  map() {
    this._zone.run(() => {
      this._propertyListingService.propertyListingResult = this.listings;
      this.router.navigateByUrl('/map-listing');
    });

  }

  async deleteProperty(item) {
    let alert = await this._utilitiesService.getAlertCtrl().create({
      header: 'Are you sure you want to delete this property?',
      // message: message,
      buttons: [
        {
          text: 'Yes',
          handler: data => {
            this.deleteListing(item);
            console.log('OK clicked');
          }
        },
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  deleteListing(item) {
    let params = {
      listing_id: item.id
    }
    this._propertyListingService.deleteListing(params, () => {
      this.fetchMyPropertyListing();
    });
  }

  editListing(item) {
    this._propertyListingService.propertyDetailPhotos = item.listing_images;
    this._propertyListingService.form = {
      update: true,
      pca_member_id: this._accountService.userData.id,
      property_segment: item.property_segment,
      main_category: item.main_category,
      sub_category: item.sub_category,
      land_use: item.land_use,
      property_name: item.property_name,
      district: item.district,
      lat: item.lat,
      lng: item.lng,
      address: item.address,
      type: item.type,
      id: item.id,
      photo_url: item.photo_url,
      listing_type: item.listing_type,
      hdb_type: item.hdb_type,
      bedrooms: item.bedrooms,
      floor_size: item.floor_size,
      floor_size_type: item.floor_size_type,
      area_size: item.area_size,
      price: item.price,
      agents_to_market: item.agents_to_market,
      highlights: item.highlights,
      description: item.description,
      floor: item.floor,
      age_of_property: item.age_of_property,
      last_renovation_done: item.last_renovation_done,
      ceiling_height: item.
      _height,
      unit_no: item.unit_no,
      number_of_bathroom: item.number_of_bathroom,
      date_of_availbility: item.date_of_availbility,
      facing: item.facing,
      tenancy_lease_expiration: item.tenancy_lease_expiration,
      tenancy_current_rent: item.tenancy_current_rent,
      tenure: item.tenure,
      features: item.features,
      other_restrictions: item.other_restrictions,
      youtube_video: item.youtube_video,
      iframe_v360: item.iframe_v360
    }
    this.router.navigateByUrl('/add-listing-step-basic-info');
  }

  search(ev) {
    console.log(ev);
    // if(this.searchKeyword !== ''){
    //   this._propertyListingService.searchListingParams.keyword = this.searchKeyword;
    //   this._propertyListingService.searchListings( ()=>{
    //     this.listings = this._propertyListingService.propertyListingResult;
    //   });
    // }
  }

  getSearchListingResult() {
    this._propertyListingService.propertyListingResult;
  }

  showMap() {
    this.presentModalMap();
  }


  goBack() {
    this.location.back();
  }

  viewListing(item) {
    this._propertyListingService.selectedPropertyListingDetail = item;
    console.log(this._propertyListingService.selectedPropertyListingDetail);
    this.router.navigateByUrl('/property-listing');
  }

  whatsapp(phone) {
    return 'https://api.whatsapp.com/send?phone=' + this._utilitiesService.formatWhatsappNo(phone);
  }

  getSearchListingParam() {
    return this._propertyListingService.searchListingParams;
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SearchPropertyFilterPage
    });
    modal.onDidDismiss()
      .then((data) => {
        this.searchKeyword = this._propertyListingService.selectedKeywordSearch;
        if (this.searchKeyword !== '') {
          this._propertyListingService.searchListings(() => {
            this._zone.run(() => {
              this.listings = this._propertyListingService.propertyListingResult;
            })
          })
        }

      });
    return await modal.present();
  }

  async presentModalMap() {
    // const modal = await this.modalController.create({
    //   component: MapListingPage
    // });
    // modal.onDidDismiss()
    //   .then((data) => {
    // });
    // return await modal.present();
  }

  async presentModalSort() {
    const modal = await this.modalController.create({
      component: SortListComponent
    });
    modal.onDidDismiss()
      .then((data) => {
        this._propertyListingService.searchListings(() => {
          this._zone.run(() => {
            this.listings = this._propertyListingService.propertyListingResult;
          })
        })
      });
    return await modal.present();
  }

}
