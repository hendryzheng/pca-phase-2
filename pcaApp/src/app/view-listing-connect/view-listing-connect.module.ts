import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ViewListingConnectPage } from './view-listing-connect.page';
import { SearchPropertyFilterPage } from '../search-property-filter/search-property-filter.page';
import { SearchPropertyFilterModule } from '../search-property-filter/search-property-filter.module';

const routes: Routes = [
  {
    path: '',
    component: ViewListingConnectPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchPropertyFilterModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ViewListingConnectPage]
})
export class ViewListingConnectPageModule {}
