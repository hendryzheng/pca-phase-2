import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumCreatePostExpandPage } from './forum-create-post-expand.page';

describe('ForumCreatePostExpandPage', () => {
  let component: ForumCreatePostExpandPage;
  let fixture: ComponentFixture<ForumCreatePostExpandPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumCreatePostExpandPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumCreatePostExpandPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
