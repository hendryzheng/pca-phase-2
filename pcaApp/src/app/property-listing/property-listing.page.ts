import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { MapListingPage } from '../map-listing/map-listing.page';
import { ModalController, IonFab, AlertController } from '@ionic/angular';
import { MapViewPage } from '../map-view/map-view.page';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/services/account.service';

declare var cordova: any;

@Component({
  selector: 'app-property-listing',
  templateUrl: './property-listing.page.html',
  styleUrls: ['./property-listing.page.scss'],
})
export class PropertyListingPage implements OnInit {

  userData: any = [];
  param_id = '';
  property_id = '';
  hideCustomer: boolean = false;
  loaded: boolean = false;
  showContact: boolean = false;
  selfAgent: boolean = false;
  @ViewChild(IonFab) fab: IonFab;



  constructor(private location: Location,
              private _utilitiesService: UtilitiesService,
              private alertController: AlertController,
              private router: Router,
              private zone: NgZone,
              private _accountService: AccountService,
              private route: ActivatedRoute,
              private _propertyListingService: PropertyListingService) { }

  ngOnInit() {
  }

  async presentAlertPrompt(){
    
    const alert = await this.alertController.create({
      header: 'Please Fill In The Following To Contact Agent:',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name'
        },
        {
          name: 'mobile',
          type: 'tel',
          value: '',
          placeholder: 'Contact Number'
        },
        
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Submit',
          handler: data => {
            console.log(data);
            let params = {
              'MatchingConnect[pca_member_id]': this.userData.id,
              'MatchingConnect[customer_name]': data.name,
              'MatchingConnect[customer_mobile]': data.mobile,
              'MatchingConnect[matching_connect_details]': JSON.stringify(this.getPropertyListingDetail())
            }
            this._propertyListingService.addMatchingConnectCustomer(params, ()=>{
              this.zone.run(()=>{
                this.showContact = true;
              });
            });
            console.log('Confirm Ok');
          }
        }
      ]
    });
    if(this.param_id !== '' && typeof this.param_id !== 'undefined' && this.param_id !== null){
      await alert.present();
    }else{
      var text = 'Hi ! I am interested in this listing: '+this.getPropertyListingDetail().project_name+' (ID:'+this.getPropertyListingDetail().id+')';
      var url =  'https://api.whatsapp.com/send?text='+text+'&phone='+this._utilitiesService.formatWhatsappNo(this.getPropertyListingDetail().user.phone);
      cordova.InAppBrowser.open(url, '_system', 'location=yes');
    }
  }

  ionViewWillEnter(){
    this.param_id = this.route.snapshot.paramMap.get('id');
    this.property_id = this.route.snapshot.paramMap.get('property_id');
    console.log(this.param_id);
    console.log(this.property_id);
          
    if(this.param_id !== '' && typeof this.param_id !== 'undefined' && this.param_id !== null){
      let params = {
        user_id: this.param_id,
        user_type: 'pca_member'
      }
      this._accountService.getUserDetailParam(params, ()=>{
        this.userData = this._accountService.viewUserData;
        this.hideCustomer = true;
      });
    }else{
      if (this._accountService.isUserSessionAlive()) {
        this.loaded = false;
        this.selfAgent = true;
        this._accountService.getUserDetail(() => {
          
          this.userData = this._accountService.userData;
          this.loaded = true;
        });
      }
    }
  }

  goBack(){
    this.location.back();
  }

  getPropertyListingDetail(){
    return this._propertyListingService.selectedPropertyListingDetail;
  }



  showMap(){
    this.router.navigateByUrl('/map-view');
  }

  contact(){
    this.presentAlertPrompt();
  }

  whatsapp(phone){
    var text = 'Hi ! I am interested in this listing: '+this.getPropertyListingDetail().project_name;
    return 'https://api.whatsapp.com/send?text='+text+'&phone='+this._utilitiesService.formatWhatsappNo(phone);
  }

  openInstagram(){
    if(this.userData.instagram !== ''){
      var url = 'https://instagram.com/'+this.userData.instagram;
      cordova.InAppBrowser.open(url, '_system', 'location=yes');
    }else{
      this._utilitiesService.alertMessage('Oops','You have not added instagram yet');
    }
  }

  openFacebook(){
    if(this.userData.facebook !== ''){
      var url = 'https://facebook.com/'+this.userData.facebook;
      cordova.InAppBrowser.open(url, '_system', 'location=yes');
    }else{
      this._utilitiesService.alertMessage('Oops','You have not added facebook yet');
    }
  }

  openMail(){
    var url =  'mailto:'+this.userData.email;
    cordova.InAppBrowser.open(url, '_system', 'location=yes');
  }

  openCall(){
    var url =  'tel:'+this._utilitiesService.formatWhatsappNo(this.userData.mobile);
    cordova.InAppBrowser.open(url, '_system', 'location=yes');

  }
  openWhatsapp(){
    var url =  'https://api.whatsapp.com/send?phone='+this._utilitiesService.formatWhatsappNo(this.userData.mobile);
    cordova.InAppBrowser.open(url, '_system', 'location=yes');
  }
}
