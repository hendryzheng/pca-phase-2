import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyListingPage } from './property-listing.page';

describe('PropertyListingPage', () => {
  let component: PropertyListingPage;
  let fixture: ComponentFixture<PropertyListingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyListingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyListingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
