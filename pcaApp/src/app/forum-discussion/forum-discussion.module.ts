import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ForumDiscussionPage } from './forum-discussion.page';

const routes: Routes = [
  {
    path: '',
    component: ForumDiscussionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ForumDiscussionPage]
})
export class ForumDiscussionPageModule {}
