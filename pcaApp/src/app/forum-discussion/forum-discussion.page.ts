import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { MenuService } from 'src/services/menu.service';
import { CONFIGURATION } from 'src/services/config.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-forum-discussion',
  templateUrl: './forum-discussion.page.html',
  styleUrls: ['./forum-discussion.page.scss'],
})
export class ForumDiscussionPage implements OnInit {

  showDesc: boolean = true;

  form = {
    comment: ''
  };
  loaded: boolean = false;

  constructor(
    private _accountService: AccountService,
    private router: Router,
    private location: Location,
    private _utilitiesService: UtilitiesService,
    private _menuService: MenuService) {
  }

  ngOnInit(){

  }

  toggleDesc() {
    this.showDesc = !this.showDesc;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentPage');
  }

  getPost() {
    return this._menuService.selectedPost
  }

  viewImage(path){
    this._utilitiesService.openBrowser('Preview',path);
  }

  goBack(){
    this.location.back();
  }

  openAccount(item){
    console.log(item);
    this._accountService.viewUserData.id = item.pca_member_id;
    this._accountService.viewUserData.member_type = 'pca_member';
    this.router.navigateByUrl('/e-namecard-view');
  }

  ionViewWillEnter() {
    this._accountService.checkUserSession();
    this._menuService.getComments(() => {
      this.loaded = true;
    });
  }

  getComments(){
    return this._menuService.comments;
  }

  getAccount() {
    return this._accountService.userData;
  }
  private validateForm() {
    var validate = true;
    console.log(this.form);
    if (this._utilitiesService.isEmpty(this.form.comment)) {
      validate = false;
    }
    if (!validate) {
      this._utilitiesService.alertMessage('Validation Error', 'Comment cannot be empty');
    }
    console.log(validate);
    return validate;
  }

  submitComment() {
    if (this.validateForm()) {
      let formSubmission = {
        'Forumcomment[forum_post_id]': this.getPost().id,
        'Forumcomment[comment]': this.form.comment
      };
      if (this._accountService.userData.member_type === 'agent') {
        formSubmission['Forumcomment[agent_id]'] = this._accountService.userData.id;
      } else if(this._accountService.userData.member_type === 'pca_member'){
        formSubmission['Forumcomment[pca_member_id]'] = this._accountService.userData.id;
      } else {
        formSubmission['Forumcomment[account_id]'] = this._accountService.userData.id;
      }
      this._menuService.submitComment(formSubmission, () => {
        this.form.comment = '';
        this._utilitiesService.alertMessageCallback('Success', 'Comment submitted successfully', () => {
          // this.navCtrl.pop();
        });
      });
    }
  }

  isUserSessionAlive(){
    return this._accountService.isUserSessionAlive();
  }

  getImagePath(item){
    return CONFIGURATION.base_url+this.getPost().image;
  }

}
