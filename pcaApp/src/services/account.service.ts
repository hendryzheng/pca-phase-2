import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

import { UtilitiesService } from './utilities.service';
import { CONFIGURATION } from './config.service';
import { Badge } from "@ionic-native/badge/ngx";

declare var navigator: any;
declare var FileTransfer: any;
declare var FileUploadOptions: any;

@Injectable()
export class AccountService {

    userData: any = null;
    viewUserData: any = [];
    viewUserDataState: boolean = false;
    appVersion: any = '';
    config: any = [];
    userDetail: any = [];

    countries: any = [];
    states: any = [];
    cities: any = [];
    token: any = '';
    news_popup: any = null;
    notificationOpen: any = null;
    notification: any = [];
    notification_unread: any = '';
    pdpaConfig: any = [];

    constructor(private _httpService: HttpService,
        private badge: Badge,
        private _utilitiesService: UtilitiesService) {
        // var userData = localStorage.getItem('userData');
        // console.log(userData);
    }

    getPdpaConfig(callback: ()=>void){
        this.checkUserSession();
        this._utilitiesService.showLoading();
        let param = {
            user_id: this.userData.id,
            user_type: this.userData.member_type
        };
        return this._httpService.postRequest(CONFIGURATION.URL.getPdpaConfig, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.pdpaConfig = response.data;
                    callback();
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    register(params, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.register, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this._utilitiesService.alertMessage('Success !', response.message).then(resolve => {
                        callback();
                    });
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    isUserSessionAlive() {

        // console.log('-- ACCOUNTSERVICE isUserSessionAlive : ' + this.userData + ' --');
        if (this.userData === null) {
            return false;
        } else {
            return true;
        }
    }

    checkUserSession() {
        var userData = localStorage.getItem('userData');
        console.log(userData);
        if (!this._utilitiesService.isEmpty(userData)) {
            this.userData = JSON.parse(userData);
        }
    }

    changePassword(param, callback: () => void) {
        this._utilitiesService.showLoading();
        param['u_id'] = this.userData.id;
        param['member_type'] = this.userData.member_type;
        this._httpService.postRequest(CONFIGURATION.URL.changePassword, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {

                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    updateProfile(param, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateProfile, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    callback();
                    
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        })
    }

    updateEmail(param, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateEmail, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    callback();
                    
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        })
    }

    updatePropertyType(param, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updatePropertyType, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.userData = response.data;
                    this.userDetail = response.data;
                    localStorage.setItem('userData', JSON.stringify(this.userData));
                    callback();
                    
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        })
    }

    updatePassword(param: any, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updatePassword, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this._utilitiesService.alertMessage('Successful', response.message);
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    forgotPassword(param: any, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.forgotPassword, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this._utilitiesService.alertMessageCallback('Successful',response.message, callback);
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    updatePreference(param: any, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateProfile, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this._utilitiesService.alertMessage('Successful', response.message);
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getUserDetail(callback: () => void) {
        this.checkUserSession();
        // this._utilitiesService.showLoading();
        let param = {
            user_id: this.userData.id,
            user_type: this.userData.member_type
        };
        return this._httpService.postRequest(CONFIGURATION.URL.getUser, param).subscribe(response => {

            // this._utilitiesService.hideLoading().then(resp => {
            if (response.status === 'OK') {
                this.userData = response.data;
                this.userDetail = response.data;
                this.notification_unread = response.data.unread;
                localStorage.setItem('userData', JSON.stringify(this.userData));
                localStorage.setItem('unread',this.notification_unread);
                console.log(this.userDetail);
                callback();
            }
            // });

        },error => {
            // this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getUserDetailParam(param, callback: () => void) {
        this.checkUserSession();
        this._utilitiesService.showLoading();
        return this._httpService.postRequest(CONFIGURATION.URL.getUser, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
            if (response.status === 'OK') {
                this.viewUserData = response.data;
                callback();
            }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getUserDetailProfile(callback: () => void) {
        this.checkUserSession();
        this._utilitiesService.showLoading();
        let param = {
            user_id: this.userData.id,
            user_type: this.userData.member_type
        };
        return this._httpService.postRequest(CONFIGURATION.URL.getUser, param).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.userData = response.data;
                    this.userDetail = response.data;
                    console.log(this.userDetail);
                    callback();
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getAppVersion(callback: () => void){
        this._utilitiesService.showLoading();
        let param = {};
        this._httpService.postRequest(CONFIGURATION.URL.getAppVersion, param).subscribe(response => {
            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.appVersion = response.data;
                    callback();
                }
            });
        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getNotification(callback: () => void) {
        this.checkUserSession();
        // this._utilitiesService.showLoading();
        let param = {
            user_id: this.userData.id,
            member_type: this.userData.member_type
        };
        return this._httpService.postRequest(CONFIGURATION.URL.getNotification, param).subscribe(response => {

            // this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.notification = response.data.data;
                    this.notification_unread = response.unread;
                    this.badge.clear();
                    callback();
                }
            // });

        },error => {
            // this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    uploadImage(imageUrl: any, callback: () => void) {

        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var params = {
            client_id: CONFIGURATION.TOKEN.CLIENT_ID,
            client_token: CONFIGURATION.TOKEN.CLIENT_TOKEN,
            user_id: this.userData.id,
            user_type: this.userData.member_type
        };

        options.params = params;
        var ft = new FileTransfer();
        this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadImageProfile);
        console.log(params);
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadImageProfile, response => {
            console.log(response);
            let res = JSON.parse(response.response);
            navigator.camera.cleanup(success => {
                console.log('success');
            }, error => {
                console.log('fail');
            });
            this._utilitiesService.hideLoading().then(resp => {
                if (res.status === 'OK') {
                    this.userData.profile_picture = res.data.imageUrl;
                    
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                }
            })

        }, error => {
            this._utilitiesService.hideLoading().then(resp => {
                // error.code == FileTransferError.ABORT_ERR
                alert('An error has occurred: Code = ' + error.code);
                console.log('upload error source ' + error.source);
                console.log('upload error target ' + error.target);
            });

        }, options, true);

    }

    getConfig() {
        // this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.getConfig, {}).subscribe(response => {

            // this._utilitiesService.hideLoading().then(resp => {
            if (response.status === 'OK') {
                this.config = response.data;
                localStorage.setItem('config', JSON.stringify(response.data));
            }
            // });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    registerToken() {
        if (this.userData !== null) {
            let param = {
                user_type: this.userData.member_type,
                id: this.userData.id,
                token: this.token
            };
            console.log(param);
            this._httpService.postRequest(CONFIGURATION.URL.tokenRegistration, param)
                .subscribe(response => {
                    console.log(response);
                },error => {
                    // this._utilitiesService.handleError(error);
                    console.log(error);
                });
        } else {
            let param = {
                token: this.token
            };
            console.log(param);
            this._httpService.postRequest(CONFIGURATION.URL.tokenRegistration, param)
                .subscribe(response => {
                    console.log(response);
                },error => {
                    // this._utilitiesService.handleError(error);
                    console.log(error);
                });
        }

    }

    getLoginAccount() {
        // var userData = this.userData;
        if (!this._utilitiesService.isEmpty(this.userData)) {
            return this.userData;
        } else {
            return '';
        }
    }

    getUserId() {
        if (this.userData !== null) {
            return this.userData.id;
        }
        return '';
    }

    isLoggedIn() {
        if (this.userData !== null) {
            return true;
        } else {
            return false;
        }
    }

    isDealer() {
        if (this.userData !== null) {
            if (this.userData.member_type === 'Gold Dealer' || this.userData.member_type === 'Silver Dealer') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    isGoldDealer() {
        if (this.userData !== null) {
            if (this.userData.member_type === 'Gold Dealer') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    isSilverDealer() {
        if (this.userData !== null) {
            if (this.userData.member_type === 'Silver Dealer') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    isStaff() {
        if (this.userData !== null) {
            if (this.userData.member_type === 'Staff') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}