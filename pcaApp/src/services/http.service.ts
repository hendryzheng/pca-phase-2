import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpRequest, HttpEventType, HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError, tap } from 'rxjs/operators';
import { CONFIGURATION } from './config.service';

declare var device: any;
declare var navigator: any;
declare var FileTransfer: any;
declare var FileUploadOptions: any;
@Injectable()
export class HttpService {

    constructor (private http: Http) {

    }

    public ObjecttoParams(obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
    };


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.status}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
    /** Log a HeroService message with the MessageService */
    private log(message: string) {
        console.log(message);
    }

    getRequest(url: string) {
        var version = localStorage.getItem('version');
        var isIos = localStorage.getItem('isIos');
        url += '?isIos=' + isIos + '&version=' + version + '&client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        return this.http.get(url).pipe(
            map((response: any) => response.json()),
            catchError(this.handleError('getRequestPlain : ' + url, []))
        );
    }

    getRequestRaw(url: string) {
        // var version = localStorage.getItem('version');
        // var isIos = localStorage.getItem('isIos');
        // url += '?isIos=' + isIos + '&version=' + version + '&client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        return this.http.get(url).pipe(
            map((response: any) => response.json()),
            catchError(this.handleError('getRequestPlain : ' + url, []))
        );
    }

    getRequestRaw99co(url: string) {
        // var version = localStorage.getItem('version');
        // var isIos = localStorage.getItem('isIos');
        // url += '?isIos=' + isIos + '&version=' + version + '&client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded','Authorization': 'Basic JnRFaWxkJSRaRnV4QEtUaEs1N0xCaHFBJElGQ2ZnT3NiZF5DN0piI1dSKEh2QiVeSmY=' });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(url, options).pipe(
            map((response: any) => response.json()),
            catchError(this.handleError('getRequestPlain : ' + url, []))
        );
    }

    postRequest(url: string, params: any) {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        params['client_id'] = CONFIGURATION.TOKEN.CLIENT_ID;
        params['client_token'] = CONFIGURATION.TOKEN.CLIENT_TOKEN;
        params['version'] = localStorage.getItem('version');
        params['isIos'] = localStorage.getItem('isIos');
        return this.http.post(CONFIGURATION.apiEndpoint + url, this.ObjecttoParams(params), options)
            .pipe(
                map((response: any) => response.json()),
                catchError(this.handleError('postRequest : ' + url, []))
            );
    }

    getUser(){
        var userData = localStorage.getItem('userData');
        console.log(userData);
        var userDataParse = null;
        if (userData) {
            userDataParse = JSON.parse(userData);
            return userDataParse;
        }
    }

    uploadImage(endpointUrl: any, imageUrl: any, params, callback: () => void, fallback: () => void) {
        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        
        var userData = localStorage.getItem('userData');
        console.log(userData);
        if (userData) {
            let parseUserData = JSON.parse(userData);
            params['u_id'] = parseUserData.id;
            params['user_type'] = parseUserData.member_type;
            params['client_id'] = CONFIGURATION.TOKEN.CLIENT_ID;
            params['client_token'] =  CONFIGURATION.TOKEN.CLIENT_TOKEN;
        }
        console.log(params);
        options.params = params;
        var ft = new FileTransfer();
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + endpointUrl, response => {
            console.log(response);
            let res = JSON.parse(response.response);
            navigator.camera.cleanup(success => {
                console.log('success');
            }, error => {
                console.log('fail');
            });
            if (res.status === 'OK') {
                callback();
            } else {
                fallback();
            }

        }, error => {
            // error.code == FileTransferError.ABORT_ERR
            alert('An error has occurred: Code = ' + error.code);
            console.log('upload error source ' + error.source);
            console.log('upload error target ' + error.target);
        }, options, true);
    }

}