import { Injectable } from "@angular/core";
import { UtilitiesService } from "./utilities.service";
import { HttpService } from "./http.service";
import { CONFIGURATION } from "./config.service";
import { AccountService } from "./account.service";
import { Badge } from "@ionic-native/badge/ngx";

declare var FileUploadOptions: any;
declare var FileTransfer: any;
declare var navigator: any;

@Injectable()
export class MenuService {
    public menu_list: any = [];
    public selectedMenu: any = [];
    public listing: any = [];
    public promotion: any = [];
    public selectedPromotion: any = [];
    public post: any = [];
    public selectedPost: any = [];
    public comments: any = [];
    public banner: any = [];
    public pca_categories: any = [];
    public pca_listing: any = [];
    public selected_pca_category: any = [];
    public menuName: any = '';
    public selected_pca_listing: any = [];
    public selected_listing: any = [];
    public calculators: any = [];
    public research: any = [];
    public autocomplete: any = [];
    public listing_result: any = [];
    public videoMarketing: any = [];
    public eNamecardState: any = '';
    public customerList: any = [];
    public customer: any = [];
    public matching_connect: any = null;
    public listing_connect: any = null;
    public matching_connect_obj: any = null;
    public customer_type: any = '';


    constructor(private _utilitiesService: UtilitiesService,
        private _httpService: HttpService,
        private badge: Badge,
        private _accountService: AccountService) {
        // this.categories = CATEGORIES;
    }

    submitContactForm(params, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.submitContactForm, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }
    
    checkUserSession(){
        this._accountService.checkUserSession();
    }

    getUserData(){
        return this._accountService.userData;
    }

    getCustomerData(callback: ()=>void){
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
            params['token'] = this._accountService.token;
        }
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.getCustomerData, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.customerList = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                callback();
            });
            console.log(error);
        });
    }

    uploadImageSignature(customer_id, imageUrl: any, callback: (res) => void) {
        this._accountService.checkUserSession();

        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var params = {
            customer_id: customer_id,
            client_id: CONFIGURATION.TOKEN.CLIENT_ID,
            client_token: CONFIGURATION.TOKEN.CLIENT_TOKEN,
            user_id: this._accountService.userData.id,
            user_type: this._accountService.userData.member_type
        };

        options.params = params;
        var ft = new FileTransfer();
        this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadCustomerSignature);
        console.log(params);
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadCustomerSignature, response => {
            console.log(response);
            let res = JSON.parse(response.response);
            navigator.camera.cleanup(success => {
                console.log('success');
            }, error => {
                console.log('fail');
            });
            this._utilitiesService.hideLoading().then(resp => {
                if (res.status === 'OK') {
                    
                    callback(res);
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                }
            })

        }, error => {
            this._utilitiesService.hideLoading().then(resp => {
                // error.code == FileTransferError.ABORT_ERR
                alert('An error has occurred: Code = ' + error.code);
                console.log('upload error source ' + error.source);
                console.log('upload error target ' + error.target);
            });

        }, options, true);

    }

    submitAddNewCustomer(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.addCustomerData, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.customer = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    updateCustomer(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateCustomer, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.customer = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    deleteCustomer(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.deleteCustomer, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    submitPromotionContactForm(params, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.submitPromotionContactForm, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getMenu(callback: () => void) {
        // this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
            params['token'] = this._accountService.token;
        }
        this.matching_connect = [];
        this.matching_connect_obj = null;
        this._httpService.postRequest(CONFIGURATION.URL.getMenu, params).subscribe(response => {
            // this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.menu_list = response.data.data;
                    this._accountService.notification_unread = response.data.unread;
                    if(typeof response.data.matching_connect.results !== 'undefined'){
                        this.matching_connect = response.data.matching_connect.results.data.sections[0].listings;
                        this.matching_connect_obj = response.data.matching_connect.search_obj;
                    }
                    // this.matching_connect = response.data.matching_connect;
                    this.badge.set(this._accountService.notification_unread);
                    localStorage.setItem('menu_list', JSON.stringify(this.menu_list));
                    if (this._accountService.isUserSessionAlive() && typeof response.data.user !== 'undefined') {
                        if (response.data.user.status !== '1') {
                            localStorage.clear();
                            this._accountService.userData = null;
                        }
                    } else {

                    }
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            // });

        },error => {
            // this._utilitiesService.hideLoading().then(resolve => {
                let cache = localStorage.getItem('menu_list');
                if (cache) {
                    this.menu_list = JSON.parse(localStorage.getItem('menu_list'));
                }
                callback();
            // });
            console.log(error);
        });
    }

    getVideoMarketing(callback: ()=>void){
        this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getVideoMarketing, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.videoMarketing = response.data;
                    localStorage.setItem('video_marketing', JSON.stringify(this.videoMarketing));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                let cache = localStorage.getItem('video_marketing');
                if (cache) {
                    this.pca_categories = JSON.parse(localStorage.getItem('video_marketing'));
                }
                callback();
            });
            console.log(error);
        });
    }

    getPcaCategories(callback: () => void) {
        this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getPcaCategories, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.pca_categories = response.data;
                    localStorage.setItem('pca_categories', JSON.stringify(this.pca_categories));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                let cache = localStorage.getItem('pca_categories');
                if (cache) {
                    this.pca_categories = JSON.parse(localStorage.getItem('pca_categories'));
                }
                callback();
            });
            console.log(error);
        });
    }

    getCalculators(callback: () => void) {
        this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getCalculators, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.calculators = response.data;
                    localStorage.setItem('calculators', JSON.stringify(this.calculators));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                let cache = localStorage.getItem('calculators');
                if (cache) {
                    this.calculators = JSON.parse(localStorage.getItem('calculators'));
                }
                callback();
            });
            console.log(error);
        });
    }

    searchListing(params, callback: ()=> void){
        // this._utilitiesService.showLoading();
        this._accountService.checkUserSession();

        this._httpService.postRequest(CONFIGURATION.URL.searchListing, params).subscribe(response => {
            // this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    if(typeof response.data !== 'undefined'){
                        this.listing_result = response.data;
                    }
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            // });

        },error => {
            // this._utilitiesService.hideLoading().then(resolve => {

                callback();
            // });
            console.log(error);
        });
    }

    getAutoComplete(params, callback: () => void) {
        // this._utilitiesService.showLoading();
        this._accountService.checkUserSession();

        this._httpService.postRequest(CONFIGURATION.URL.getAutoComplete, params).subscribe(response => {
            // this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    if(typeof response.data.sections !== 'undefined'){
                        this.autocomplete = response.data.sections;
                    }
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            // });

        },error => {
            // this._utilitiesService.hideLoading().then(resolve => {

                callback();
            // });
            console.log(error);
        });
    }

    getResearch(callback: () => void) {
        this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getResearch, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.research = response.data;
                    localStorage.setItem('research', JSON.stringify(this.research));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                let cache = localStorage.getItem('research');
                if (cache) {
                    this.research = JSON.parse(localStorage.getItem('calculators'));
                }
                callback();
            });
            console.log(error);
        });
    }

    getPcaListing(callback: () => void) {
        this._utilitiesService.showLoading();
        var params = {
            cat_id: this.selected_pca_category.id
        };
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getPcaListing, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.pca_listing = response.data;
                    localStorage.setItem('pca_listing', JSON.stringify(this.pca_listing));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                let cache = localStorage.getItem('pca_listing');
                if (cache) {
                    this.pca_categories = JSON.parse(localStorage.getItem('pca_listing'));
                }
                callback();
            });
            console.log(error);
        });
    }

    getBanner(callback: () => void) {
        let params = {};
        this._httpService.postRequest(CONFIGURATION.URL.getBanner, params).subscribe(response => {
            if (response.status === 'OK') {
                this.banner = response.data;
                callback();
            } else {
                this._utilitiesService.alertMessage('Oops! Something happened', response.message);
            }

        },error => {
            // this._utilitiesService.alertMessage('Oops! Something happened', 'Please check your internet connection and try again later');
            console.log(error);
        });
    }

    getPromotion(callback: () => void) {
        this._utilitiesService.showLoading();
        var params = [];

        this._httpService.postRequest(CONFIGURATION.URL.getPromotion, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.promotion = response.data;
                    localStorage.setItem('promotion', JSON.stringify(this.promotion));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                let cache = localStorage.getItem('promotion');
                if (cache) {
                    this.menu_list = JSON.parse(localStorage.getItem('promotion'));
                }
                callback();
            });
            console.log(error);
        });
    }

    getListing(callback: () => void) {
        this._utilitiesService.showLoading();
        this._accountService.checkUserSession();
        var params = {
            menu_id: this.selectedMenu.id
        };
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
            params['token'] = this._accountService.token;
        }


        this._httpService.postRequest(CONFIGURATION.URL.getListing, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.listing = response.data;
                    // localStorage.setItem('comments', JSON.stringify(this.post));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                // let cache = localStorage.getItem('post');
                // if (cache) {
                //     this.post = JSON.parse(localStorage.getItem('post'));
                // }
                // callback();
                this._utilitiesService.alertMessage('Oops! Something happened', 'Please check your internet connection and try again later');
            });
            console.log(error);
        });
    }

    getPost(callback: () => void) {
        this._utilitiesService.showLoading();
        var params = [];

        this._httpService.postRequest(CONFIGURATION.URL.getPost, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.post = response.data;
                    localStorage.setItem('post', JSON.stringify(this.post));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                let cache = localStorage.getItem('post');
                if (cache) {
                    this.post = JSON.parse(localStorage.getItem('post'));
                }
                callback();
            });
            console.log(error);
        });
    }

    searchForum(params, callback: () => void) {
        // this._utilitiesService.showLoading();

        this._httpService.postRequest(CONFIGURATION.URL.searchForum, params).subscribe(response => {
            // this._utilitiesService.hideLoading().then(resolve => {
            if (response.status === 'OK') {
                this.post = response.data;
                // localStorage.setItem('post', JSON.stringify(this.post));
                callback();
            } else {
                this._utilitiesService.alertMessage('Oops! Something happened', response.message);
            }
            // });

        },error => {
            // this._utilitiesService.hideLoading().then(resolve => {
            // let cache = localStorage.getItem('post');
            // if (cache) {
            //     this.post = JSON.parse(localStorage.getItem('post'));
            // }
            callback();
            // });
            console.log(error);
        });
    }

    getComments(callback: () => void) {
        this._utilitiesService.showLoading();
        var params = {
            forum_post_id: this.selectedPost.id
        };

        this._httpService.postRequest(CONFIGURATION.URL.getComments, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.comments = response.data;
                    // localStorage.setItem('comments', JSON.stringify(this.post));
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                // let cache = localStorage.getItem('post');
                // if (cache) {
                //     this.post = JSON.parse(localStorage.getItem('post'));
                // }
                // callback();
                this._utilitiesService.alertMessage('Oops! Something happened', 'Please check your internet connection and try again later');
            });
            console.log(error);
        });
    }

    submitPost(params, callback: (id: any) => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.submitPost, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    callback(response.data.id);
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                console.log(error);
            });
        });
    }

    submitComment(params, callback: () => void) {
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.submitComment, params).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                if (response.status === 'OK') {
                    this.comments = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                console.log(error);
            });
        });
    }

    uploadPostImage(post_id, imageUrl: any, callback: () => void) {
        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var params = {
            client_id: CONFIGURATION.TOKEN.CLIENT_ID,
            client_token: CONFIGURATION.TOKEN.CLIENT_TOKEN,
            post_id: post_id
        };

        options.params = params;
        var ft = new FileTransfer();
        this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadPostImage);
        console.log(params);
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadPostImage, response => {
            console.log(response);
            let res = JSON.parse(response.response);
            navigator.camera.cleanup(success => {
                console.log('success');
            }, error => {
                console.log('fail');
            });
            this._utilitiesService.hideLoading().then(resp => {
                if (res.status === 'OK') {
                    // this._utilitiesService.alertMessage('Successful', res.data.message);
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                }
            })

        }, error => {
            this._utilitiesService.hideLoading().then(resp => {
                // error.code == FileTransferError.ABORT_ERR
                alert('An error has occurred: Code = ' + error.code);
                console.log('upload error source ' + error.source);
                console.log('upload error target ' + error.target);
            });

        }, options);
    }

}
