import { Injectable } from "@angular/core";
import { UtilitiesService } from "./utilities.service";
import { HttpService } from "./http.service";
import { CONFIGURATION } from "./config.service";
import { AccountService } from "./account.service";
import { Badge } from "@ionic-native/badge/ngx";

declare var FileUploadOptions: any;
declare var FileTransfer: any;
declare var navigator: any;

@Injectable()
export class PropertyListingService {
    public property_listing: any = [];
    public myPropertyListing: any = [];
    public publicPropertyListing: any = [];
    public propertyListingResult: any = [];
    public propertyListingCount: any = [];
    public listing: any = [];
    public step1Location: any = [];
    public step1SelectedLocation: any = [];
    public step1PropertyDetail: any = [];
    public propertyListingDetail: any = [];
    public searchInputRequestResult: any = [];
    public selectedKeywordSearch: any = '';
    public selectedKeywordSearchSubtitle: any = '';
    public requestInformationList: any = [];
    public selectedPropertyListingDetail: any = [];
    public selectedViewUser: any = [];
    public matchingConnectCustomer: any = [];
    public pcaPropertyListing: any = [];

    editSearchListingParams = false;
    searchListingParamLat: any = '';
    searchListingParamLng: any = '';
    public searchListingParams: any = {
        search_id: '',
        location_name: '',
        location_subtitle:'',
        search_name: '',
        query_coords: '',
        property_segments: '',
        listing_type: 'sale',
        main_category: '',
        keywords: '',
        sub_categories: '',
        query_type: '',
        query_ids: '',
        radius_max: 100,
        location_query: '',
        price_min: 100000,
        price_max:20000000,
        floorarea_min: 100,
        floorarea_max: 10000,
        rooms: 'any',
        is_new_launch: 'any',
        tenure: 'any',
        sort_field: '',
        sort_order: ''
    };
    propertyDetailPhotos: any = [];

    public listing_categories = [
        {
            key: 'residential',
            value: 'Residential',
            main_categories: [
                {
                    key: 'hdb',
                    value: 'HDB',
                    sub_categories: [
                        {
                            key: 'hdb_2r',
                            value: 'HDB 2 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_3r',
                            value: 'HDB 3 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_4r',
                            value: 'HDB 4 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_5r',
                            value: 'HDB 5 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_6r',
                            value: 'HDB 6 rooms',
                            checked: false
                        },
                        {
                            key: 'hdb_executive',
                            value: 'HDB Executive',
                            checked: false
                        }
                    ]
                },
                {
                    key: 'condo',
                    value: 'Condo',
                    sub_categories : [
                        {
                            key: 'generic_condo',
                            value: 'Generic Condo',
                            checked: false
                        },
                        {
                            key: 'condo_apartment',
                            value: 'Apartment',
                            checked: false
                        },
                        {
                            key: 'walkup',
                            value: 'Walk-up',
                            checked: false
                        }
                    ]
                },
                {
                    key: 'landed',
                    value: 'Landed',
                    sub_categories : [
                        {
                            key: 'terraced_house',
                            value: 'Terraced House',
                            checked: false
                        },
                        {
                            key: 'corner_terrace',
                            value: 'Corner Terrace',
                            checked: false
                        },
                        {
                            key: 'semi_detached',
                            value: 'Semi-detached house',
                            checked: false
                        },
                        {
                            key: 'bungalow',
                            value: 'Bungalow / Detached House',
                            checked: false
                        },
                        {
                            key: 'goodclass_bungalow',
                            value: 'Good Class Bungalow',
                            checked: false
                        },
                        {
                            key: 'shophouse',
                            value: 'Shophouse',
                            checked: false
                        },
                        {
                            key: 'conservationhouse',
                            value: 'Conservation House',
                            checked: false
                        },
                        {
                            key: 'townhouse',
                            value: 'Townhouse',
                            checked: false
                        },
                        {
                            key: 'clusterhouse',
                            value: 'Cluster House',
                            checked: false
                        },
                        {
                            key: 'landonly',
                            value: 'Land Only',
                            checked: false
                        },
                    ]
                }
            ]
        },
        {
            key: 'commercial',
            value: 'Commercial',
            main_categories : [
                {
                    key: 'retail',
                    value: 'Retail'
                },
                {
                    key: 'office',
                    value: 'Office'
                },
            ]
        },
        {
            key: 'industrial',
            value: 'Industrial',
            main_categories : [
                {
                    key: 'general_industrial',
                    value: 'General Industrial'
                },
                {
                    key: 'warehouse',
                    value: 'Warehouse'
                },
                {
                    key: 'showroom',
                    value: 'showroom'
                },
                {
                    key: 'dormitory',
                    value: 'dormitory'
                },
                {
                    key: 'ebiz',
                    value: 'E-Business'
                },
                {
                    key: 'fnb',
                    value: 'F&B / Canteen'
                },
                {
                    key: 'childcare',
                    value: 'Childcare'
                },
                {
                    key: 'industrial_office',
                    value: 'Industrial Office'
                },
                {
                    key: 'shop',
                    value: 'Shop'
                },
                {
                    key: 'factory',
                    value: 'Factory'
                },
            ]
        },
        {
            key: 'land',
            value: 'Land',
            main_categories : [
                {
                    key: 'general_land',
                    value: 'General Land'
                },
                {
                    key: 'land_w_building',
                    value: 'Land with Building'
                },
            ]
        }
    ]

    public form = {
        update: false,
        pca_member_id: 0,
        property_segment: '',
        main_category: '',
        sub_category: '',
        land_use: '',
        property_name: '',
        district:'',
        lat: 0,
        lng: 0,
        address: '',
        type: '',
        id: '',
        photo_url: '',
        listing_type: '',
        hdb_type: '',
        bedrooms: '',
        floor_size: '',
        floor_size_type:'sqft',
        area_size: '',
        price: '',
        agents_to_market: '',
        highlights: '',
        description: '',
        floor: '',
        age_of_property: '',
        last_renovation_done: '',
        ceiling_height: '',
        unit_no: '',
        number_of_bathroom: '',
        date_of_availbility: '',
        facing: '',
        tenancy_lease_expiration: '',
        tenancy_current_rent: '',
        tenure: '',
        features: '',
        other_restrictions: '',
        youtube_video: '',
        iframe_v360: ''
    }
    public uploadSucceed = 0;
    public featuresOptions = [
        {
            'value':'balcony',
            'label':'Balcony',
            checked: false
         },
         {
            'value':'bomb_shelter',
            'label':'Bomb Shelter',
            checked: false
         },
         {
            'value':'city_view',
            'label':'City View',
            checked: false
         },
         {
            'value':'dual_key',
            'label':'Dual Key',
            checked: false
         },
         {
            'value':'duplex_maisonette',
            'label':'Duplex / Maisonette',
            checked: false
         },
         {
            'value':'fibre_ready',
            'label':'Fibre Ready',
            checked: false
         },
         {
            'value':'greenery_view',
            'label':'Greenery View',
            checked: false
         },
         {
            'value':'ground_floor',
            'label':'Ground Floor',
            checked: false
         },
         {
            'value':'high_ceiling',
            'label':'High Ceiling',
            checked: false
         },
         {
            'value':'high_floor',
            'label':'High Floor',
            checked: false
         },
         {
            'value':'loft',
            'label':'Loft',
            checked: false
         },
         {
            'value':'low_floor',
            'label':'Low Floor',
            checked: false
         },
         {
            'value':'maid_room',
            'label':'Maid\'s Room',
            checked: false
         },
         {
            'value':'mid_floor',
            'label':'Mid Floor',
            checked: false
         },
         {
            'value':'patio_pes',
            'label':'Patio / PES',
            checked: false
         },
         {
            'value':'penthouse',
            'label':'Penthouse',
            checked: false
         },
         {
            'value':'pool_view',
            'label':'Pool View',
            checked: false
         },
         {
            'value':'renovated',
            'label':'Renovated',
            checked: false
         },
         {
            'value':'roof_terrace',
            'label':'Roof Terrace',
            checked: false
         },
         {
            'value':'sea_view',
            'label':'Sea View',
            checked: false
         },
         {
            'value':'top_floor',
            'label':'Top Floor',
            checked: false
         },
         {
            'value':'utility_room',
            'label':'Utility Room',
            checked: false
         }
    ]
    public shortFormMap = {
        hdb:'HDB',
        condo:'Condo',
        landed:'Landed',
        retail:'Retail',
        office:'Office',
        shophouse:'Shophouse',
        general_industrial:'General industrial',
        warehouse:'Warehouse',
        showroom:'Showroom',
        dormitory:'Dormitory',
        ebiz:'e-Business/Media',
        fnb:'F&B/Canteen',
        childcare:'Childcare',
        industrial_office:'Industrial office',
        shop:'Shop',
        factory:'Factory',
        general_land:'General land',
        land_w_building:'Land with building'
    }

    constructor(private _utilitiesService: UtilitiesService,
        private _httpService: HttpService,
        private badge: Badge,
        private _accountService: AccountService) {
        // this.categories = CATEGORIES;
    }

    addMatchingConnectCustomer(params, callback: ()=>void){
        this._accountService.checkUserSession();
        this._utilitiesService.showLoading();

        this._httpService.postRequest(CONFIGURATION.URL.addMatchingConnectCustomer, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getMatchingConnectCustomer(params, callback: ()=>void){
        this._accountService.checkUserSession();
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.getMatchingConnectCustomer, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.matchingConnectCustomer = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getMyPropertyListing(callback: ()=>void){
        this._accountService.checkUserSession();
        this._utilitiesService.showLoading();
        let params = [];
        params['u_id'] = this._accountService.userData.id;
        params['member_type'] = this._accountService.userData.member_type;
        this._httpService.postRequest(CONFIGURATION.URL.getMyPropertyListing, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.myPropertyListing = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getPublicPropertyListing(params, callback: ()=>void){
        this._accountService.checkUserSession();
        // this._utilitiesService.showLoading();
        // if(this._accountService.isUserSessionAlive()){
        //     params['u_id'] = this._accountService.userData.id;
        //     params['member_type'] = this._accountService.userData.member_type;
        // }
        let params_ = this._httpService.ObjecttoParams(params);
        let url = CONFIGURATION.apiEndpoint+CONFIGURATION.URL.searchPropertyListing+'?'+params_;
        this._httpService.getRequestRaw(url).subscribe(response => {

            // this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.publicPropertyListing = [];
                    if(response.data.data.sections.length > 0){
                        this.publicPropertyListing = response.data.data.sections[0].listings;
                    }
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            // });

        },error => {
            // this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getRequestInformationList(callback: ()=>void){
        this._utilitiesService.showLoading();
        var params = [];
        params['pca_member_id'] = this._accountService.userData.id;

        this._httpService.postRequest(CONFIGURATION.URL.getRequestInformation, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.requestInformationList = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    deleteRequestInformation(params, callback: ()=>void){
        this._accountService.checkUserSession();
        this._utilitiesService.showLoading();

        this._httpService.postRequest(CONFIGURATION.URL.deleteRequestInformation, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    addRequestInformation(params, callback: ()=>void){
        this._accountService.checkUserSession();
        this._utilitiesService.showLoading();
        params['pca_member_id'] = this._accountService.userData.id;

        this._httpService.postRequest(CONFIGURATION.URL.addRequestInformation, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.searchListingParams.search_id = response.data.id;
                    if(response.data.message !== null){
                        this._utilitiesService.alertMessageCallback('Information', response.data.message, callback);
                    }
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    getRequestInformation(params, callback: ()=>void){

    }

    getPCAListingMatch(callback: ()=>void){
        let params_2 = {
            location_query: this.searchListingParams.location_name,
            pca_member_id: this._accountService.userData.id
        }
        this._utilitiesService.showLoading();
        if(this.searchListingParams.location_name !== ''){
            this._httpService.postRequest(CONFIGURATION.URL.getPCAPropertyListing, params_2).subscribe(response =>{
                this._utilitiesService.hideLoading().then(resolve => {
                    this.pcaPropertyListing = response.data;
                    localStorage.setItem('matchingConnectPca',JSON.stringify(this.pcaPropertyListing));
                    callback();
                });
            }, error => {
                this._utilitiesService.hideLoading().then(resolve => {
                    callback();
                });
                console.log(error);
            }); 
        }
    }

    searchListings(callback: ()=>void){
        this.propertyListingResult = [];
        this.propertyListingCount = 0;
        
        let params = this._httpService.ObjecttoParams(this.searchListingParams);
        let url = CONFIGURATION.apiEndpoint+CONFIGURATION.URL.searchPropertyListing+'?'+params;
        this._utilitiesService.showLoading();
        
        this._httpService.getRequestRaw(url).subscribe(response => {
            this._utilitiesService.hideLoading().then(resolve => {
                // if (response.status === 'OK') {
                    if(typeof response.data !== 'undefined' && response.data !== null){
                        if(response.data.data.sections.length > 0){
                            this.propertyListingResult = response.data.data.sections[0].listings;
                            this.propertyListingCount = response.data.data.count.total;
                            localStorage.setItem('matchingConnect99co',JSON.stringify(this.propertyListingResult));

                        }else{
                            this.propertyListingCount = 0;
                        }
                        
                        callback();
                    }else{
                        callback();
                    }
                    
                // } else {
                //     this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                // }
            });

        },error => {
            this._utilitiesService.hideLoading().then(resolve => {
                callback();
            });
            console.log(error);
        });
    }

    getLocationStepDetail(id,property_segment, main_category, callback: ()=>void){
        let url = 'https://www.99.co/api/v1/web/dashboard/listing-util/address/'+id+'?scale=4&property_segment='+property_segment+'&main_category='+main_category;
        this._httpService.getRequestRaw(url).subscribe(response => {
            // this._utilitiesService.hideLoading().then(resolve => {
                // if (response.status === 'OK') {
                    if(typeof response.data !== 'undefined'){
                        this.step1PropertyDetail = response.data.items;
                        callback();
                    }
                    
                // } else {
                //     this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                // }
            // });

        },error => {
            // this._utilitiesService.hideLoading().then(resolve => {
                callback();
            // });
            console.log(error);
        });
    }

    getInputRequestInfo(input, callback: ()=>void){
        let url = 'https://www.99.co/api/v2/web/autocomplete/location?input='+input;
        this._httpService.getRequestRaw(url).subscribe(response => {
            // this._utilitiesService.hideLoading().then(resolve => {
                // if (response.status === 'OK') {
                    if(typeof response.data !== 'undefined'){
                        this.searchInputRequestResult = response.data.sections;
                        callback();
                    }
                    
                // } else {
                //     this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                // }
            // });

        },error => {
            // this._utilitiesService.hideLoading().then(resolve => {
                callback();
            // });
            console.log(error);
        });
    }

    getLocationStep1(query, property_segment,callback: ()=>void){
        
        // this._utilitiesService.showLoading();
        let url = 'https://www.99.co/api/v1/web/dashboard/listing-util/address?query='+query+'&property_segment='+property_segment;
        this._httpService.getRequestRaw(url).subscribe(response => {
            // this._utilitiesService.hideLoading().then(resolve => {
                // if (response.status === 'OK') {
                    if(typeof response.data !== 'undefined'){
                        this.step1Location = response.data.items;
                        callback();
                    }
                    
                // } else {
                //     this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                // }
            // });

        },error => {
            // this._utilitiesService.hideLoading().then(resolve => {
                callback();
            // });
            console.log(error);
        });
    }

    uploadImageSignature(customer_id, imageUrl: any, callback: (res) => void) {
        this._accountService.checkUserSession();

        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var params = {
            customer_id: customer_id,
            client_id: CONFIGURATION.TOKEN.CLIENT_ID,
            client_token: CONFIGURATION.TOKEN.CLIENT_TOKEN,
            user_id: this._accountService.userData.id,
            user_type: this._accountService.userData.member_type
        };

        options.params = params;
        var ft = new FileTransfer();
        this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadCustomerSignature);
        console.log(params);
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadCustomerSignature, response => {
            console.log(response);
            let res = JSON.parse(response.response);
            navigator.camera.cleanup(success => {
                console.log('success');
            }, error => {
                console.log('fail');
            });
            this._utilitiesService.hideLoading().then(resp => {
                if (res.status === 'OK') {
                    
                    callback(res);
                } else {
                    this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                }
            })

        }, error => {
            this._utilitiesService.hideLoading().then(resp => {
                // error.code == FileTransferError.ABORT_ERR
                alert('An error has occurred: Code = ' + error.code);
                console.log('upload error source ' + error.source);
                console.log('upload error target ' + error.target);
            });

        }, options, true);

    }

    async uploadImage(params, imageUrl: any, fileName: any, callback: (res) => void, fallback: () => void) {

        // var options = new FileUploadOptions();
        params['client_id'] = CONFIGURATION.TOKEN.CLIENT_ID;
        params['client_token'] = CONFIGURATION.TOKEN.CLIENT_TOKEN;
        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        options.params = params;
        var ft = new FileTransfer();

        // const ft: FileTransferObject = this.transfer.create();
        // this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadPropertyImage);
        console.log(options);
        await ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadPropertyImage, response => {
            console.log(response);
            let res = JSON.parse(response.response);
            navigator.camera.cleanup(success => {
                console.log('success');
            }, error => {
                console.log('fail cleanup');
            });
            // this._utilitiesService.hideLoading().then(resp => {
            if (res.status === 'OK') {
                res.data.fileName = fileName;
                callback(res.data);
                this.uploadSucceed++;
            } else {
                this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                fallback();
            }
            // })

        }, error => {
            // this._utilitiesService.hideLoading().then(resp => {
            // error.code == FileTransferError.ABORT_ERR
            console.log(error);
            // alert('An error has occurred: Code = ' + error.code);
            console.log('upload error source ' + error.source);
            console.log('upload error target ' + error.target);
            fallback();
            // });

        }, options, true);

    }

    createNewListing(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        if(this.form.update == true){
            var url = CONFIGURATION.URL.updatePropertyListing;
        }else{
            var url = CONFIGURATION.URL.addNewPropertyListing;
        }
        this._httpService.postRequest(url, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.propertyListingDetail = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    updateListing(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updatePropertyListing, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    this.propertyListingDetail = response.data;
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    deleteListing(params, callback: ()=>void){
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.deletePropertyListing, params).subscribe(response => {

            this._utilitiesService.hideLoading().then(resp => {
                if (response.status === 'OK') {
                    callback();
                } else {
                    this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });

        },error => {
            this._utilitiesService.hideLoading();
            console.log(error);
        });
    }

    

}
