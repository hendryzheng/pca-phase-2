import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Location } from '@angular/common';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
var AddListingStepBasicInfoPage = /** @class */ (function () {
    function AddListingStepBasicInfoPage(_propertyListingService, _utilitiesService, router, location) {
        this._propertyListingService = _propertyListingService;
        this._utilitiesService = _utilitiesService;
        this.router = router;
        this.location = location;
    }
    AddListingStepBasicInfoPage.prototype.ngOnInit = function () {
    };
    AddListingStepBasicInfoPage.prototype.getStep1PropertyDetail = function () {
        return this._propertyListingService.step1PropertyDetail;
    };
    AddListingStepBasicInfoPage.prototype.getStep1SelectedLocation = function () {
        return this._propertyListingService.step1SelectedLocation;
    };
    AddListingStepBasicInfoPage.prototype.goBack = function () {
        this.location.back();
    };
    AddListingStepBasicInfoPage.prototype.getShortFormMap = function () {
        return this._propertyListingService.shortFormMap;
    };
    AddListingStepBasicInfoPage.prototype.getForm = function () {
        return this._propertyListingService.form;
    };
    AddListingStepBasicInfoPage.prototype.continue = function () {
        if (this.validateForm()) {
            this.router.navigateByUrl('/add-listing-step-details');
        }
        else {
            this._utilitiesService.alertMessage('Incomplete information', 'Please fill up all information to proceed');
        }
    };
    AddListingStepBasicInfoPage.prototype.validateForm = function () {
        var validate = true;
        if (this.getForm().listing_type == '') {
            validate = false;
        }
        if (this.getForm().floor_size == '') {
            validate = false;
        }
        if (this.getForm().floor_size_type == '') {
            validate = false;
        }
        if (this.getForm().price == '') {
            validate = false;
        }
        if (this.getForm().agents_to_market == '') {
            validate = false;
        }
        return validate;
    };
    AddListingStepBasicInfoPage = tslib_1.__decorate([
        Component({
            selector: 'app-add-listing-step-basic-info',
            templateUrl: './add-listing-step-basic-info.page.html',
            styleUrls: ['./add-listing-step-basic-info.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [PropertyListingService,
            UtilitiesService,
            Router,
            Location])
    ], AddListingStepBasicInfoPage);
    return AddListingStepBasicInfoPage;
}());
export { AddListingStepBasicInfoPage };
//# sourceMappingURL=add-listing-step-basic-info.page.js.map