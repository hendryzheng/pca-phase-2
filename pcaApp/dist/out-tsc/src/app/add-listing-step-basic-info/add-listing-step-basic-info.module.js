import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddListingStepBasicInfoPage } from './add-listing-step-basic-info.page';
var routes = [
    {
        path: '',
        component: AddListingStepBasicInfoPage
    }
];
var AddListingStepBasicInfoPageModule = /** @class */ (function () {
    function AddListingStepBasicInfoPageModule() {
    }
    AddListingStepBasicInfoPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AddListingStepBasicInfoPage]
        })
    ], AddListingStepBasicInfoPageModule);
    return AddListingStepBasicInfoPageModule;
}());
export { AddListingStepBasicInfoPageModule };
//# sourceMappingURL=add-listing-step-basic-info.module.js.map