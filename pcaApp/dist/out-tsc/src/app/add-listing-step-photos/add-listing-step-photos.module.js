import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddListingStepPhotosPage } from './add-listing-step-photos.page';
var routes = [
    {
        path: '',
        component: AddListingStepPhotosPage
    }
];
var AddListingStepPhotosPageModule = /** @class */ (function () {
    function AddListingStepPhotosPageModule() {
    }
    AddListingStepPhotosPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AddListingStepPhotosPage]
        })
    ], AddListingStepPhotosPageModule);
    return AddListingStepPhotosPageModule;
}());
export { AddListingStepPhotosPageModule };
//# sourceMappingURL=add-listing-step-photos.module.js.map