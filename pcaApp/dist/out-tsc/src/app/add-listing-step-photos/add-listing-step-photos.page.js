import * as tslib_1 from "tslib";
import { Component, NgZone } from '@angular/core';
import { PropertyListingService } from 'src/services/property-listing.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { Crop } from '@ionic-native/crop/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { MenuService } from 'src/services/menu.service';
var AddListingStepPhotosPage = /** @class */ (function () {
    function AddListingStepPhotosPage(_propertyListingService, _utilitiesService, _menuService, router, imagePicker, crop, file, _zone, photoViewer, base64, _DomSanitizationService, location) {
        this._propertyListingService = _propertyListingService;
        this._utilitiesService = _utilitiesService;
        this._menuService = _menuService;
        this.router = router;
        this.imagePicker = imagePicker;
        this.crop = crop;
        this.file = file;
        this._zone = _zone;
        this.photoViewer = photoViewer;
        this.base64 = base64;
        this._DomSanitizationService = _DomSanitizationService;
        this.location = location;
        this.fileUrl = null;
        this.imageArr = [];
        this.imageArrDisplay = [];
        this.imageDelete = [];
        this.imageUploading = false;
        this.interval = null;
    }
    AddListingStepPhotosPage.prototype.ngOnInit = function () {
    };
    AddListingStepPhotosPage.prototype.ionViewWillEnter = function () {
        this._menuService.checkUserSession();
        this._propertyListingService.form.pca_member_id = this._menuService.getUserData().id;
    };
    AddListingStepPhotosPage.prototype.getStep1PropertyDetail = function () {
        return this._propertyListingService.step1PropertyDetail;
    };
    AddListingStepPhotosPage.prototype.getStep1SelectedLocation = function () {
        return this._propertyListingService.step1SelectedLocation;
    };
    AddListingStepPhotosPage.prototype.goBack = function () {
        this.location.back();
    };
    AddListingStepPhotosPage.prototype.getShortFormMap = function () {
        return this._propertyListingService.shortFormMap;
    };
    AddListingStepPhotosPage.prototype.getForm = function () {
        return this._propertyListingService.form;
    };
    AddListingStepPhotosPage.prototype.view = function (item) {
        this.photoViewer.show(item.path, '');
    };
    AddListingStepPhotosPage.prototype.takePhotoOption = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._utilitiesService.getAlertCtrl().create({
                            header: 'Choose your option',
                            // message: message,
                            buttons: [
                                {
                                    text: 'Take Photo',
                                    handler: function (data) {
                                        _this.selectImageCamera();
                                        console.log('OK clicked');
                                    }
                                },
                                {
                                    text: 'Choose From Gallery',
                                    handler: function (data) {
                                        _this.selectImageGallery();
                                        console.log('OK clicked');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    AddListingStepPhotosPage.prototype.selectImageCamera = function () {
        this.actionSelectPicture(Camera.PictureSourceType.CAMERA);
    };
    AddListingStepPhotosPage.prototype.continue = function () {
        var _this = this;
        console.log(this.getForm());
        this._propertyListingService.createNewListing(this.getForm(), function () {
            if (_this.imageArr.length > 0) {
                _this._utilitiesService.showLoadingSyncImages();
                _this._propertyListingService.uploadSucceed = 0;
                for (var i = 0; i < _this.imageArr.length; i++) {
                    if (_this.imageArr[i].uploaded == false) {
                        var params = {
                            'id': _this._propertyListingService.propertyListingDetail.id
                        };
                        _this._propertyListingService.uploadImage(params, _this.imageArr[i].path, _this.imageArr[i].fileName, function (res) {
                            console.log(_this.imageArr);
                            console.log(res);
                            for (var j = 0; j < _this.imageArr.length; j++) {
                                if (_this.imageArr[j].fileName == res.fileName) {
                                    _this.imageArr[j].uploaded = true;
                                    _this.imageArr[j].path = res.imageUrl;
                                }
                            }
                        }, function () {
                            _this._utilitiesService.hideLoading().then(function (resp) {
                                _this._utilitiesService.alertMessage('Error', 'There is a problem with image uploading at the moment. Please try again later');
                            });
                        });
                    }
                }
                _this.interval = setInterval(function () {
                    var count = 0;
                    console.log(_this.imageArr);
                    console.log(_this._propertyListingService.uploadSucceed);
                    for (var j = 0; j < _this.imageArr.length; j++) {
                        if (_this.imageArr[j].uploaded == true) {
                            count++;
                        }
                    }
                    if (count == _this.imageArr.length) {
                        clearInterval(_this.interval);
                        _this._utilitiesService.hideLoading().then(function (resp) {
                            setTimeout(function () {
                                _this._utilitiesService.alertMessageCallback('Upload success', 'Listing images uploaded successfully', function () {
                                    _this._menuService.eNamecardState = 'my-listing';
                                    _this.router.navigate(['/tabs'], { replaceUrl: true });
                                });
                            }, 1500);
                        });
                    }
                }, 1000);
            }
            else {
                _this._utilitiesService.alertMessageCallback('Upload success', 'Property listing created successfully', function () {
                    _this._menuService.eNamecardState = 'my-listing';
                    _this.router.navigate(['/tabs'], { replaceUrl: true });
                });
            }
        });
    };
    AddListingStepPhotosPage.prototype.actionSelectPicture = function (source) {
        var _this = this;
        navigator.camera.getPicture(function (imageData) {
            _this._zone.run(function () {
                // plugins.crop.promise(imageData, {
                //   quality: 5
                // }).then(newPath => {
                _this.base64.encodeFile(imageData).then(function (base64File) {
                    // console.log(base64File);
                    _this._zone.run(function () {
                        var temp = {
                            filePath: '-',
                            uploaded: false,
                            fileName: Math.random().toString(36).substring(10),
                            path: base64File
                        };
                        _this.imageArr.push(temp);
                        // var date = new Date();
                        // var timestamp = date.getTime();
                        // let filename = timestamp + '.jpeg';
                        // this.writeFile(base64File, filename);
                    });
                }, function (err) {
                    console.log(err);
                });
                // },
                //   error => {
                //     console.log('CROP ERROR -> ' + JSON.stringify(error));
                //     // alert('CROP ERROR: ' + JSON.stringify(error));
                //   }
                // );
            });
        }, function (error) {
            console.log(error);
        }, {
            destinationType: Camera.DestinationType.FILE_URI,
            quality: 5,
            correctOrientation: true,
            sourceType: source
        });
    };
    AddListingStepPhotosPage.prototype.selectImageGallery = function () {
        this.actionSelectPicture(Camera.PictureSourceType.SAVEDPHOTOALBUM);
    };
    AddListingStepPhotosPage.prototype.promptDelete = function (callback) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alertController, alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        alertController = this._utilitiesService.alertCtrl;
                        return [4 /*yield*/, alertController.create({
                                header: 'Confirmation',
                                message: 'Are you sure to delete this item?',
                                buttons: [
                                    {
                                        text: 'Yes',
                                        handler: function (data) {
                                            console.log('OK clicked');
                                            callback();
                                        }
                                    },
                                    {
                                        text: 'No',
                                        handler: function (data) {
                                            console.log('No clicked');
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [2 /*return*/, alert.present()];
                }
            });
        });
    };
    AddListingStepPhotosPage.prototype.delete = function (item) {
        var _this = this;
        console.log(item);
        this.promptDelete(function () {
            for (var i = 0; i < _this.imageArr.length; i++) {
                if (_this.imageArr[i].fileName == item.fileName) {
                    _this._zone.run(function () {
                        if (_this.imageArr[i].uploaded == true) {
                            _this.imageDelete.push(_this.imageArr[i].fileName);
                        }
                        _this.imageArr.splice(i, 1);
                        _this.imageArrDisplay.splice(i, 1);
                        // this.saveCurrentRecord();
                    });
                }
            }
        });
    };
    AddListingStepPhotosPage.prototype.readImgFile = function (filePath, fileName) {
        this.file.readAsDataURL(filePath, fileName).then(function (result) {
            return result;
        });
    };
    //here is the method is used to write a file in storage  
    AddListingStepPhotosPage.prototype.writeFile = function (base64Data, fileName) {
        var _this = this;
        var contentType = this.getContentType(base64Data);
        var DataBlob = this.base64toBlob(base64Data, contentType);
        // here iam mentioned this line this.file.dataDirectory is a native pre-defined file path storage. You can change a file path whatever pre-defined method.  
        var filePath = this.file.externalDataDirectory;
        this.file.writeFile(filePath, fileName, DataBlob, contentType).then(function (success) {
            _this.file.readAsDataURL(filePath, fileName).then(function (result) {
                // console.log(result);
                _this._zone.run(function () {
                    var temp = {
                        filePath: filePath,
                        fileName: fileName,
                        path: filePath + fileName
                    };
                    _this.imageArr.push(temp);
                    var temp_2 = {
                        filePath: filePath,
                        fileName: fileName,
                        path: result
                    };
                    _this.imageArrDisplay.push(temp_2);
                    // this.saveCurrentRecord();
                });
            });
            console.log("File Writed Successfully", success);
        }).catch(function (err) {
            console.log("Error Occured While Writing File", err);
        });
    };
    //here is the method is used to get content type of an bas64 data  
    AddListingStepPhotosPage.prototype.getContentType = function (base64Data) {
        var block = base64Data.split(";");
        var contentType = block[0].split(":")[1];
        return contentType;
    };
    //here is the method is used to convert base64 data to blob data  
    AddListingStepPhotosPage.prototype.base64toBlob = function (b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = atob(b64Data.replace(/^data:image\/\*;charset=utf-8;base64,/, ''));
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        var blob = new Blob([byteArray], {
            type: undefined
        });
        return blob;
    };
    AddListingStepPhotosPage.prototype.selectImageCrop = function () {
        var _this = this;
        this.imagePicker.getPictures({ maximumImagesCount: 1, outputType: 0 }).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                // console.log('Image URI: ' + results[i]);
                _this.crop.crop(results[i], { quality: 50 })
                    .then(function (newImage) {
                    // console.log('new image path is: ' + newImage);
                }, function (error) { return console.error('Error cropping image', error); });
            }
        }, function (err) { console.log(err); });
    };
    AddListingStepPhotosPage = tslib_1.__decorate([
        Component({
            selector: 'app-add-listing-step-photos',
            templateUrl: './add-listing-step-photos.page.html',
            styleUrls: ['./add-listing-step-photos.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [PropertyListingService,
            UtilitiesService,
            MenuService,
            Router,
            ImagePicker,
            Crop,
            File,
            NgZone,
            PhotoViewer,
            Base64,
            DomSanitizer,
            Location])
    ], AddListingStepPhotosPage);
    return AddListingStepPhotosPage;
}());
export { AddListingStepPhotosPage };
//# sourceMappingURL=add-listing-step-photos.page.js.map