import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { AccountService } from 'src/services/account.service';
import { Router } from '@angular/router';
import { MenuService } from 'src/services/menu.service';
import { Location } from '@angular/common';
var AddCustomerPage = /** @class */ (function () {
    function AddCustomerPage(_utilitiesService, router, location, _menuService, _accountService) {
        this._utilitiesService = _utilitiesService;
        this.router = router;
        this.location = location;
        this._menuService = _menuService;
        this._accountService = _accountService;
        this.form = {
            name: '',
            phone: '',
            email: '',
            gender: '',
            address: '',
            purpose_of_contact: '',
            listing_type: '',
            property_type: '',
            property_address: '',
            asking_price: '',
            built_in_area: '',
            type: '',
            pdpa_signed: false
        };
        this.purpose_of_contact = [
            {
                val: 'Follow up on your property availability',
                isChecked: false
            },
            {
                val: 'Potential Buyer / Tenant Enquiries',
                isChecked: false
            },
            {
                val: 'Property Market Update / Report',
                isChecked: false
            },
            {
                val: 'New Launches in Singapore',
                isChecked: false
            },
            {
                val: 'New Launches in Overseas',
                isChecked: false
            }
        ];
    }
    AddCustomerPage.prototype.ngOnInit = function () {
    };
    AddCustomerPage.prototype.ionViewWillEnter = function () {
        this._accountService.checkUserSession();
        this.form.type = this._menuService.customer_type;
    };
    AddCustomerPage.prototype.submit = function () {
    };
    AddCustomerPage.prototype.validateForm = function () {
        var validate = true;
        console.log(this.form);
        if (this._utilitiesService.isEmpty(this.form.name)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.phone)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.email)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.gender)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.address)) {
            validate = false;
        }
        if (this.form.pdpa_signed == false) {
            validate = false;
        }
        if (!validate) {
            this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details and ensure your customer has agreed to the PDPA consent');
        }
        console.log(validate);
        return validate;
    };
    AddCustomerPage.prototype.getCustomerType = function () {
        return this._menuService.customer_type;
    };
    AddCustomerPage.prototype.goBack = function () {
        this.location.back();
    };
    AddCustomerPage.prototype.submitWithPdpa = function () {
        var _this = this;
        if (this.validateForm()) {
            var formSubmission = {
                'Customer[fullname]': this.form.name,
                'Customer[email]': this.form.email,
                'Customer[mobile]': this.form.phone,
                'Customer[gender]': this.form.gender,
                'Customer[address]': this.form.address,
                'Customer[pca_member_id]': this._accountService.userData.id,
                'user_type': this._accountService.userData.member_type,
                'user_id': this._accountService.userData.id
            };
            this._menuService.submitAddNewCustomer(formSubmission, function () {
                _this.router.navigateByUrl('/property-marketing-consent');
            });
        }
    };
    AddCustomerPage.prototype.submitWithoutPdpa = function () {
        var _this = this;
        if (this.validateForm()) {
            var formSubmission = {
                'Customer[fullname]': this.form.name,
                'Customer[status]': 1,
                'Customer[email]': this.form.email,
                'Customer[mobile]': this.form.phone,
                'Customer[gender]': this.form.gender,
                'Customer[address]': this.form.address,
                'Customer[pca_member_id]': this._accountService.userData.id,
                'user_type': this._accountService.userData.member_type,
                'user_id': this._accountService.userData.id
            };
            this._menuService.submitAddNewCustomer(formSubmission, function () {
                _this.location.back();
            });
        }
    };
    AddCustomerPage = tslib_1.__decorate([
        Component({
            selector: 'app-add-customer',
            templateUrl: './add-customer.page.html',
            styleUrls: ['./add-customer.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [UtilitiesService,
            Router,
            Location,
            MenuService,
            AccountService])
    ], AddCustomerPage);
    return AddCustomerPage;
}());
export { AddCustomerPage };
//# sourceMappingURL=add-customer.page.js.map