import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddCustomerPage } from './add-customer.page';
var routes = [
    {
        path: '',
        component: AddCustomerPage
    }
];
var AddCustomerPageModule = /** @class */ (function () {
    function AddCustomerPageModule() {
    }
    AddCustomerPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AddCustomerPage]
        })
    ], AddCustomerPageModule);
    return AddCustomerPageModule;
}());
export { AddCustomerPageModule };
//# sourceMappingURL=add-customer.module.js.map