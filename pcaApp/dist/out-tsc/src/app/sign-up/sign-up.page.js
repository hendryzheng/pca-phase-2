import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
var SignUpPage = /** @class */ (function () {
    function SignUpPage(location, _accountService, _utilitiesService) {
        this.location = location;
        this._accountService = _accountService;
        this._utilitiesService = _utilitiesService;
        this.form = {
            firstname: '',
            lastname: '',
            gender: '',
            isAgent: false,
            email: '',
            password: '',
            confirm_password: '',
            mobile: '',
            agree: false
        };
    }
    SignUpPage.prototype.ngOnInit = function () {
    };
    SignUpPage.prototype.callbackSuccess = function () {
        this._utilitiesService.hideLoading();
        this.location.back();
    };
    SignUpPage.prototype.goBack = function () {
        this.location.back();
    };
    SignUpPage.prototype.toggleAgent = function (event) {
        console.log(event);
    };
    SignUpPage.prototype.ngAfterViewInit = function () {
        var tabs = document.querySelectorAll('.show-tabbar');
        if (tabs !== null) {
            Object.keys(tabs).map(function (key) {
                tabs[key].style.display = 'none';
            });
        }
    };
    SignUpPage.prototype.ionViewWillLeave = function () {
        var tabs = document.querySelectorAll('.show-tabbar');
        if (tabs !== null) {
            Object.keys(tabs).map(function (key) {
                tabs[key].style.display = 'flex';
            });
        }
    };
    SignUpPage.prototype.submitRegister = function () {
        var _this = this;
        if (this.validateForm()) {
            var formSubmission = {
                'Account[firstname]': this.form.firstname,
                'Account[lastname]': this.form.lastname,
                'Account[password]': this.form.password,
                'Account[email]': this.form.email,
                'Account[mobile]': this.form.mobile,
                'Account[gender]': this.form.gender,
                'isAgent': this.form.isAgent
            };
            this._accountService.register(formSubmission, function () {
                _this.callbackSuccess();
            });
        }
    };
    SignUpPage.prototype.validateForm = function () {
        var validate = true;
        console.log(this.form);
        if (this._utilitiesService.isEmpty(this.form.firstname)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.lastname)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.email)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.mobile)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.password)) {
            validate = false;
        }
        if (this.form.password !== this.form.confirm_password) {
            this._utilitiesService.alertMessage('Validation Error', 'Password not equal with Confirm Password');
            return false;
        }
        if (this.form.isAgent) {
            if (this.form.email.indexOf('@') >= 0) {
                this._utilitiesService.alertMessage('Validation Error', 'Please only include your email username without @');
                return false;
            }
        }
        if (this.form.agree === false) {
            this._utilitiesService.alertMessage('Validation Error', 'Please agree to the terms to proceed.');
            return false;
        }
        if (!validate) {
            this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
            return false;
        }
        console.log(validate);
        return validate;
    };
    // go to login page
    SignUpPage.prototype.login = function () {
        this.location.back();
    };
    SignUpPage = tslib_1.__decorate([
        Component({
            selector: 'app-sign-up',
            templateUrl: './sign-up.page.html',
            styleUrls: ['./sign-up.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location,
            AccountService,
            UtilitiesService])
    ], SignUpPage);
    return SignUpPage;
}());
export { SignUpPage };
//# sourceMappingURL=sign-up.page.js.map