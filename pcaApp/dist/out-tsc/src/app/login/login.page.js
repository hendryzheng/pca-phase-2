import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { HttpService } from 'src/services/http.service';
import { AlertController } from '@ionic/angular';
import { Location } from '@angular/common';
var LoginPage = /** @class */ (function () {
    function LoginPage(router, location, forgotCtrl, _utilitiesService, _httpService, _accountService) {
        this.router = router;
        this.location = location;
        this.forgotCtrl = forgotCtrl;
        this._utilitiesService = _utilitiesService;
        this._httpService = _httpService;
        this._accountService = _accountService;
        this.form = {
            email: '',
            password: ''
        };
        this.token = '';
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    // go to register page
    LoginPage.prototype.register = function () {
        this.router.navigateByUrl('/sign-up');
    };
    LoginPage.prototype.ngAfterViewInit = function () {
        // let tabs = document.querySelectorAll('.show-tabbar');
        // if (tabs !== null) {
        //   Object.keys(tabs).map((key) => {
        //     tabs[key].style.display = 'none';
        //   });
        // }
    };
    LoginPage.prototype.ionViewWillLeave = function () {
        // let tabs = document.querySelectorAll('.show-tabbar');
        // if (tabs !== null) {
        //   Object.keys(tabs).map((key) => {
        //     tabs[key].style.display = 'flex';
        //   });
        // }
    };
    LoginPage.prototype.goBack = function () {
        this.location.back();
    };
    LoginPage.prototype.callbackSuccess = function (response) {
        localStorage.setItem('userData', JSON.stringify(response));
        this._accountService.userData = response;
        this._accountService.registerToken();
        this.router.navigate(['/tabs'], { replaceUrl: true });
    };
    LoginPage.prototype.loginSubmit = function () {
        var _this = this;
        if (this.validateForm()) {
            this._utilitiesService.showLoading();
            this._httpService.postRequest(CONFIGURATION.URL.login, this.form).subscribe(function (response) {
                console.log(response);
                var responseHttp = response;
                _this._utilitiesService.hideLoading().then(function (response) {
                    if (responseHttp.status === 'OK') {
                        _this.callbackSuccess(responseHttp.data);
                    }
                    else {
                        _this._utilitiesService.alertMessage('Oops! Something happened', responseHttp.message);
                    }
                });
            }, function (error) {
                _this._utilitiesService.hideLoading();
                _this._utilitiesService.alertMessage('Oops! Something happened', '');
                console.log(error);
            });
        }
    };
    LoginPage.prototype.focusInput = function (event) {
        var total = 0;
        var container = null;
        var _rec = function (obj) {
            total += obj.offsetTop;
            var par = obj.offsetParent;
            if (par && par.localName !== 'ion-content') {
                _rec(par);
            }
            else {
                container = par;
            }
        };
        _rec(event.target);
        container.scrollToPoint(0, total - 50, 400);
    };
    LoginPage.prototype.validateForm = function () {
        var validate = true;
        console.log(this.form);
        if (this._utilitiesService.isEmpty(this.form.email)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.password)) {
            validate = false;
        }
        if (!validate) {
            this._utilitiesService.alertMessage('Oops! Something happened', CONFIGURATION.MESSAGE.REGISTER_MANDATORY);
        }
        console.log(validate);
        return validate;
    };
    LoginPage.prototype.forgotPwd = function () {
        alert('a');
        this.forgotPass();
    };
    LoginPage.prototype.forgotPass = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var forgot;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.forgotCtrl.create({
                            subHeader: 'Forgot Password?',
                            message: "Enter you email address to send a reset link password.",
                            inputs: [
                                {
                                    name: 'email',
                                    placeholder: 'Email',
                                    type: 'email'
                                },
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    handler: function (data) {
                                        console.log('Cancel clicked');
                                    }
                                },
                                {
                                    text: 'Send',
                                    handler: function (data) {
                                        console.log('Send clicked');
                                        var param = {
                                            email: data.email
                                        };
                                        _this._accountService.forgotPassword(param, function () {
                                        });
                                        // let toast = this.toastCtrl.create({
                                        //   message: 'Email was sended successfully',
                                        //   duration: 3000,
                                        //   position: 'top',
                                        //   cssClass: 'dark-trans',
                                        //   closeButtonText: 'OK',
                                        //   showCloseButton: true
                                        // });
                                        // toast.present();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        forgot = _a.sent();
                        return [4 /*yield*/, forgot.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            Location,
            AlertController,
            UtilitiesService,
            HttpService,
            AccountService])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.page.js.map