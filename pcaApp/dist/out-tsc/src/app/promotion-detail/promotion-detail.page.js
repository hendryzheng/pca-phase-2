import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { MenuService } from 'src/services/menu.service';
import { Location } from '@angular/common';
var PromotionDetailPage = /** @class */ (function () {
    function PromotionDetailPage(_utilitiesService, ga, location, _menuService) {
        this._utilitiesService = _utilitiesService;
        this.ga = ga;
        this.location = location;
        this._menuService = _menuService;
        this.form = {
            name: '',
            email: '',
            phone: '',
            agent_name: '',
            agent_email: '',
            agent_phone: ''
        };
    }
    PromotionDetailPage.prototype.ngOnInit = function () {
    };
    PromotionDetailPage.prototype.goBack = function () {
        this.location.back();
    };
    PromotionDetailPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad PromotionDetailPage');
        this.ga.startTrackerWithId('UA-121102215-1')
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView('Promotion Detail > ' + _this.getSelectedPromotion().title);
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
    };
    PromotionDetailPage.prototype.getSelectedPromotion = function () {
        return this._menuService.selectedPromotion;
    };
    PromotionDetailPage.prototype.submit = function () {
        var _this = this;
        if (this.validateForm()) {
            var formSubmission = {
                'Form[name]': this.form.name,
                'Form[email]': this.form.email,
                'Form[phone]': this.form.phone,
                'Form[agent_name]': this.form.agent_name,
                'Form[agent_email]': this.form.agent_email,
                'Form[agent_phone]': this.form.agent_phone,
                'Form[promotion_id]': this.getSelectedPromotion().id
            };
            this._menuService.submitPromotionContactForm(formSubmission, function () {
                _this._utilitiesService.alertMessageCallback('Success', 'Form submitted successfully', function () {
                    _this.form = {
                        name: '',
                        email: '',
                        phone: '',
                        agent_name: '',
                        agent_email: '',
                        agent_phone: ''
                    };
                });
            });
        }
    };
    PromotionDetailPage.prototype.validateForm = function () {
        var validate = true;
        console.log(this.form);
        if (this._utilitiesService.isEmpty(this.form.name)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.email)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.phone)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.agent_name)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.agent_email)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.agent_phone)) {
            validate = false;
        }
        if (!validate) {
            this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
        }
        console.log(validate);
        return validate;
    };
    PromotionDetailPage = tslib_1.__decorate([
        Component({
            selector: 'app-promotion-detail',
            templateUrl: './promotion-detail.page.html',
            styleUrls: ['./promotion-detail.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [UtilitiesService,
            GoogleAnalytics,
            Location,
            MenuService])
    ], PromotionDetailPage);
    return PromotionDetailPage;
}());
export { PromotionDetailPage };
//# sourceMappingURL=promotion-detail.page.js.map