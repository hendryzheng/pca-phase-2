import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PromotionDetailPage } from './promotion-detail.page';
var routes = [
    {
        path: '',
        component: PromotionDetailPage
    }
];
var PromotionDetailPageModule = /** @class */ (function () {
    function PromotionDetailPageModule() {
    }
    PromotionDetailPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PromotionDetailPage]
        })
    ], PromotionDetailPageModule);
    return PromotionDetailPageModule;
}());
export { PromotionDetailPageModule };
//# sourceMappingURL=promotion-detail.module.js.map