import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
var AddListingStepAddressPage = /** @class */ (function () {
    function AddListingStepAddressPage(_accountService, router, location, _propertyListingService) {
        this._accountService = _accountService;
        this.router = router;
        this.location = location;
        this._propertyListingService = _propertyListingService;
        this.search = '';
        this.disabled = true;
        this.locationList = [];
    }
    AddListingStepAddressPage.prototype.ngOnInit = function () {
    };
    AddListingStepAddressPage.prototype.getForm = function () {
        return this._propertyListingService.form;
    };
    AddListingStepAddressPage.prototype.goBack = function () {
        this.location.back();
    };
    AddListingStepAddressPage.prototype.change = function (ev) {
        console.log(ev.target.value);
        if (ev.target.value == 'industrial' || ev.target.value == 'commercial' || ev.target.value == 'land') {
            this.disabled = true;
        }
        else {
            this.disabled = false;
        }
        this.getForm().main_category = '';
        this.getForm().sub_category = '';
        this.getForm().land_use = '';
        this.locationList = [];
    };
    AddListingStepAddressPage.prototype.getShortFormMap = function () {
        return this._propertyListingService.shortFormMap;
    };
    AddListingStepAddressPage.prototype.changeMainCategory = function (ev) {
        if (ev.target.value == 'industrial') {
            this.disabled = true;
        }
        else {
            this.disabled = false;
        }
    };
    AddListingStepAddressPage.prototype.changeLandUse = function (ev) {
        this.disabled = false;
    };
    AddListingStepAddressPage.prototype.searchLocation = function ($ev) {
        var _this = this;
        var query = this.search;
        if (query == '') {
            this.locationList = [];
        }
        else {
            if (this.getForm().main_category !== '') {
                query += '&main_category=' + this.getForm().main_category;
            }
            if (this.getForm().land_use !== '') {
                query += '&land_use=' + this.getForm().land_use;
            }
            this._propertyListingService.getLocationStep1(query, this.getForm().property_segment, function () {
                _this.locationList = _this._propertyListingService.step1Location;
            });
        }
    };
    AddListingStepAddressPage.prototype.selectLocation = function (item) {
        var _this = this;
        this._propertyListingService.step1SelectedLocation = item;
        this._propertyListingService.form.address = item.address;
        this._propertyListingService.form.id = item.id;
        this._propertyListingService.form.lat = item.coordinates.lat;
        this._propertyListingService.form.lng = item.coordinates.lng;
        this._propertyListingService.form.district = item.district;
        this._propertyListingService.form.photo_url = item.photo_url;
        this._propertyListingService.form.property_name = item.name;
        this._propertyListingService.getLocationStepDetail(item.id, this.getForm().property_segment, item.type, function () {
            _this.router.navigateByUrl('/add-listing-step-basic-info');
        });
    };
    AddListingStepAddressPage = tslib_1.__decorate([
        Component({
            selector: 'app-add-listing-step-address',
            templateUrl: './add-listing-step-address.page.html',
            styleUrls: ['./add-listing-step-address.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AccountService,
            Router,
            Location,
            PropertyListingService])
    ], AddListingStepAddressPage);
    return AddListingStepAddressPage;
}());
export { AddListingStepAddressPage };
//# sourceMappingURL=add-listing-step-address.page.js.map