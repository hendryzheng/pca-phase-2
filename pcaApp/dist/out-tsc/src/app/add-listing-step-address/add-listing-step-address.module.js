import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddListingStepAddressPage } from './add-listing-step-address.page';
var routes = [
    {
        path: '',
        component: AddListingStepAddressPage
    }
];
var AddListingStepAddressPageModule = /** @class */ (function () {
    function AddListingStepAddressPageModule() {
    }
    AddListingStepAddressPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AddListingStepAddressPage]
        })
    ], AddListingStepAddressPageModule);
    return AddListingStepAddressPageModule;
}());
export { AddListingStepAddressPageModule };
//# sourceMappingURL=add-listing-step-address.module.js.map