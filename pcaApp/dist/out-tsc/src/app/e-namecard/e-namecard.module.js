import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ENamecardPage } from './e-namecard.page';
var routes = [
    {
        path: '',
        component: ENamecardPage
    }
];
var ENamecardPageModule = /** @class */ (function () {
    function ENamecardPageModule() {
    }
    ENamecardPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ENamecardPage]
        })
    ], ENamecardPageModule);
    return ENamecardPageModule;
}());
export { ENamecardPageModule };
//# sourceMappingURL=e-namecard.module.js.map