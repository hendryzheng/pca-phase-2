import * as tslib_1 from "tslib";
import { Component, NgZone } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { MenuService } from 'src/services/menu.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { PropertyListingService } from 'src/services/property-listing.service';
import { UtilitiesService } from 'src/services/utilities.service';
var ENamecardPage = /** @class */ (function () {
    function ENamecardPage(_accountService, location, router, _utilitiesService, _DomSanitizationService, _zone, _menuService, _propertyListingService) {
        this._accountService = _accountService;
        this.location = location;
        this.router = router;
        this._utilitiesService = _utilitiesService;
        this._DomSanitizationService = _DomSanitizationService;
        this._zone = _zone;
        this._menuService = _menuService;
        this._propertyListingService = _propertyListingService;
        this.isNotif = true;
        this.notifState = 'properties';
        this.customerState = 'PDPA';
        this.loaded = false;
        this.img_url = 'assets/img/person-placeholder.png';
        this.customerList = [];
        this.myListing = [];
        this.showContact = false;
        this.contact = [];
    }
    ENamecardPage.prototype.ngOnInit = function () {
    };
    ENamecardPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            this.loaded = false;
            this._accountService.getUserDetail(function () {
                if (_this._accountService.userData.profile_picture !== null) {
                    _this.img_url = CONFIGURATION.base_url + _this._accountService.userData.profile_picture;
                }
                _this.loaded = true;
            });
        }
        if (this._menuService.eNamecardState !== '') {
            this.notifState = this._menuService.eNamecardState;
            this.toggleNotif(this.notifState);
            this._menuService.eNamecardState = '';
        }
        else {
            this.fetchMyPropertyListing();
        }
    };
    ENamecardPage.prototype.addCustomer = function (type) {
        this._menuService.customer_type = type;
        this.router.navigateByUrl('/add-customer');
    };
    ENamecardPage.prototype.toggleNotif = function (string) {
        this.notifState = string;
        if (this.notifState == 'video-marketing') {
            this.fetchVideoMarketing();
        }
        else if (this.notifState == 'customer') {
            this.fetchCustomerData();
        }
        else if (this.notifState == 'my-listing') {
            this.fetchMyPropertyListing();
        }
    };
    ENamecardPage.prototype.toggleCustomer = function (string) {
        this.customerState = string;
    };
    ENamecardPage.prototype.getUserData = function () {
        return this._accountService.userData;
    };
    ENamecardPage.prototype.fetchCustomerData = function () {
        var _this = this;
        this.customerList = [];
        this._menuService.getCustomerData(function () {
            _this._zone.run(function () {
                _this.customerList = _this._menuService.customerList;
                console.log(_this.customerList);
            });
        });
    };
    ENamecardPage.prototype.fetchMyPropertyListing = function () {
        var _this = this;
        this.myListing = [];
        this._propertyListingService.getPublicPropertyListing(function () {
            _this.myListing = _this._propertyListingService.publicPropertyListing;
        });
    };
    ENamecardPage.prototype.contactAgent = function (item) {
        this.contact = item;
        this.showContact = true;
    };
    ENamecardPage.prototype.viewListing = function (item) {
        this._propertyListingService.selectedPropertyListingDetail = item;
        console.log(this._propertyListingService.selectedPropertyListingDetail);
        this.router.navigateByUrl('/property-listing');
    };
    ENamecardPage.prototype.deleteCustomerConfirmation = function (item) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._utilitiesService.getAlertCtrl().create({
                            header: 'Are you sure you want to delete this customer?',
                            // message: message,
                            buttons: [
                                {
                                    text: 'Yes',
                                    handler: function (data) {
                                        _this.deleteCustomer(item);
                                        console.log('OK clicked');
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    handler: function (data) {
                                        console.log('Cancel clicked');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ENamecardPage.prototype.editCustomer = function (item) {
        this._menuService.customer = item;
        this.router.navigateByUrl('/edit-customer');
    };
    ENamecardPage.prototype.deleteCustomer = function (item) {
        var params = {
            id: item.id
        };
        this._menuService.deleteCustomer(params, function () {
        });
    };
    ENamecardPage.prototype.viewCustomer = function (item) {
    };
    ENamecardPage.prototype.getCustomerData = function () {
        return this.customerList;
    };
    ENamecardPage.prototype.fetchVideoMarketing = function () {
        this._menuService.getVideoMarketing(function () {
        });
    };
    ENamecardPage.prototype.goBack = function () {
        this.location.back();
    };
    ENamecardPage.prototype.getVideoMarketing = function () {
        return this._menuService.videoMarketing;
    };
    ENamecardPage.prototype.getContact = function () {
        return this.contact;
    };
    ENamecardPage = tslib_1.__decorate([
        Component({
            selector: 'app-e-namecard',
            templateUrl: './e-namecard.page.html',
            styleUrls: ['./e-namecard.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AccountService,
            Location,
            Router,
            UtilitiesService,
            DomSanitizer,
            NgZone,
            MenuService,
            PropertyListingService])
    ], ENamecardPage);
    return ENamecardPage;
}());
export { ENamecardPage };
//# sourceMappingURL=e-namecard.page.js.map