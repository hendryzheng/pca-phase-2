import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CalculatorPage } from './calculator.page';
var routes = [
    {
        path: '',
        component: CalculatorPage
    }
];
var CalculatorPageModule = /** @class */ (function () {
    function CalculatorPageModule() {
    }
    CalculatorPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CalculatorPage]
        })
    ], CalculatorPageModule);
    return CalculatorPageModule;
}());
export { CalculatorPageModule };
//# sourceMappingURL=calculator.module.js.map