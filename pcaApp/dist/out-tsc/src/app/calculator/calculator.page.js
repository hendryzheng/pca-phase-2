import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
var CalculatorPage = /** @class */ (function () {
    function CalculatorPage(ga, iab, _menuService, _utilitiesService) {
        this.ga = ga;
        this.iab = iab;
        this._menuService = _menuService;
        this._utilitiesService = _utilitiesService;
        this.loaded = false;
        this.calculator = [];
    }
    CalculatorPage.prototype.ngOnInit = function () {
    };
    CalculatorPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._menuService.getCalculators(function () {
            _this.calculator = _this._menuService.calculators;
            _this.loaded = true;
        });
    };
    CalculatorPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CalculatorPage');
    };
    CalculatorPage.prototype.open = function (item) {
        var _this = this;
        this.ga.startTrackerWithId('UA-121102215-1')
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView('PCA Agent Tools - Calculator - ' + item.name);
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        this._utilitiesService.openBrowser(item.ame, item.link);
    };
    CalculatorPage = tslib_1.__decorate([
        Component({
            selector: 'app-calculator',
            templateUrl: './calculator.page.html',
            styleUrls: ['./calculator.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [GoogleAnalytics,
            InAppBrowser,
            MenuService,
            UtilitiesService])
    ], CalculatorPage);
    return CalculatorPage;
}());
export { CalculatorPage };
//# sourceMappingURL=calculator.page.js.map