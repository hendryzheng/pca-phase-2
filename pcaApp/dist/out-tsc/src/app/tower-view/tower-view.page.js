import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var TowerViewPage = /** @class */ (function () {
    function TowerViewPage(location) {
        this.location = location;
    }
    TowerViewPage.prototype.ngOnInit = function () {
    };
    TowerViewPage.prototype.goBack = function () {
        this.location.back();
    };
    TowerViewPage = tslib_1.__decorate([
        Component({
            selector: 'app-tower-view',
            templateUrl: './tower-view.page.html',
            styleUrls: ['./tower-view.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], TowerViewPage);
    return TowerViewPage;
}());
export { TowerViewPage };
//# sourceMappingURL=tower-view.page.js.map