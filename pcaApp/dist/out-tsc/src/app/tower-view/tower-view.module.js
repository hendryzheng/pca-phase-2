import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TowerViewPage } from './tower-view.page';
var routes = [
    {
        path: '',
        component: TowerViewPage
    }
];
var TowerViewPageModule = /** @class */ (function () {
    function TowerViewPageModule() {
    }
    TowerViewPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TowerViewPage]
        })
    ], TowerViewPageModule);
    return TowerViewPageModule;
}());
export { TowerViewPageModule };
//# sourceMappingURL=tower-view.module.js.map