import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { CONFIGURATION } from 'src/services/config.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Location } from '@angular/common';
var HomeMenuPage = /** @class */ (function () {
    function HomeMenuPage(_menuService, _accountService, router, location, ga, iab, _utilitiesService) {
        this._menuService = _menuService;
        this._accountService = _accountService;
        this.router = router;
        this.location = location;
        this.ga = ga;
        this.iab = iab;
        this._utilitiesService = _utilitiesService;
        this.menuList = [];
    }
    HomeMenuPage.prototype.ionViewWillEnter = function () {
        this.menuList = this._menuService.menu_list;
    };
    HomeMenuPage.prototype.ngOnInit = function () {
    };
    HomeMenuPage.prototype.getUserType = function () {
        if (this._accountService.isUserSessionAlive()) {
            return this._accountService.userData.member_type;
        }
        else {
            return '';
        }
    };
    HomeMenuPage.prototype.goBack = function () {
        this.location.back();
    };
    HomeMenuPage.prototype.alertPermission = function () {
        this._utilitiesService.alertMessage('Permission Denied', 'Your account is not authorized to access this menu');
    };
    HomeMenuPage.prototype.view = function (item) {
        var _this = this;
        this._menuService.selectedMenu = item;
        var allowed = false;
        if (item.access_status !== '1') {
            if (item.access_status === '2') {
                if (this.getUserType() === 'agent') {
                    allowed = true;
                }
                else {
                    allowed = false;
                    // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
                }
            }
            else if (item.access_status === '3') {
                if (this.getUserType() === 'account') {
                    allowed = true;
                    // this.router.navigateByUrl('/categories')
                }
                else {
                    allowed = false;
                    // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
                }
            }
            else {
                if (this.getUserType() === 'pca_member') {
                    allowed = true;
                    // this.router.navigateByUrl('/categories')
                }
                else {
                    allowed = false;
                    // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
                }
            }
        }
        else {
            allowed = true;
        }
        if (item.is_pca_affiliaties === '1') {
            this.ga.startTrackerWithId('UA-121102215-1')
                .then(function () {
                console.log('Google analytics is ready now');
                _this.ga.trackView('Homepage - PCA Affiliates & Partners');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
                .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
            if (allowed) {
                this.router.navigateByUrl('/pca-categories');
            }
            else {
                this.alertPermission();
            }
        }
        else if (item.is_connect_to_project === '1') {
            this.ga.startTrackerWithId('UA-121102215-1')
                .then(function () {
                console.log('Google analytics is ready now');
                _this.ga.trackView('Homepage - Connect to Portals');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
                .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
            if (allowed) {
                var base_url = CONFIGURATION.base_url;
                // this.iab.create(base_url + '/connect_to_project','_blank');
                // this._utilitiesService.openBrowser('Connect to Portals', base_url + '/connect_to_project');
                this.router.navigateByUrl('/connect-to-project');
            }
            else {
                this.alertPermission();
            }
        }
        else if (item.is_pca_agent_tools === '1') {
            this.ga.startTrackerWithId('UA-121102215-1')
                .then(function () {
                console.log('Google analytics is ready now');
                _this.ga.trackView('Homepage - PCA Agent Tools');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
                .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
            if (allowed) {
                // this.nav.push(PcaAgentToolsPage);
                this.router.navigateByUrl('/pca-agent-tools');
            }
            else {
                this.alertPermission();
            }
            // this._utilitiesService.openBrowser('Connect to Portals', CONFIGURATION.apiEndpoint + 'connect_to_project');
        }
        else {
            if (allowed) {
                if (item.menu_name.indexOf('Senior Leaders') !== -1) {
                    this._menuService.listing = item.listing;
                    this.router.navigateByUrl('/pca-senior-leaders');
                }
                else {
                    this.router.navigateByUrl('/categories');
                }
            }
            else {
                this.alertPermission();
            }
        }
    };
    HomeMenuPage = tslib_1.__decorate([
        Component({
            selector: 'app-home-menu',
            templateUrl: './home-menu.page.html',
            styleUrls: ['./home-menu.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            AccountService,
            Router,
            Location,
            GoogleAnalytics,
            InAppBrowser,
            UtilitiesService])
    ], HomeMenuPage);
    return HomeMenuPage;
}());
export { HomeMenuPage };
//# sourceMappingURL=home-menu.page.js.map