import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Base64 } from '@ionic-native/base64/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ThemeableBrowser } from '@ionic-native/themeable-browser/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { GoogleMaps } from '@ionic-native/google-maps/ngx';
import { Badge } from '@ionic-native/badge/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { AccountService } from 'src/services/account.service';
import { HttpService } from 'src/services/http.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { MenuService } from 'src/services/menu.service';
import { HttpModule } from '@angular/http';
import { SignaturePadModule } from 'angular2-signaturepad';
import { PropertyListingService } from 'src/services/property-listing.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [AppComponent],
            entryComponents: [],
            imports: [BrowserModule, HttpModule, IonicModule.forRoot({ scrollPadding: true,
                    scrollAssist: true }), AppRoutingModule, SignaturePadModule],
            providers: [
                Base64,
                StatusBar,
                SplashScreen,
                Keyboard,
                AccountService,
                HttpService,
                UtilitiesService,
                ImagePicker,
                PhotoViewer,
                FileTransfer,
                File,
                Crop,
                Camera,
                PhotoViewer,
                Base64,
                PropertyListingService,
                MenuService,
                InAppBrowser,
                Geolocation,
                BarcodeScanner,
                ThemeableBrowser,
                GoogleAnalytics,
                GoogleMaps,
                Badge,
                AppVersion,
                DatePicker,
                { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map