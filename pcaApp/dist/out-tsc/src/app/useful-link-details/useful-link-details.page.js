import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var UsefulLinkDetailsPage = /** @class */ (function () {
    function UsefulLinkDetailsPage() {
    }
    UsefulLinkDetailsPage.prototype.ngOnInit = function () {
    };
    UsefulLinkDetailsPage = tslib_1.__decorate([
        Component({
            selector: 'app-useful-link-details',
            templateUrl: './useful-link-details.page.html',
            styleUrls: ['./useful-link-details.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UsefulLinkDetailsPage);
    return UsefulLinkDetailsPage;
}());
export { UsefulLinkDetailsPage };
//# sourceMappingURL=useful-link-details.page.js.map