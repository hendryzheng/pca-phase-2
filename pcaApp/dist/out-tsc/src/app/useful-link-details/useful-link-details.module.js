import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { UsefulLinkDetailsPage } from './useful-link-details.page';
var routes = [
    {
        path: '',
        component: UsefulLinkDetailsPage
    }
];
var UsefulLinkDetailsPageModule = /** @class */ (function () {
    function UsefulLinkDetailsPageModule() {
    }
    UsefulLinkDetailsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [UsefulLinkDetailsPage]
        })
    ], UsefulLinkDetailsPageModule);
    return UsefulLinkDetailsPageModule;
}());
export { UsefulLinkDetailsPageModule };
//# sourceMappingURL=useful-link-details.module.js.map