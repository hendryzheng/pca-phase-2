import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var PcaEventsPage = /** @class */ (function () {
    function PcaEventsPage(location) {
        this.location = location;
    }
    PcaEventsPage.prototype.ngOnInit = function () {
    };
    PcaEventsPage.prototype.goBack = function () {
        this.location.back();
    };
    PcaEventsPage = tslib_1.__decorate([
        Component({
            selector: 'app-pca-events',
            templateUrl: './pca-events.page.html',
            styleUrls: ['./pca-events.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], PcaEventsPage);
    return PcaEventsPage;
}());
export { PcaEventsPage };
//# sourceMappingURL=pca-events.page.js.map