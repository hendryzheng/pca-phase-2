import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PcaEventsPage } from './pca-events.page';
var routes = [
    {
        path: '',
        component: PcaEventsPage
    }
];
var PcaEventsPageModule = /** @class */ (function () {
    function PcaEventsPageModule() {
    }
    PcaEventsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PcaEventsPage]
        })
    ], PcaEventsPageModule);
    return PcaEventsPageModule;
}());
export { PcaEventsPageModule };
//# sourceMappingURL=pca-events.module.js.map