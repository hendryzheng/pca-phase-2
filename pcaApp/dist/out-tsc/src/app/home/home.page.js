import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { PopoverController, IonSlides } from '@ionic/angular';
import { UtilitiesService } from 'src/services/utilities.service';
import { MenuService } from 'src/services/menu.service';
import { AccountService } from 'src/services/account.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Router } from '@angular/router';
import { CONFIGURATION } from 'src/services/config.service';
import { PropertyListingService } from 'src/services/property-listing.service';
var HomePage = /** @class */ (function () {
    function HomePage(popoverCtrl, _utilitiesService, _propertyListingService, _menuService, router, _accountService, ga, iab) {
        this.popoverCtrl = popoverCtrl;
        this._utilitiesService = _utilitiesService;
        this._propertyListingService = _propertyListingService;
        this._menuService = _menuService;
        this.router = router;
        this._accountService = _accountService;
        this.ga = ga;
        this.iab = iab;
        this.menuList = [];
        this.img_url = 'assets/img/person-placeholder.png';
        this.showSearch = false;
        this.listing_result = [];
        this.myInput = '';
        this.loaded = false;
        this.sliderOpts = {
            autoplay: true,
            pager: true,
            zoom: {
                maxRatio: 5
            }
        };
    }
    HomePage.prototype.ngOnInit = function () {
    };
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.menuList = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            this.loaded = false;
            this._accountService.getUserDetail(function () {
                if (_this._accountService.userData.profile_picture !== null) {
                    _this.img_url = CONFIGURATION.base_url + _this._accountService.userData.profile_picture;
                }
                _this.loaded = true;
            });
        }
        this._menuService.getMenu(function () {
            for (var i = 0; i < _this._menuService.menu_list.length; i++) {
                if (i < 10) {
                    _this.menuList.push(_this._menuService.menu_list[i]);
                }
            }
            _this.menuList = _this._utilitiesService.chunk(_this.menuList, 5);
            // this.menuList = this._utilitiesService.chunk(this._menuService.menu_list, 5);
            console.log(_this.menuList);
        });
        this._menuService.getBanner(function () {
            // this.Slides.startAutoplay();
        });
        this.ga.startTrackerWithId('UA-121102215-1')
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView('Homepage - PCA');
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
    };
    HomePage.prototype.onInput = function (ev) {
        var _this = this;
        console.log(ev);
        if (ev.type == 'input') {
            var params = {
                keyword: ev.target.value
            };
            if (ev.target.value == '') {
                this.showSearch = false;
            }
            else {
                this._menuService.searchListing(params, function () {
                    _this.showSearch = true;
                    _this.listing_result = _this._menuService.listing_result;
                });
            }
        }
    };
    HomePage.prototype.checkFocus = function () {
    };
    HomePage.prototype.updateProfile = function () {
        // this.nav.push(ProfilePage);
        this.router.navigateByUrl('/profile');
    };
    HomePage.prototype.checkBlur = function () {
        if (this.myInput == '') {
            this.showSearch = false;
            this.listing_result = [];
        }
    };
    HomePage.prototype.onCancel = function (ev) {
        console.log(ev);
        this.showSearch = false;
        this.listing_result = [];
    };
    HomePage.prototype.getIconPath = function (item) {
        if (item.icon_path !== null) {
            return item.icon_path;
        }
        else {
            return this._menuService.selectedMenu.icon_path;
        }
    };
    HomePage.prototype.getUserType = function () {
        if (this._accountService.isUserSessionAlive()) {
            return this._accountService.userData.member_type;
        }
        else {
            return '';
        }
    };
    HomePage.prototype.viewAllMatchingConnect = function () {
        // for (let key in this._propertyListingService.searchListingParams) {
        //   for (let k in this._menuService.matching_connect_obj) {
        //     if (key == k) {
        //       this._propertyListingService.searchListingParams[key] = this._menuService.matching_connect_obj[k];
        //       break;
        //     }
        //   }
        //   // Use `key` and `value`
        // }
        // this._propertyListingService.searchListingParams.search_id = this._menuService.matching_connect_obj.id;
        // this._propertyListingService.selectedKeywordSearch = this._menuService.matching_connect_obj.location_name;
        // console.log(this._propertyListingService.searchListingParams);
        this.router.navigateByUrl('/request-information-list');
    };
    HomePage.prototype.getMatchingConnect = function () {
        return this._menuService.matching_connect;
    };
    HomePage.prototype.getMatchingConnnectObj = function () {
        return this._menuService.matching_connect_obj;
    };
    HomePage.prototype.createSearch = function () {
        this._propertyListingService.searchListingParams = {
            search_id: '',
            location_name: '',
            location_subtitle: '',
            search_name: '',
            query_coords: '',
            listing_type: 'sale',
            main_category: 'hdb',
            keywords: '',
            sub_categories: '',
            query_type: '',
            query_ids: '',
            radius_max: 1000,
            price_min: 100000,
            price_max: 20000000,
            floorarea_min: 500,
            floorarea_max: 10000,
            rooms: 'any',
            is_new_launch: 'any',
            tenure: 'any',
            sort_field: '',
            sort_order: ''
        };
        this.router.navigateByUrl('/request-information');
    };
    HomePage.prototype.viewListing = function (item) {
        this._propertyListingService.selectedPropertyListingDetail = item;
        console.log(this._propertyListingService.selectedPropertyListingDetail);
        this.router.navigateByUrl('/property-listing');
    };
    HomePage.prototype.openAttachment = function (item) {
        var _this = this;
        var base_url = CONFIGURATION.apiEndpoint;
        this.ga.startTrackerWithId('UA-121102215-1')
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView('Homepage Menu ' + _this.getMenu() + ' > ' + item.listing_name);
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        if (item.listing_type == '2') {
            console.log(item.listing_attachment);
            this._utilitiesService.openBrowser(item.listing_name, item.listing_attachment);
            // const browser = this.iab.create(item.listing_attachment, '_blank', 'location=no');
        }
        else if (item.listing_type == '3') {
            console.log(item);
            window.InAppYouTube.openVideo(item.youtube_id, {
                fullscreen: true
            }, function (result) {
                console.log(JSON.stringify(result));
            }, function (reason) {
                console.log(reason);
            });
            // this.youtube.openVideo(item.youtube_id);
        }
        else {
            this._menuService.selected_listing = item;
            this._menuService.selected_listing.image_path = base_url + item.listing_attachment;
            this.router.navigateByUrl('/preview-image');
            // this._utilitiesService.openBrowser(item.listing_name, base_url + item.listing_attachment);
            // this.iab.create(base_url+item.listing_attachment, '_blank', 'location=no');
        }
    };
    HomePage.prototype.openSearch = function () {
        this.router.navigateByUrl('/search');
    };
    HomePage.prototype.getUserData = function () {
        return this._accountService.userData;
    };
    HomePage.prototype.isUserLoggedIn = function () {
        return this._accountService.isUserSessionAlive();
    };
    HomePage.prototype.alertPermission = function () {
        this._utilitiesService.alertMessage('Permission Denied', 'Your account is not authorized to access this menu');
    };
    HomePage.prototype.view = function (item) {
        var _this = this;
        this._menuService.selectedMenu = item;
        var allowed = false;
        if (item.access_status !== '1') {
            if (item.access_status === '2') {
                if (this.getUserType() === 'agent') {
                    allowed = true;
                }
                else {
                    allowed = false;
                    // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
                }
            }
            else if (item.access_status === '3') {
                if (this.getUserType() === 'account') {
                    allowed = true;
                    // this.router.navigateByUrl('/categories')
                }
                else {
                    allowed = false;
                    // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
                }
            }
            else {
                if (this.getUserType() === 'pca_member') {
                    allowed = true;
                    // this.router.navigateByUrl('/categories')
                }
                else {
                    allowed = false;
                    // this._utilitiesService.alertMessage('Permission Denied','Your account is not authorized to access this menu');
                }
            }
        }
        else {
            allowed = true;
        }
        if (item.is_pca_affiliaties === '1') {
            this.ga.startTrackerWithId('UA-121102215-1')
                .then(function () {
                console.log('Google analytics is ready now');
                _this.ga.trackView('Homepage - PCA Affiliates & Partners');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
                .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
            if (allowed) {
                this.router.navigateByUrl('/pca-categories');
            }
            else {
                this.alertPermission();
            }
        }
        else if (item.is_connect_to_project === '1') {
            this.ga.startTrackerWithId('UA-121102215-1')
                .then(function () {
                console.log('Google analytics is ready now');
                _this.ga.trackView('Homepage - Connect to Portals');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
                .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
            if (allowed) {
                var base_url = CONFIGURATION.base_url;
                // this.iab.create(base_url + '/connect_to_project','_blank');
                // this._utilitiesService.openBrowser('Connect to Portals', base_url + '/connect_to_project');
                this.router.navigateByUrl('/connect-to-project');
            }
            else {
                this.alertPermission();
            }
        }
        else if (item.is_pca_agent_tools === '1') {
            this.ga.startTrackerWithId('UA-121102215-1')
                .then(function () {
                console.log('Google analytics is ready now');
                _this.ga.trackView('Homepage - PCA Agent Tools');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
                .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
            if (allowed) {
                // this.nav.push(PcaAgentToolsPage);
                this.router.navigateByUrl('/pca-agent-tools');
            }
            else {
                this.alertPermission();
            }
            // this._utilitiesService.openBrowser('Connect to Portals', CONFIGURATION.apiEndpoint + 'connect_to_project');
        }
        else {
            if (allowed) {
                if (item.menu_name.indexOf('Senior Leaders') !== -1) {
                    this._menuService.listing = item.listing;
                    this.router.navigateByUrl('/pca-senior-leaders');
                }
                else {
                    this.router.navigateByUrl('/categories');
                }
            }
            else {
                this.alertPermission();
            }
        }
    };
    HomePage.prototype.openBanner = function (item) {
        var _this = this;
        this.ga.startTrackerWithId('UA-121102215-1')
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView('Homepage Banner - ' + item.listing_name);
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        if (item.listing_id !== null) {
            if (item.listing_type === '2') {
                // const browser = this.iab.create(item.listing_attachment, '_blank', 'location=no');
                this._utilitiesService.openBrowser(item.listing_name, item.listing_attachment);
            }
            else {
                var base_url = CONFIGURATION.base_url;
                // const browser = this.iab.create(base_url + item.listing_attachment, '_blank', 'location=no');
                this._utilitiesService.openBrowser(item.listing_name, base_url + item.listing_attachment);
            }
        }
        else if (item.url_link !== null && item.url_link !== '') {
            this._utilitiesService.openBrowser(item.url_link, item.url_link);
        }
        else if (item.pca_affiliates_listing_id !== null) {
            var temp = {
                id: item.pca_id,
                banner_image: item.pca_banner_image,
                title: item.pca_banner_title,
                description: item.pca_description
            };
            this._menuService.selected_pca_listing = temp;
            // this.nav.push(PcaContactFormPage);
        }
    };
    HomePage.prototype.presentNotifications = function (myEvent) {
        console.log(myEvent);
        // let popover = this.popoverCtrl.create(NotificationsPage);
        // popover.present({
        //   ev: myEvent
        // });
    };
    HomePage.prototype.getMenu = function () {
        return this._menuService.menu_list;
    };
    HomePage.prototype.getBanner = function () {
        return this._menuService.banner;
    };
    tslib_1.__decorate([
        ViewChild('mySlides'),
        tslib_1.__metadata("design:type", IonSlides)
    ], HomePage.prototype, "Slides", void 0);
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: './home.page.html',
            styleUrls: ['./home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [PopoverController,
            UtilitiesService,
            PropertyListingService,
            MenuService,
            Router,
            AccountService,
            GoogleAnalytics,
            InAppBrowser])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map