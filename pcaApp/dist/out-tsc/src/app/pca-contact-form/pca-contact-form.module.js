import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PcaContactFormPage } from './pca-contact-form.page';
var routes = [
    {
        path: '',
        component: PcaContactFormPage
    }
];
var PcaContactFormPageModule = /** @class */ (function () {
    function PcaContactFormPageModule() {
    }
    PcaContactFormPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PcaContactFormPage]
        })
    ], PcaContactFormPageModule);
    return PcaContactFormPageModule;
}());
export { PcaContactFormPageModule };
//# sourceMappingURL=pca-contact-form.module.js.map