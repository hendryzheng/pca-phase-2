import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
var PcaContactFormPage = /** @class */ (function () {
    function PcaContactFormPage(_menuService, location, _utilitiesService) {
        this._menuService = _menuService;
        this.location = location;
        this._utilitiesService = _utilitiesService;
        this.form = {
            name: '',
            email: '',
            phone: '',
            agent_name: '',
            agent_email: '',
            agent_phone: ''
        };
    }
    PcaContactFormPage.prototype.ngOnInit = function () {
    };
    PcaContactFormPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PcaContactFormPage');
    };
    PcaContactFormPage.prototype.getMenu = function () {
        return this._menuService.selectedMenu.menu_name;
    };
    PcaContactFormPage.prototype.getSelectedPcaCategory = function () {
        return this._menuService.selected_pca_category;
    };
    PcaContactFormPage.prototype.getSelectedPcaListing = function () {
        return this._menuService.selected_pca_listing;
    };
    PcaContactFormPage.prototype.submit = function () {
        var _this = this;
        if (this.validateForm()) {
            var formSubmission = {
                'Form[name]': this.form.name,
                'Form[email]': this.form.email,
                'Form[phone]': this.form.phone,
                'Form[agent_name]': this.form.agent_name,
                'Form[agent_email]': this.form.agent_email,
                'Form[agent_phone]': this.form.agent_phone,
                'Form[pca_affiliates_listing_id]': this.getSelectedPcaListing().id
            };
            this._menuService.submitContactForm(formSubmission, function () {
                _this._utilitiesService.alertMessageCallback('Success', 'Form submitted successfully', function () {
                    _this.form = {
                        name: '',
                        email: '',
                        phone: '',
                        agent_name: '',
                        agent_email: '',
                        agent_phone: ''
                    };
                });
            });
        }
    };
    PcaContactFormPage.prototype.validateForm = function () {
        var validate = true;
        console.log(this.form);
        if (this._utilitiesService.isEmpty(this.form.name)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.email)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.phone)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.agent_name)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.agent_email)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.agent_phone)) {
            validate = false;
        }
        if (!validate) {
            this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
        }
        console.log(validate);
        return validate;
    };
    PcaContactFormPage.prototype.openWebsite = function () {
        this._utilitiesService.openBrowser(this.getSelectedPcaListing().title, this.getSelectedPcaListing().website);
    };
    PcaContactFormPage.prototype.goBack = function () {
        this.location.back();
    };
    PcaContactFormPage = tslib_1.__decorate([
        Component({
            selector: 'app-pca-contact-form',
            templateUrl: './pca-contact-form.page.html',
            styleUrls: ['./pca-contact-form.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            Location,
            UtilitiesService])
    ], PcaContactFormPage);
    return PcaContactFormPage;
}());
export { PcaContactFormPage };
//# sourceMappingURL=pca-contact-form.page.js.map