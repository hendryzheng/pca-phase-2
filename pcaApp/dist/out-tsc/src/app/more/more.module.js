import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MorePage } from './more.page';
var routes = [
    {
        path: '',
        component: MorePage
    }
];
var MorePageModule = /** @class */ (function () {
    function MorePageModule() {
    }
    MorePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [MorePage]
        })
    ], MorePageModule);
    return MorePageModule;
}());
export { MorePageModule };
//# sourceMappingURL=more.module.js.map