import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var MorePage = /** @class */ (function () {
    function MorePage(location) {
        this.location = location;
    }
    MorePage.prototype.ngOnInit = function () {
    };
    MorePage.prototype.goBack = function () {
        this.location.back();
    };
    MorePage = tslib_1.__decorate([
        Component({
            selector: 'app-more',
            templateUrl: './more.page.html',
            styleUrls: ['./more.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], MorePage);
    return MorePage;
}());
export { MorePage };
//# sourceMappingURL=more.page.js.map