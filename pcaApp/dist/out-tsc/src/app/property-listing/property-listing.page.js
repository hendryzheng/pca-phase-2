import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
var PropertyListingPage = /** @class */ (function () {
    function PropertyListingPage(location, _utilitiesService, router, _propertyListingService) {
        this.location = location;
        this._utilitiesService = _utilitiesService;
        this.router = router;
        this._propertyListingService = _propertyListingService;
    }
    PropertyListingPage.prototype.ngOnInit = function () {
    };
    PropertyListingPage.prototype.goBack = function () {
        this.location.back();
    };
    PropertyListingPage.prototype.getPropertyListingDetail = function () {
        return this._propertyListingService.selectedPropertyListingDetail;
    };
    PropertyListingPage.prototype.showMap = function () {
        this.router.navigateByUrl('/map-view');
    };
    PropertyListingPage.prototype.whatsapp = function (phone) {
        return 'https://api.whatsapp.com/send?phone=' + this._utilitiesService.formatWhatsappNo(phone);
    };
    PropertyListingPage = tslib_1.__decorate([
        Component({
            selector: 'app-property-listing',
            templateUrl: './property-listing.page.html',
            styleUrls: ['./property-listing.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location,
            UtilitiesService,
            Router,
            PropertyListingService])
    ], PropertyListingPage);
    return PropertyListingPage;
}());
export { PropertyListingPage };
//# sourceMappingURL=property-listing.page.js.map