import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PropertyListingPage } from './property-listing.page';
var routes = [
    {
        path: '',
        component: PropertyListingPage
    }
];
var PropertyListingPageModule = /** @class */ (function () {
    function PropertyListingPageModule() {
    }
    PropertyListingPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PropertyListingPage]
        })
    ], PropertyListingPageModule);
    return PropertyListingPageModule;
}());
export { PropertyListingPageModule };
//# sourceMappingURL=property-listing.module.js.map