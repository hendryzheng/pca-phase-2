import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { Platform } from '@ionic/angular';
import { MenuService } from 'src/services/menu.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { UtilitiesService } from 'src/services/utilities.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(platform, _menuService, iab, _utilitiesService, _accountService, ga, router, location, barcodeScanner) {
        this.platform = platform;
        this._menuService = _menuService;
        this.iab = iab;
        this._utilitiesService = _utilitiesService;
        this._accountService = _accountService;
        this.ga = ga;
        this.router = router;
        this.location = location;
        this.barcodeScanner = barcodeScanner;
        this.img_url = 'assets/img/person-placeholder.png';
        this.loaded = false;
    }
    ProfilePage.prototype.ngOnInit = function () {
    };
    ProfilePage.prototype.goBack = function () {
        this.location.back();
    };
    ProfilePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._accountService.checkUserSession();
        if (this._accountService.userData.profile_picture !== null) {
            this.img_url = CONFIGURATION.base_url + this._accountService.userData.profile_picture;
        }
        setTimeout(function () {
            _this.loaded = true;
        }, 1000);
    };
    ProfilePage.prototype.getUserData = function () {
        return this._accountService.userData;
    };
    ProfilePage.prototype.openQR = function () {
        var _this = this;
        this.barcodeScanner.scan().then(function (barcodeData) {
            console.log('Barcode data', barcodeData);
            var text = barcodeData.text;
            console.log('Scanned something', text);
            _this._utilitiesService.openBrowser('QR Result', text);
        }).catch(function (err) {
            console.log('Error', err);
        });
    };
    ProfilePage.prototype.openMaps = function () {
        // this.navCtrl.push(MapsCategoriesPage);
        this.router.navigateByUrl('/maps');
    };
    ProfilePage.prototype.viewCompass = function () {
        this._utilitiesService.openBrowser('Compass', CONFIGURATION.apiEndpoint + 'compass');
    };
    ProfilePage.prototype.logout = function () {
        localStorage.clear();
        this._accountService.userData = null;
        this.router.navigate(['/splash-login'], { replaceUrl: true });
    };
    ProfilePage.prototype.isUserSessionAlive = function () {
        return this._accountService.isUserSessionAlive();
    };
    ProfilePage.prototype.share = function () {
        var url = 'PLAYSTORE/APPSTORE URL < URL HERE >';
        if (this.platform.is('ios')) {
            url = 'https://itunes.apple.com/us/app/property-connect-alliance/id1400685329?ls=1&mt=8';
        }
        else {
            url = 'https://play.google.com/store/apps/details?id=com.propertyconnectalliance.sg';
        }
        navigator.share({
            'title': 'Download and Install PropertyConnectAlliance App now !',
            'text': 'Download and Install PropertyConnectAlliance App now !',
            'url': url
        }).then(function () {
            console.log('Successful share');
        }).catch(function (error) {
            console.log('Error sharing:', error);
        });
    };
    ProfilePage.prototype.isLoggedIn = function () {
        return this._accountService.isUserSessionAlive();
    };
    ProfilePage = tslib_1.__decorate([
        Component({
            selector: 'app-profile',
            templateUrl: './profile.page.html',
            styleUrls: ['./profile.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            MenuService,
            InAppBrowser,
            UtilitiesService,
            AccountService,
            GoogleAnalytics,
            Router,
            Location,
            BarcodeScanner])
    ], ProfilePage);
    return ProfilePage;
}());
export { ProfilePage };
//# sourceMappingURL=profile.page.js.map