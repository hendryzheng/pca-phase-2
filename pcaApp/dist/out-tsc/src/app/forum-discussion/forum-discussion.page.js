import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { MenuService } from 'src/services/menu.service';
import { CONFIGURATION } from 'src/services/config.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
var ForumDiscussionPage = /** @class */ (function () {
    function ForumDiscussionPage(_accountService, router, location, _utilitiesService, _menuService) {
        this._accountService = _accountService;
        this.router = router;
        this.location = location;
        this._utilitiesService = _utilitiesService;
        this._menuService = _menuService;
        this.showDesc = true;
        this.form = {
            comment: ''
        };
        this.loaded = false;
    }
    ForumDiscussionPage.prototype.ngOnInit = function () {
    };
    ForumDiscussionPage.prototype.toggleDesc = function () {
        this.showDesc = !this.showDesc;
    };
    ForumDiscussionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CommentPage');
    };
    ForumDiscussionPage.prototype.getPost = function () {
        return this._menuService.selectedPost;
    };
    ForumDiscussionPage.prototype.viewImage = function (path) {
        this._utilitiesService.openBrowser('Preview', path);
    };
    ForumDiscussionPage.prototype.goBack = function () {
        this.location.back();
    };
    ForumDiscussionPage.prototype.openAccount = function (item) {
        console.log(item);
        this._accountService.viewUserData.id = item.pca_member_id;
        this._accountService.viewUserData.member_type = 'pca_member';
        this.router.navigateByUrl('/e-namecard-view');
    };
    ForumDiscussionPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._accountService.checkUserSession();
        this._menuService.getComments(function () {
            _this.loaded = true;
        });
    };
    ForumDiscussionPage.prototype.getComments = function () {
        return this._menuService.comments;
    };
    ForumDiscussionPage.prototype.getAccount = function () {
        return this._accountService.userData;
    };
    ForumDiscussionPage.prototype.validateForm = function () {
        var validate = true;
        console.log(this.form);
        if (this._utilitiesService.isEmpty(this.form.comment)) {
            validate = false;
        }
        if (!validate) {
            this._utilitiesService.alertMessage('Validation Error', 'Comment cannot be empty');
        }
        console.log(validate);
        return validate;
    };
    ForumDiscussionPage.prototype.submitComment = function () {
        var _this = this;
        if (this.validateForm()) {
            var formSubmission = {
                'Forumcomment[forum_post_id]': this.getPost().id,
                'Forumcomment[comment]': this.form.comment
            };
            if (this._accountService.userData.member_type === 'agent') {
                formSubmission['Forumcomment[agent_id]'] = this._accountService.userData.id;
            }
            else if (this._accountService.userData.member_type === 'pca_member') {
                formSubmission['Forumcomment[pca_member_id]'] = this._accountService.userData.id;
            }
            else {
                formSubmission['Forumcomment[account_id]'] = this._accountService.userData.id;
            }
            this._menuService.submitComment(formSubmission, function () {
                _this.form.comment = '';
                _this._utilitiesService.alertMessageCallback('Success', 'Comment submitted successfully', function () {
                    // this.navCtrl.pop();
                });
            });
        }
    };
    ForumDiscussionPage.prototype.isUserSessionAlive = function () {
        return this._accountService.isUserSessionAlive();
    };
    ForumDiscussionPage.prototype.getImagePath = function (item) {
        return CONFIGURATION.base_url + this.getPost().image;
    };
    ForumDiscussionPage = tslib_1.__decorate([
        Component({
            selector: 'app-forum-discussion',
            templateUrl: './forum-discussion.page.html',
            styleUrls: ['./forum-discussion.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AccountService,
            Router,
            Location,
            UtilitiesService,
            MenuService])
    ], ForumDiscussionPage);
    return ForumDiscussionPage;
}());
export { ForumDiscussionPage };
//# sourceMappingURL=forum-discussion.page.js.map