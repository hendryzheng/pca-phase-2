import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ForumDiscussionPage } from './forum-discussion.page';
var routes = [
    {
        path: '',
        component: ForumDiscussionPage
    }
];
var ForumDiscussionPageModule = /** @class */ (function () {
    function ForumDiscussionPageModule() {
    }
    ForumDiscussionPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ForumDiscussionPage]
        })
    ], ForumDiscussionPageModule);
    return ForumDiscussionPageModule;
}());
export { ForumDiscussionPageModule };
//# sourceMappingURL=forum-discussion.module.js.map