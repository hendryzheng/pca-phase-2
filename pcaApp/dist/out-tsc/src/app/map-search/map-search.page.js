import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var MapSearchPage = /** @class */ (function () {
    function MapSearchPage(location) {
        this.location = location;
    }
    MapSearchPage.prototype.ngOnInit = function () {
    };
    MapSearchPage.prototype.goBack = function () {
        this.location.back();
    };
    MapSearchPage = tslib_1.__decorate([
        Component({
            selector: 'app-map-search',
            templateUrl: './map-search.page.html',
            styleUrls: ['./map-search.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], MapSearchPage);
    return MapSearchPage;
}());
export { MapSearchPage };
//# sourceMappingURL=map-search.page.js.map