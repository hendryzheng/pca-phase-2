import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MapSearchPage } from './map-search.page';
var routes = [
    {
        path: '',
        component: MapSearchPage
    }
];
var MapSearchPageModule = /** @class */ (function () {
    function MapSearchPageModule() {
    }
    MapSearchPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [MapSearchPage]
        })
    ], MapSearchPageModule);
    return MapSearchPageModule;
}());
export { MapSearchPageModule };
//# sourceMappingURL=map-search.module.js.map