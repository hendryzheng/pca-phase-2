import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HomeMoreMenuPage } from './home-more-menu.page';
var routes = [
    {
        path: '',
        component: HomeMoreMenuPage
    }
];
var HomeMoreMenuPageModule = /** @class */ (function () {
    function HomeMoreMenuPageModule() {
    }
    HomeMoreMenuPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [HomeMoreMenuPage]
        })
    ], HomeMoreMenuPageModule);
    return HomeMoreMenuPageModule;
}());
export { HomeMoreMenuPageModule };
//# sourceMappingURL=home-more-menu.module.js.map