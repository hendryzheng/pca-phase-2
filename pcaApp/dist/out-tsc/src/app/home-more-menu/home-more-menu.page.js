import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
var HomeMoreMenuPage = /** @class */ (function () {
    function HomeMoreMenuPage(_menuService, location, router) {
        this._menuService = _menuService;
        this.location = location;
        this.router = router;
    }
    HomeMoreMenuPage.prototype.ngOnInit = function () {
    };
    HomeMoreMenuPage.prototype.customerData = function () {
        this._menuService.eNamecardState = 'customer';
        this.router.navigateByUrl('/e-namecard');
    };
    HomeMoreMenuPage.prototype.map = function () {
        this.router.navigateByUrl('/maps');
    };
    HomeMoreMenuPage.prototype.marketing = function () {
        this._menuService.eNamecardState = 'video-marketing';
        this.router.navigateByUrl('/e-namecard');
    };
    HomeMoreMenuPage.prototype.goBack = function () {
        this.location.back();
    };
    HomeMoreMenuPage = tslib_1.__decorate([
        Component({
            selector: 'app-home-more-menu',
            templateUrl: './home-more-menu.page.html',
            styleUrls: ['./home-more-menu.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            Location,
            Router])
    ], HomeMoreMenuPage);
    return HomeMoreMenuPage;
}());
export { HomeMoreMenuPage };
//# sourceMappingURL=home-more-menu.page.js.map