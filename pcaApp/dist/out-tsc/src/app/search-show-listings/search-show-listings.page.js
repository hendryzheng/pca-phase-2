import * as tslib_1 from "tslib";
import { Component, NgZone, ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { SearchPropertyFilterPage } from '../search-property-filter/search-property-filter.page';
import { ModalController } from '@ionic/angular';
import { SortListComponent } from '../sort-list/sort-list.component';
import { UtilitiesService } from 'src/services/utilities.service';
import { MapListingPage } from '../map-listing/map-listing.page';
var SearchShowListingsPage = /** @class */ (function () {
    function SearchShowListingsPage(location, geolocation, _zone, _utilitiesService, router, modalController, _propertyListingService) {
        this.location = location;
        this.geolocation = geolocation;
        this._zone = _zone;
        this._utilitiesService = _utilitiesService;
        this.router = router;
        this.modalController = modalController;
        this._propertyListingService = _propertyListingService;
        this.searchKeyword = '';
        this.listings = [];
        this.height = 500;
        this.width = 500;
        this.basemap = null;
        this.isLocationTurnedOn = false;
        this.showContact = false;
        this.contact = [];
    }
    SearchShowListingsPage.prototype.ngOnInit = function () {
    };
    SearchShowListingsPage.prototype.ionViewWillEnter = function () {
        this._propertyListingService.searchListingParams['page_num'] = 1;
        this._propertyListingService.searchListingParams['page_size'] = 20;
        this.searchKeyword = this._propertyListingService.selectedKeywordSearch;
        this.geolocation.getCurrentPosition().then(function (resp) {
            // if(this._propertyListingService.searchListingParams.query_coords == ''){
            //   // this._propertyListingService.searchListingParams.query_coords = resp.coords.latitude+','+resp.coords.longitude;
            // }
            // this._propertyListingService.searchListings(() => {
            //   this._zone.run(() => {
            //     this.listings = this._propertyListingService.propertyListingResult;
            //   })
            // })
        });
        this.listings = this._propertyListingService.propertyListingResult;
        if (this.searchKeyword == '') {
            this.presentModal();
        }
    };
    SearchShowListingsPage.prototype.getContact = function () {
        return this.contact;
    };
    SearchShowListingsPage.prototype.contactAgent = function (item) {
        this.contact = item;
        this.showContact = true;
    };
    SearchShowListingsPage.prototype.showMap = function () {
        this.presentModalMap();
    };
    SearchShowListingsPage.prototype.goBack = function () {
        this.location.back();
    };
    SearchShowListingsPage.prototype.viewListing = function (item) {
        this._propertyListingService.selectedPropertyListingDetail = item;
        console.log(this._propertyListingService.selectedPropertyListingDetail);
        this.router.navigateByUrl('/property-listing');
    };
    SearchShowListingsPage.prototype.whatsapp = function (phone) {
        return 'https://api.whatsapp.com/send?phone=' + this._utilitiesService.formatWhatsappNo(phone);
    };
    SearchShowListingsPage.prototype.getSearchListingParam = function () {
        return this._propertyListingService.searchListingParams;
    };
    SearchShowListingsPage.prototype.presentModal = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: SearchPropertyFilterPage
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss()
                            .then(function (data) {
                            _this.searchKeyword = _this._propertyListingService.selectedKeywordSearch;
                            _this._propertyListingService.searchListings(function () {
                                _this._zone.run(function () {
                                    _this.listings = _this._propertyListingService.propertyListingResult;
                                });
                            });
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SearchShowListingsPage.prototype.close = function () {
        this.showContact = false;
    };
    SearchShowListingsPage.prototype.presentModalMap = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: MapListingPage
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss()
                            .then(function (data) {
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SearchShowListingsPage.prototype.presentModalSort = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: SortListComponent
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss()
                            .then(function (data) {
                            _this._propertyListingService.searchListings(function () {
                                _this._zone.run(function () {
                                    _this.listings = _this._propertyListingService.propertyListingResult;
                                });
                            });
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SearchShowListingsPage.prototype.viewMap = function (item) {
        this._propertyListingService.selectedPropertyListingDetail = item;
        console.log(this._propertyListingService.selectedPropertyListingDetail);
        this.router.navigateByUrl('/map-view');
    };
    SearchShowListingsPage.prototype.search = function (ev) {
        console.log(ev);
        // if(this.searchKeyword !== ''){
        //   this._propertyListingService.searchListingParams.keyword = this.searchKeyword;
        //   this._propertyListingService.searchListings( ()=>{
        //     this.listings = this._propertyListingService.propertyListingResult;
        //   });
        // }
    };
    SearchShowListingsPage.prototype.getSearchListingResult = function () {
        this._propertyListingService.propertyListingResult;
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", ElementRef)
    ], SearchShowListingsPage.prototype, "mapContainer", void 0);
    SearchShowListingsPage = tslib_1.__decorate([
        Component({
            selector: 'app-search-show-listings',
            templateUrl: './search-show-listings.page.html',
            styleUrls: ['./search-show-listings.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location,
            Geolocation,
            NgZone,
            UtilitiesService,
            Router,
            ModalController,
            PropertyListingService])
    ], SearchShowListingsPage);
    return SearchShowListingsPage;
}());
export { SearchShowListingsPage };
//# sourceMappingURL=search-show-listings.page.js.map