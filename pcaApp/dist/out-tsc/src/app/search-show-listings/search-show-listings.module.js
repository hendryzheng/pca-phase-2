import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SearchShowListingsPage } from './search-show-listings.page';
import { SearchPropertyFilterPage } from '../search-property-filter/search-property-filter.page';
import { SortListComponent } from '../sort-list/sort-list.component';
import { MapListingPage } from '../map-listing/map-listing.page';
var routes = [
    {
        path: '',
        component: SearchShowListingsPage
    }
];
var SearchShowListingsPageModule = /** @class */ (function () {
    function SearchShowListingsPageModule() {
    }
    SearchShowListingsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            entryComponents: [SearchPropertyFilterPage, SortListComponent, MapListingPage],
            declarations: [MapListingPage, SearchShowListingsPage, SearchPropertyFilterPage, SortListComponent]
        })
    ], SearchShowListingsPageModule);
    return SearchShowListingsPageModule;
}());
export { SearchShowListingsPageModule };
//# sourceMappingURL=search-show-listings.module.js.map