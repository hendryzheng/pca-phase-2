import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ChartResultPage } from './chart-result.page';
var routes = [
    {
        path: '',
        component: ChartResultPage
    }
];
var ChartResultPageModule = /** @class */ (function () {
    function ChartResultPageModule() {
    }
    ChartResultPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ChartResultPage]
        })
    ], ChartResultPageModule);
    return ChartResultPageModule;
}());
export { ChartResultPageModule };
//# sourceMappingURL=chart-result.module.js.map