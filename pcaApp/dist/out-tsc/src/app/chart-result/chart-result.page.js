import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var ChartResultPage = /** @class */ (function () {
    function ChartResultPage() {
        this.isNotif = true;
        this.notifState = 'avg-psf';
    }
    ChartResultPage.prototype.ngOnInit = function () {
    };
    ChartResultPage.prototype.toggleNotif = function (string) {
        this.notifState = string;
    };
    ChartResultPage = tslib_1.__decorate([
        Component({
            selector: 'app-chart-result',
            templateUrl: './chart-result.page.html',
            styleUrls: ['./chart-result.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ChartResultPage);
    return ChartResultPage;
}());
export { ChartResultPage };
//# sourceMappingURL=chart-result.page.js.map