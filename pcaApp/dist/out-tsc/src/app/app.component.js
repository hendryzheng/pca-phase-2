import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, statusBar, splashScreen, _accountService, _utilitiesService) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this._accountService = _accountService;
        this._utilitiesService = _utilitiesService;
        this.initializeApp();
    }
    AppComponent.prototype.ngAfterViewInit = function () {
        // This element never changes.
        this._accountService.checkUserSession();
    };
    AppComponent.prototype.ngOnInit = function () {
        this._accountService.checkUserSession();
    };
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.statusBar.styleDefault();
            _this.statusBar.overlaysWebView(false);
            if (typeof window.FirebasePlugin !== 'undefined') {
                document.addEventListener('deviceready', function () {
                    if (_this.platform.is('ios')) {
                        window.FirebasePlugin.grantPermission();
                        window.FirebasePlugin.getToken(function (token) {
                            // save this server-side and use it to push notifications to this device
                            console.log(token);
                            if (token !== null) {
                                _this._accountService.token = token;
                                _this._accountService.registerToken();
                            }
                        }, function (error) {
                            console.error(error);
                        });
                        window.FirebasePlugin.onTokenRefresh(function (token) {
                            // save this server-side and use it to push notifications to this device
                            console.log(token);
                            if (token !== null) {
                                _this._accountService.token = token;
                                _this._accountService.registerToken();
                            }
                        }, function (error) {
                            console.error(error);
                        });
                    }
                    else {
                        window.FirebasePlugin.getToken(function (token) {
                            // save this server-side and use it to push notifications to this device
                            console.log(token);
                            if (token !== null) {
                                _this._accountService.token = token;
                                _this._accountService.registerToken();
                            }
                        }, function (error) {
                            console.error(error);
                        });
                        window.FirebasePlugin.onTokenRefresh(function (token) {
                            // save this server-side and use it to push notifications to this device
                            console.log(token);
                            if (token !== null) {
                                _this._accountService.token = token;
                                _this._accountService.registerToken();
                            }
                        }, function (error) {
                            console.error(error);
                        });
                    }
                    // window.FirebasePlugin.onNotificationOpen(payload => {
                    //   console.log(payload);
                    //   console.log(payload.aps);
                    //   console.log(payload.aps.alert);
                    //   this._utilitiesService.alertMessageCallback('Notification received', payload.aps.alert.title, () => {
                    //     // this.nav.setRoot(TabsPage);
                    //   })
                    // }, (error) => {
                    //   console.error(error);
                    // });
                }, false);
            }
            // if (this.platform.is('ios')) {
            // let
            //   appEl = <HTMLElement>(document.getElementsByTagName('ION-APP')[0]),
            //   appElHeight = appEl.clientHeight;
            // // this.keyboard.disableScroll(true);
            // cordova.plugins.Keyboard.shrinkView(true);
            // console.log(cordova.plugins.Keyboard);
            // Keyboard.shrinkView(true);
            // console.log(Keyboard);
            // document.addEventListener('deviceready', ()=> {
            //   cordova.plugins.Keyboard.shrinkView(true)
            //   window.addEventListener('keyboardDidShow', ()=> {
            //     console.log('aaaa');
            //     document.activeElement.scrollIntoView()
            //   })
            // })
            // window.addEventListener('keyboardDidShow', (e) => {
            //   appEl.style.height = (appElHeight - (<any>e).keyboardHeight) + 'px';
            //   const offset = $(document.activeElement).offset().top;
            //   let height = (offset - e.keyboardHeight)*-1;
            //   height = height > 0 ? 0 : height;      
            //   $('body').animate({ 'marginTop': height + 'px' }, 100);
            //   console.log('keyboard show');
            // });
            // window.addEventListener('keyboardDidHide', () => {
            //   appEl.style.height = '100%';
            //   cordova.plugins.Keyboard.hide();
            //   $('body').animate({ 'marginTop': 0 + 'px' }, 100);
            //   console.log('keyboard hide');
            // });
            // }
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            StatusBar,
            SplashScreen,
            AccountService,
            UtilitiesService])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map