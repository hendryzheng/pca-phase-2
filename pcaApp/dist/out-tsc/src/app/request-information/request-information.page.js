import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UtilitiesService } from 'src/services/utilities.service';
var RequestInformationPage = /** @class */ (function () {
    function RequestInformationPage(location, router, alertCtrl, _utilitiesService, _propertyListingService) {
        this.location = location;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this._utilitiesService = _utilitiesService;
        this._propertyListingService = _propertyListingService;
        this.from = 100000;
        this.to = 20000000;
        this.fromFloor = 500;
        this.toFloor = 10000;
        this.subcategories = [];
        this.hideSearch = true;
        this.searchKeywordListingResult = [];
        this.searchKeywordModel = '';
        this.height = '500';
    }
    RequestInformationPage.prototype.ngOnInit = function () {
    };
    RequestInformationPage.prototype.ionViewWillEnter = function () {
        var physicalScreenHeight = window.screen.height;
        this.height = physicalScreenHeight;
        var obj = this._propertyListingService.listing_categories[0].main_categories[0];
        this.subcategories = obj.sub_categories;
        if (this._propertyListingService.searchListingParams.sub_categories !== '') {
            var subcat = this._propertyListingService.searchListingParams.sub_categories.split(',');
            for (var i = 0; i < this.subcategories.length; i++) {
                for (var j = 0; j < subcat.length; j++) {
                    if (this.subcategories[i].key == subcat[j]) {
                        this.subcategories[i].checked = true;
                    }
                }
            }
        }
    };
    RequestInformationPage.prototype.goBack = function () {
        this.location.back();
    };
    RequestInformationPage.prototype.change = function (ev) {
        console.log(ev);
        this.from = ev.detail.value.lower;
        this._propertyListingService.searchListingParams.price_min = this.from;
        this.to = ev.detail.value.upper;
        this._propertyListingService.searchListingParams.price_max = this.to;
    };
    RequestInformationPage.prototype.changeFloorArea = function (ev) {
        console.log(ev);
        this.fromFloor = ev.detail.value.lower;
        this._propertyListingService.searchListingParams.floorarea_min = this.fromFloor;
        this.toFloor = ev.detail.value.upper;
        this._propertyListingService.searchListingParams.floorarea_max = this.toFloor;
    };
    RequestInformationPage.prototype.getSearchListingParam = function () {
        return this._propertyListingService.searchListingParams;
    };
    RequestInformationPage.prototype.mainCatChosen = function (item) {
        this.subcategories = item.sub_categories;
        console.log(item);
    };
    RequestInformationPage.prototype.saveSearch = function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var forgot;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            subHeader: 'Keep your search?',
                            message: "You may keep your search and name it as you wish. We will keep you notified if there is new listing for your search.",
                            inputs: [
                                {
                                    name: 'search_name',
                                    placeholder: 'Name your search',
                                    value: this._propertyListingService.searchListingParams.search_name,
                                    type: 'text'
                                },
                            ],
                            buttons: [
                                {
                                    text: 'Cancel and Continue',
                                    handler: function (data) {
                                        console.log('Cancel clicked');
                                        _this.router.navigateByUrl('/matching-request');
                                    }
                                },
                                {
                                    text: 'Save and Search',
                                    handler: function (data) {
                                        console.log('Send clicked');
                                        if (_this._propertyListingService.selectedKeywordSearch !== '' && _this._propertyListingService.selectedKeywordSearchSubtitle !== '') {
                                            params['search_name'] = data.search_name;
                                            params['location_name'] = _this._propertyListingService.selectedKeywordSearch;
                                            params['location_subtitle'] = _this._propertyListingService.selectedKeywordSearchSubtitle;
                                        }
                                        _this._propertyListingService.addRequestInoformation(params, function () {
                                            _this.router.navigateByUrl('/matching-request');
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        forgot = _a.sent();
                        return [4 /*yield*/, forgot.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RequestInformationPage.prototype.search = function () {
        console.log(this.getSearchListingParam());
        if (this.validateSearchParam()) {
            var params = JSON.parse(JSON.stringify(this._propertyListingService.searchListingParams));
            this.saveSearch(params);
        }
    };
    RequestInformationPage.prototype.searchKeyword = function (ev) {
        var _this = this;
        if (ev.target.value == '') {
            this.hideSearch = true;
            this.searchKeywordListingResult = [];
            this._propertyListingService.selectedKeywordSearch = '';
            this._propertyListingService.searchListingParams.query_type = '';
            this._propertyListingService.searchListingParams.query_ids = '';
            this._propertyListingService.searchListingParams.query_coords = '';
        }
        else {
            this._propertyListingService.getInputRequestInfo(ev.target.value, function () {
                _this.searchKeywordListingResult = _this._propertyListingService.searchInputRequestResult;
                _this.hideSearch = false;
            });
        }
    };
    RequestInformationPage.prototype.selectKeywordResult = function (item) {
        console.log(item);
        this._propertyListingService.selectedKeywordSearch = item.title;
        this._propertyListingService.selectedKeywordSearchSubtitle = item.subtitle;
        this._propertyListingService.searchListingParams.query_type = item.type;
        this._propertyListingService.searchListingParams.query_ids = item.id;
        this._propertyListingService.searchListingParams.query_coords = item.coordinates.lat + ',' + item.coordinates.lng;
        this.hideSearch = true;
    };
    RequestInformationPage.prototype.validateSearchParam = function () {
        var subcategories = '';
        for (var i = 0; i < this.subcategories.length; i++) {
            if (this.subcategories[i].checked) {
                subcategories += this.subcategories[i].key + ',';
            }
        }
        if (subcategories !== '') {
            subcategories = subcategories.substring(0, subcategories.length - 1);
        }
        this._propertyListingService.searchListingParams.sub_categories = subcategories;
        if (this._propertyListingService.selectedKeywordSearch == '') {
            this._utilitiesService.alertMessage('Location is compulsory', 'Please select location to narrow down your search');
            return false;
        }
        if (this._propertyListingService.searchListingParams.sub_categories == '') {
            this._utilitiesService.alertMessage('Subcategories is compulsory', 'Please select subcategories to narrow down your search');
            return false;
        }
        return true;
    };
    RequestInformationPage = tslib_1.__decorate([
        Component({
            selector: 'app-request-information',
            templateUrl: './request-information.page.html',
            styleUrls: ['./request-information.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location,
            Router,
            AlertController,
            UtilitiesService,
            PropertyListingService])
    ], RequestInformationPage);
    return RequestInformationPage;
}());
export { RequestInformationPage };
//# sourceMappingURL=request-information.page.js.map