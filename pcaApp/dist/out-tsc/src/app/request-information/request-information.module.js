import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RequestInformationPage } from './request-information.page';
var routes = [
    {
        path: '',
        component: RequestInformationPage
    }
];
var RequestInformationPageModule = /** @class */ (function () {
    function RequestInformationPageModule() {
    }
    RequestInformationPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [RequestInformationPage]
        })
    ], RequestInformationPageModule);
    return RequestInformationPageModule;
}());
export { RequestInformationPageModule };
//# sourceMappingURL=request-information.module.js.map