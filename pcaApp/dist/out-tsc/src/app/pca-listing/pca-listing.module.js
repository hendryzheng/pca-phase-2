import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PcaListingPage } from './pca-listing.page';
var routes = [
    {
        path: '',
        component: PcaListingPage
    }
];
var PcaListingPageModule = /** @class */ (function () {
    function PcaListingPageModule() {
    }
    PcaListingPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PcaListingPage]
        })
    ], PcaListingPageModule);
    return PcaListingPageModule;
}());
export { PcaListingPageModule };
//# sourceMappingURL=pca-listing.module.js.map