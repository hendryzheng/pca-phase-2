import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
var PcaListingPage = /** @class */ (function () {
    function PcaListingPage(_menuService, router, location) {
        this._menuService = _menuService;
        this.router = router;
        this.location = location;
        this.pcaListing = [];
    }
    PcaListingPage.prototype.ngOnInit = function () {
    };
    PcaListingPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._menuService.getPcaListing(function () {
            _this.pcaListing = _this._menuService.pca_listing;
        });
    };
    PcaListingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PcaCategoriesPage');
    };
    PcaListingPage.prototype.getPcaListing = function () {
        return this.pcaListing;
    };
    PcaListingPage.prototype.getMenu = function () {
        return this._menuService.selected_pca_category.categories_name;
    };
    PcaListingPage.prototype.goBack = function () {
        this.location.back();
    };
    PcaListingPage.prototype.viewItem = function (item) {
        this._menuService.selected_pca_listing = item;
        this.router.navigateByUrl('/pca-contact-form');
        // this.navCtrl.push(PcaContactFormPage);
    };
    PcaListingPage = tslib_1.__decorate([
        Component({
            selector: 'app-pca-listing',
            templateUrl: './pca-listing.page.html',
            styleUrls: ['./pca-listing.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            Router,
            Location])
    ], PcaListingPage);
    return PcaListingPage;
}());
export { PcaListingPage };
//# sourceMappingURL=pca-listing.page.js.map