import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RequestInformationListPage } from './request-information-list.page';
var routes = [
    {
        path: '',
        component: RequestInformationListPage
    }
];
var RequestInformationListPageModule = /** @class */ (function () {
    function RequestInformationListPageModule() {
    }
    RequestInformationListPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [RequestInformationListPage]
        })
    ], RequestInformationListPageModule);
    return RequestInformationListPageModule;
}());
export { RequestInformationListPageModule };
//# sourceMappingURL=request-information-list.module.js.map