import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
var RequestInformationListPage = /** @class */ (function () {
    function RequestInformationListPage(location, alertCtrl, _propertyListingService, router) {
        this.location = location;
        this.alertCtrl = alertCtrl;
        this._propertyListingService = _propertyListingService;
        this.router = router;
        this.informationList = [];
    }
    RequestInformationListPage.prototype.ngOnInit = function () {
    };
    RequestInformationListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._propertyListingService.getRequestInformationList(function () {
            _this.informationList = _this._propertyListingService.requestInformationList;
        });
    };
    RequestInformationListPage.prototype.goBack = function () {
        this.location.back();
    };
    RequestInformationListPage.prototype.formatDate = function (dt) {
        return moment(dt, 'YYYY-MM-DD hh:mm:ss').format('MMM DD hh:mm:ss');
    };
    RequestInformationListPage.prototype.search = function (item) {
        for (var key in this._propertyListingService.searchListingParams) {
            for (var k in item) {
                if (key == k) {
                    this._propertyListingService.searchListingParams[key] = item[k];
                    break;
                }
            }
            // Use `key` and `value`
        }
        this.saveSearch(item);
    };
    RequestInformationListPage.prototype.createSearch = function () {
        this._propertyListingService.searchListingParams = {
            search_id: '',
            location_name: '',
            location_subtitle: '',
            search_name: '',
            query_coords: '',
            listing_type: 'sale',
            main_category: 'hdb',
            keywords: '',
            sub_categories: '',
            query_type: '',
            query_ids: '',
            radius_max: 1000,
            price_min: 100000,
            price_max: 20000000,
            floorarea_min: 500,
            floorarea_max: 10000,
            rooms: 'any',
            is_new_launch: 'any',
            tenure: 'any',
            sort_field: '',
            sort_order: ''
        };
        this.router.navigateByUrl('/request-information');
    };
    RequestInformationListPage.prototype.saveSearch = function (item) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var forgot;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            subHeader: 'Choose your option?',
                            message: "Do you want to update your search?",
                            buttons: [
                                {
                                    text: 'Yes I want to Edit',
                                    handler: function (data) {
                                        console.log('Cancel clicked');
                                        _this._propertyListingService.searchListingParams.search_id = item.id;
                                        _this._propertyListingService.selectedKeywordSearch = item.location_name;
                                        _this._propertyListingService.searchListingParams.search_name = item.search_name;
                                        _this._propertyListingService.editSearchListingParams = true;
                                        console.log(_this._propertyListingService.searchListingParams);
                                        _this.router.navigateByUrl('/request-information');
                                    }
                                },
                                {
                                    text: 'No, Show me Search Results',
                                    handler: function (data) {
                                        console.log('Send clicked');
                                        _this._propertyListingService.searchListingParams.search_id = item.id;
                                        _this._propertyListingService.selectedKeywordSearch = item.location_name;
                                        console.log(_this._propertyListingService.searchListingParams);
                                        _this.router.navigateByUrl('/matching-request');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        forgot = _a.sent();
                        return [4 /*yield*/, forgot.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RequestInformationListPage = tslib_1.__decorate([
        Component({
            selector: 'app-request-information-list',
            templateUrl: './request-information-list.page.html',
            styleUrls: ['./request-information-list.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location,
            AlertController,
            PropertyListingService,
            Router])
    ], RequestInformationListPage);
    return RequestInformationListPage;
}());
export { RequestInformationListPage };
//# sourceMappingURL=request-information-list.page.js.map