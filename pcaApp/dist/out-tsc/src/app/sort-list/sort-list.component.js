import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PropertyListingService } from 'src/services/property-listing.service';
var SortListComponent = /** @class */ (function () {
    function SortListComponent(modalController, _propertyListingService) {
        this.modalController = modalController;
        this._propertyListingService = _propertyListingService;
    }
    SortListComponent.prototype.ngOnInit = function () { };
    SortListComponent.prototype.select = function (sort_field, sort_order) {
        this._propertyListingService.searchListingParams.sort_field = sort_field;
        this._propertyListingService.searchListingParams.sort_order = sort_order;
        this.modalController.dismiss();
    };
    SortListComponent = tslib_1.__decorate([
        Component({
            selector: 'app-sort-list',
            templateUrl: './sort-list.component.html',
            styleUrls: ['./sort-list.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ModalController,
            PropertyListingService])
    ], SortListComponent);
    return SortListComponent;
}());
export { SortListComponent };
//# sourceMappingURL=sort-list.component.js.map