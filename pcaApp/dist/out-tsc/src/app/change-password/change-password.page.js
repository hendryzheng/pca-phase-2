import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var ChangePasswordPage = /** @class */ (function () {
    function ChangePasswordPage(location) {
        this.location = location;
    }
    ChangePasswordPage.prototype.ngOnInit = function () {
    };
    ChangePasswordPage.prototype.goBack = function () {
        this.location.back();
    };
    ChangePasswordPage = tslib_1.__decorate([
        Component({
            selector: 'app-change-password',
            templateUrl: './change-password.page.html',
            styleUrls: ['./change-password.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], ChangePasswordPage);
    return ChangePasswordPage;
}());
export { ChangePasswordPage };
//# sourceMappingURL=change-password.page.js.map