import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
var PcaAgentToolsPage = /** @class */ (function () {
    function PcaAgentToolsPage(_utilitiesService, router, location) {
        this._utilitiesService = _utilitiesService;
        this.router = router;
        this.location = location;
    }
    PcaAgentToolsPage.prototype.ngOnInit = function () {
    };
    PcaAgentToolsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PcaAgentToolsPage');
    };
    PcaAgentToolsPage.prototype.calculators = function () {
        this.router.navigateByUrl('/calculator');
    };
    // research() {
    //   this.navCtrl.push(ResearchPage);
    // }
    PcaAgentToolsPage.prototype.goBack = function () {
        this.location.back();
    };
    PcaAgentToolsPage.prototype.valuation = function () {
        this._utilitiesService.openBrowser('Valuation', 'https://forms.uob.com.sg/property/agent/valuation/');
    };
    PcaAgentToolsPage = tslib_1.__decorate([
        Component({
            selector: 'app-pca-agent-tools',
            templateUrl: './pca-agent-tools.page.html',
            styleUrls: ['./pca-agent-tools.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [UtilitiesService,
            Router,
            Location])
    ], PcaAgentToolsPage);
    return PcaAgentToolsPage;
}());
export { PcaAgentToolsPage };
//# sourceMappingURL=pca-agent-tools.page.js.map