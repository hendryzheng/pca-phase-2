import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UtilitiesService } from 'src/services/utilities.service';
var MapsPage = /** @class */ (function () {
    function MapsPage(router, _utilitiesService, location, geolocation) {
        this.router = router;
        this._utilitiesService = _utilitiesService;
        this.location = location;
        this.geolocation = geolocation;
        this.height = 500;
        this.width = 500;
        this.basemap = null;
        this.isLocationTurnedOn = false;
    }
    MapsPage.prototype.ngOnInit = function () {
    };
    MapsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('ionViewDidLoad MapsPage');
        var physicalScreenHeight = window.screen.height;
        this.height = physicalScreenHeight;
        var body = document.getElementsByTagName('body');
        var physicalScreenWidth = body[0].offsetWidth;
        this.width = physicalScreenWidth;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.isLocationTurnedOn = true;
            console.log(resp);
            _this.isLocationTurnedOn = true;
            console.log(resp);
            _this.loadmap(resp.coords.latitude, resp.coords.longitude);
            // this._utilitiesService.openBrowser('Map',CONFIGURATION.base_url+'/onemap.php?lat='+resp.coords.latitude+'&lng='+resp.coords.longitude+'&width='+this.width+'&height='+this.height);
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    MapsPage.prototype.ionViewDidEnter = function () {
        this.geolocation.getCurrentPosition().then(function (resp) {
        });
    };
    MapsPage.prototype.goBack = function () {
        this.location.back();
    };
    MapsPage.prototype.loadmap = function (lat, lng) {
        L.Icon.Default.imagePath = "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/images"; // missing setup
        var center = L.bounds([1.56073, 104.11475], [1.16, 103.502]).getCenter();
        var map = L.map('mapdiv').setView([center.x, center.y], 12);
        var basemap = L.tileLayer('https://maps-{s}.onemap.sg/v3/Original/{z}/{x}/{y}.png', {
            detectRetina: true,
            maxZoom: 18,
            minZoom: 11
        });
        map.setMaxBounds([[1.56073, 104.1147], [1.16, 103.502]]);
        basemap.addTo(map);
        var marker = new L.Marker([lat, lng], { bounceOnAdd: false }).addTo(map);
        var popup = L.popup()
            .setLatLng([lat, lng])
            .setContent('You are here!')
            .openOn(map);
    };
    MapsPage.prototype.getWidth = function () {
        return this.width;
    };
    MapsPage.prototype.getHeight = function () {
        return this.height;
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", ElementRef)
    ], MapsPage.prototype, "mapContainer", void 0);
    MapsPage = tslib_1.__decorate([
        Component({
            selector: 'app-maps',
            templateUrl: './maps.page.html',
            styleUrls: ['./maps.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            UtilitiesService,
            Location,
            Geolocation])
    ], MapsPage);
    return MapsPage;
}());
export { MapsPage };
//# sourceMappingURL=maps.page.js.map