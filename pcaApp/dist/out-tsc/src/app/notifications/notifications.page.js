import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { Badge } from '@ionic-native/badge/ngx';
import { AccountService } from 'src/services/account.service';
import { Location } from '@angular/common';
var NotificationsPage = /** @class */ (function () {
    function NotificationsPage(_utilitiesService, badge, location, _accountService) {
        this._utilitiesService = _utilitiesService;
        this.badge = badge;
        this.location = location;
        this._accountService = _accountService;
        this.isNotif = true;
        this.notifState = 'all';
    }
    NotificationsPage.prototype.close = function () {
        // this.viewCtrl.dismiss();
    };
    NotificationsPage.prototype.ngOnInit = function () {
    };
    NotificationsPage.prototype.goBack = function () {
        this.location.back();
    };
    NotificationsPage.prototype.ionViewWillEnter = function () {
        var tabs = document.querySelectorAll('.show-tabbar');
        if (tabs !== null) {
            Object.keys(tabs).map(function (key) {
                tabs[key].style.display = 'flex';
            });
        }
        this._accountService.getNotification(function () {
        });
    };
    NotificationsPage.prototype.clearBadges = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var badge, e_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.badge.clear()];
                    case 1:
                        badge = _a.sent();
                        console.log(badge);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    NotificationsPage.prototype.ngAfterViewInit = function () {
        var tabs = document.querySelectorAll('.show-tabbar');
        if (tabs !== null) {
            Object.keys(tabs).map(function (key) {
                tabs[key].style.display = 'flex';
            });
        }
    };
    NotificationsPage.prototype.open = function (item) {
        if (item.url !== null && typeof item.url !== 'undefined' && item.url !== '') {
            if (item.url.indexOf("http") != -1) {
                var url = 'http://' + item.url;
                this._utilitiesService.openBrowser(item.title, url);
            }
            else {
                this._utilitiesService.openBrowser(item.title, item.url);
            }
        }
    };
    NotificationsPage.prototype.getNotification = function () {
        return this._accountService.notification;
    };
    NotificationsPage.prototype.toggleNotif = function (string) {
        this.notifState = string;
    };
    NotificationsPage = tslib_1.__decorate([
        Component({
            selector: 'app-notifications',
            templateUrl: './notifications.page.html',
            styleUrls: ['./notifications.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [UtilitiesService,
            Badge,
            Location,
            AccountService])
    ], NotificationsPage);
    return NotificationsPage;
}());
export { NotificationsPage };
//# sourceMappingURL=notifications.page.js.map