import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MyListingPage } from './my-listing.page';
var routes = [
    {
        path: '',
        component: MyListingPage
    }
];
var MyListingPageModule = /** @class */ (function () {
    function MyListingPageModule() {
    }
    MyListingPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [MyListingPage]
        })
    ], MyListingPageModule);
    return MyListingPageModule;
}());
export { MyListingPageModule };
//# sourceMappingURL=my-listing.module.js.map