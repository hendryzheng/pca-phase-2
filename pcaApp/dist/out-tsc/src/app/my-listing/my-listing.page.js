import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { PropertyListingService } from 'src/services/property-listing.service';
import { AccountService } from 'src/services/account.service';
import { Location } from '@angular/common';
var MyListingPage = /** @class */ (function () {
    function MyListingPage(_propertyListingService, location, _accountService) {
        this._propertyListingService = _propertyListingService;
        this.location = location;
        this._accountService = _accountService;
        this.myListing = [];
    }
    MyListingPage.prototype.ngOnInit = function () {
    };
    MyListingPage.prototype.ionViewWillEnter = function () {
        this._accountService.checkUserSession();
        this.fetchMyPropertyListing();
    };
    MyListingPage.prototype.getUserData = function () {
        return this._accountService.userData;
    };
    MyListingPage.prototype.fetchMyPropertyListing = function () {
        var _this = this;
        this.myListing = [];
        this._propertyListingService.getMyPropertyListing(function () {
            _this.myListing = _this._propertyListingService.myPropertyListing;
        });
    };
    MyListingPage.prototype.goBack = function () {
        this.location.back();
    };
    MyListingPage = tslib_1.__decorate([
        Component({
            selector: 'app-my-listing',
            templateUrl: './my-listing.page.html',
            styleUrls: ['./my-listing.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [PropertyListingService,
            Location,
            AccountService])
    ], MyListingPage);
    return MyListingPage;
}());
export { MyListingPage };
//# sourceMappingURL=my-listing.page.js.map