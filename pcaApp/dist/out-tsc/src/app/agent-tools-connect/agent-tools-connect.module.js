import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AgentToolsConnectPage } from './agent-tools-connect.page';
var routes = [
    {
        path: '',
        component: AgentToolsConnectPage
    }
];
var AgentToolsConnectPageModule = /** @class */ (function () {
    function AgentToolsConnectPageModule() {
    }
    AgentToolsConnectPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AgentToolsConnectPage]
        })
    ], AgentToolsConnectPageModule);
    return AgentToolsConnectPageModule;
}());
export { AgentToolsConnectPageModule };
//# sourceMappingURL=agent-tools-connect.module.js.map