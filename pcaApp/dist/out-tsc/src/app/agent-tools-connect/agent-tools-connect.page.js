import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var AgentToolsConnectPage = /** @class */ (function () {
    function AgentToolsConnectPage() {
    }
    AgentToolsConnectPage.prototype.ngOnInit = function () {
    };
    AgentToolsConnectPage = tslib_1.__decorate([
        Component({
            selector: 'app-agent-tools-connect',
            templateUrl: './agent-tools-connect.page.html',
            styleUrls: ['./agent-tools-connect.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], AgentToolsConnectPage);
    return AgentToolsConnectPage;
}());
export { AgentToolsConnectPage };
//# sourceMappingURL=agent-tools-connect.page.js.map