import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
var TabsPage = /** @class */ (function () {
    function TabsPage(_accountService, _menuService, router) {
        this._accountService = _accountService;
        this._menuService = _menuService;
        this.router = router;
    }
    TabsPage.prototype.ionViewWillEnter = function () {
        this._accountService.checkUserSession();
        if (this._menuService.eNamecardState !== '') {
            this.router.navigateByUrl('/e-namecard');
        }
    };
    TabsPage.prototype.ngOnInit = function () {
    };
    TabsPage.prototype.getUserData = function () {
        return this._accountService.userData;
    };
    TabsPage.prototype.isLoggedIn = function () {
        return this._accountService.isUserSessionAlive();
    };
    TabsPage = tslib_1.__decorate([
        Component({
            selector: 'app-tabs',
            templateUrl: './tabs.page.html',
            styleUrls: ['./tabs.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AccountService, MenuService, Router])
    ], TabsPage);
    return TabsPage;
}());
export { TabsPage };
//# sourceMappingURL=tabs.page.js.map