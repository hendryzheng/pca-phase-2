import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AccountService } from 'src/services/account.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
var SplashLoginPage = /** @class */ (function () {
    function SplashLoginPage(router, _accountService, appVersion, _utilitiesService, platform) {
        this.router = router;
        this._accountService = _accountService;
        this.appVersion = appVersion;
        this._utilitiesService = _utilitiesService;
        this.platform = platform;
        this.token = '';
    }
    SplashLoginPage.prototype.ngOnInit = function () {
    };
    SplashLoginPage.prototype.ngAfterViewInit = function () {
        // let tabs = document.querySelectorAll('.show-tabbar');
        // if (tabs !== null) {
        //   Object.keys(tabs).map((key) => {
        //     tabs[key].style.display = 'none';
        //   });
        // }
    };
    SplashLoginPage.prototype.ionViewWillLeave = function () {
        // let tabs = document.querySelectorAll('.show-tabbar');
        // if (tabs !== null) {
        //   Object.keys(tabs).map((key) => {
        //     tabs[key].style.display = 'flex';
        //   });
        // }
    };
    SplashLoginPage.prototype.callbackSessionLogin = function () {
        if (this._accountService.isUserSessionAlive()) {
            this.router.navigate(['/tabs'], { replaceUrl: true });
        }
    };
    SplashLoginPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // let tabs = document.querySelectorAll('.show-tabbar');
        // if (tabs !== null) {
        //   Object.keys(tabs).map((key) => {
        //     tabs[key].style.display = 'none';
        //   });
        // }
        this._accountService.checkUserSession();
        if (typeof cordova == 'undefined') {
            this.callbackSessionLogin();
        }
        document.addEventListener('deviceready', function () {
            _this.appVersion.getVersionNumber().then(function (response) {
                var num = response.toString().replace(/\./g, '');
                console.log(num);
                _this._accountService.getAppVersion(function () {
                    if (parseInt(num) > 0 && _this._accountService.appVersion !== '') {
                        if (_this.platform.is('ios')) {
                            var version = _this._accountService.appVersion.ios_version;
                            var ios_url_1 = _this._accountService.appVersion.ios_url;
                            if (version > num) {
                                _this._utilitiesService.alertMessageCallback('Oops !', 'Your app version is outdated. Please update your app', function () {
                                    localStorage.clear();
                                    _this._accountService.userData = [];
                                    var ref = cordova.InAppBrowser.open(ios_url_1, '_system', 'location=yes');
                                    ref.addEventListener('loadstop', function (event) {
                                        ref.show();
                                    });
                                });
                            }
                            else {
                                _this.callbackSessionLogin();
                            }
                        }
                        else {
                            var version = _this._accountService.appVersion.android_version;
                            var android_url_1 = _this._accountService.appVersion.android_url;
                            if (version > num) {
                                _this._utilitiesService.alertMessageCallback('Oops !', 'Your app version is outdated. Please update your app', function () {
                                    localStorage.clear();
                                    _this._accountService.userData = [];
                                    var ref = cordova.InAppBrowser.open(android_url_1, '_system', 'location=yes');
                                    ref.addEventListener('loadstop', function (event) {
                                        ref.show();
                                    });
                                });
                            }
                            else {
                                _this.callbackSessionLogin();
                            }
                        }
                    }
                    else {
                        alert('error');
                    }
                });
            });
        });
    };
    SplashLoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LandingPage');
    };
    SplashLoginPage.prototype.register = function () {
        this.router.navigateByUrl('/sign-up');
    };
    SplashLoginPage.prototype.login = function () {
        this.router.navigateByUrl('/login');
    };
    SplashLoginPage.prototype.home = function () {
    };
    SplashLoginPage = tslib_1.__decorate([
        Component({
            selector: 'app-splash-login',
            templateUrl: './splash-login.page.html',
            styleUrls: ['./splash-login.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            AccountService,
            AppVersion,
            UtilitiesService,
            Platform])
    ], SplashLoginPage);
    return SplashLoginPage;
}());
export { SplashLoginPage };
//# sourceMappingURL=splash-login.page.js.map