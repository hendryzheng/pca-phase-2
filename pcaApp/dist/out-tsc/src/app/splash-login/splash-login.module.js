import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SplashLoginPage } from './splash-login.page';
var routes = [
    {
        path: '',
        component: SplashLoginPage
    }
];
var SplashLoginPageModule = /** @class */ (function () {
    function SplashLoginPageModule() {
    }
    SplashLoginPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [SplashLoginPage]
        })
    ], SplashLoginPageModule);
    return SplashLoginPageModule;
}());
export { SplashLoginPageModule };
//# sourceMappingURL=splash-login.module.js.map