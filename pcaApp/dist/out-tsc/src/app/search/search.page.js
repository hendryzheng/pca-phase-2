import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Location } from '@angular/common';
import { CONFIGURATION } from 'src/services/config.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { IonSearchbar } from '@ionic/angular';
var SearchPage = /** @class */ (function () {
    function SearchPage(_menuService, ga, router, _utilitiesService, location) {
        this._menuService = _menuService;
        this.ga = ga;
        this.router = router;
        this._utilitiesService = _utilitiesService;
        this.location = location;
        this.showSearch = false;
        this.listing_result = [];
        this.myInput = '';
    }
    SearchPage.prototype.ngOnInit = function () {
    };
    SearchPage.prototype.ngAfterViewInit = function () {
        // setTimeout(() => this.inputEl.nativeElement.focus(),1000);
        this.searchbar.setFocus();
    };
    SearchPage.prototype.onInput = function (ev) {
        var _this = this;
        console.log(ev);
        if (ev.type == 'ionInput') {
            var params = {
                keyword: ev.target.value
            };
            if (ev.target.value == '') {
                this.showSearch = false;
            }
            else {
                this._menuService.searchListing(params, function () {
                    _this.showSearch = true;
                    _this.listing_result = _this._menuService.listing_result;
                });
            }
        }
    };
    SearchPage.prototype.getIconPath = function (item) {
        if (item.icon_path !== null) {
            return item.icon_path;
        }
        else {
            return this._menuService.selectedMenu.icon_path;
        }
    };
    SearchPage.prototype.openAttachment = function (item) {
        var base_url = CONFIGURATION.apiEndpoint;
        this.ga.startTrackerWithId('UA-121102215-1')
            .then(function () {
            console.log('Google analytics is ready now');
            // this.ga.trackView('Homepage Menu ' + this.getMenu() + ' > ' + item.listing_name);
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        if (item.listing_type == '2') {
            console.log(item.listing_attachment);
            this._utilitiesService.openBrowser(item.listing_name, item.listing_attachment);
            // const browser = this.iab.create(item.listing_attachment, '_blank', 'location=no');
        }
        else if (item.listing_type == '3') {
            console.log(item);
            window.InAppYouTube.openVideo(item.youtube_id, {
                fullscreen: true
            }, function (result) {
                console.log(JSON.stringify(result));
            }, function (reason) {
                console.log(reason);
            });
            // this.youtube.openVideo(item.youtube_id);
        }
        else {
            this._menuService.selected_listing = item;
            this._menuService.selected_listing.image_path = base_url + item.listing_attachment;
            this.router.navigateByUrl('/preview-image');
            // this._utilitiesService.openBrowser(item.listing_name, base_url + item.listing_attachment);
            // this.iab.create(base_url+item.listing_attachment, '_blank', 'location=no');
        }
    };
    SearchPage.prototype.goBack = function () {
        this.location.back();
    };
    SearchPage.prototype.checkBlur = function () {
        if (this.myInput == '') {
            this.showSearch = false;
            this.listing_result = [];
        }
    };
    SearchPage.prototype.onCancel = function (ev) {
        console.log(ev);
        this.showSearch = false;
        this.listing_result = [];
    };
    tslib_1.__decorate([
        ViewChild('search'),
        tslib_1.__metadata("design:type", IonSearchbar)
    ], SearchPage.prototype, "searchbar", void 0);
    SearchPage = tslib_1.__decorate([
        Component({
            selector: 'app-search',
            templateUrl: './search.page.html',
            styleUrls: ['./search.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            GoogleAnalytics,
            Router,
            UtilitiesService,
            Location])
    ], SearchPage);
    return SearchPage;
}());
export { SearchPage };
//# sourceMappingURL=search.page.js.map