import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { Location } from '@angular/common';
var ENamecardViewPage = /** @class */ (function () {
    function ENamecardViewPage(_accountService, location) {
        this._accountService = _accountService;
        this.location = location;
        this.isNotif = true;
        this.notifState = 'my-listing';
        this.loaded = false;
        this.img_url = 'assets/img/person-placeholder.png';
    }
    ENamecardViewPage.prototype.ngOnInit = function () {
    };
    ENamecardViewPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._accountService.checkUserSession();
        var param = {
            user_id: this._accountService.viewUserData.id,
            user_type: this._accountService.viewUserData.member_type
        };
        this._accountService.getUserDetailParam(param, function () {
            if (_this._accountService.viewUserData.profile_picture !== null) {
                _this.img_url = CONFIGURATION.base_url + _this._accountService.viewUserData.profile_picture;
            }
        });
    };
    ENamecardViewPage.prototype.goBack = function () {
        this.location.back();
    };
    ENamecardViewPage.prototype.getUserData = function () {
        return this._accountService.viewUserData;
    };
    ENamecardViewPage = tslib_1.__decorate([
        Component({
            selector: 'app-e-namecard-view',
            templateUrl: './e-namecard-view.page.html',
            styleUrls: ['./e-namecard-view.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AccountService,
            Location])
    ], ENamecardViewPage);
    return ENamecardViewPage;
}());
export { ENamecardViewPage };
//# sourceMappingURL=e-namecard-view.page.js.map