import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ENamecardViewPage } from './e-namecard-view.page';
var routes = [
    {
        path: '',
        component: ENamecardViewPage
    }
];
var ENamecardViewPageModule = /** @class */ (function () {
    function ENamecardViewPageModule() {
    }
    ENamecardViewPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ENamecardViewPage]
        })
    ], ENamecardViewPageModule);
    return ENamecardViewPageModule;
}());
export { ENamecardViewPageModule };
//# sourceMappingURL=e-namecard-view.module.js.map