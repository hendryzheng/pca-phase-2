import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PcaCategoriesPage } from './pca-categories.page';
var routes = [
    {
        path: '',
        component: PcaCategoriesPage
    }
];
var PcaCategoriesPageModule = /** @class */ (function () {
    function PcaCategoriesPageModule() {
    }
    PcaCategoriesPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PcaCategoriesPage]
        })
    ], PcaCategoriesPageModule);
    return PcaCategoriesPageModule;
}());
export { PcaCategoriesPageModule };
//# sourceMappingURL=pca-categories.module.js.map