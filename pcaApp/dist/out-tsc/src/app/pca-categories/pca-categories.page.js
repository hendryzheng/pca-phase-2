import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
var PcaCategoriesPage = /** @class */ (function () {
    function PcaCategoriesPage(_menuService, router, location) {
        this._menuService = _menuService;
        this.router = router;
        this.location = location;
        this.pcaCategories = [];
    }
    PcaCategoriesPage.prototype.ngOnInit = function () {
    };
    PcaCategoriesPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._menuService.getPcaCategories(function () {
            _this.pcaCategories = _this._menuService.pca_categories;
        });
    };
    PcaCategoriesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PcaCategoriesPage');
    };
    PcaCategoriesPage.prototype.getPcaCategories = function () {
        return this._menuService.pca_categories;
    };
    PcaCategoriesPage.prototype.getMenu = function () {
        return this._menuService.selectedMenu.menu_name;
    };
    PcaCategoriesPage.prototype.goBack = function () {
        this.location.back();
    };
    PcaCategoriesPage.prototype.viewItem = function (item) {
        this._menuService.selected_pca_category = item;
        this.router.navigateByUrl('/pca-listing');
    };
    PcaCategoriesPage = tslib_1.__decorate([
        Component({
            selector: 'app-pca-categories',
            templateUrl: './pca-categories.page.html',
            styleUrls: ['./pca-categories.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            Router,
            Location])
    ], PcaCategoriesPage);
    return PcaCategoriesPage;
}());
export { PcaCategoriesPage };
//# sourceMappingURL=pca-categories.page.js.map