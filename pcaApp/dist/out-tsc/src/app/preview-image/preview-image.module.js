import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PreviewImagePage } from './preview-image.page';
var routes = [
    {
        path: '',
        component: PreviewImagePage
    }
];
var PreviewImagePageModule = /** @class */ (function () {
    function PreviewImagePageModule() {
    }
    PreviewImagePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PreviewImagePage]
        })
    ], PreviewImagePageModule);
    return PreviewImagePageModule;
}());
export { PreviewImagePageModule };
//# sourceMappingURL=preview-image.module.js.map