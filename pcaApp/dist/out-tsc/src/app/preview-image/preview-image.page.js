import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Location } from '@angular/common';
var PreviewImagePage = /** @class */ (function () {
    function PreviewImagePage(_menuService, location) {
        this._menuService = _menuService;
        this.location = location;
        this.detail = [];
    }
    PreviewImagePage.prototype.ngOnInit = function () {
    };
    PreviewImagePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PreviewImagePage');
    };
    PreviewImagePage.prototype.getDetail = function () {
        return this._menuService.selected_listing;
    };
    PreviewImagePage.prototype.goBack = function () {
        this.location.back();
    };
    PreviewImagePage = tslib_1.__decorate([
        Component({
            selector: 'app-preview-image',
            templateUrl: './preview-image.page.html',
            styleUrls: ['./preview-image.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            Location])
    ], PreviewImagePage);
    return PreviewImagePage;
}());
export { PreviewImagePage };
//# sourceMappingURL=preview-image.page.js.map