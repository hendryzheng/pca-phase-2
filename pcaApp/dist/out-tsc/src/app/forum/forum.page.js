import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AccountService } from 'src/services/account.service';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { CONFIGURATION } from 'src/services/config.service';
import { Location } from '@angular/common';
var ForumPage = /** @class */ (function () {
    function ForumPage(_accountService, router, _menuService, alertCtrl, location, _utilitiesService) {
        this._accountService = _accountService;
        this.router = router;
        this._menuService = _menuService;
        this.alertCtrl = alertCtrl;
        this.location = location;
        this._utilitiesService = _utilitiesService;
        this.search_input = '';
        this.loaded = false;
        this.img_url = 'assets/img/person-placeholder.png';
    }
    ForumPage.prototype.ngOnInit = function () {
    };
    ForumPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            if (this._accountService.userData.profile_picture !== null) {
                this.img_url = CONFIGURATION.base_url + this._accountService.userData.profile_picture;
            }
        }
        this._menuService.getPost(function () {
            _this.loaded = true;
        });
    };
    ForumPage.prototype.getUserData = function () {
        return this._accountService.userData;
    };
    ForumPage.prototype.getPost = function () {
        return this._menuService.post;
    };
    ForumPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForumPage');
    };
    ForumPage.prototype.goBack = function () {
        this.location.back();
    };
    ForumPage.prototype.eventHandler = function (event) {
        var _this = this;
        console.log(event, event.keyCode, event.keyIdentifier);
        var params = {
            keyword: event.target.value
        };
        if (event.target.value === '') {
            this.loaded = false;
            this._menuService.getPost(function () {
                _this.loaded = true;
            });
        }
        else {
            this.loaded = false;
            this._menuService.searchForum(params, function () {
                _this.loaded = true;
            });
        }
    };
    ForumPage.prototype.createPost = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var forgot;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._accountService.isUserSessionAlive()) return [3 /*break*/, 1];
                        this.router.navigateByUrl('/forum-create-post');
                        return [3 /*break*/, 4];
                    case 1: return [4 /*yield*/, this.alertCtrl.create({
                            subHeader: 'Oops !',
                            message: 'Only member is allowed to submit a post. You ',
                            buttons: [
                                {
                                    text: 'Register Now',
                                    handler: function () {
                                        // this.navCtrl.pop();
                                        // this.navCtrl.push(RegisterPage);
                                        _this.router.navigateByUrl('/sign-up');
                                    }
                                },
                                {
                                    text: 'Sign In As Member',
                                    handler: function () {
                                        // this.navCtrl.pop();
                                        _this.router.navigateByUrl('/login');
                                    }
                                }
                            ]
                        })];
                    case 2:
                        forgot = _a.sent();
                        return [4 /*yield*/, forgot.present()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ForumPage.prototype.createPostHandler = function () {
        this.createPost();
    };
    ForumPage.prototype.viewComments = function (item) {
        this._menuService.selectedPost = item;
        this.router.navigateByUrl('/forum-discussion');
        // this.navCtrl.push(CommentPage);
    };
    ForumPage.prototype.getImagePath = function (item) {
        return CONFIGURATION.base_url + item.image;
    };
    ForumPage = tslib_1.__decorate([
        Component({
            selector: 'app-forum',
            templateUrl: './forum.page.html',
            styleUrls: ['./forum.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AccountService,
            Router,
            MenuService,
            AlertController,
            Location,
            UtilitiesService])
    ], ForumPage);
    return ForumPage;
}());
export { ForumPage };
//# sourceMappingURL=forum.page.js.map