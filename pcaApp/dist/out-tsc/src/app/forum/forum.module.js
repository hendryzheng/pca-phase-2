import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ForumPage } from './forum.page';
var routes = [
    {
        path: '',
        component: ForumPage
    }
];
var ForumPageModule = /** @class */ (function () {
    function ForumPageModule() {
    }
    ForumPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ForumPage]
        })
    ], ForumPageModule);
    return ForumPageModule;
}());
export { ForumPageModule };
//# sourceMappingURL=forum.module.js.map