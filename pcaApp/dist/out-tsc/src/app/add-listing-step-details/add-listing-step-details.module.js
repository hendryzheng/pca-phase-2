import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddListingStepDetailsPage } from './add-listing-step-details.page';
var routes = [
    {
        path: '',
        component: AddListingStepDetailsPage
    }
];
var AddListingStepDetailsPageModule = /** @class */ (function () {
    function AddListingStepDetailsPageModule() {
    }
    AddListingStepDetailsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AddListingStepDetailsPage]
        })
    ], AddListingStepDetailsPageModule);
    return AddListingStepDetailsPageModule;
}());
export { AddListingStepDetailsPageModule };
//# sourceMappingURL=add-listing-step-details.module.js.map