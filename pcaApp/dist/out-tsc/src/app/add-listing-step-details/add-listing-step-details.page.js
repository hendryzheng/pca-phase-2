import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';
import { PropertyListingService } from 'src/services/property-listing.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import * as moment from 'moment';
var AddListingStepDetailsPage = /** @class */ (function () {
    function AddListingStepDetailsPage(_propertyListingService, _utilitiesService, router, datePicker, location) {
        this._propertyListingService = _propertyListingService;
        this._utilitiesService = _utilitiesService;
        this.router = router;
        this.datePicker = datePicker;
        this.location = location;
    }
    AddListingStepDetailsPage.prototype.ngOnInit = function () {
    };
    AddListingStepDetailsPage.prototype.getStep1PropertyDetail = function () {
        return this._propertyListingService.step1PropertyDetail;
    };
    AddListingStepDetailsPage.prototype.getStep1SelectedLocation = function () {
        return this._propertyListingService.step1SelectedLocation;
    };
    AddListingStepDetailsPage.prototype.goBack = function () {
        this.location.back();
    };
    AddListingStepDetailsPage.prototype.getShortFormMap = function () {
        return this._propertyListingService.shortFormMap;
    };
    AddListingStepDetailsPage.prototype.getForm = function () {
        return this._propertyListingService.form;
    };
    AddListingStepDetailsPage.prototype.openDatePickerAvailability = function () {
        var _this = this;
        this.datePicker.show({
            date: new Date(),
            mode: 'date',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(function (date) {
            console.log('Got date: ', date);
            var dt = moment(date).format('YYYY-MM-DD');
            _this._propertyListingService.form.date_of_availbility = dt;
            console.log(dt);
        }, function (err) { return console.log('Error occurred while getting date: ', err); });
    };
    AddListingStepDetailsPage.prototype.openDatePickerTenancyLease = function () {
        var _this = this;
        this.datePicker.show({
            date: new Date(),
            mode: 'date',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(function (date) {
            console.log('Got date: ', date);
            var dt = moment(date).format('YYYY-MM-DD');
            _this._propertyListingService.form.tenancy_lease_expiration = dt;
            console.log(dt);
        }, function (err) { return console.log('Error occurred while getting date: ', err); });
    };
    AddListingStepDetailsPage.prototype.continue = function () {
        var selected = '';
        for (var i = 0; i < this._propertyListingService.featuresOptions.length; i++) {
            if (this._propertyListingService.featuresOptions[i].checked) {
                selected += this._propertyListingService.featuresOptions[i]['value'] + ',';
            }
        }
        selected = selected.substring(0, selected.length - 1);
        this._propertyListingService.form.features = selected;
        this.router.navigateByUrl('/add-listing-step-photos');
    };
    AddListingStepDetailsPage.prototype.getFeaturesOptions = function () {
        return this._propertyListingService.featuresOptions;
    };
    AddListingStepDetailsPage = tslib_1.__decorate([
        Component({
            selector: 'app-add-listing-step-details',
            templateUrl: './add-listing-step-details.page.html',
            styleUrls: ['./add-listing-step-details.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [PropertyListingService,
            UtilitiesService,
            Router,
            DatePicker,
            Location])
    ], AddListingStepDetailsPage);
    return AddListingStepDetailsPage;
}());
export { AddListingStepDetailsPage };
//# sourceMappingURL=add-listing-step-details.page.js.map