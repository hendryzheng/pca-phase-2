import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PromotionsPage } from './promotions.page';
var routes = [
    {
        path: '',
        component: PromotionsPage
    }
];
var PromotionsPageModule = /** @class */ (function () {
    function PromotionsPageModule() {
    }
    PromotionsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PromotionsPage]
        })
    ], PromotionsPageModule);
    return PromotionsPageModule;
}());
export { PromotionsPageModule };
//# sourceMappingURL=promotions.module.js.map