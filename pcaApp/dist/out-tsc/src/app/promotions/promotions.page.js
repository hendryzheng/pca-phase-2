import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
var PromotionsPage = /** @class */ (function () {
    function PromotionsPage(_menuService, ga, router, location, _utilitiesService) {
        this._menuService = _menuService;
        this.ga = ga;
        this.router = router;
        this.location = location;
        this._utilitiesService = _utilitiesService;
    }
    PromotionsPage.prototype.ngOnInit = function () {
    };
    PromotionsPage.prototype.goBack = function () {
        this.location.back();
    };
    PromotionsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._menuService.getPromotion(function () {
        });
        this.ga.startTrackerWithId('UA-121102215-1')
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView('Promotion Page');
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
    };
    PromotionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PromotionPage');
    };
    PromotionsPage.prototype.viewDetail = function (item) {
        this._menuService.selectedPromotion = item;
        this.router.navigateByUrl('/promotion-detail');
    };
    PromotionsPage.prototype.getPromotion = function () {
        return this._menuService.promotion;
    };
    PromotionsPage = tslib_1.__decorate([
        Component({
            selector: 'app-promotions',
            templateUrl: './promotions.page.html',
            styleUrls: ['./promotions.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            GoogleAnalytics,
            Router,
            Location,
            UtilitiesService])
    ], PromotionsPage);
    return PromotionsPage;
}());
export { PromotionsPage };
//# sourceMappingURL=promotions.page.js.map