import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ChangeEmailPage } from './change-email.page';
var routes = [
    {
        path: '',
        component: ChangeEmailPage
    }
];
var ChangeEmailPageModule = /** @class */ (function () {
    function ChangeEmailPageModule() {
    }
    ChangeEmailPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ChangeEmailPage]
        })
    ], ChangeEmailPageModule);
    return ChangeEmailPageModule;
}());
export { ChangeEmailPageModule };
//# sourceMappingURL=change-email.module.js.map