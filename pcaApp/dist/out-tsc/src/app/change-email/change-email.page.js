import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { CONFIGURATION } from 'src/services/config.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
var ChangeEmailPage = /** @class */ (function () {
    function ChangeEmailPage(_accountService, location, _utilitiesService) {
        this._accountService = _accountService;
        this.location = location;
        this._utilitiesService = _utilitiesService;
        this.form = {
            email: '',
            newEmail: '',
            user_type: '',
            user_id: '',
        };
        this.showImage = false;
        this.imageUrl = 'assets/img/placeholder.png';
        this.cameraUpdate = false;
        this.loaded = false;
    }
    ChangeEmailPage.prototype.ngOnInit = function () {
    };
    ChangeEmailPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._accountService.checkUserSession();
        this.form.user_id = this._accountService.userData.id;
        this.form.user_type = this._accountService.userData.member_type;
        this._accountService.getUserDetailProfile(function () {
            _this.form.email = _this._accountService.userData.email;
            if (_this._accountService.userData.profile_picture !== null) {
                _this.imageUrl = CONFIGURATION.base_url + _this._accountService.userData.profile_picture;
            }
            _this.loaded = true;
        });
    };
    ChangeEmailPage.prototype.goBack = function () {
        this.location.back();
    };
    ChangeEmailPage.prototype.saveEmail = function () {
        var _this = this;
        if (this.form.newEmail !== '') {
            var param = {
                user_type: this.form.user_type,
                user_id: this.form.user_id,
                email: this.form.newEmail
            };
            this._accountService.updateEmail(param, function () {
                _this._utilitiesService.alertMessageCallback('Successful', 'Email updated successfully', function () {
                    // this.navCtrl.setRoot(HomePage);
                });
            });
        }
        else {
            this._utilitiesService.alertMessage('Validation Error', 'New email cannot be empty');
        }
    };
    ChangeEmailPage = tslib_1.__decorate([
        Component({
            selector: 'app-change-email',
            templateUrl: './change-email.page.html',
            styleUrls: ['./change-email.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AccountService,
            Location,
            UtilitiesService])
    ], ChangeEmailPage);
    return ChangeEmailPage;
}());
export { ChangeEmailPage };
//# sourceMappingURL=change-email.page.js.map