import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MapViewPage } from './map-view.page';
var routes = [
    {
        path: '',
        component: MapViewPage
    }
];
var MapViewPageModule = /** @class */ (function () {
    function MapViewPageModule() {
    }
    MapViewPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [MapViewPage]
        })
    ], MapViewPageModule);
    return MapViewPageModule;
}());
export { MapViewPageModule };
//# sourceMappingURL=map-view.module.js.map