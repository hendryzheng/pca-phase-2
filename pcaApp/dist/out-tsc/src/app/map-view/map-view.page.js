import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UtilitiesService } from 'src/services/utilities.service';
import { PropertyListingService } from 'src/services/property-listing.service';
var MapViewPage = /** @class */ (function () {
    function MapViewPage(router, _utilitiesService, _propertyListingService, location, geolocation) {
        this.router = router;
        this._utilitiesService = _utilitiesService;
        this._propertyListingService = _propertyListingService;
        this.location = location;
        this.geolocation = geolocation;
        this.height = 500;
        this.width = 500;
        this.basemap = null;
        this.isLocationTurnedOn = false;
    }
    MapViewPage.prototype.ngOnInit = function () {
    };
    MapViewPage.prototype.ionViewWillEnter = function () {
        console.log('ionViewDidLoad MapsPage');
        var physicalScreenHeight = window.screen.height;
        this.height = physicalScreenHeight;
        var body = document.getElementsByTagName('body');
        var physicalScreenWidth = body[0].offsetWidth;
        this.width = physicalScreenWidth;
        this.loadmap(this._propertyListingService.selectedPropertyListingDetail.location.lat, this._propertyListingService.selectedPropertyListingDetail.location.lng);
    };
    MapViewPage.prototype.getSelectedListing = function () {
        return this._propertyListingService.selectedPropertyListingDetail;
    };
    MapViewPage.prototype.ionViewDidEnter = function () {
        this.geolocation.getCurrentPosition().then(function (resp) {
        });
    };
    MapViewPage.prototype.goBack = function () {
        this.location.back();
    };
    MapViewPage.prototype.loadmap = function (lat, lng) {
        L.Icon.Default.imagePath = "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/images"; // missing setup
        var center = L.bounds([1.56073, 104.11475], [1.16, 103.502]).getCenter();
        var map = L.map('mapdiv').setView([center.x, center.y], 16);
        var basemap = L.tileLayer('https://maps-{s}.onemap.sg/v3/Original/{z}/{x}/{y}.png', {
            detectRetina: true,
            maxZoom: 18,
            minZoom: 12
        });
        map.setMaxBounds([[1.56073, 104.1147], [1.16, 103.502]]);
        basemap.addTo(map);
        var content = document.getElementById('map-pin').innerHTML;
        var marker = new L.Marker([lat, lng], { bounceOnAdd: false }).addTo(map);
        var popup = L.popup()
            .setLatLng([lat, lng])
            .setContent(content)
            .openOn(map);
    };
    MapViewPage.prototype.getWidth = function () {
        return this.width;
    };
    MapViewPage.prototype.getHeight = function () {
        return this.height;
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", ElementRef)
    ], MapViewPage.prototype, "mapContainer", void 0);
    MapViewPage = tslib_1.__decorate([
        Component({
            selector: 'app-map-view',
            templateUrl: './map-view.page.html',
            styleUrls: ['./map-view.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            UtilitiesService,
            PropertyListingService,
            Location,
            Geolocation])
    ], MapViewPage);
    return MapViewPage;
}());
export { MapViewPage };
//# sourceMappingURL=map-view.page.js.map