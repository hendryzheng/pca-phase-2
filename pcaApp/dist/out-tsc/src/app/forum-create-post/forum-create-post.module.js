import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ForumCreatePostPage } from './forum-create-post.page';
var routes = [
    {
        path: '',
        component: ForumCreatePostPage
    }
];
var ForumCreatePostPageModule = /** @class */ (function () {
    function ForumCreatePostPageModule() {
    }
    ForumCreatePostPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ForumCreatePostPage]
        })
    ], ForumCreatePostPageModule);
    return ForumCreatePostPageModule;
}());
export { ForumCreatePostPageModule };
//# sourceMappingURL=forum-create-post.module.js.map