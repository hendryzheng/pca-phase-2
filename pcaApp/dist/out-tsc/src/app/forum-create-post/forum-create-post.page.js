import * as tslib_1 from "tslib";
import { Component, NgZone } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { MenuService } from 'src/services/menu.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
import { CONFIGURATION } from 'src/services/config.service';
var ForumCreatePostPage = /** @class */ (function () {
    function ForumCreatePostPage(_zone, _accountService, _menuService, _location, _utilitiesService) {
        this._zone = _zone;
        this._accountService = _accountService;
        this._menuService = _menuService;
        this._location = _location;
        this._utilitiesService = _utilitiesService;
        this.showImage = false;
        this.imageUrl = 'assets/img/placeholder.png';
        this.img_url = 'assets/img/person-placeholder.png';
        this.form = {
            title: '',
            content: '',
            user_type: this._accountService.userData.member_type,
            user_id: this._accountService.userData.id
        };
    }
    ForumCreatePostPage.prototype.ngOnInit = function () {
    };
    ForumCreatePostPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreatePostPage');
    };
    ForumCreatePostPage.prototype.ionViewWillEnter = function () {
        this._accountService.checkUserSession();
        if (this._accountService.userData.profile_picture !== null) {
            this.img_url = CONFIGURATION.base_url + this._accountService.userData.profile_picture;
        }
    };
    ForumCreatePostPage.prototype.getUserData = function () {
        return this._accountService.userData;
    };
    ForumCreatePostPage.prototype.getAccount = function () {
        return this._accountService.userData;
    };
    ForumCreatePostPage.prototype.validateForm = function () {
        var validate = true;
        console.log(this.form);
        if (this._utilitiesService.isEmpty(this.form.title)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.content)) {
            validate = false;
        }
        if (!validate) {
            this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details');
        }
        console.log(validate);
        return validate;
    };
    ForumCreatePostPage.prototype.createPost = function () {
        var _this = this;
        if (this.validateForm()) {
            var formSubmission = {
                'Forumpost[post_title]': this.form.title,
                'Forumpost[post_description]': this.form.content
            };
            if (this._accountService.userData.member_type === 'agent') {
                formSubmission['Forumpost[agent_id]'] = this._accountService.userData.id;
            }
            else {
                formSubmission['Forumpost[account_id]'] = this._accountService.userData.id;
            }
            this._menuService.submitPost(formSubmission, function (id) {
                console.log(id);
                if (_this.imageUrl !== 'assets/img/placeholder.png') {
                    _this._menuService.uploadPostImage(id, _this.imageUrl, function () {
                        _this._utilitiesService.alertMessageCallback('Success', 'Post submitted successfully', function () {
                            _this._location.back();
                        });
                    });
                }
                else {
                    _this._utilitiesService.alertMessageCallback('Success', 'Post submitted successfully', function () {
                        _this._location.back();
                    });
                }
            });
        }
    };
    ForumCreatePostPage.prototype.goBack = function () {
        this._location.back();
    };
    ForumCreatePostPage.prototype.toBase64 = function (url) {
        return new Promise(function (resolve) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {
                    resolve(reader.result);
                };
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.send();
        });
    };
    ForumCreatePostPage.prototype.takePhotoCamera = function () {
        var _this = this;
        navigator.camera.getPicture(function (imageData) {
            _this._zone.run(function () {
                plugins.crop.promise(imageData, {
                    quality: 75
                }).then(function (newPath) {
                    return _this.toBase64(newPath).then(function (base64Img) {
                        _this._zone.run(function () {
                            console.log(base64Img);
                            _this.showImage = true;
                            _this.imageUrl = base64Img;
                            // this._studentService.uploadStudentImage(this.imageUrl, () => { });
                        });
                    });
                }, function (error) {
                    console.log('CROP ERROR -> ' + JSON.stringify(error));
                    // alert('CROP ERROR: ' + JSON.stringify(error));
                });
            });
        }, function (error) {
            console.log(error);
        }, {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA
        });
    };
    ForumCreatePostPage.prototype.takePhotoGallery = function () {
        var _this = this;
        navigator.camera.getPicture(function (imageData) {
            _this._zone.run(function () {
                plugins.crop.promise(imageData, {
                    quality: 75
                }).then(function (newPath) {
                    return _this.toBase64(newPath).then(function (base64Img) {
                        _this._zone.run(function () {
                            console.log(base64Img);
                            _this.showImage = true;
                            _this.imageUrl = base64Img;
                            // this._studentService.uploadStudentImage(this.imageUrl, () => { });
                        });
                    });
                }, function (error) {
                    console.log('CROP ERROR -> ' + JSON.stringify(error));
                    // alert('CROP ERROR: ' + JSON.stringify(error));
                });
            });
        }, function (error) {
            console.log(error);
        }, {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY
        });
    };
    ForumCreatePostPage = tslib_1.__decorate([
        Component({
            selector: 'app-forum-create-post',
            templateUrl: './forum-create-post.page.html',
            styleUrls: ['./forum-create-post.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NgZone,
            AccountService,
            MenuService,
            Location,
            UtilitiesService])
    ], ForumCreatePostPage);
    return ForumCreatePostPage;
}());
export { ForumCreatePostPage };
//# sourceMappingURL=forum-create-post.page.js.map