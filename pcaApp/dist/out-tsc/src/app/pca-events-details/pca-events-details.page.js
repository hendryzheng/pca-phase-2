import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var PcaEventsDetailsPage = /** @class */ (function () {
    function PcaEventsDetailsPage(location) {
        this.location = location;
    }
    PcaEventsDetailsPage.prototype.ngOnInit = function () {
    };
    PcaEventsDetailsPage.prototype.goBack = function () {
        this.location.back();
    };
    PcaEventsDetailsPage = tslib_1.__decorate([
        Component({
            selector: 'app-pca-events-details',
            templateUrl: './pca-events-details.page.html',
            styleUrls: ['./pca-events-details.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], PcaEventsDetailsPage);
    return PcaEventsDetailsPage;
}());
export { PcaEventsDetailsPage };
//# sourceMappingURL=pca-events-details.page.js.map