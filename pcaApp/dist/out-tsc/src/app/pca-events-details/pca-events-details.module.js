import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PcaEventsDetailsPage } from './pca-events-details.page';
var routes = [
    {
        path: '',
        component: PcaEventsDetailsPage
    }
];
var PcaEventsDetailsPageModule = /** @class */ (function () {
    function PcaEventsDetailsPageModule() {
    }
    PcaEventsDetailsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PcaEventsDetailsPage]
        })
    ], PcaEventsDetailsPageModule);
    return PcaEventsDetailsPageModule;
}());
export { PcaEventsDetailsPageModule };
//# sourceMappingURL=pca-events-details.module.js.map