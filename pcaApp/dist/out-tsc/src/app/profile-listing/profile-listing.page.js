import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var ProfileListingPage = /** @class */ (function () {
    function ProfileListingPage(location) {
        this.location = location;
        this.isNotif = true;
        this.notifState = 'for-sale';
    }
    ProfileListingPage.prototype.ngOnInit = function () {
    };
    ProfileListingPage.prototype.toggleNotif = function (string) {
        this.notifState = string;
    };
    ProfileListingPage.prototype.goBack = function () {
        this.location.back();
    };
    ProfileListingPage = tslib_1.__decorate([
        Component({
            selector: 'app-profile-listing',
            templateUrl: './profile-listing.page.html',
            styleUrls: ['./profile-listing.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], ProfileListingPage);
    return ProfileListingPage;
}());
export { ProfileListingPage };
//# sourceMappingURL=profile-listing.page.js.map