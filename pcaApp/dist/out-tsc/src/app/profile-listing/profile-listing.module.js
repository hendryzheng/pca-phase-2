import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ProfileListingPage } from './profile-listing.page';
var routes = [
    {
        path: '',
        component: ProfileListingPage
    }
];
var ProfileListingPageModule = /** @class */ (function () {
    function ProfileListingPageModule() {
    }
    ProfileListingPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ProfileListingPage]
        })
    ], ProfileListingPageModule);
    return ProfileListingPageModule;
}());
export { ProfileListingPageModule };
//# sourceMappingURL=profile-listing.module.js.map