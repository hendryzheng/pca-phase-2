import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PcaSeniorLeadersPage } from './pca-senior-leaders.page';
var routes = [
    {
        path: '',
        component: PcaSeniorLeadersPage
    }
];
var PcaSeniorLeadersPageModule = /** @class */ (function () {
    function PcaSeniorLeadersPageModule() {
    }
    PcaSeniorLeadersPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PcaSeniorLeadersPage]
        })
    ], PcaSeniorLeadersPageModule);
    return PcaSeniorLeadersPageModule;
}());
export { PcaSeniorLeadersPageModule };
//# sourceMappingURL=pca-senior-leaders.module.js.map