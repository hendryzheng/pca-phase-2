import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
import { CONFIGURATION } from 'src/services/config.service';
var PcaSeniorLeadersPage = /** @class */ (function () {
    function PcaSeniorLeadersPage(_menuService, router, ga, _utilitiesService, location) {
        this._menuService = _menuService;
        this.router = router;
        this.ga = ga;
        this._utilitiesService = _utilitiesService;
        this.location = location;
    }
    PcaSeniorLeadersPage.prototype.ngOnInit = function () {
    };
    PcaSeniorLeadersPage.prototype.ionViewWillEnter = function () {
    };
    PcaSeniorLeadersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PcaCategoriesPage');
    };
    PcaSeniorLeadersPage.prototype.getListing = function () {
        return this._menuService.listing;
    };
    PcaSeniorLeadersPage.prototype.getMenu = function () {
        return this._menuService.selectedMenu.menu_name;
    };
    PcaSeniorLeadersPage.prototype.getMenuObj = function () {
        return this._menuService.selectedMenu;
    };
    PcaSeniorLeadersPage.prototype.goBack = function () {
        this.location.back();
    };
    PcaSeniorLeadersPage.prototype.viewItem = function (item) {
    };
    PcaSeniorLeadersPage.prototype.getBase = function () {
        return CONFIGURATION.base_url;
    };
    PcaSeniorLeadersPage = tslib_1.__decorate([
        Component({
            selector: 'app-pca-senior-leaders',
            templateUrl: './pca-senior-leaders.page.html',
            styleUrls: ['./pca-senior-leaders.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            Router,
            GoogleAnalytics,
            UtilitiesService,
            Location])
    ], PcaSeniorLeadersPage);
    return PcaSeniorLeadersPage;
}());
export { PcaSeniorLeadersPage };
//# sourceMappingURL=pca-senior-leaders.page.js.map