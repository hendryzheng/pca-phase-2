import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PdpaAgreementPage } from './pdpa-agreement.page';
var routes = [
    {
        path: '',
        component: PdpaAgreementPage
    }
];
var PdpaAgreementPageModule = /** @class */ (function () {
    function PdpaAgreementPageModule() {
    }
    PdpaAgreementPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PdpaAgreementPage]
        })
    ], PdpaAgreementPageModule);
    return PdpaAgreementPageModule;
}());
export { PdpaAgreementPageModule };
//# sourceMappingURL=pdpa-agreement.module.js.map