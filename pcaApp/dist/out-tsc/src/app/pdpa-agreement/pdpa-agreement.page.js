import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var PdpaAgreementPage = /** @class */ (function () {
    function PdpaAgreementPage(location) {
        this.location = location;
    }
    PdpaAgreementPage.prototype.ngOnInit = function () {
    };
    PdpaAgreementPage.prototype.goBack = function () {
        this.location.back();
    };
    PdpaAgreementPage = tslib_1.__decorate([
        Component({
            selector: 'app-pdpa-agreement',
            templateUrl: './pdpa-agreement.page.html',
            styleUrls: ['./pdpa-agreement.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], PdpaAgreementPage);
    return PdpaAgreementPage;
}());
export { PdpaAgreementPage };
//# sourceMappingURL=pdpa-agreement.page.js.map