import * as tslib_1 from "tslib";
import { Component, NgZone } from '@angular/core';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Base64 } from '@ionic-native/base64/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { CONFIGURATION } from 'src/services/config.service';
import { Location } from '@angular/common';
var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(_utilitiesService, _zone, base64, location, _DomSanitizationService, _accountService) {
        this._utilitiesService = _utilitiesService;
        this._zone = _zone;
        this.base64 = base64;
        this.location = location;
        this._DomSanitizationService = _DomSanitizationService;
        this._accountService = _accountService;
        this.form = {
            firstname: '',
            title: '',
            lastname: '',
            gender: '',
            marketing_video: '',
            facebook: '',
            instagram: '',
            wechat: '',
            user_type: '',
            user_id: '',
            email: '',
            mobile: ''
        };
        this.showImage = false;
        this.imageUrl = 'assets/img/placeholder.png';
        this.cameraUpdate = false;
        this.loaded = false;
    }
    EditProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    EditProfilePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._accountService.checkUserSession();
        this.form.user_id = this._accountService.userData.id;
        this.form.user_type = this._accountService.userData.member_type;
        this._accountService.getUserDetailProfile(function () {
            _this.form.email = _this._accountService.userData.email;
            _this.form.title = _this._accountService.userData.title;
            _this.form.firstname = _this._accountService.userData.firstname;
            _this.form.lastname = _this._accountService.userData.lastname;
            _this.form.gender = _this._accountService.userData.gender;
            _this.form.facebook = _this._accountService.userData.facebook;
            _this.form.marketing_video = _this._accountService.userData.marketing_video;
            _this.form.instagram = _this._accountService.userData.instagram;
            _this.form.wechat = _this._accountService.userData.wechat;
            _this.form.mobile = _this._accountService.userData.mobile;
            if (_this._accountService.userData.profile_picture !== null) {
                _this.imageUrl = CONFIGURATION.base_url + _this._accountService.userData.profile_picture;
            }
            _this.loaded = true;
        });
    };
    EditProfilePage.prototype.goBack = function () {
        this.location.back();
    };
    EditProfilePage.prototype.updateProfile = function () {
        var _this = this;
        this._accountService.updateProfile(this.form, function () {
            if (_this.cameraUpdate) {
                _this._accountService.uploadImage(_this.imageUrl, function () {
                    _this.imageUrl = _this._accountService.userData.profile_picture;
                    _this._utilitiesService.alertMessageCallback('Successful', 'Profile updated successfully', function () {
                        // this.navCtrl.setRoot(HomePage);
                    });
                });
            }
            else {
                _this._utilitiesService.alertMessageCallback('Successful', 'Profile updated successfully', function () {
                    // this.navCtrl.setRoot(HomePage);
                });
            }
        });
    };
    EditProfilePage.prototype.takePhotoOptionPopup = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alertController, alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        alertController = this._utilitiesService.getAlertCtrl();
                        return [4 /*yield*/, alertController.create({
                                header: 'Choose option',
                                message: '',
                                buttons: [
                                    {
                                        text: 'Take Photo',
                                        handler: function (data) {
                                            console.log('OK clicked');
                                            _this.takePhotoCamera();
                                        }
                                    },
                                    {
                                        text: 'Choose from Gallery',
                                        handler: function (data) {
                                            console.log('OK clicked');
                                            _this.takePhotoGallery();
                                        }
                                    },
                                ]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [2 /*return*/, alert.present()];
                }
            });
        });
    };
    EditProfilePage.prototype.takePhotoCamera = function () {
        var _this = this;
        navigator.camera.getPicture(function (imageData) {
            _this._zone.run(function () {
                plugins.crop.promise(imageData, {
                    quality: 75
                }).then(function (newPath) {
                    _this.base64.encodeFile(newPath).then(function (base64File) {
                        console.log(base64File);
                        _this._zone.run(function () {
                            _this.showImage = true;
                            _this.imageUrl = base64File;
                            // console.log(this.imageUrl);
                            _this.cameraUpdate = true;
                        });
                    }, function (err) {
                        console.log(err);
                    });
                }, function (error) {
                    console.log('CROP ERROR -> ' + JSON.stringify(error));
                    // alert('CROP ERROR: ' + JSON.stringify(error));
                });
            });
        }, function (error) {
            console.log(error);
        }, {
            destinationType: Camera.DestinationType.FILE_URI,
            quality: 75,
            correctOrientation: true,
            sourceType: Camera.PictureSourceType.CAMERA
        });
    };
    EditProfilePage.prototype.takePhotoGallery = function () {
        var _this = this;
        navigator.camera.getPicture(function (imageData) {
            _this._zone.run(function () {
                plugins.crop.promise(imageData, {
                    quality: 75
                }).then(function (newPath) {
                    _this.base64.encodeFile(newPath).then(function (base64File) {
                        console.log(base64File);
                        _this._zone.run(function () {
                            _this.showImage = true;
                            _this.imageUrl = base64File;
                            // console.log(this.imageUrl);
                            _this.cameraUpdate = true;
                        });
                    }, function (err) {
                        console.log(err);
                    });
                }, function (error) {
                    console.log('CROP ERROR -> ' + JSON.stringify(error));
                    // alert('CROP ERROR: ' + JSON.stringify(error));
                });
            });
        }, function (error) {
            console.log(error);
        }, {
            quality: 75,
            correctOrientation: true,
            encodingType: Camera.EncodingType.JPEG,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM
        });
    };
    EditProfilePage.prototype.toBase64 = function (url) {
        return new Promise(function (resolve) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {
                    resolve(reader.result);
                };
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.send();
        });
    };
    EditProfilePage.prototype.resize = function (base64Img, width, height) {
        var img = new Image();
        img.src = base64Img;
        var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
        canvas.width = width;
        canvas.height = height;
        ctx.drawImage(img, 0, 0, width, height);
        return canvas.toDataURL('image/jpeg');
    };
    EditProfilePage = tslib_1.__decorate([
        Component({
            selector: 'app-edit-profile',
            templateUrl: './edit-profile.page.html',
            styleUrls: ['./edit-profile.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [UtilitiesService,
            NgZone,
            Base64,
            Location,
            DomSanitizer,
            AccountService])
    ], EditProfilePage);
    return EditProfilePage;
}());
export { EditProfilePage };
//# sourceMappingURL=edit-profile.page.js.map