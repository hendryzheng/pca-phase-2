import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { PropertyListingService } from 'src/services/property-listing.service';
import { AlertController, ModalController } from '@ionic/angular';
import { UtilitiesService } from 'src/services/utilities.service';
var SearchPropertyFilterPage = /** @class */ (function () {
    function SearchPropertyFilterPage(alertCtrl, _utilitiesService, modalController, _propertyListingService) {
        this.alertCtrl = alertCtrl;
        this._utilitiesService = _utilitiesService;
        this.modalController = modalController;
        this._propertyListingService = _propertyListingService;
        this.from = 100000;
        this.to = 20000000;
        this.fromFloor = 500;
        this.toFloor = 10000;
        this.subcategories = [];
        this.hideSearch = true;
        this.searchKeywordListingResult = [];
        this.searchKeywordModel = '';
        this.height = '500';
    }
    SearchPropertyFilterPage.prototype.ionViewWillEnter = function () {
        var physicalScreenHeight = window.screen.height;
        this.height = physicalScreenHeight;
        var obj = this._propertyListingService.listing_categories[0].main_categories[0];
        this.subcategories = obj.sub_categories;
    };
    SearchPropertyFilterPage.prototype.change = function (ev) {
        console.log(ev);
        this.from = ev.detail.value.lower;
        this._propertyListingService.searchListingParams.price_min = this.from;
        this.to = ev.detail.value.upper;
        this._propertyListingService.searchListingParams.price_max = this.to;
    };
    SearchPropertyFilterPage.prototype.changeFloorArea = function (ev) {
        console.log(ev);
        this.fromFloor = ev.detail.value.lower;
        this._propertyListingService.searchListingParams.floorarea_min = this.fromFloor;
        this.toFloor = ev.detail.value.upper;
        this._propertyListingService.searchListingParams.floorarea_max = this.toFloor;
    };
    SearchPropertyFilterPage.prototype.close = function () {
        if (this.validateSearchParam()) {
            this.modalController.dismiss();
        }
    };
    SearchPropertyFilterPage.prototype.getSearchListingParam = function () {
        return this._propertyListingService.searchListingParams;
    };
    SearchPropertyFilterPage.prototype.mainCatChosen = function (item) {
        this.subcategories = item.sub_categories;
        console.log(item);
    };
    SearchPropertyFilterPage.prototype.search = function () {
        console.log(this.getSearchListingParam());
        var params = JSON.parse(JSON.stringify(this._propertyListingService.searchListingParams));
        if (this.validateSearchParam()) {
            this.modalController.dismiss();
        }
    };
    SearchPropertyFilterPage.prototype.searchKeyword = function (ev) {
        var _this = this;
        if (ev.target.value == '') {
            this.hideSearch = true;
            this.searchKeywordListingResult = [];
            this._propertyListingService.selectedKeywordSearch = '';
            this._propertyListingService.searchListingParams.query_type = '';
            this._propertyListingService.searchListingParams.query_ids = '';
            this._propertyListingService.searchListingParams.query_coords = '';
        }
        else {
            this._propertyListingService.getInputRequestInfo(ev.target.value, function () {
                _this.searchKeywordListingResult = _this._propertyListingService.searchInputRequestResult;
                _this.hideSearch = false;
            });
        }
    };
    SearchPropertyFilterPage.prototype.selectKeywordResult = function (item) {
        console.log(item);
        this._propertyListingService.selectedKeywordSearch = item.title;
        this._propertyListingService.selectedKeywordSearchSubtitle = item.subtitle;
        this._propertyListingService.searchListingParams.query_type = item.type;
        this._propertyListingService.searchListingParams.query_ids = item.id;
        this._propertyListingService.searchListingParams.query_coords = item.coordinates.lat + ',' + item.coordinates.lng;
        this.hideSearch = true;
    };
    SearchPropertyFilterPage.prototype.validateSearchParam = function () {
        var subcategories = '';
        for (var i = 0; i < this.subcategories.length; i++) {
            if (this.subcategories[i].checked) {
                subcategories += this.subcategories[i].key + ',';
            }
        }
        if (subcategories !== '') {
            subcategories = subcategories.substring(0, subcategories.length - 1);
        }
        this._propertyListingService.searchListingParams.sub_categories = subcategories;
        if (this._propertyListingService.selectedKeywordSearch == '') {
            this._utilitiesService.alertMessage('Location is compulsory', 'Please select location to narrow down your search');
            return false;
        }
        // if (this._propertyListingService.searchListingParams.sub_categories == '') {
        //   this._utilitiesService.alertMessage('Subcategories is compulsory', 'Please select subcategories to narrow down your search');
        //   return false;
        // }
        return true;
    };
    SearchPropertyFilterPage = tslib_1.__decorate([
        Component({
            selector: 'app-search-property-filter',
            templateUrl: './search-property-filter.page.html',
            styleUrls: ['./search-property-filter.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AlertController,
            UtilitiesService,
            ModalController,
            PropertyListingService])
    ], SearchPropertyFilterPage);
    return SearchPropertyFilterPage;
}());
export { SearchPropertyFilterPage };
//# sourceMappingURL=search-property-filter.page.js.map