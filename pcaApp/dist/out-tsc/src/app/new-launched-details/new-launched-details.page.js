import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var NewLaunchedDetailsPage = /** @class */ (function () {
    function NewLaunchedDetailsPage(location) {
        this.location = location;
    }
    NewLaunchedDetailsPage.prototype.ngOnInit = function () {
    };
    NewLaunchedDetailsPage.prototype.goBack = function () {
        this.location.back();
    };
    NewLaunchedDetailsPage = tslib_1.__decorate([
        Component({
            selector: 'app-new-launched-details',
            templateUrl: './new-launched-details.page.html',
            styleUrls: ['./new-launched-details.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], NewLaunchedDetailsPage);
    return NewLaunchedDetailsPage;
}());
export { NewLaunchedDetailsPage };
//# sourceMappingURL=new-launched-details.page.js.map