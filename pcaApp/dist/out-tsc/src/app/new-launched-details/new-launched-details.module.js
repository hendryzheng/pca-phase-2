import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NewLaunchedDetailsPage } from './new-launched-details.page';
var routes = [
    {
        path: '',
        component: NewLaunchedDetailsPage
    }
];
var NewLaunchedDetailsPageModule = /** @class */ (function () {
    function NewLaunchedDetailsPageModule() {
    }
    NewLaunchedDetailsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [NewLaunchedDetailsPage]
        })
    ], NewLaunchedDetailsPageModule);
    return NewLaunchedDetailsPageModule;
}());
export { NewLaunchedDetailsPageModule };
//# sourceMappingURL=new-launched-details.module.js.map