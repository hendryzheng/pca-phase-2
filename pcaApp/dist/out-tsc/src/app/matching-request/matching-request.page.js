import * as tslib_1 from "tslib";
import { Component, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { PropertyListingService } from 'src/services/property-listing.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { UtilitiesService } from 'src/services/utilities.service';
var MatchingRequestPage = /** @class */ (function () {
    function MatchingRequestPage(location, geolocation, _utilitiesService, router, _zone, _propertyListingService) {
        this.location = location;
        this.geolocation = geolocation;
        this._utilitiesService = _utilitiesService;
        this.router = router;
        this._zone = _zone;
        this._propertyListingService = _propertyListingService;
        this.isNotif = true;
        this.notifState = 'listing-grid';
        this.searchKeyword = '';
        this.listings = [];
        this.listingCount = 0;
    }
    MatchingRequestPage.prototype.ngOnInit = function () {
    };
    MatchingRequestPage.prototype.toggleNotif = function (string) {
        this.notifState = string;
    };
    MatchingRequestPage.prototype.whatsapp = function (phone) {
        return 'https://api.whatsapp.com/send?phone=' + this._utilitiesService.formatWhatsappNo(phone);
    };
    MatchingRequestPage.prototype.viewListing = function (item) {
        this._propertyListingService.selectedPropertyListingDetail = item;
        console.log(this._propertyListingService.selectedPropertyListingDetail);
        this.router.navigateByUrl('/property-listing');
    };
    MatchingRequestPage.prototype.viewMap = function (item) {
        this._propertyListingService.selectedPropertyListingDetail = item;
        console.log(this._propertyListingService.selectedPropertyListingDetail);
        this.router.navigateByUrl('/map-view');
    };
    MatchingRequestPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this._propertyListingService.searchListingParams['page_num'] = 1;
        this._propertyListingService.searchListingParams['page_size'] = 20;
        this.searchKeyword = this._propertyListingService.selectedKeywordSearch;
        this.geolocation.getCurrentPosition().then(function (resp) {
            // if(this._propertyListingService.searchListingParams.query_coords == ''){
            //   // this._propertyListingService.searchListingParams.query_coords = resp.coords.latitude+','+resp.coords.longitude;
            // }
            _this._propertyListingService.searchListings(function () {
                _this._zone.run(function () {
                    _this.listingCount = _this._propertyListingService.propertyListingCount;
                    _this.listings = _this._propertyListingService.propertyListingResult;
                });
            });
        });
    };
    MatchingRequestPage.prototype.goBack = function () {
        this.location.back();
    };
    MatchingRequestPage.prototype.search = function (ev) {
        console.log(ev);
        // if(this.searchKeyword !== ''){
        //   this._propertyListingService.searchListingParams.keyword = this.searchKeyword;
        //   this._propertyListingService.searchListings( ()=>{
        //     this.listings = this._propertyListingService.propertyListingResult;
        //   });
        // }
    };
    MatchingRequestPage.prototype.getSearchListingResult = function () {
        this._propertyListingService.propertyListingResult;
    };
    MatchingRequestPage = tslib_1.__decorate([
        Component({
            selector: 'app-matching-request',
            templateUrl: './matching-request.page.html',
            styleUrls: ['./matching-request.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location,
            Geolocation,
            UtilitiesService,
            Router,
            NgZone,
            PropertyListingService])
    ], MatchingRequestPage);
    return MatchingRequestPage;
}());
export { MatchingRequestPage };
//# sourceMappingURL=matching-request.page.js.map