import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MatchingRequestPage } from './matching-request.page';
var routes = [
    {
        path: '',
        component: MatchingRequestPage
    }
];
var MatchingRequestPageModule = /** @class */ (function () {
    function MatchingRequestPageModule() {
    }
    MatchingRequestPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [MatchingRequestPage]
        })
    ], MatchingRequestPageModule);
    return MatchingRequestPageModule;
}());
export { MatchingRequestPageModule };
//# sourceMappingURL=matching-request.module.js.map