import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ConnectToProjectPage } from './connect-to-project.page';
var routes = [
    {
        path: '',
        component: ConnectToProjectPage
    }
];
var ConnectToProjectPageModule = /** @class */ (function () {
    function ConnectToProjectPageModule() {
    }
    ConnectToProjectPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ConnectToProjectPage]
        })
    ], ConnectToProjectPageModule);
    return ConnectToProjectPageModule;
}());
export { ConnectToProjectPageModule };
//# sourceMappingURL=connect-to-project.module.js.map