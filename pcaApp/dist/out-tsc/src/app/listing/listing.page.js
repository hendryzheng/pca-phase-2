import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MenuService } from 'src/services/menu.service';
import { Router } from '@angular/router';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { UtilitiesService } from 'src/services/utilities.service';
import { CONFIGURATION } from 'src/services/config.service';
import { Location } from '@angular/common';
var ListingPage = /** @class */ (function () {
    function ListingPage(_menuService, router, ga, _utilitiesService, location) {
        this._menuService = _menuService;
        this.router = router;
        this.ga = ga;
        this._utilitiesService = _utilitiesService;
        this.location = location;
        this.pcaCategories = [];
    }
    ListingPage.prototype.ngOnInit = function () {
    };
    ListingPage.prototype.ionViewWillEnter = function () {
    };
    ListingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PcaCategoriesPage');
    };
    ListingPage.prototype.getPcaCategories = function () {
        return this._menuService.pca_categories;
    };
    ListingPage.prototype.getMenu = function () {
        return this._menuService.selected_pca_category.category_name;
    };
    ListingPage.prototype.getMenuObj = function () {
        return this._menuService.selectedMenu;
    };
    ListingPage.prototype.getListing = function () {
        return this._menuService.listing;
    };
    ListingPage.prototype.openListing = function (item) {
        this._menuService.selected_pca_category = item;
        this.router.navigateByUrl('/pca-listing');
    };
    ListingPage.prototype.goBack = function () {
        this.location.back();
    };
    ListingPage.prototype.getBase = function () {
        return CONFIGURATION.base_url;
    };
    ListingPage.prototype.viewItem = function (item) {
    };
    ListingPage.prototype.openAttachment = function (item) {
        var _this = this;
        var base_url = CONFIGURATION.apiEndpoint;
        this.ga.startTrackerWithId('UA-121102215-1')
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView('Homepage Menu ' + _this.getMenu() + ' > ' + item.listing_name);
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        if (item.listing_type == '2') {
            console.log(item.listing_attachment);
            this._utilitiesService.openBrowser(item.listing_name, item.listing_attachment);
            // const browser = this.iab.create(item.listing_attachment, '_blank', 'location=no');
        }
        else if (item.listing_type == '3') {
            console.log(item);
            window.InAppYouTube.openVideo(item.youtube_id, {
                fullscreen: true
            }, function (result) {
                console.log(JSON.stringify(result));
            }, function (reason) {
                console.log(reason);
            });
            // this.youtube.openVideo(item.youtube_id);
        }
        else {
            this._menuService.selected_listing = item;
            this._menuService.selected_listing.image_path = base_url + item.listing_attachment;
            // this.navCtrl.push(PreviewImagePage);
            this.router.navigateByUrl('/preview-image');
            // this._utilitiesService.openBrowser(item.listing_name, base_url + item.listing_attachment);
            // this.iab.create(base_url+item.listing_attachment, '_blank', 'location=no');
        }
    };
    ListingPage = tslib_1.__decorate([
        Component({
            selector: 'app-listing',
            templateUrl: './listing.page.html',
            styleUrls: ['./listing.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            Router,
            GoogleAnalytics,
            UtilitiesService,
            Location])
    ], ListingPage);
    return ListingPage;
}());
export { ListingPage };
//# sourceMappingURL=listing.page.js.map