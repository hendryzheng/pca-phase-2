import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PropertyMarketingConsentPage } from './property-marketing-consent.page';
import { SignaturePadModule } from 'angular2-signaturepad';
var routes = [
    {
        path: '',
        component: PropertyMarketingConsentPage
    }
];
var PropertyMarketingConsentPageModule = /** @class */ (function () {
    function PropertyMarketingConsentPageModule() {
    }
    PropertyMarketingConsentPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                SignaturePadModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PropertyMarketingConsentPage]
        })
    ], PropertyMarketingConsentPageModule);
    return PropertyMarketingConsentPageModule;
}());
export { PropertyMarketingConsentPageModule };
//# sourceMappingURL=property-marketing-consent.module.js.map