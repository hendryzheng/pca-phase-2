import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { MenuService } from 'src/services/menu.service';
import { AccountService } from 'src/services/account.service';
import { UtilitiesService } from 'src/services/utilities.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
var PropertyMarketingConsentPage = /** @class */ (function () {
    function PropertyMarketingConsentPage(_menuService, _utilitiesService, location, router, _accountService) {
        this._menuService = _menuService;
        this._utilitiesService = _utilitiesService;
        this.location = location;
        this.router = router;
        this._accountService = _accountService;
        this.signaturePadOptions = {
            'minWidth': 2,
            'canvasWidth': 340,
            'canvasHeight': 200
        };
        this.signatureImage = null;
        this.signed = false;
        this.email = '';
        this.agent_email = '';
    }
    PropertyMarketingConsentPage.prototype.ionViewWillEnter = function () {
        this._accountService.checkUserSession();
        this.agent_email = this._accountService.userData.email;
        this.email = this._menuService.customer.email;
    };
    PropertyMarketingConsentPage.prototype.ngOnInit = function () {
    };
    PropertyMarketingConsentPage.prototype.goBack = function () {
        this.location.back();
    };
    PropertyMarketingConsentPage.prototype.drawCancel = function () {
        // this.navCtrl.push(HomePage);
    };
    PropertyMarketingConsentPage.prototype.drawComplete = function () {
        this.signatureImage = this.signaturePad.toDataURL();
        this.signed = true;
        // this.navCtrl.push(HomePage, {signatureImage: this.signatureImage});
    };
    PropertyMarketingConsentPage.prototype.drawClear = function () {
        this.signed = false;
        this.signatureImage = null;
        this.signaturePad.clear();
    };
    PropertyMarketingConsentPage.prototype.cancel = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alertController, alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        alertController = this._utilitiesService.getAlertCtrl();
                        return [4 /*yield*/, alertController.create({
                                header: 'Cancellation',
                                message: 'Are you sure you want to cancel? By proceeding to cancel, your customer has not agreed to the PDPA Consent and Property Marketing Consent. You shall be redirected to home dashbaord page.',
                                buttons: [
                                    {
                                        text: 'OK',
                                        handler: function (data) {
                                            _this.router.navigate(['/tabs'], { replaceUrl: true });
                                        }
                                    },
                                    {
                                        text: 'Cancel',
                                        handler: function (data) {
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [2 /*return*/, alert.present()];
                }
            });
        });
    };
    PropertyMarketingConsentPage.prototype.submit = function () {
        var _this = this;
        this.signatureImage = this.signaturePad.toDataURL();
        console.log(this.signatureImage);
        if (this.signatureImage !== null && this.signatureImage.length !== 2266) {
            var customer_id = this._menuService.customer.id;
            this._menuService.uploadImageSignature(customer_id, this.signatureImage, function (res) {
                _this._utilitiesService.alertMessageCallback('Customer added successfully', 'You have successfully added this customer', function () {
                    _this._menuService.eNamecardState = 'customer';
                    _this.router.navigate(['/tabs'], { replaceUrl: true });
                });
            });
        }
        else {
            this._utilitiesService.alertMessage('Signature Empty', 'Please let customer sign on the signature pad section');
        }
    };
    tslib_1.__decorate([
        ViewChild(SignaturePad),
        tslib_1.__metadata("design:type", SignaturePad)
    ], PropertyMarketingConsentPage.prototype, "signaturePad", void 0);
    PropertyMarketingConsentPage = tslib_1.__decorate([
        Component({
            selector: 'app-property-marketing-consent',
            templateUrl: './property-marketing-consent.page.html',
            styleUrls: ['./property-marketing-consent.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService,
            UtilitiesService,
            Location,
            Router,
            AccountService])
    ], PropertyMarketingConsentPage);
    return PropertyMarketingConsentPage;
}());
export { PropertyMarketingConsentPage };
//# sourceMappingURL=property-marketing-consent.page.js.map