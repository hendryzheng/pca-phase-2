import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
import { PropertyListingService } from 'src/services/property-listing.service';
import { ModalController } from '@ionic/angular';
var MapListingPage = /** @class */ (function () {
    function MapListingPage(_propertyListingService, modalController) {
        this._propertyListingService = _propertyListingService;
        this.modalController = modalController;
        this.height = 500;
        this.width = 500;
        this.basemap = null;
        this.isLocationTurnedOn = false;
    }
    MapListingPage.prototype.ngOnInit = function () {
    };
    MapListingPage.prototype.ionViewWillEnter = function () {
        var physicalScreenHeight = window.screen.height;
        this.height = physicalScreenHeight;
        var body = document.getElementsByTagName('body');
        var physicalScreenWidth = body[0].offsetWidth;
        this.width = physicalScreenWidth;
        this.loadmap();
    };
    MapListingPage.prototype.close = function () {
        this.modalController.dismiss();
    };
    MapListingPage.prototype.loadmap = function () {
        L.Icon.Default.imagePath = "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/images"; // missing setup
        var center = L.bounds([1.56073, 104.11475], [1.16, 103.502]).getCenter();
        var map = L.map('mapdiv').setView([center.x, center.y], 15);
        var basemap = L.tileLayer('https://maps-{s}.onemap.sg/v3/Original/{z}/{x}/{y}.png', {
            detectRetina: true,
            maxZoom: 16,
            minZoom: 10
        });
        map.setMaxBounds([[1.56073, 104.1147], [1.16, 103.502]]);
        basemap.addTo(map);
        // var content = document.getElementById('map-pin').innerHTML;
        for (var i = 0; i < this._propertyListingService.propertyListingResult.length; i++) {
            var content = this.getContentHtml(this._propertyListingService.propertyListingResult[i]);
            var m = new L.Marker([this._propertyListingService.propertyListingResult[i].location.lat, this._propertyListingService.propertyListingResult[i].location.lng], { bounceOnAdd: false }).addTo(map), p = new L.Popup({ autoClose: false, closeOnClick: false })
                .setContent(content)
                .setLatLng([this._propertyListingService.propertyListingResult[i].location.lat, this._propertyListingService.propertyListingResult[i].location.lng]).addTo(map);
            m.bindPopup(p);
        }
        ;
    };
    MapListingPage.prototype.getContentHtml = function (item) {
        var html = '<div class="map-pin-property"><div class="details"><ion-item><ion-thumbnail slot="start">';
        html += '<img src="' + item.photo_url + '">';
        html += '</ion-thumbnail><ion-label><h3>' + item.project_name + '</h3>';
        html += '<div class="place"><ion-icon name="md-pin"></ion-icon>';
        html += '<span>' + item.address_line_1 + ' ' + item.address_line_2 + '</span></div>';
        html += '<div class="price"><ion-button color="primary" size="small">' + item.attributes.price_formatted + '</ion-button>';
        html += '</div></ion-label></ion-item></div></div>';
        return html;
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", ElementRef)
    ], MapListingPage.prototype, "mapContainer", void 0);
    MapListingPage = tslib_1.__decorate([
        Component({
            selector: 'app-map-listing',
            templateUrl: './map-listing.page.html',
            styleUrls: ['./map-listing.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [PropertyListingService, ModalController])
    ], MapListingPage);
    return MapListingPage;
}());
export { MapListingPage };
//# sourceMappingURL=map-listing.page.js.map