import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var PropertyFilterPage = /** @class */ (function () {
    function PropertyFilterPage(location) {
        this.location = location;
        this.isNotif = true;
        this.notifState = 'private';
    }
    PropertyFilterPage.prototype.ngOnInit = function () {
    };
    PropertyFilterPage.prototype.goBack = function () {
        this.location.back();
    };
    PropertyFilterPage.prototype.toggleNotif = function (string) {
        this.notifState = string;
    };
    PropertyFilterPage = tslib_1.__decorate([
        Component({
            selector: 'app-property-filter',
            templateUrl: './property-filter.page.html',
            styleUrls: ['./property-filter.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], PropertyFilterPage);
    return PropertyFilterPage;
}());
export { PropertyFilterPage };
//# sourceMappingURL=property-filter.page.js.map