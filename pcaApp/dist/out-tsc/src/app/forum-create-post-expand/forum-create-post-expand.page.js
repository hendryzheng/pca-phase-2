import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var ForumCreatePostExpandPage = /** @class */ (function () {
    function ForumCreatePostExpandPage() {
    }
    ForumCreatePostExpandPage.prototype.ngOnInit = function () {
    };
    ForumCreatePostExpandPage = tslib_1.__decorate([
        Component({
            selector: 'app-forum-create-post-expand',
            templateUrl: './forum-create-post-expand.page.html',
            styleUrls: ['./forum-create-post-expand.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ForumCreatePostExpandPage);
    return ForumCreatePostExpandPage;
}());
export { ForumCreatePostExpandPage };
//# sourceMappingURL=forum-create-post-expand.page.js.map