import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ForumCreatePostExpandPage } from './forum-create-post-expand.page';
var routes = [
    {
        path: '',
        component: ForumCreatePostExpandPage
    }
];
var ForumCreatePostExpandPageModule = /** @class */ (function () {
    function ForumCreatePostExpandPageModule() {
    }
    ForumCreatePostExpandPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ForumCreatePostExpandPage]
        })
    ], ForumCreatePostExpandPageModule);
    return ForumCreatePostExpandPageModule;
}());
export { ForumCreatePostExpandPageModule };
//# sourceMappingURL=forum-create-post-expand.module.js.map