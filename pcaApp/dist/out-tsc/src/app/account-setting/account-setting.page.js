import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var AccountSettingPage = /** @class */ (function () {
    function AccountSettingPage(location) {
        this.location = location;
    }
    AccountSettingPage.prototype.ngOnInit = function () {
    };
    AccountSettingPage.prototype.goBack = function () {
        this.location.back();
    };
    AccountSettingPage = tslib_1.__decorate([
        Component({
            selector: 'app-account-setting',
            templateUrl: './account-setting.page.html',
            styleUrls: ['./account-setting.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], AccountSettingPage);
    return AccountSettingPage;
}());
export { AccountSettingPage };
//# sourceMappingURL=account-setting.page.js.map