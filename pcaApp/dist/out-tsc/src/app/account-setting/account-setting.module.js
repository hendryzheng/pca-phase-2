import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AccountSettingPage } from './account-setting.page';
var routes = [
    {
        path: '',
        component: AccountSettingPage
    }
];
var AccountSettingPageModule = /** @class */ (function () {
    function AccountSettingPageModule() {
    }
    AccountSettingPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AccountSettingPage]
        })
    ], AccountSettingPageModule);
    return AccountSettingPageModule;
}());
export { AccountSettingPageModule };
//# sourceMappingURL=account-setting.module.js.map