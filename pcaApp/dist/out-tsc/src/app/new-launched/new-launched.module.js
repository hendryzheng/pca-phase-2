import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NewLaunchedPage } from './new-launched.page';
var routes = [
    {
        path: '',
        component: NewLaunchedPage
    }
];
var NewLaunchedPageModule = /** @class */ (function () {
    function NewLaunchedPageModule() {
    }
    NewLaunchedPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [NewLaunchedPage]
        })
    ], NewLaunchedPageModule);
    return NewLaunchedPageModule;
}());
export { NewLaunchedPageModule };
//# sourceMappingURL=new-launched.module.js.map