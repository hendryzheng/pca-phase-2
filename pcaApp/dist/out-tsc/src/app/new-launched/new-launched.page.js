import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var NewLaunchedPage = /** @class */ (function () {
    function NewLaunchedPage(location) {
        this.location = location;
    }
    NewLaunchedPage.prototype.ngOnInit = function () {
    };
    NewLaunchedPage.prototype.goBack = function () {
        this.location.back();
    };
    NewLaunchedPage = tslib_1.__decorate([
        Component({
            selector: 'app-new-launched',
            templateUrl: './new-launched.page.html',
            styleUrls: ['./new-launched.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], NewLaunchedPage);
    return NewLaunchedPage;
}());
export { NewLaunchedPage };
//# sourceMappingURL=new-launched.page.js.map