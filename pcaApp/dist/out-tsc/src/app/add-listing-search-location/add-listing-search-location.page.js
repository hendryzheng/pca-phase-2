import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var AddListingSearchLocationPage = /** @class */ (function () {
    function AddListingSearchLocationPage() {
    }
    AddListingSearchLocationPage.prototype.ngOnInit = function () {
    };
    AddListingSearchLocationPage = tslib_1.__decorate([
        Component({
            selector: 'app-add-listing-search-location',
            templateUrl: './add-listing-search-location.page.html',
            styleUrls: ['./add-listing-search-location.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], AddListingSearchLocationPage);
    return AddListingSearchLocationPage;
}());
export { AddListingSearchLocationPage };
//# sourceMappingURL=add-listing-search-location.page.js.map