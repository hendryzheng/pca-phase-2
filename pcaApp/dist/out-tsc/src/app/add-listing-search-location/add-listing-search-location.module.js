import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddListingSearchLocationPage } from './add-listing-search-location.page';
var routes = [
    {
        path: '',
        component: AddListingSearchLocationPage
    }
];
var AddListingSearchLocationPageModule = /** @class */ (function () {
    function AddListingSearchLocationPageModule() {
    }
    AddListingSearchLocationPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AddListingSearchLocationPage]
        })
    ], AddListingSearchLocationPageModule);
    return AddListingSearchLocationPageModule;
}());
export { AddListingSearchLocationPageModule };
//# sourceMappingURL=add-listing-search-location.module.js.map