import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
var WorkAtHomePage = /** @class */ (function () {
    function WorkAtHomePage(location) {
        this.location = location;
    }
    WorkAtHomePage.prototype.ngOnInit = function () {
    };
    WorkAtHomePage.prototype.goBack = function () {
        this.location.back();
    };
    WorkAtHomePage = tslib_1.__decorate([
        Component({
            selector: 'app-work-at-home',
            templateUrl: './work-at-home.page.html',
            styleUrls: ['./work-at-home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Location])
    ], WorkAtHomePage);
    return WorkAtHomePage;
}());
export { WorkAtHomePage };
//# sourceMappingURL=work-at-home.page.js.map