import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { WorkAtHomePage } from './work-at-home.page';
var routes = [
    {
        path: '',
        component: WorkAtHomePage
    }
];
var WorkAtHomePageModule = /** @class */ (function () {
    function WorkAtHomePageModule() {
    }
    WorkAtHomePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [WorkAtHomePage]
        })
    ], WorkAtHomePageModule);
    return WorkAtHomePageModule;
}());
export { WorkAtHomePageModule };
//# sourceMappingURL=work-at-home.module.js.map