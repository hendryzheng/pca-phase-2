import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EditCustomerPage } from './edit-customer.page';
import { SignaturePadModule } from 'angular2-signaturepad';
var routes = [
    {
        path: '',
        component: EditCustomerPage
    }
];
var EditCustomerPageModule = /** @class */ (function () {
    function EditCustomerPageModule() {
    }
    EditCustomerPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                SignaturePadModule,
                RouterModule.forChild(routes)
            ],
            declarations: [EditCustomerPage]
        })
    ], EditCustomerPageModule);
    return EditCustomerPageModule;
}());
export { EditCustomerPageModule };
//# sourceMappingURL=edit-customer.module.js.map