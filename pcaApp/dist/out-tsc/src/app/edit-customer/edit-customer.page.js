import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { UtilitiesService } from 'src/services/utilities.service';
import { Router } from '@angular/router';
import { MenuService } from 'src/services/menu.service';
import { AccountService } from 'src/services/account.service';
import { Location } from '@angular/common';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { CONFIGURATION } from 'src/services/config.service';
var EditCustomerPage = /** @class */ (function () {
    function EditCustomerPage(_utilitiesService, router, location, _menuService, _accountService) {
        this._utilitiesService = _utilitiesService;
        this.router = router;
        this.location = location;
        this._menuService = _menuService;
        this._accountService = _accountService;
        this.signaturePadOptions = {
            'minWidth': 2,
            'canvasWidth': 340,
            'canvasHeight': 200
        };
        this.signatureImage = null;
        this.signed = false;
        this.email = '';
        this.agent_email = '';
        this.toggleSignature = false;
        this.form = {
            name: '',
            phone: '',
            email: '',
            gender: '',
            address: '',
            pdpa_signed: false
        };
    }
    EditCustomerPage.prototype.ngOnInit = function () {
    };
    EditCustomerPage.prototype.toggle = function () {
        this.toggleSignature = !this.toggleSignature;
    };
    EditCustomerPage.prototype.goBack = function () {
        this.location.back();
    };
    EditCustomerPage.prototype.ionViewWillEnter = function () {
        this._accountService.checkUserSession();
        this.form.name = this._menuService.customer.fullname;
        this.form.email = this._menuService.customer.email;
        this.form.gender = this._menuService.customer.gender;
        this.form.phone = this._menuService.customer.mobile;
        this.form.address = this._menuService.customer.address;
        if (this._menuService.customer.status == '1') {
            this.form.pdpa_signed = true;
        }
        else {
            this.toggleSignature = false;
        }
    };
    EditCustomerPage.prototype.submit = function () {
    };
    EditCustomerPage.prototype.getCustomer = function () {
        return this._menuService.customer;
    };
    EditCustomerPage.prototype.validateForm = function () {
        var validate = true;
        console.log(this.form);
        if (this._utilitiesService.isEmpty(this.form.name)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.phone)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.email)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.gender)) {
            validate = false;
        }
        if (this._utilitiesService.isEmpty(this.form.address)) {
            validate = false;
        }
        if (this.form.pdpa_signed == false) {
            validate = false;
        }
        if (!validate) {
            this._utilitiesService.alertMessage('Validation Error', 'Please fill up all details and ensure your customer has agreed to the PDPA consent');
        }
        console.log(validate);
        return validate;
    };
    EditCustomerPage.prototype.drawCancel = function () {
        // this.navCtrl.push(HomePage);
    };
    EditCustomerPage.prototype.drawComplete = function () {
        this.signatureImage = this.signaturePad.toDataURL();
        this.signed = true;
        console.log(this.signatureImage);
        // this.navCtrl.push(HomePage, {signatureImage: this.signatureImage});
    };
    EditCustomerPage.prototype.drawClear = function () {
        this.signed = false;
        this.signatureImage = null;
        this.signaturePad.clear();
        console.log(this.signaturePad.toDataURL());
        console.log(this.signaturePad.toDataURL().length);
    };
    EditCustomerPage.prototype.getBaseUrl = function () {
        return CONFIGURATION.base_url;
    };
    EditCustomerPage.prototype.update = function () {
        var _this = this;
        if (this.validateForm()) {
            var formSubmission = {
                'id': this._menuService.customer.id,
                'Customer[fullname]': this.form.name,
                'Customer[email]': this.form.email,
                'Customer[mobile]': this.form.phone,
                'Customer[gender]': this.form.gender,
                'Customer[address]': this.form.address
            };
            this._menuService.updateCustomer(formSubmission, function () {
                _this.signatureImage = _this.signaturePad.toDataURL();
                console.log(_this.signatureImage);
                if (_this.signatureImage !== null && _this.signatureImage.length !== 2266) {
                    var customer_id = _this._menuService.customer.id;
                    _this._menuService.uploadImageSignature(customer_id, _this.signatureImage, function (res) {
                        _this._utilitiesService.alertMessageCallback('Customer added successfully', 'You have successfully added this customer', function () {
                            _this._menuService.eNamecardState = 'customer';
                            _this.location.back();
                        });
                    });
                }
                else {
                    _this._menuService.eNamecardState = 'customer';
                    _this.location.back();
                }
            });
        }
    };
    tslib_1.__decorate([
        ViewChild(SignaturePad),
        tslib_1.__metadata("design:type", SignaturePad)
    ], EditCustomerPage.prototype, "signaturePad", void 0);
    EditCustomerPage = tslib_1.__decorate([
        Component({
            selector: 'app-edit-customer',
            templateUrl: './edit-customer.page.html',
            styleUrls: ['./edit-customer.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [UtilitiesService,
            Router,
            Location,
            MenuService,
            AccountService])
    ], EditCustomerPage);
    return EditCustomerPage;
}());
export { EditCustomerPage };
//# sourceMappingURL=edit-customer.page.js.map