import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { CONFIGURATION } from './config.service';
var HttpService = /** @class */ (function () {
    function HttpService(http) {
        this.http = http;
    }
    HttpService.prototype.ObjecttoParams = function (obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
    };
    ;
    HttpService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            _this.log(operation + " failed: " + error.status);
            // Let the app keep running by returning an empty result.
            return of(result);
        };
    };
    /** Log a HeroService message with the MessageService */
    HttpService.prototype.log = function (message) {
        console.log(message);
    };
    HttpService.prototype.getRequest = function (url) {
        var version = localStorage.getItem('version');
        var isIos = localStorage.getItem('isIos');
        url += '?isIos=' + isIos + '&version=' + version + '&client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        return this.http.get(url).pipe(map(function (response) { return response.json(); }), catchError(this.handleError('getRequestPlain : ' + url, [])));
    };
    HttpService.prototype.getRequestRaw = function (url) {
        // var version = localStorage.getItem('version');
        // var isIos = localStorage.getItem('isIos');
        // url += '?isIos=' + isIos + '&version=' + version + '&client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        return this.http.get(url).pipe(map(function (response) { return response.json(); }), catchError(this.handleError('getRequestPlain : ' + url, [])));
    };
    HttpService.prototype.getRequestRaw99co = function (url) {
        // var version = localStorage.getItem('version');
        // var isIos = localStorage.getItem('isIos');
        // url += '?isIos=' + isIos + '&version=' + version + '&client_id=' + CONFIGURATION.TOKEN.CLIENT_ID + '&client_token=' + CONFIGURATION.TOKEN.CLIENT_TOKEN;
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Basic JnRFaWxkJSRaRnV4QEtUaEs1N0xCaHFBJElGQ2ZnT3NiZF5DN0piI1dSKEh2QiVeSmY=' });
        var options = new RequestOptions({ headers: headers });
        return this.http.get(url, options).pipe(map(function (response) { return response.json(); }), catchError(this.handleError('getRequestPlain : ' + url, [])));
    };
    HttpService.prototype.postRequest = function (url, params) {
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new RequestOptions({ headers: headers });
        params['client_id'] = CONFIGURATION.TOKEN.CLIENT_ID;
        params['client_token'] = CONFIGURATION.TOKEN.CLIENT_TOKEN;
        params['version'] = localStorage.getItem('version');
        params['isIos'] = localStorage.getItem('isIos');
        return this.http.post(CONFIGURATION.apiEndpoint + url, this.ObjecttoParams(params), options)
            .pipe(map(function (response) { return response.json(); }), catchError(this.handleError('postRequest : ' + url, [])));
    };
    HttpService.prototype.getUser = function () {
        var userData = localStorage.getItem('userData');
        console.log(userData);
        var userDataParse = null;
        if (userData) {
            userDataParse = JSON.parse(userData);
            return userDataParse;
        }
    };
    HttpService.prototype.uploadImage = function (endpointUrl, imageUrl, params, callback, fallback) {
        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var userData = localStorage.getItem('userData');
        console.log(userData);
        if (userData) {
            var parseUserData = JSON.parse(userData);
            params['u_id'] = parseUserData.id;
            params['user_type'] = parseUserData.member_type;
            params['client_id'] = CONFIGURATION.TOKEN.CLIENT_ID;
            params['client_token'] = CONFIGURATION.TOKEN.CLIENT_TOKEN;
        }
        console.log(params);
        options.params = params;
        var ft = new FileTransfer();
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + endpointUrl, function (response) {
            console.log(response);
            var res = JSON.parse(response.response);
            navigator.camera.cleanup(function (success) {
                console.log('success');
            }, function (error) {
                console.log('fail');
            });
            if (res.status === 'OK') {
                callback();
            }
            else {
                fallback();
            }
        }, function (error) {
            // error.code == FileTransferError.ABORT_ERR
            alert('An error has occurred: Code = ' + error.code);
            console.log('upload error source ' + error.source);
            console.log('upload error target ' + error.target);
        }, options, true);
    };
    HttpService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [Http])
    ], HttpService);
    return HttpService;
}());
export { HttpService };
//# sourceMappingURL=http.service.js.map