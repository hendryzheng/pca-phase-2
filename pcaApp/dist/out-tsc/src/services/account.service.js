import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { UtilitiesService } from './utilities.service';
import { CONFIGURATION } from './config.service';
import { Badge } from "@ionic-native/badge/ngx";
var AccountService = /** @class */ (function () {
    function AccountService(_httpService, badge, _utilitiesService) {
        this._httpService = _httpService;
        this.badge = badge;
        this._utilitiesService = _utilitiesService;
        this.userData = null;
        this.viewUserData = [];
        this.appVersion = '';
        this.config = [];
        this.userDetail = [];
        this.countries = [];
        this.states = [];
        this.cities = [];
        this.token = '';
        this.news_popup = null;
        this.notificationOpen = null;
        this.notification = [];
        this.notification_unread = '';
        this.pdpaConfig = [];
        // var userData = localStorage.getItem('userData');
        // console.log(userData);
    }
    AccountService.prototype.getPdpaConfig = function (callback) {
        var _this = this;
        this.checkUserSession();
        this._utilitiesService.showLoading();
        var param = {
            user_id: this.userData.id,
            user_type: this.userData.member_type
        };
        return this._httpService.postRequest(CONFIGURATION.URL.getPdpaConfig, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.pdpaConfig = response.data;
                    callback();
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.register = function (params, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.register, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this._utilitiesService.alertMessage('Success !', response.message).then(function (resolve) {
                        callback();
                    });
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.isUserSessionAlive = function () {
        // console.log('-- ACCOUNTSERVICE isUserSessionAlive : ' + this.userData + ' --');
        if (this.userData === null) {
            return false;
        }
        else {
            return true;
        }
    };
    AccountService.prototype.checkUserSession = function () {
        var userData = localStorage.getItem('userData');
        console.log(userData);
        if (!this._utilitiesService.isEmpty(userData)) {
            this.userData = JSON.parse(userData);
        }
    };
    AccountService.prototype.changePassword = function (param, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        param['u_id'] = this.userData.id;
        param['member_type'] = this.userData.member_type;
        this._httpService.postRequest(CONFIGURATION.URL.changePassword, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.updateProfile = function (param, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateProfile, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.updateEmail = function (param, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateEmail, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.updatePassword = function (param, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updatePassword, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this._utilitiesService.alertMessage('Successful', response.message);
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.forgotPassword = function (param, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.forgotPassword, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this._utilitiesService.alertMessageCallback('Successful', response.message, callback);
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.updatePreference = function (param, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateProfile, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this._utilitiesService.alertMessage('Successful', response.message);
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.getUserDetail = function (callback) {
        var _this = this;
        this.checkUserSession();
        // this._utilitiesService.showLoading();
        var param = {
            user_id: this.userData.id,
            user_type: this.userData.member_type
        };
        return this._httpService.postRequest(CONFIGURATION.URL.getUser, param).subscribe(function (response) {
            // this._utilitiesService.hideLoading().then(resp => {
            if (response.status === 'OK') {
                _this.userData = response.data;
                _this.userDetail = response.data;
                console.log(_this.userDetail);
                callback();
            }
            // });
        }, function (error) {
            // this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.getUserDetailParam = function (param, callback) {
        var _this = this;
        this.checkUserSession();
        this._utilitiesService.showLoading();
        return this._httpService.postRequest(CONFIGURATION.URL.getUser, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.viewUserData = response.data;
                    callback();
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.getUserDetailProfile = function (callback) {
        var _this = this;
        this.checkUserSession();
        this._utilitiesService.showLoading();
        var param = {
            user_id: this.userData.id,
            user_type: this.userData.member_type
        };
        return this._httpService.postRequest(CONFIGURATION.URL.getUser, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.userData = response.data;
                    _this.userDetail = response.data;
                    console.log(_this.userDetail);
                    callback();
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.getAppVersion = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var param = {};
        this._httpService.postRequest(CONFIGURATION.URL.getAppVersion, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.appVersion = response.data;
                    callback();
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.getNotification = function (callback) {
        var _this = this;
        this.checkUserSession();
        this._utilitiesService.showLoading();
        var param = {
            user_id: this.userData.id,
            member_type: this.userData.member_type
        };
        return this._httpService.postRequest(CONFIGURATION.URL.getNotification, param).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.notification = response.data.data;
                    _this.notification_unread = response.unread;
                    _this.badge.clear();
                    callback();
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.uploadImage = function (imageUrl, callback) {
        var _this = this;
        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var params = {
            client_id: CONFIGURATION.TOKEN.CLIENT_ID,
            client_token: CONFIGURATION.TOKEN.CLIENT_TOKEN,
            user_id: this.userData.id,
            user_type: this.userData.member_type
        };
        options.params = params;
        var ft = new FileTransfer();
        this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadImageProfile);
        console.log(params);
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadImageProfile, function (response) {
            console.log(response);
            var res = JSON.parse(response.response);
            navigator.camera.cleanup(function (success) {
                console.log('success');
            }, function (error) {
                console.log('fail');
            });
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (res.status === 'OK') {
                    _this.userData.profile_picture = res.data.imageUrl;
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                // error.code == FileTransferError.ABORT_ERR
                alert('An error has occurred: Code = ' + error.code);
                console.log('upload error source ' + error.source);
                console.log('upload error target ' + error.target);
            });
        }, options, true);
    };
    AccountService.prototype.getConfig = function () {
        var _this = this;
        // this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.getConfig, {}).subscribe(function (response) {
            // this._utilitiesService.hideLoading().then(resp => {
            if (response.status === 'OK') {
                _this.config = response.data;
                localStorage.setItem('config', JSON.stringify(response.data));
            }
            // });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    AccountService.prototype.registerToken = function () {
        if (this.userData !== null) {
            var param = {
                user_type: this.userData.member_type,
                id: this.userData.id,
                token: this.token
            };
            console.log(param);
            this._httpService.postRequest(CONFIGURATION.URL.tokenRegistration, param)
                .subscribe(function (response) {
                console.log(response);
            }, function (error) {
                // this._utilitiesService.handleError(error);
                console.log(error);
            });
        }
        else {
            var param = {
                token: this.token
            };
            console.log(param);
            this._httpService.postRequest(CONFIGURATION.URL.tokenRegistration, param)
                .subscribe(function (response) {
                console.log(response);
            }, function (error) {
                // this._utilitiesService.handleError(error);
                console.log(error);
            });
        }
    };
    AccountService.prototype.getLoginAccount = function () {
        // var userData = this.userData;
        if (!this._utilitiesService.isEmpty(this.userData)) {
            return this.userData;
        }
        else {
            return '';
        }
    };
    AccountService.prototype.getUserId = function () {
        if (this.userData !== null) {
            return this.userData.id;
        }
        return '';
    };
    AccountService.prototype.isLoggedIn = function () {
        if (this.userData !== null) {
            return true;
        }
        else {
            return false;
        }
    };
    AccountService.prototype.isDealer = function () {
        if (this.userData !== null) {
            if (this.userData.member_type === 'Gold Dealer' || this.userData.member_type === 'Silver Dealer') {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    AccountService.prototype.isGoldDealer = function () {
        if (this.userData !== null) {
            if (this.userData.member_type === 'Gold Dealer') {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    AccountService.prototype.isSilverDealer = function () {
        if (this.userData !== null) {
            if (this.userData.member_type === 'Silver Dealer') {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    AccountService.prototype.isStaff = function () {
        if (this.userData !== null) {
            if (this.userData.member_type === 'Staff') {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    AccountService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpService,
            Badge,
            UtilitiesService])
    ], AccountService);
    return AccountService;
}());
export { AccountService };
//# sourceMappingURL=account.service.js.map