import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { AlertController, Platform, LoadingController } from '@ionic/angular';
import { ThemeableBrowser } from '@ionic-native/themeable-browser/ngx';
var UtilitiesService = /** @class */ (function () {
    function UtilitiesService(alertCtrl, themeableBrowser, plt, _loadingController) {
        this.alertCtrl = alertCtrl;
        this.themeableBrowser = themeableBrowser;
        this.plt = plt;
        this._loadingController = _loadingController;
        this.loading = null;
        this.isLoading = false;
        this.showSearchBar = false;
        this.showHeaderLink = true;
        this.showSearch = true;
        this.months = [
            {
                val: '01',
                text: 'Jan'
            },
            {
                val: '02',
                text: 'Feb'
            },
            {
                val: '03',
                text: 'Mar'
            },
            {
                val: '04',
                text: 'Apr'
            },
            {
                val: '05',
                text: 'May'
            },
            {
                val: '06',
                text: 'Jun'
            },
            {
                val: '07',
                text: 'Jul'
            },
            {
                val: '08',
                text: 'Aug'
            },
            {
                val: '09',
                text: 'Sep'
            },
            {
                val: '10',
                text: 'Oct'
            },
            {
                val: '11',
                text: 'Nov'
            },
            {
                val: '12',
                text: 'Dec'
            },
        ];
        this.years = [];
        var currentTime = new Date();
        var year = currentTime.getFullYear();
        for (var i = year - 5; i <= year; i++) {
            var temp = { val: i };
            this.years.push(temp);
        }
    }
    UtilitiesService.prototype.isEmpty = function (param) {
        if (param === '' || typeof param === 'undefined' || param === 'null' || param === null) {
            return true;
        }
        else {
            return false;
        }
    };
    UtilitiesService.prototype.formatFloat = function (value) {
        return parseFloat(value).toFixed(2);
    };
    UtilitiesService.prototype.flashAlertMessage = function (title, message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alertController, alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        alertController = document.querySelector('ion-alert-controller');
                        return [4 /*yield*/, alertController.componentOnReady()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, alertController.create({
                                header: title,
                                message: message,
                                buttons: [
                                    {
                                        text: 'OK',
                                        handler: function (data) {
                                            return true;
                                        }
                                    }
                                ]
                            })];
                    case 2:
                        alert = _a.sent();
                        return [2 /*return*/, alert.present()];
                }
            });
        });
    };
    UtilitiesService.prototype.getAlertCtrl = function () {
        return this.alertCtrl;
    };
    UtilitiesService.prototype.chunk = function (arr, size) {
        var newArr = [];
        for (var i = 0; i < arr.length; i += size) {
            newArr.push(arr.slice(i, i + size));
        }
        return newArr;
    };
    UtilitiesService.prototype.alertMessage = function (title, message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alertController, alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        alertController = this.alertCtrl;
                        return [4 /*yield*/, alertController.create({
                                header: title,
                                message: message,
                                buttons: [
                                    {
                                        text: 'OK',
                                        handler: function (data) {
                                            console.log('OK clicked');
                                        }
                                    },
                                ]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [2 /*return*/, alert.present()];
                }
            });
        });
    };
    UtilitiesService.prototype.formatDate = function (dateString) {
        if (this.plt.is('ios')) {
            var t = dateString.split(/[- :]/);
            // Apply each element to the Date function
            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
            var date = new Date(d);
        }
        else {
            var date = new Date(dateString);
        }
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
        ];
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    };
    UtilitiesService.prototype.showLoading = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this._loadingController.create({
                                duration: 5000,
                            }).then(function (a) {
                                a.present().then(function () {
                                    console.log('presented');
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort presenting'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UtilitiesService.prototype.showLoadingSyncImages = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this._loadingController.create({
                                message: 'Please wait, while the images data is being uploaded to server',
                                duration: 500000,
                            }).then(function (a) {
                                a.present().then(function () {
                                    console.log('presented');
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort presenting'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UtilitiesService.prototype.hideLoading = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = false;
                        return [4 /*yield*/, this._loadingController.dismiss().then(function () { return console.log('dismissed'); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UtilitiesService.prototype.alertMessageCallback = function (title, message, callback) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alertController, alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        alertController = this.alertCtrl;
                        return [4 /*yield*/, alertController.create({
                                header: title,
                                message: message,
                                buttons: [
                                    {
                                        text: 'OK',
                                        handler: function (data) {
                                            console.log('OK clicked');
                                            callback();
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [2 /*return*/, alert.present()];
                }
            });
        });
    };
    UtilitiesService.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
            // alert(body.message);
            var alertTitle = 'Oops ! Ada yg tidak beres !';
            this.alertMessage(alertTitle, body.message);
        }
        else {
            errMsg = error.message ? error.message : error.toString();
            // this.alertMessage('Oops ! Something Happened', errMsg);
            var alertTitle = 'Oops ! Ada yg tidak beres !';
            this.alertMessage(alertTitle, errMsg);
        }
    };
    UtilitiesService.prototype.getMonths = function () {
        return this.months;
    };
    UtilitiesService.prototype.getYears = function () {
        return this.years;
    };
    UtilitiesService.prototype.getCurrentMonth = function () {
        var currentTime = new Date();
        var month = currentTime.getMonth() + 1;
        return month;
    };
    UtilitiesService.prototype.getCurrentYear = function () {
        var currentTime = new Date();
        var year = currentTime.getFullYear();
        return year;
    };
    UtilitiesService.prototype.formatWhatsappNo = function (mobile_no) {
        var mobile_no = mobile_no.replace('+', '');
        if (mobile_no.charAt(0) == '9') {
            mobile_no = '65' + mobile_no;
        }
        return mobile_no;
    };
    UtilitiesService.prototype.openBrowserAndroid = function (title, url) {
        var options = {
            statusbar: {
                color: '#ffffffff'
            },
            toolbar: {
                height: 40,
                color: '#025bb5'
            },
            title: {
                color: '#ffffffff',
                showPageTitle: true,
                staticText: title
            },
            closeButton: {
                wwwImage: 'assets/img/close.png',
                align: 'left',
                event: 'closePressed'
            },
            backButtonCanClose: true
        };
        var browser = this.themeableBrowser.create(url, '_blank', options);
    };
    UtilitiesService.prototype.openBrowser = function (title, url) {
        if (this.plt.is('ios')) {
            var options = {
                statusbar: {
                    color: '#ffffffff'
                },
                toolbar: {
                    height: 40,
                    color: '#025bb5'
                },
                title: {
                    color: '#ffffffff',
                    showPageTitle: true,
                    staticText: title
                },
                closeButton: {
                    wwwImage: 'assets/img/close.png',
                    align: 'left',
                    event: 'closePressed'
                },
                backButtonCanClose: true
            };
            var browser = this.themeableBrowser.create(url, '_blank', options);
            // this.showLoading();
            // browser.on('loadstart').subscribe(event => {
            //     console.log("loadstart");
            //     this.hideLoading().then(resp => {
            //         browser.show();
            //     });
            // });
            // browser.on('closePressed').subscribe(event => {
            //     console.log("closePressed");
            // })
        }
        else {
            this.openBrowserAndroid(title, url);
        }
    };
    UtilitiesService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [AlertController,
            ThemeableBrowser,
            Platform,
            LoadingController])
    ], UtilitiesService);
    return UtilitiesService;
}());
export { UtilitiesService };
//# sourceMappingURL=utilities.service.js.map