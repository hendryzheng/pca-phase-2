import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { UtilitiesService } from "./utilities.service";
import { HttpService } from "./http.service";
import { CONFIGURATION } from "./config.service";
import { AccountService } from "./account.service";
import { Badge } from "@ionic-native/badge/ngx";
var PropertyListingService = /** @class */ (function () {
    function PropertyListingService(_utilitiesService, _httpService, badge, _accountService) {
        this._utilitiesService = _utilitiesService;
        this._httpService = _httpService;
        this.badge = badge;
        this._accountService = _accountService;
        this.property_listing = [];
        this.myPropertyListing = [];
        this.publicPropertyListing = [];
        this.propertyListingResult = [];
        this.propertyListingCount = [];
        this.listing = [];
        this.step1Location = [];
        this.step1SelectedLocation = [];
        this.step1PropertyDetail = [];
        this.propertyListingDetail = [];
        this.searchInputRequestResult = [];
        this.selectedKeywordSearch = '';
        this.selectedKeywordSearchSubtitle = '';
        this.requestInformationList = [];
        this.selectedPropertyListingDetail = [];
        this.editSearchListingParams = false;
        this.searchListingParams = {
            search_id: '',
            location_name: '',
            location_subtitle: '',
            search_name: '',
            query_coords: '',
            listing_type: 'sale',
            main_category: 'hdb',
            keywords: '',
            sub_categories: '',
            query_type: '',
            query_ids: '',
            radius_max: 1000,
            price_min: 100000,
            price_max: 20000000,
            floorarea_min: 500,
            floorarea_max: 10000,
            rooms: 'any',
            is_new_launch: 'any',
            tenure: 'any',
            sort_field: '',
            sort_order: ''
        };
        this.listing_categories = [
            {
                key: 'residential',
                value: 'Residential',
                main_categories: [
                    {
                        key: 'hdb',
                        value: 'HDB',
                        sub_categories: [
                            {
                                key: 'hdb_2r',
                                value: 'HDB 2 rooms',
                                checked: false
                            },
                            {
                                key: 'hdb_3r',
                                value: 'HDB 3 rooms',
                                checked: false
                            },
                            {
                                key: 'hdb_4r',
                                value: 'HDB 4 rooms',
                                checked: false
                            },
                            {
                                key: 'hdb_5r',
                                value: 'HDB 5 rooms',
                                checked: false
                            },
                            {
                                key: 'hdb_6r',
                                value: 'HDB 6 rooms',
                                checked: false
                            },
                            {
                                key: 'hdb_executive',
                                value: 'HDB Executive',
                                checked: false
                            }
                        ]
                    },
                    {
                        key: 'condo',
                        value: 'Condo',
                        sub_categories: [
                            {
                                key: 'generic_condo',
                                value: 'Generic Condo',
                                checked: false
                            },
                            {
                                key: 'condo_apartment',
                                value: 'Apartment',
                                checked: false
                            },
                            {
                                key: 'walkup',
                                value: 'Walk-up',
                                checked: false
                            }
                        ]
                    },
                    {
                        key: 'landed',
                        value: 'Landed',
                        sub_categories: [
                            {
                                key: 'terraced_house',
                                value: 'Terraced House',
                                checked: false
                            },
                            {
                                key: 'corner_terrace',
                                value: 'Corner Terrace',
                                checked: false
                            },
                            {
                                key: 'semi_detached',
                                value: 'Semi-detached house',
                                checked: false
                            },
                            {
                                key: 'bungalow',
                                value: 'Bungalow / Detached House',
                                checked: false
                            },
                            {
                                key: 'goodclass_bungalow',
                                value: 'Good Class Bungalow',
                                checked: false
                            },
                            {
                                key: 'shophouse',
                                value: 'Shophouse',
                                checked: false
                            },
                            {
                                key: 'conservationhouse',
                                value: 'Conservation House',
                                checked: false
                            },
                            {
                                key: 'townhouse',
                                value: 'Townhouse',
                                checked: false
                            },
                            {
                                key: 'clusterhouse',
                                value: 'Cluster House',
                                checked: false
                            },
                            {
                                key: 'landonly',
                                value: 'Land Only',
                                checked: false
                            },
                        ]
                    }
                ]
            },
            {
                key: 'commercial',
                value: 'Commercial',
                main_categories: [
                    {
                        key: 'retail',
                        value: 'Retail'
                    },
                    {
                        key: 'office',
                        value: 'Office'
                    },
                ]
            },
            {
                key: 'industrial',
                value: 'Industrial',
                main_categories: [
                    {
                        key: 'general_industrial',
                        value: 'General Industrial'
                    },
                    {
                        key: 'warehouse',
                        value: 'Warehouse'
                    },
                    {
                        key: 'showroom',
                        value: 'showroom'
                    },
                    {
                        key: 'dormitory',
                        value: 'dormitory'
                    },
                    {
                        key: 'ebiz',
                        value: 'E-Business'
                    },
                    {
                        key: 'fnb',
                        value: 'F&B / Canteen'
                    },
                    {
                        key: 'childcare',
                        value: 'Childcare'
                    },
                    {
                        key: 'industrial_office',
                        value: 'Industrial Office'
                    },
                    {
                        key: 'shop',
                        value: 'Shop'
                    },
                    {
                        key: 'factory',
                        value: 'Factory'
                    },
                ]
            },
            {
                key: 'land',
                value: 'Land',
                main_categories: [
                    {
                        key: 'general_land',
                        value: 'General Land'
                    },
                    {
                        key: 'land_w_building',
                        value: 'Land with Building'
                    },
                ]
            }
        ];
        this.form = {
            pca_member_id: 0,
            property_segment: '',
            main_category: '',
            sub_category: '',
            land_use: '',
            property_name: '',
            district: '',
            lat: 0,
            lng: 0,
            address: '',
            type: '',
            id: '',
            photo_url: '',
            listing_type: '',
            hdb_type: '',
            bedrooms: '',
            floor_size: '',
            floor_size_type: 'sqft',
            area_size: '',
            price: '',
            agents_to_market: '',
            highlights: '',
            description: '',
            floor: '',
            unit_no: '',
            number_of_bathroom: '',
            date_of_availbility: '',
            facing: '',
            tenancy_lease_expiration: '',
            tenancy_current_rent: '',
            tenure: '',
            features: '',
            other_restrictions: '',
            youtube_video: '',
            iframe_v360: ''
        };
        this.uploadSucceed = 0;
        this.featuresOptions = [
            {
                'value': 'balcony',
                'label': 'Balcony',
                checked: false
            },
            {
                'value': 'bomb_shelter',
                'label': 'Bomb Shelter',
                checked: false
            },
            {
                'value': 'city_view',
                'label': 'City View',
                checked: false
            },
            {
                'value': 'dual_key',
                'label': 'Dual Key',
                checked: false
            },
            {
                'value': 'duplex_maisonette',
                'label': 'Duplex / Maisonette',
                checked: false
            },
            {
                'value': 'fibre_ready',
                'label': 'Fibre Ready',
                checked: false
            },
            {
                'value': 'greenery_view',
                'label': 'Greenery View',
                checked: false
            },
            {
                'value': 'ground_floor',
                'label': 'Ground Floor',
                checked: false
            },
            {
                'value': 'high_ceiling',
                'label': 'High Ceiling',
                checked: false
            },
            {
                'value': 'high_floor',
                'label': 'High Floor',
                checked: false
            },
            {
                'value': 'loft',
                'label': 'Loft',
                checked: false
            },
            {
                'value': 'low_floor',
                'label': 'Low Floor',
                checked: false
            },
            {
                'value': 'maid_room',
                'label': 'Maid\'s Room',
                checked: false
            },
            {
                'value': 'mid_floor',
                'label': 'Mid Floor',
                checked: false
            },
            {
                'value': 'patio_pes',
                'label': 'Patio / PES',
                checked: false
            },
            {
                'value': 'penthouse',
                'label': 'Penthouse',
                checked: false
            },
            {
                'value': 'pool_view',
                'label': 'Pool View',
                checked: false
            },
            {
                'value': 'renovated',
                'label': 'Renovated',
                checked: false
            },
            {
                'value': 'roof_terrace',
                'label': 'Roof Terrace',
                checked: false
            },
            {
                'value': 'sea_view',
                'label': 'Sea View',
                checked: false
            },
            {
                'value': 'top_floor',
                'label': 'Top Floor',
                checked: false
            },
            {
                'value': 'utility_room',
                'label': 'Utility Room',
                checked: false
            }
        ];
        this.shortFormMap = {
            hdb: 'HDB',
            condo: 'Condo',
            landed: 'Landed',
            retail: 'Retail',
            office: 'Office',
            shophouse: 'Shophouse',
            general_industrial: 'General industrial',
            warehouse: 'Warehouse',
            showroom: 'Showroom',
            dormitory: 'Dormitory',
            ebiz: 'e-Business/Media',
            fnb: 'F&B/Canteen',
            childcare: 'Childcare',
            industrial_office: 'Industrial office',
            shop: 'Shop',
            factory: 'Factory',
            general_land: 'General land',
            land_w_building: 'Land with building'
        };
        // this.categories = CATEGORIES;
    }
    PropertyListingService.prototype.getMyPropertyListing = function (callback) {
        var _this = this;
        this._accountService.checkUserSession();
        this._utilitiesService.showLoading();
        var params = [];
        params['u_id'] = this._accountService.userData.id;
        params['member_type'] = this._accountService.userData.member_type;
        this._httpService.postRequest(CONFIGURATION.URL.getMyPropertyListing, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.myPropertyListing = response.data;
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    PropertyListingService.prototype.getPublicPropertyListing = function (callback) {
        var _this = this;
        this._accountService.checkUserSession();
        this._utilitiesService.showLoading();
        var params = [];
        params['u_id'] = this._accountService.userData.id;
        params['member_type'] = this._accountService.userData.member_type;
        this._httpService.postRequest(CONFIGURATION.URL.getPublicPropertyListing, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.publicPropertyListing = response.data.data.sections[0].listings;
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    PropertyListingService.prototype.getRequestInformationList = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = [];
        params['pca_member_id'] = this._accountService.userData.id;
        this._httpService.postRequest(CONFIGURATION.URL.getRequestInformation, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.requestInformationList = response.data;
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    PropertyListingService.prototype.addRequestInformation = function (params, callback) {
        var _this = this;
        this._accountService.checkUserSession();
        this._utilitiesService.showLoading();
        params['pca_member_id'] = this._accountService.userData.id;
        this._httpService.postRequest(CONFIGURATION.URL.addRequestInformation, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.searchListingParams.search_id = response.data.id;
                    if (response.data.message !== null) {
                        _this._utilitiesService.alertMessageCallback('Information', response.data.message, callback);
                    }
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    PropertyListingService.prototype.getRequestInformation = function (params, callback) {
    };
    PropertyListingService.prototype.searchListings = function (callback) {
        var _this = this;
        var params = this._httpService.ObjecttoParams(this.searchListingParams);
        var url = CONFIGURATION.apiEndpoint + CONFIGURATION.URL.searchPropertyListing + '?' + params;
        this._utilitiesService.showLoading();
        this._httpService.getRequestRaw(url).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                // if (response.status === 'OK') {
                if (typeof response.data !== 'undefined') {
                    _this.propertyListingResult = response.data.data.sections[0].listings;
                    _this.propertyListingCount = response.data.data.count.total;
                    callback();
                }
                // } else {
                //     this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                // }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                callback();
            });
            console.log(error);
        });
    };
    PropertyListingService.prototype.getLocationStepDetail = function (id, property_segment, main_category, callback) {
        var _this = this;
        var url = 'https://www.99.co/api/v1/web/dashboard/listing-util/address/' + id + '?scale=4&property_segment=' + property_segment + '&main_category=' + main_category;
        this._httpService.getRequestRaw(url).subscribe(function (response) {
            // this._utilitiesService.hideLoading().then(resolve => {
            // if (response.status === 'OK') {
            if (typeof response.data !== 'undefined') {
                _this.step1PropertyDetail = response.data.items;
                callback();
            }
            // } else {
            //     this._utilitiesService.alertMessage('Oops! Something happened', response.message);
            // }
            // });
        }, function (error) {
            // this._utilitiesService.hideLoading().then(resolve => {
            callback();
            // });
            console.log(error);
        });
    };
    PropertyListingService.prototype.getInputRequestInfo = function (input, callback) {
        var _this = this;
        var url = 'https://www.99.co/api/v2/web/autocomplete/location?input=' + input;
        this._httpService.getRequestRaw(url).subscribe(function (response) {
            // this._utilitiesService.hideLoading().then(resolve => {
            // if (response.status === 'OK') {
            if (typeof response.data !== 'undefined') {
                _this.searchInputRequestResult = response.data.sections;
                callback();
            }
            // } else {
            //     this._utilitiesService.alertMessage('Oops! Something happened', response.message);
            // }
            // });
        }, function (error) {
            // this._utilitiesService.hideLoading().then(resolve => {
            callback();
            // });
            console.log(error);
        });
    };
    PropertyListingService.prototype.getLocationStep1 = function (query, property_segment, callback) {
        var _this = this;
        // this._utilitiesService.showLoading();
        var url = 'https://www.99.co/api/v1/web/dashboard/listing-util/address?query=' + query + '&property_segment=' + property_segment;
        this._httpService.getRequestRaw(url).subscribe(function (response) {
            // this._utilitiesService.hideLoading().then(resolve => {
            // if (response.status === 'OK') {
            if (typeof response.data !== 'undefined') {
                _this.step1Location = response.data.items;
                callback();
            }
            // } else {
            //     this._utilitiesService.alertMessage('Oops! Something happened', response.message);
            // }
            // });
        }, function (error) {
            // this._utilitiesService.hideLoading().then(resolve => {
            callback();
            // });
            console.log(error);
        });
    };
    PropertyListingService.prototype.uploadImageSignature = function (customer_id, imageUrl, callback) {
        var _this = this;
        this._accountService.checkUserSession();
        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var params = {
            customer_id: customer_id,
            client_id: CONFIGURATION.TOKEN.CLIENT_ID,
            client_token: CONFIGURATION.TOKEN.CLIENT_TOKEN,
            user_id: this._accountService.userData.id,
            user_type: this._accountService.userData.member_type
        };
        options.params = params;
        var ft = new FileTransfer();
        this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadCustomerSignature);
        console.log(params);
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadCustomerSignature, function (response) {
            console.log(response);
            var res = JSON.parse(response.response);
            navigator.camera.cleanup(function (success) {
                console.log('success');
            }, function (error) {
                console.log('fail');
            });
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (res.status === 'OK') {
                    callback(res);
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                // error.code == FileTransferError.ABORT_ERR
                alert('An error has occurred: Code = ' + error.code);
                console.log('upload error source ' + error.source);
                console.log('upload error target ' + error.target);
            });
        }, options, true);
    };
    PropertyListingService.prototype.uploadImage = function (params, imageUrl, fileName, callback, fallback) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var options, ft;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // var options = new FileUploadOptions();
                        params['client_id'] = CONFIGURATION.TOKEN.CLIENT_ID;
                        params['client_token'] = CONFIGURATION.TOKEN.CLIENT_TOKEN;
                        options = new FileUploadOptions();
                        options.fileKey = 'file';
                        options.fileName = 'myphoto.jpg';
                        options.mimeType = 'image/jpeg';
                        options.chunkedMode = false;
                        options.headers = {
                            Connection: 'close'
                        };
                        options.params = params;
                        ft = new FileTransfer();
                        // const ft: FileTransferObject = this.transfer.create();
                        // this._utilitiesService.showLoading();
                        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadPropertyImage);
                        console.log(options);
                        return [4 /*yield*/, ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadPropertyImage, function (response) {
                                console.log(response);
                                var res = JSON.parse(response.response);
                                navigator.camera.cleanup(function (success) {
                                    console.log('success');
                                }, function (error) {
                                    console.log('fail cleanup');
                                });
                                // this._utilitiesService.hideLoading().then(resp => {
                                if (res.status === 'OK') {
                                    res.data.fileName = fileName;
                                    callback(res.data);
                                    _this.uploadSucceed++;
                                }
                                else {
                                    _this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                                    fallback();
                                }
                                // })
                            }, function (error) {
                                // this._utilitiesService.hideLoading().then(resp => {
                                // error.code == FileTransferError.ABORT_ERR
                                console.log(error);
                                // alert('An error has occurred: Code = ' + error.code);
                                console.log('upload error source ' + error.source);
                                console.log('upload error target ' + error.target);
                                fallback();
                                // });
                            }, options, true)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PropertyListingService.prototype.createNewListing = function (params, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.addNewPropertyListing, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.propertyListingDetail = response.data;
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    PropertyListingService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [UtilitiesService,
            HttpService,
            Badge,
            AccountService])
    ], PropertyListingService);
    return PropertyListingService;
}());
export { PropertyListingService };
//# sourceMappingURL=property-listing.service.js.map