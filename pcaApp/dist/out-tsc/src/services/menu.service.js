import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { UtilitiesService } from "./utilities.service";
import { HttpService } from "./http.service";
import { CONFIGURATION } from "./config.service";
import { AccountService } from "./account.service";
import { Badge } from "@ionic-native/badge/ngx";
var MenuService = /** @class */ (function () {
    function MenuService(_utilitiesService, _httpService, badge, _accountService) {
        this._utilitiesService = _utilitiesService;
        this._httpService = _httpService;
        this.badge = badge;
        this._accountService = _accountService;
        this.menu_list = [];
        this.selectedMenu = [];
        this.listing = [];
        this.promotion = [];
        this.selectedPromotion = [];
        this.post = [];
        this.selectedPost = [];
        this.comments = [];
        this.banner = [];
        this.pca_categories = [];
        this.pca_listing = [];
        this.selected_pca_category = [];
        this.menuName = '';
        this.selected_pca_listing = [];
        this.selected_listing = [];
        this.calculators = [];
        this.research = [];
        this.autocomplete = [];
        this.listing_result = [];
        this.videoMarketing = [];
        this.eNamecardState = '';
        this.customerList = [];
        this.customer = [];
        this.matching_connect = null;
        this.matching_connect_obj = null;
        this.customer_type = '';
        // this.categories = CATEGORIES;
    }
    MenuService.prototype.submitContactForm = function (params, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.submitContactForm, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    MenuService.prototype.checkUserSession = function () {
        this._accountService.checkUserSession();
    };
    MenuService.prototype.getUserData = function () {
        return this._accountService.userData;
    };
    MenuService.prototype.getCustomerData = function (callback) {
        var _this = this;
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
            params['token'] = this._accountService.token;
        }
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.getCustomerData, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.customerList = response.data;
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                callback();
            });
            console.log(error);
        });
    };
    MenuService.prototype.uploadImageSignature = function (customer_id, imageUrl, callback) {
        var _this = this;
        this._accountService.checkUserSession();
        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var params = {
            customer_id: customer_id,
            client_id: CONFIGURATION.TOKEN.CLIENT_ID,
            client_token: CONFIGURATION.TOKEN.CLIENT_TOKEN,
            user_id: this._accountService.userData.id,
            user_type: this._accountService.userData.member_type
        };
        options.params = params;
        var ft = new FileTransfer();
        this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadCustomerSignature);
        console.log(params);
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadCustomerSignature, function (response) {
            console.log(response);
            var res = JSON.parse(response.response);
            navigator.camera.cleanup(function (success) {
                console.log('success');
            }, function (error) {
                console.log('fail');
            });
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (res.status === 'OK') {
                    callback(res);
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                // error.code == FileTransferError.ABORT_ERR
                alert('An error has occurred: Code = ' + error.code);
                console.log('upload error source ' + error.source);
                console.log('upload error target ' + error.target);
            });
        }, options, true);
    };
    MenuService.prototype.submitAddNewCustomer = function (params, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.addCustomerData, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.customer = response.data;
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    MenuService.prototype.updateCustomer = function (params, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.updateCustomer, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    _this.customer = response.data;
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    MenuService.prototype.deleteCustomer = function (params, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.deleteCustomer, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    MenuService.prototype.submitPromotionContactForm = function (params, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.submitPromotionContactForm, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (response.status === 'OK') {
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Look what happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading();
            console.log(error);
        });
    };
    MenuService.prototype.getMenu = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
            params['token'] = this._accountService.token;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getMenu, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.menu_list = response.data.data;
                    _this._accountService.notification_unread = response.data.unread;
                    if (typeof response.data.matching_connect.results !== 'undefined') {
                        _this.matching_connect = response.data.matching_connect.results.data.sections[0].listings;
                        _this.matching_connect_obj = response.data.matching_connect.search_obj;
                    }
                    // this.matching_connect = response.data.matching_connect;
                    _this.badge.set(_this._accountService.notification_unread);
                    localStorage.setItem('menu_list', JSON.stringify(_this.menu_list));
                    if (_this._accountService.isUserSessionAlive() && typeof response.data.user !== 'undefined') {
                        if (response.data.user.status !== '1') {
                            localStorage.clear();
                            _this._accountService.userData = null;
                        }
                    }
                    else {
                    }
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                var cache = localStorage.getItem('menu_list');
                if (cache) {
                    _this.menu_list = JSON.parse(localStorage.getItem('menu_list'));
                }
                callback();
            });
            console.log(error);
        });
    };
    MenuService.prototype.getVideoMarketing = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getVideoMarketing, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.videoMarketing = response.data;
                    localStorage.setItem('video_marketing', JSON.stringify(_this.videoMarketing));
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                var cache = localStorage.getItem('video_marketing');
                if (cache) {
                    _this.pca_categories = JSON.parse(localStorage.getItem('video_marketing'));
                }
                callback();
            });
            console.log(error);
        });
    };
    MenuService.prototype.getPcaCategories = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getPcaCategories, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.pca_categories = response.data;
                    localStorage.setItem('pca_categories', JSON.stringify(_this.pca_categories));
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                var cache = localStorage.getItem('pca_categories');
                if (cache) {
                    _this.pca_categories = JSON.parse(localStorage.getItem('pca_categories'));
                }
                callback();
            });
            console.log(error);
        });
    };
    MenuService.prototype.getCalculators = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getCalculators, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.calculators = response.data;
                    localStorage.setItem('calculators', JSON.stringify(_this.calculators));
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                var cache = localStorage.getItem('calculators');
                if (cache) {
                    _this.calculators = JSON.parse(localStorage.getItem('calculators'));
                }
                callback();
            });
            console.log(error);
        });
    };
    MenuService.prototype.searchListing = function (params, callback) {
        var _this = this;
        // this._utilitiesService.showLoading();
        this._accountService.checkUserSession();
        this._httpService.postRequest(CONFIGURATION.URL.searchListing, params).subscribe(function (response) {
            // this._utilitiesService.hideLoading().then(resolve => {
            if (response.status === 'OK') {
                if (typeof response.data !== 'undefined') {
                    _this.listing_result = response.data;
                }
                callback();
            }
            else {
                _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
            }
            // });
        }, function (error) {
            // this._utilitiesService.hideLoading().then(resolve => {
            callback();
            // });
            console.log(error);
        });
    };
    MenuService.prototype.getAutoComplete = function (params, callback) {
        var _this = this;
        // this._utilitiesService.showLoading();
        this._accountService.checkUserSession();
        this._httpService.postRequest(CONFIGURATION.URL.getAutoComplete, params).subscribe(function (response) {
            // this._utilitiesService.hideLoading().then(resolve => {
            if (response.status === 'OK') {
                if (typeof response.data.sections !== 'undefined') {
                    _this.autocomplete = response.data.sections;
                }
                callback();
            }
            else {
                _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
            }
            // });
        }, function (error) {
            // this._utilitiesService.hideLoading().then(resolve => {
            callback();
            // });
            console.log(error);
        });
    };
    MenuService.prototype.getResearch = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = [];
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getResearch, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.research = response.data;
                    localStorage.setItem('research', JSON.stringify(_this.research));
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                var cache = localStorage.getItem('research');
                if (cache) {
                    _this.research = JSON.parse(localStorage.getItem('calculators'));
                }
                callback();
            });
            console.log(error);
        });
    };
    MenuService.prototype.getPcaListing = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = {
            cat_id: this.selected_pca_category.id
        };
        this._accountService.checkUserSession();
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getPcaListing, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.pca_listing = response.data;
                    localStorage.setItem('pca_listing', JSON.stringify(_this.pca_listing));
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                var cache = localStorage.getItem('pca_listing');
                if (cache) {
                    _this.pca_categories = JSON.parse(localStorage.getItem('pca_listing'));
                }
                callback();
            });
            console.log(error);
        });
    };
    MenuService.prototype.getBanner = function (callback) {
        var _this = this;
        var params = {};
        this._httpService.postRequest(CONFIGURATION.URL.getBanner, params).subscribe(function (response) {
            if (response.status === 'OK') {
                _this.banner = response.data;
                callback();
            }
            else {
                _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
            }
        }, function (error) {
            // this._utilitiesService.alertMessage('Oops! Something happened', 'Please check your internet connection and try again later');
            console.log(error);
        });
    };
    MenuService.prototype.getPromotion = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = [];
        this._httpService.postRequest(CONFIGURATION.URL.getPromotion, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.promotion = response.data;
                    localStorage.setItem('promotion', JSON.stringify(_this.promotion));
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                var cache = localStorage.getItem('promotion');
                if (cache) {
                    _this.menu_list = JSON.parse(localStorage.getItem('promotion'));
                }
                callback();
            });
            console.log(error);
        });
    };
    MenuService.prototype.getListing = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._accountService.checkUserSession();
        var params = {
            menu_id: this.selectedMenu.id
        };
        if (this._accountService.isUserSessionAlive()) {
            params['user_id'] = this._accountService.userData.id;
            params['user_type'] = this._accountService.userData.member_type;
            params['token'] = this._accountService.token;
        }
        this._httpService.postRequest(CONFIGURATION.URL.getListing, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.listing = response.data;
                    // localStorage.setItem('comments', JSON.stringify(this.post));
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                // let cache = localStorage.getItem('post');
                // if (cache) {
                //     this.post = JSON.parse(localStorage.getItem('post'));
                // }
                // callback();
                _this._utilitiesService.alertMessage('Oops! Something happened', 'Please check your internet connection and try again later');
            });
            console.log(error);
        });
    };
    MenuService.prototype.getPost = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = [];
        this._httpService.postRequest(CONFIGURATION.URL.getPost, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.post = response.data;
                    localStorage.setItem('post', JSON.stringify(_this.post));
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                var cache = localStorage.getItem('post');
                if (cache) {
                    _this.post = JSON.parse(localStorage.getItem('post'));
                }
                callback();
            });
            console.log(error);
        });
    };
    MenuService.prototype.searchForum = function (params, callback) {
        // this._utilitiesService.showLoading();
        var _this = this;
        this._httpService.postRequest(CONFIGURATION.URL.searchForum, params).subscribe(function (response) {
            // this._utilitiesService.hideLoading().then(resolve => {
            if (response.status === 'OK') {
                _this.post = response.data;
                // localStorage.setItem('post', JSON.stringify(this.post));
                callback();
            }
            else {
                _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
            }
            // });
        }, function (error) {
            // this._utilitiesService.hideLoading().then(resolve => {
            // let cache = localStorage.getItem('post');
            // if (cache) {
            //     this.post = JSON.parse(localStorage.getItem('post'));
            // }
            callback();
            // });
            console.log(error);
        });
    };
    MenuService.prototype.getComments = function (callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        var params = {
            forum_post_id: this.selectedPost.id
        };
        this._httpService.postRequest(CONFIGURATION.URL.getComments, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.comments = response.data;
                    // localStorage.setItem('comments', JSON.stringify(this.post));
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                // let cache = localStorage.getItem('post');
                // if (cache) {
                //     this.post = JSON.parse(localStorage.getItem('post'));
                // }
                // callback();
                _this._utilitiesService.alertMessage('Oops! Something happened', 'Please check your internet connection and try again later');
            });
            console.log(error);
        });
    };
    MenuService.prototype.submitPost = function (params, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.submitPost, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    callback(response.data.id);
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                console.log(error);
            });
        });
    };
    MenuService.prototype.submitComment = function (params, callback) {
        var _this = this;
        this._utilitiesService.showLoading();
        this._httpService.postRequest(CONFIGURATION.URL.submitComment, params).subscribe(function (response) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                if (response.status === 'OK') {
                    _this.comments = response.data;
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', response.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resolve) {
                console.log(error);
            });
        });
    };
    MenuService.prototype.uploadPostImage = function (post_id, imageUrl, callback) {
        var _this = this;
        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.fileName = 'myphoto.jpg';
        options.mimeType = 'image/jpeg';
        options.chunkedMode = false;
        options.headers = {
            Connection: 'close'
        };
        var params = {
            client_id: CONFIGURATION.TOKEN.CLIENT_ID,
            client_token: CONFIGURATION.TOKEN.CLIENT_TOKEN,
            post_id: post_id
        };
        options.params = params;
        var ft = new FileTransfer();
        this._utilitiesService.showLoading();
        console.log(CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadPostImage);
        console.log(params);
        ft.upload(imageUrl, CONFIGURATION.apiEndpoint + CONFIGURATION.URL.uploadPostImage, function (response) {
            console.log(response);
            var res = JSON.parse(response.response);
            navigator.camera.cleanup(function (success) {
                console.log('success');
            }, function (error) {
                console.log('fail');
            });
            _this._utilitiesService.hideLoading().then(function (resp) {
                if (res.status === 'OK') {
                    // this._utilitiesService.alertMessage('Successful', res.data.message);
                    callback();
                }
                else {
                    _this._utilitiesService.alertMessage('Oops! Something happened', res.data.message);
                }
            });
        }, function (error) {
            _this._utilitiesService.hideLoading().then(function (resp) {
                // error.code == FileTransferError.ABORT_ERR
                alert('An error has occurred: Code = ' + error.code);
                console.log('upload error source ' + error.source);
                console.log('upload error target ' + error.target);
            });
        }, options);
    };
    MenuService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [UtilitiesService,
            HttpService,
            Badge,
            AccountService])
    ], MenuService);
    return MenuService;
}());
export { MenuService };
//# sourceMappingURL=menu.service.js.map