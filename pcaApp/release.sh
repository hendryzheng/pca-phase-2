#!/bin/bash    
# 
# Creates a signed and zipaligned APK from your Ionic project
#
# Place your keystore in the root of your project and name it <company>.keystore
# Use this script as following :
# $ ./release.sh [company] [version]
#
# Don't forget to gitignore your key and your compiled apks.
# 
# Original at https://gist.github.com/th3m4ri0/acc2003adc7dffdbbad6
# Author : Erwan d'Orgeville<info@erwandorgeville.com>

# Abort if any command returns something else than 0
set -e

company="pca"
version="1.0.0"
appname_dirty=${PWD##*/}
appname=${appname_dirty//[^a-zA-Z]} # Keeps only a-z letters

# if [[ -z "$1" ]]; then
#     echo "No company name provided, aborting..."
#     exit 1
# fi
# if [[ -z "$2" ]]; then
#     echo "No version provided, aborting..."
#     exit 1
# fi

echo "---> Starting build v$version"
#keytool -genkey -v -keystore $company.keystore -alias $company -keyalg RSA -keysize 2048 -validity 10000
sudo ionic cordova build --release android

echo ""
echo ""
echo "---> Input the password for the key"
sudo jarsigner -sigalg SHA1withRSA -digestalg SHA1 -keystore $company.keystore platforms/android/build/outputs/apk/release/android-release-unsigned.apk $company
sudo cp platforms/android/build/outputs/apk/release/android-release-unsigned.apk platforms/android/build/outputs/apk/release/com.$company.$appname.v$version-unaligned.apk

echo ""
echo ""
echo "---> Zipaligning"

sudo mkdir -p releases/

sudo [ -f releases/com.$company.$appname.v$version.apk ] && sudo rm releases/com.$company.$appname.v$version.apk || echo "File Removed"


sudo /Users/hendryzheng/.android-sdk-macosx/build-tools/26.0.0/zipalign -v 4 platforms/android/build/outputs/apk/release/com.$company.$appname.v$version-unaligned.apk releases/com.$company.$appname.v$version.apk

echo ""
echo ""
echo "---> App released ! Look for com.$company.$appname.v$version.apk"

open releases/
